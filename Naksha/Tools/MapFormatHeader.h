/**
 * @file MapFormatHeader.h
 * @brief MapFormatHeader
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_MAP_FORMAT_HEADER_H_
#define NAKSHA_MAP_FORMAT_HEADER_H_

#include "Naksha/Common/PrettyTypeInfo.h"

namespace Naksha {

/**@brief Header for Naksha Map files
*
* @ingroup Tools
*/
struct MapFormatHeader {

  /// Constructor from Map file
  template<class MapType>
  MapFormatHeader(const MapType& map, int version, const std::string& data_format)
    : header_string("Naksha Map File"),
      version(version),
      resolution(map.resolution()),
      number_of_voxels(map.size()),
      map_type_info(PRETTY_TYPE_INFO(MapType)),
      key_type_info(PRETTY_TYPE_INFO(typename MapType::KeyType)),
      payload_type_info(PRETTY_TYPE_INFO(typename MapType::Payload)),
      data_format(data_format) {
  }

  /// Default Constructor
  MapFormatHeader()
    : header_string("Naksha Map File"),
      version(0),
      resolution(0.1),
      number_of_voxels(0),
      map_type_info(""),
      key_type_info(""),
      payload_type_info(""),
      data_format("") {
  }

  std::string header_string;
  int version;

  double resolution;
  std::size_t number_of_voxels;

  std::string map_type_info;
  std::string key_type_info;
  std::string payload_type_info;

  std::string data_format;
};

}  // namespace Naksha

namespace boost {
namespace serialization {

template<class Archive>
inline void serialize(Archive & ar, Naksha::MapFormatHeader & header, const unsigned int file_version){
  ar & header.header_string;
  ar & header.version;
  ar & header.resolution;
  ar & header.number_of_voxels;
  ar & header.map_type_info;
  ar & header.key_type_info;
  ar & header.payload_type_info;
  ar & header.data_format;
}

} // namespace serialization
} // namespace boost

#endif // end NAKSHA_MAP_FORMAT_HEADER_H_
