/**
 * @file ImportExport.h
 * @brief ImportExport
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_IMPORT_EXPORT_H_
#define NAKSHA_IMPORT_EXPORT_H_

#include <boost/mpl/vector.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/mpl/copy.hpp>

#include "Naksha/Keys/Key.h"
#include "Naksha/Payload/PayloadMaker.h"


namespace Naksha {

/**@brief Save Naksha Map
 *
 * @param[in] map       - Naksha Map
 * @param[in] file_path - file_path to save to
 * @return true if successfully saved
 *
 * @ingroup Tools
 */
template<class MapType>
bool saveMap(const MapType& map, const std::string& file_path);


/**@brief Load a map file to Naksha Map
 *
 * @tparam MapType  - Some valid Naksha Map type
 *
 * @param[in] file_path - file_path to the map file
 * @return  pointer to loaded Naksha Map (NULL if unsuccessfully)
 *
 * @ingroup Tools
 */
template<class MapType>
MapType* loadMap(const std::string& file_path);



/// Supported KeyTypes (See Keys)
typedef boost::mpl::vector
    <   Key<2, unsigned short int>
      , Key<3, unsigned short int>
      , Key<2, unsigned int>
      , Key<3, unsigned int>
      , Key<2, unsigned long int>
      , Key<3, unsigned long int>
    > SupportedIOKeys;

/// List of IO Supported Payloads
typedef boost::mpl::vector
    <   EmptyPayload
      , ColoredPayload
      , DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable>
      , DiscreteRandomVariable<2, double, LogOddsTag, BinaryRandomVariable>
      , DiscreteRandomVariable<6, float, LogProbabilityTag>
      , DiscreteRandomVariable<8, float, LogProbabilityTag>
      , DiscreteRandomVariable<9, float, LogProbabilityTag>
      , DiscreteRandomVariable<10, float, LogProbabilityTag>
      , DiscreteRandomVariable<11, float, LogProbabilityTag>
      , DiscreteRandomVariable<19, float, LogProbabilityTag>
      , ColoredRandomVariable<DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<2, double, LogOddsTag, BinaryRandomVariable> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<9, float, LogProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<10, float, LogProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<11, float, LogProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<19, float, LogProbabilityTag> >::type
      , TSDFPayload
    > SupportedIOPayloads;

}  // namespace Naksha

#include "Naksha/Tools/ImportExport-impl.hpp"

#endif // end NAKSHA_IMPORT_EXPORT_H_
