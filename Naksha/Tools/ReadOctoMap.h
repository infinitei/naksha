/**
 * @file ReadOctoMap.h
 * @brief ReadOctoMap
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_TOOLS_READ_OCTOMAP_H_
#define NAKSHA_TOOLS_READ_OCTOMAP_H_

#include <string>

namespace Naksha {

/**@brief Reads a [OctoMap](http://octomap.github.io/) file to a Naksha Map.
 *
 *
 * @tparam MapType needs to be an OctreeMap type
 *
 * @param file_path - path of OctoMap file
 * @return pointer to the loaded Map (NULL if unsucessfull)
 *
 * Example Usage:
 * @snippet importOctoMap.cpp Example usage of readOctomapFile
 *
 * @ingroup Tools
 */
template<class MapType>
MapType* readOctoMapFile(const std::string& file_path);

}  // namespace Naksha

#include "Naksha/Tools/ReadOctoMap-impl.hpp"

#endif // NAKSHA_TOOLS_READ_OCTOMAP_H_
