/**
 * @file ImportExport-impl.hpp
 * @brief ImportExport-impl
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_IMPORT_EXPORT_IMPL_HPP_
#define NAKSHA_IMPORT_EXPORT_IMPL_HPP_

#include "Naksha/Common/MakeAlignedContainer.h"
#include "Naksha/Tools/MapFormatHeader.h"
#include "Naksha/Payload/ConvertPayload.h"
#include <boost/mpl/contains.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/filesystem.hpp>
#include <fstream>

namespace Naksha {

namespace detail {

typedef boost::make_variant_over<
      boost::mpl::transform<SupportedIOKeys, std::vector<boost::mpl::_> >::type
    >::type VariantKeyStdVector;

typedef boost::make_variant_over<
      boost::mpl::transform<SupportedIOPayloads, MakeStdVector<boost::mpl::_> >::type
    >::type VariantPayloadStdVector;

}  // namespace detail


template<class MapType>
bool saveMap(const MapType& map, const std::string& file_path) {

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::Payload Payload;

  static_assert((boost::mpl::contains<SupportedIOKeys, KeyType>::type::value),
                          "KeyType not supported for IO");

  static_assert((boost::mpl::contains<SupportedIOPayloads, Payload>::type::value),
                          "Payload not supported for IO");

  boost::filesystem::path bfp(file_path);
  std::ofstream ofs(file_path.c_str(), std::ios::out | std::ios::binary);
  if (!ofs.is_open()) {
    std::cout << "Error: Could not Open " << bfp << std::endl;
    return false;
  }

  typename MapType::size_type map_size = map.size();


  typedef std::vector<KeyType> KeyStdVector;
  typedef typename MakeStdVector<Payload>::type PayloadStdVector;

  detail::VariantKeyStdVector variant_keys = KeyStdVector();
  detail::VariantPayloadStdVector variant_payloads = PayloadStdVector();

  {
    KeyStdVector& keys = boost::get<KeyStdVector>(variant_keys);
    PayloadStdVector& payloads = boost::get<PayloadStdVector>(variant_payloads);

    keys.reserve(map_size);
    payloads.reserve(map_size);

    for (typename MapType::const_iterator it = map.begin(); it != map.end(); ++it) {
      keys.push_back(it.key());
      payloads.push_back(it->payload());
    }
  }


  {  // use scope to ensure archive goes out of scope before stream
    typedef boost::archive::binary_oarchive Archive;
    Archive oa(ofs);
    MapFormatHeader header(map, 1, "Key-Payload");
    oa << header;
    oa << variant_keys;
    oa << variant_payloads;
  }

  ofs.close();
  std::cout << "Saved Map as " << bfp.filename() << " FileSize = "
      << boost::filesystem::file_size(bfp) / 1048576. << " MB.\n";
  return true;
}

namespace detail {

template <class MapType>
struct GenerateMap : public boost::static_visitor<> {
  typedef typename MapType::KeyType KeyType;
  typedef std::vector<KeyType> KeyStdVector;

  GenerateMap(MapType* map, const KeyStdVector* const keys)
      : map_(map),
        keys_(keys){}

  template <typename T>
  void operator()(const T& loaded_payloads) {
    BOOST_ASSERT_MSG(loaded_payloads.size() == keys_->size(), "Number of Keys and payload do not match");
    map_->reserve(keys_->size());

    typename KeyStdVector::const_iterator key_it = keys_->begin();
    typename KeyStdVector::const_iterator key_end = keys_->end();
    typename T::const_iterator loaded_payload_it = loaded_payloads.begin();

    for (; key_it != key_end; ++key_it, ++loaded_payload_it) {
      typedef typename MapType::Payload Payload;
      map_->addNewVoxel(*key_it, convertPayload<Payload>(*loaded_payload_it));
    }
  }

  MapType* map_;
  const KeyStdVector* const keys_;
};

}

/**@brief Load a map file to Naksha Map
 *
 * @tparam MapType  - Some valid Naksha Map type
 *
 * @param[in] file_path - file_path to the map file
 * @return  pointer to loaded Naksha Map (NULL if unsuccesfull)
 */
template<class MapType>
MapType* loadMap(const std::string& file_path) {
  typedef typename MapType::KeyType KeyType;
  static_assert((boost::mpl::contains<SupportedIOKeys, KeyType>::type::value),
                          "KeyType not supported for IO");

  boost::filesystem::path bfp(file_path);
  std::ifstream ifs(file_path.c_str(), std::ios::in | std::ios::binary);
  if (!ifs.is_open()) {
    std::cout << "Error: Could not Open " << bfp << std::endl;
    return NULL;
  }

  std::cout << "Loading " << MapType::Dimension << "D Map " << bfp.filename() << " FileSize = "
      << boost::filesystem::file_size(bfp) / 1048576. << " MB. ..." << std::flush;

  MapFormatHeader header;
  detail::VariantKeyStdVector variant_keys;
  detail::VariantPayloadStdVector variant_payloads;

  {  // use scope to ensure archive goes out of scope before stream
    typedef boost::archive::binary_iarchive Archive;
    Archive ia(ifs);

    ia >> header;
    ia >> variant_keys;
    ia >> variant_payloads;
  }

  ifs.close();

  typedef std::vector<KeyType> KeyStdVector;
  if(typeid(KeyStdVector) != variant_keys.type()) {
    std::cout << "Error: KeyTypes not compatible.\n";
    return NULL;
  }

  MapType* map = new MapType(header.resolution);
  detail::GenerateMap<MapType> generate_map(map, &boost::get<KeyStdVector>(variant_keys));
  boost::apply_visitor(generate_map, variant_payloads);

  std::cout << " Done. Map Size = " << map->size() << " voxels\n";
  return map;
}


}  // namespace Naksha

#endif // end NAKSHA_IMPORT_EXPORT_IMPL_HPP_
