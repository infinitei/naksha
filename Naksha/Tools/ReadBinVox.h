/**
 * @file ReadBinVox.h
 * @brief Read BinVox Files
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_TOOLS_READ_BINVOX_H_
#define NAKSHA_TOOLS_READ_BINVOX_H_

#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <tuple>
#include <Eigen/Core>

namespace Naksha {

/**@brief Reads a BinVox file to load the voxels to a Naksha MapType
 *
 * This reads voxels data stored in BinVox format:
 * -  http://www.cs.princeton.edu/~min/binvox/ (BinVox Tool)
 * -  http://www.cs.princeton.edu/~min/binvox/binvox.html (BinVox Format)
 *
 * BinVox simply dumps all voxel data into a contigious array.
 *
 * @param file_path
 * @return tuple of voxels, dimension, translation and scale
 *
 * Example Usage:
 * @code
 *  std::vector<unsigned char> voxels;
 *  Eigen::Vector3i dimension;
 *  Eigen::Vector3f translation;
 *  float scale;
 *  std::tie(voxels, dimension, translation, scale) = Naksha::readBinVoxFile(argv[1]);
 * @endcode
 *
 * @ingroup Tools
 */
std::tuple<std::vector<unsigned char>, Eigen::Vector3i,  Eigen::Vector3f, float>
readBinVoxFile(const std::string& file_path) {
  typedef unsigned char BinVoxDataType;
  std::vector<BinVoxDataType> voxels;
  Eigen::Vector3i dimension = Eigen::Vector3i::Constant(-1);
  Eigen::Vector3f translation;
  float scale;

  std::ifstream input_file(file_path.c_str(), std::ios::in | std::ios::binary);
  if ( !input_file.is_open() ) {
    std::cout << "Error: Reading Binvox file: " << file_path << "\n";
    return std::make_tuple(voxels, dimension, translation, scale);
  }

  //------------------------------- Read header-----------------------------//
  std::string line;
  input_file >> line;  // #binvox
  if (line.compare("#binvox") != 0) {
    std::cout << "Error: first line reads [" << line << "] instead of [#binvox]\n";
    return std::make_tuple(voxels, dimension, translation, scale);
  }
  int version;
  input_file >> version;
  std::cout << "Reading Binvox Ver. " << version;

  bool done = false;
  while (input_file.good() && !done) {
    input_file >> line;
    if (line.compare("data") == 0)
      done = 1;
    else if (line.compare("dim") == 0) {
      input_file >> dimension.x() >> dimension.y() >> dimension.z();
    }
    else if (line.compare("translate") == 0) {
      input_file >> translation.x() >> translation.y() >> translation.z();
    }
    else if (line.compare("scale") == 0) {
      input_file >> scale;
    }
    else {
      std::cout << "  unrecognized keyword [" << line << "], skipping\n";
      char c;
      do {  // skip until end of line
        c = input_file.get();
      } while (input_file.good() && (c != '\n'));

    }
  }
  if (!done) {
    std::cout << "Error: reading header\n";
    return std::make_tuple(voxels, dimension, translation, scale);
  }
  if ((dimension.array() < 0).any()) {
    std::cout << "Error: Missing dimensions in header\n";
    return std::make_tuple(voxels, dimension, translation, scale);
  }

  //----------------------------- Read Voxel data-----------------------------//

  std::size_t size = dimension.prod();
  voxels.reserve(size);

  std::cout << " Reading a " << dimension.x() << "x" << dimension.y() << "x" << dimension.z() << " voxel grid." << std::flush;

  std::size_t occupied_voxels_count = 0;

  input_file.unsetf(std::ios::skipws);  // need to read every byte now (!)
  BinVoxDataType temp;
  input_file >> temp;  // read the linefeed char

  while ((voxels.size() < size) && input_file.good()) {
    BinVoxDataType value;
    BinVoxDataType count;
    input_file >> value >> count;

    if (input_file.good()) {
      if ((voxels.size() + count) > size) {
        std::cout << "Error Reading BinVox data. Size in header is incorrect\n";
        return std::make_tuple(voxels, dimension, translation, scale);
      }
      for (BinVoxDataType i = 0; i < count; ++i)
        voxels.push_back(value);

      if (value)
        occupied_voxels_count += count;
    }  // if file still ok

  }  // while

  input_file.close();
  std::cout << " Found " << occupied_voxels_count << " occupied voxels from " << voxels.size() << "\n";
  return std::make_tuple(voxels, dimension, translation, scale);
}

}  // namespace Naksha




#endif // NAKSHA_TOOLS_READ_BINVOX_H_
