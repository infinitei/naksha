/**
 * @file ReadOctoMap-impl.hpp
 * @brief ReadOctoMap-impl
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_READ_OCTOMAP_IMPL_HPP_
#define NAKSHA_READ_OCTOMAP_IMPL_HPP_

#include "Naksha/Common/ProbabilityUtils.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include <boost/assert.hpp>
#include <fstream>
#include <iostream>
#include <bitset>

namespace Naksha {

namespace detail {

template<class VoxelNodeType>
void readOctoMapNodeDataRecursively(std::istream& is, VoxelNodeType* node, bool has_color, bool is_hybrid) {

  // read node data
  typedef typename VoxelNodeType::Payload::RVType::DataType RVDataType;
  RVDataType rv_data;
  is.read((char*) &rv_data, sizeof(RVDataType)); // Node Data
  node->payload().setData(rv_data, LogProbabilityTag());

  if(is_hybrid) {
    int number_of_nodes;
    is.read((char*) &number_of_nodes, sizeof(int)); // number of nodes
  }

  if(has_color) {
    typedef typename VoxelNodeType::Payload::ColoredPayloadType::DataType ColorDataType;
    ColorDataType color;
    is.read((char*) &color, sizeof(ColorDataType)); // color
    node->payload().setColor(color);
  }

  char children_char;
  is.read((char*)&children_char, sizeof(char)); // child existence

  // read existing children
  std::bitset<8> children ((unsigned long long) children_char);
  for (unsigned int i=0; i<8; i++) {
    if (children[i] == 1){
      bool child_created = node->createChild(i);
      BOOST_VERIFY(child_created);
      readOctoMapNodeDataRecursively(is, node->childPtr(i), has_color, is_hybrid);
    }
  }
}

bool readOctoMapHeader(std::istream& is, std::string& id, unsigned& size, double& res) {
  id = "";
  size = 0;
  res = 0.0;

  std::string token;

  std::getline(is, token);
  if (token.compare("# Octomap OcTree file") != 0) {
    std::cout << "First line of file does not start with Octomap header\n";
    return false;
  }

  bool headerRead = false;
  while (is.good() && !headerRead) {
    is >> token;
    if (token == "data") {
      headerRead = true;
      // skip forward to end of line
      char c;
      do {
        c = is.get();
      } while (is.good() && (c != '\n'));
    } else if (token.compare(0, 1, "#") == 0) {
      // comment line, skip forward until end of line:
      char c;
      do {
        c = is.get();
      } while (is.good() && (c != '\n'));
    } else if (token == "id")
      is >> id;
    else if (token == "res")
      is >> res;
    else if (token == "size")
      is >> size;
    else {
      std::cout << "Unknown keyword in OcTree header, skipping: " << token << "\n";
      char c;
      do {
        c = is.get();
      } while (is.good() && (c != '\n'));
    }
  }

  if (!headerRead) {
    std::cout << "Error reading OcTree header\n";
    return false;
  }

  if (id == "") {
    std::cout << "Error reading OcTree header, ID not set\n";
    return false;
  }

  if (res <= 0.0) {
    std::cout << "Error reading OcTree header, res <= 0.0\n";
    return false;
  }

  if (id == "1") {
    std::cout << "Using a deprecated id " << id
              << " changing to \"OcTree\" (you should update your file header)\n";
    id = "OcTree";
  }
  return true;
}

}  // end namespace detail

template<class MapType>
MapType* readOctoMapFile(const std::string& file_path) {
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  static_assert(VoxelNodeType::MaxNumOfChilds == 8,
                          "This requires an octree (8 child) map");

  std::ifstream ifs(file_path.c_str(), std::ios::in | std::ios::binary);
  if (!ifs.is_open()) {
    std::cout << "Error: Could not Open " << file_path << "\n";
    return NULL;
  }

  // Read Header
  std::string id;
  unsigned size;
  double resolution;
  if (!detail::readOctoMapHeader(ifs, id, size, resolution))
    return NULL;


  // get Map type from id string
  if ((id != "OcTree") && (id != "ColorOcTree") && (id != "ColoredHybridOcTree9f")) {
    std::cout << "Cannot Read OctoMap of type: " << id << "\n";
    return NULL;
  }

  bool has_color = (id.find("Color") != std::string::npos);
  bool is_hybrid = (id.find("Hybrid") != std::string::npos);

  // Verify that RVtype dimension matches
  typedef typename MapType::Payload::RVType RVType;
  if ((RVType::Dimension != (is_hybrid ? 9 : 2))) {
    std::cout << "Cannot load " << id << " with " << PRETTY_TYPE(RVType, "Naksha", 0) << "\n";
    return NULL;
  }

  // Verify that RVtype DataType sizes matches
  if (sizeof(typename RVType::DataType) != (is_hybrid ? 9 : 1) * sizeof(float)) {
    std::cout << "Cannot load " << id << " with " << PRETTY_TYPE(RVType, "Naksha", 0) << "\n";
    return NULL;
  }

  std::cout << "Reading Octomap of type " << id << " from " << file_path << " ..." << std::flush;

  // Read Data
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  VoxelNodeType* root_node = new VoxelNodeType();
  detail::readOctoMapNodeDataRecursively(ifs, root_node, has_color, is_hybrid);

  MapType* map = new MapType(resolution);
  map->resetRootNode(root_node);

  ifs.close();
  std::cout << " Done. Loaded Map with " << map->size() << " voxels. \n";
  return map;
}

}  // namespace Naksha

#endif // end NAKSHA_READ_OCTOMAP_IMPL_HPP_
