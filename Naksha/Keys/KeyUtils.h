/**
 * @file KeyUtils.h
 * @brief KeyUtils
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_KEY_UTILS_H_
#define NAKSHA_KEY_UTILS_H_

namespace Naksha {

/**
 * @brief Get child index from key at some tree depth
 *
 * @param[in] key discrete location in some \ref Keys format
 * @param[in] depth_diff depth_diff = 0 indicates immediate child
 * @return child index (0..N)
 *
 * @tparam KeyType e.g Key (should be auto deduced)
 *
 *
 * @ingroup Keys
 */
template<class KeyType>
unsigned int computeChildIndex(const KeyType& key, const int depth_diff) {
  if (KeyType::Dimension == 3) {
    return ((key.value[0] & (1 << depth_diff)) >> depth_diff)
        + (((key.value[1] & (1 << depth_diff)) >> depth_diff) << 1)
        + (((key.value[2] & (1 << depth_diff)) >> depth_diff) << 2);
  } else {
    return ((key.value[0] & (1 << depth_diff)) >> depth_diff)
        + (((key.value[1] & (1 << depth_diff)) >> depth_diff) << 1);

  }
}

/**
 * @brief Computes the key of a child node from current key and child index
 *
 * @param[in] child_index index of child node (0..N)
 * @param[in] center_offset_key constant offset of octree keys
 * @param[in] parent_key current (parent) key
 *
 * @return computed child key
 *
 * @tparam KeyType e.g Key (should be auto deduced)
 *
 * This is used by Tree Node Iterators and thus also in find method() of
 * Tree based data structures like TreeDatastructure.
 *
 * @ingroup Keys
 */
template<class KeyType>
KeyType computeChildKey(
      const unsigned int child_index,
      const typename KeyType::Scalar center_offset_key,
      const KeyType& parent_key) {

  KeyType child_key(parent_key.value);

  child_key.value[0] += (child_index & 1) ? center_offset_key : (-center_offset_key - (center_offset_key ? 0 : 1));
  child_key.value[1] += (child_index & 2) ? center_offset_key : (-center_offset_key - (center_offset_key ? 0 : 1));
  if (KeyType::Dimension == 3) {
    child_key.value[2] += (child_index & 4) ? center_offset_key : (-center_offset_key - (center_offset_key ? 0 : 1));
  }
  return child_key;
}

} // namespace Naksha

#endif // NAKSHA_KEY_UTILS_H_
