/**
 * @file Key.h
 * @brief A discrete spatial adressing key
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_KEY_H_
#define NAKSHA_KEY_H_

#include "Naksha/Common/EigenBoostSerialization.h"
#include <boost/serialization/access.hpp>
#include <tuple>
#include <limits>

namespace Naksha {

/**
 * @brief Key is a discrete address for a 3D voxel.
 *
 * Rather than continuous addressing mechanisms like voxel center
 * coordinates, it simply treats one voxel length as 1 unit.
 *
 * @tparam _Scalar should be some integer type
 * @tparam _Dimension is either 2(2D map) or 3 (3D map)
 *
 * @ingroup Keys
 */
template<int _Dimension, typename _Scalar = unsigned short int>
class Key {
public:

  /// Scalar Type (integer type) defaults to unsigned short int
  typedef _Scalar Scalar;

  enum {
    Dimension = _Dimension ///< Map dimension either 2(2D map) or 3 (3D map)
  };

  /// DataType is a Eigen Vector of size Dim
  typedef Eigen::Matrix<Scalar, Dimension, 1> DataType;

  /// Default Constructor (Initializes to zero)
  Key()
      : value(DataType::Zero()) {
  }

  /// Constructor from Eigen Expressions
  template <typename Derived>
  Key(const Eigen::DenseBase<Derived>& val)
      : value(val) {
  }

  /// Constructor for 3D map
  Key(Scalar a, Scalar b, Scalar c)
      : value(DataType(a, b, c)) {
    static_assert(_Dimension == 3, "This constructor is for 3D Map");
  }

  /// Constructor for 2D map
  Key(Scalar a, Scalar b)
      : value(DataType(a, b)) {
    static_assert(_Dimension == 2, "This constructor is for 2D Map");
  }

  template<typename Index>
  const Scalar& operator[](Index i) const {
    return value[i];
  }

  template<typename Index>
  Scalar& operator[](Index i) {
    return value[i];
  }

  /// Equal to operator
  bool operator==(const Key& other) const {
    return value == other.value;
  }

  /// Not-Equal to operator
  bool operator!=(const Key& other) const {
    return value != other.value;
  }

  /// Less than Operator
  bool operator<(const Key& other) const {
    return std::tie(value.x(), value.y(), value.z()) < std::tie(other.value.x(), other.value.y(), other.value.z());
  }

  /// Simply prints the Key
  void print(std::ostream& stream) const {
    const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
    stream << "Key" << Dimension << "D " << value.format(vecfmt);
  }

  /// Primary Payload
  DataType value;

private:
  static_assert(Dimension == 2 || Dimension == 3, "You can either have 2D or 3D Map");
  static_assert(std::is_integral<Scalar>::value, "Key Scalar can only be of integral type");

  /// Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    static_assert(DataType::SizeAtCompileTime != Eigen::Dynamic, "Need to have Fixed Size Eigen Matrix");
    ar & boost::serialization::make_array(value.data(), DataType::SizeAtCompileTime);
  }

public:
  /// This typedef determines if we need Eigen' specialized aligned new Operators
  typedef boost::mpl::int_<(sizeof(DataType)%16)==0> EigenNeedsToAlign;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW_IF(EigenNeedsToAlign::value)
};


/// Standard stream operator overload for Key
template<int Dim, typename Scalar>
std::ostream& operator<<(std::ostream& os, const Key<Dim, Scalar>& key) {
  key.print(os);
  return os;
}

} // end namespace Naksha

#include "Naksha/Common/BoostSerializationTraits.h"

BOOST_CLASS_IMPLEMENTATION_PP(Naksha::Key, (int)(class), object_serializable)
BOOST_CLASS_TRACKING_PP(Naksha::Key, (int)(class), track_never)

#endif // NAKSHA_KEY_H_
