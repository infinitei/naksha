/**
 * @file DenseKeyIterator.h
 * @brief 
 * 
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_DENSE_KEY_ITERATOR_H_
#define NAKSHA_DENSE_KEY_ITERATOR_H_

#include "Naksha/Common/Trange.h"

#include <Eigen/Geometry>
#include <type_traits>

namespace Naksha {

/**@brief Dense Keys Iterator over a bounded volume
 * This is to easily iterate over a bounded set of Keys
 *
 * @tparam KeyType type of \ref Keys used e.g. Key
 *
 * @ingroup Keys
 */
template<class KeyType>
class DenseKeysIterator
    : public boost::iterator_facade<
        DenseKeysIterator<KeyType>,
        const KeyType,
        std::bidirectional_iterator_tag> {
public:
  typedef typename KeyType::DataType KeyDataType; ///< Datatype of KeyType
  typedef typename KeyType::Scalar KeyScalar;     ///< Scalar type of KeyType
  typedef Eigen::AlignedBox< KeyScalar, KeyType::Dimension > BbxType; ///< typdef of Eigen's AlignedBox

private:
  static_assert(std::is_integral<KeyScalar>::value, "Can only handle Keys with integral type scalar");

public:

  /**@brief (Default) Constructor from Key
   *
   * @param key set current iterator key
   * @param bbx_min sets bbx_min key (defaults to min possible)
   * @param bbx_max sets bbx_max key (defaults to max possible)
   */
  DenseKeysIterator(const KeyType& key = KeyType(),
      const KeyDataType& bbx_min = KeyDataType::Constant(std::numeric_limits<KeyScalar>::min()),
      const KeyDataType& bbx_max = KeyDataType::Constant(std::numeric_limits<KeyScalar>::max()) )
        : key_(key), bbx_(BbxType(bbx_min, bbx_max)) {
      assert(bbx_.contains(key.value));
    }

  /**@brief Constructor from bbx bounds
   *
   * @param bbx_min sets bbx_min key
   * @param bbx_max sets bbx_max key
   */
  DenseKeysIterator(const KeyDataType& bbx_min, const KeyDataType& bbx_max)
    : key_(KeyType(bbx_min)), bbx_(BbxType(bbx_min, bbx_max)) {
    assert(bbx_.contains(key_.value));
  }

private:
  friend class boost::iterator_core_access;
  void increment() {
    for(int i = 0; i < KeyType::Dimension; ++i ) {
      ++key_.value[i];
      if(key_.value[i] < bbx_.max()[i])
        break;
      else
        key_.value[i] = bbx_.min()[i];
    }
    if(key_.value == bbx_.min()) {
      key_.value = bbx_.max();
    }
  }
  void decrement() {
    for(int i = 0; i < KeyType::Dimension; ++i ) {
      if(key_.value[i] > bbx_.min()[i]) {
        --key_.value[i];
        break;
      }
      key_.value[i] = bbx_.max()[i] - 1;
    }
  }
  bool equal(const DenseKeysIterator& other) const {
    return key_ == other.key_;
  }
  const KeyType& dereference() const {
    return key_;
  }

  KeyType key_;
  BbxType bbx_;
};

/**@brief Range for iterating with DenseKeysIterator
 *
 * @param from Key lower bbx key
 * @param to Key upper bbx key
 * @return Iterator Range
 *
 * Example usage:
 * @code
 * BOOST_FOREACH(const Key3D& key, DenseKeyRange(Key3D(0,0,0), Key3D(3,3,3))) {
 *  std::cout << key << ' ';
 * }
 * @endcode
 * @ingroup Keys
 */
template<class KeyType>
boost::iterator_range<DenseKeysIterator<KeyType> >
DenseKeyRange(KeyType from, KeyType to) {
  return boost::make_iterator_range(
      DenseKeysIterator<KeyType>(from.value, to.value),
      DenseKeysIterator<KeyType>(to));
}

} // end namespace Naksha

#endif // NAKSHA_DENSE_KEY_ITERATOR_H_
