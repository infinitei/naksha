/**
 * @file KeyHash.h
 * @brief Hash function for Keys
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_KEY_HASH_H_
#define NAKSHA_KEY_HASH_H_

#include <Eigen/Core>
#include <cstddef>


namespace Naksha {

namespace detail {

template <int Dimension>
struct KeyHashImpl {
  template<typename KeyType>
  static std::size_t apply(const KeyType& key);
};

template<>
template<typename KeyType>
std::size_t KeyHashImpl<3>::apply(const KeyType& key) {
  typedef Eigen::Matrix<std::size_t, 3, 1> VectorType;
  return VectorType(1u, 1337u, 345637u).dot(key.value.template cast<std::size_t>());
}

template<>
template<typename KeyType>
std::size_t KeyHashImpl<2>::apply(const KeyType& key) {
  return std::size_t(key[0]) + 1337u * std::size_t(key[1]);
}

}  // namespace detail

/**@brief Hash Functor for Keys
 *
 * Simple prime number based hash function
 *
 * @ingroup Keys
 */
struct KeyHash {

  /**
   * This takes a key and returns a hash value
   *
   * @param key
   * @return hash value
   */
  template<typename KeyType>
  std::size_t operator()(const KeyType& key) const {
    return detail::KeyHashImpl<KeyType::Dimension>::apply(key);
  }

};

}  // namespace Naksha



#endif // end NAKSHA_KEY_HASH_H_
