/**
 * @file PayloadUtils.h
 * @brief PayloadUtils
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_PAYLOAD_UTILS_H_
#define NAKSHA_PAYLOAD_UTILS_H_

#include "Naksha/Common/ProbabilityUtils.h"

namespace Naksha {

/**@brief Functor for checking Is Payload Occupied
 *
 * This functionality required Payload is of RandomVariable Type.
 * Also assumes the first element of the data corresponds to Occupancy/Free
 *
 * See IsNodeOccupied to check if a node is occupied.
 *
 * @ingroup Payload
 */
template <class Payload>
class IsPayloadOccupied {
public:
  typedef typename Payload::RVType RVType;  ///< Typedef to RandomVariable Type of this Payload
  typedef typename RVType::Scalar Scalar;  ///< Typedef to Scalar type
  typedef typename RVType::ProbTypeTag ProbTypeTag; ///< Typedef to ProbTypeTag of the RandomVariable

  /// Functor Constructor
  IsPayloadOccupied(const Scalar free_prob_thresh = Scalar(0.5))
      : free_threshold_(convertRandomVariable(free_prob_thresh, ProbTypeTag(), ProbabilityTag())) {
  }

  typedef Payload argument_type;  ///< functor argument_type
  typedef bool result_type;       ///< functor result_type

  /// @return true if a Payload's RandomVariable indicates Occupied
  bool operator() (const Payload& payload) const {
    return (*payload.data() < free_threshold_);
  }

private:
  const Scalar free_threshold_;
};

/**@brief Get Mode Label of the Payload RandomVariable
 *
 *
 * @param[in] payload
 * @return mode label of the RV
 *
 * @ingroup Payload
 */
template <class Payload>
int modeOfPayload(const Payload& payload) {
  return modeOfRandomVariable(payload.getData(), typename Payload::RVType::ProbTypeTag());
}


/**@brief Functor for Mode Label of the Payload after checking for occupancy
 *
 * Unlike mode this should output to a non Free
 * label even if Free is the actual mode
 *
 * @param[in] payload of Eigen RV type
 * @return mode label of the node after occupancy checked
 *
 * @ingroup Payload
 */
template <class Payload, typename ReturnType = int>
class ModeOfPayloadOccChecked {
public:
  typedef typename Payload::RVType RVType;  ///< Typedef to RandomVariable Type of this Payload
  typedef typename RVType::Scalar Scalar;  ///< typedef to Scalar type
  typedef typename RVType::ProbTypeTag ProbTypeTag; ///< Typedef to ProbTypeTag of the RandomVariable

  /// Functor Constructor
  ModeOfPayloadOccChecked(const Scalar free_prob_thresh = Scalar(0.5))
      : free_threshold_(convertRandomVariable(free_prob_thresh, ProbTypeTag(), ProbabilityTag())) {
  }

  typedef Payload argument_type;  ///< functor argument_type
  typedef ReturnType result_type;       ///< functor result_type

  /// @return true if a Payload's RandomVariable indicates Occupied
  ReturnType operator() (const Payload& payload) const {
    if(*payload.data() < free_threshold_) {
      int mode_label;
      payload.getData().template tail<Payload::RVType::Dimension - 1>().maxCoeff(&mode_label);
      return static_cast<ReturnType>(mode_label + 1) ;
    }
    else
      return static_cast<ReturnType>(0);
  }

private:
  const Scalar free_threshold_;
};


} // namespace Naksha

#endif // NAKSHA_PAYLOAD_UTILS_H_
