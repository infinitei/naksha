/**
 * @file RandomVariableTraits.h
 * @brief 
 * 
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_RANDOM_VARIABLE_TRAITS_H_
#define NAKSHA_RANDOM_VARIABLE_TRAITS_H_

namespace Naksha {

// RandomVariable traits
template <typename Derived>
struct RandomVariableTraits;

/// Tag for Categorical Random Variable
struct CategoricalRandomVariableTag;

/// Tag for Binary Random Variable
struct BinaryRandomVariableTag;

} // end namespace Naksha

#endif // NAKSHA_RANDOM_VARIABLE_TRAITS_H_
