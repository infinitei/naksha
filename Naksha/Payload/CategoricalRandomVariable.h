/**
 * @file CategoricalRandomVariable.h
 * @brief Categorical Random Variable
 * 
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_CATEGORICAL_RANDOM_VARIABLE_H_
#define NAKSHA_CATEGORICAL_RANDOM_VARIABLE_H_

#include "Naksha/Common/EigenBoostSerialization.h"
#include "Naksha/Payload/RandomVariableTraits.h"
#include "Naksha/Common/ProbabilityUtils.h"

#include <boost/mpl/int.hpp>
#include <type_traits>

namespace Naksha {

/**@brief Base class of DiscreteRandomVariable for Categorical Random Variables
 *
 * Primarily designed for multi-dimensional Categorical Random Variables,
 * which uses Eigen Vector as the primary payload.
 *
 * This is kind of a policy class for the host class DiscreteRandomVariable.
 *
 * @ingroup DiscreteRandomVariables
 * @ingroup Payload
 */
template<typename Derived>
class CategoricalRandomVariable {
  typedef CategoricalRandomVariable<Derived> ThisType; ///< Convenience typedef of This Class

public:

  typedef typename RandomVariableTraits<Derived>::Scalar Scalar;  ///< scalar type of the data (e.g. float)

  enum {
    Dimension = RandomVariableTraits<Derived>::Dimension  ///< Dimension of the random variable
  };

private:
  static_assert(Dimension > 1, "Random Variables cannot have less than 2 dimensions");

public:

  /// Tag of the Random Variable
  typedef typename RandomVariableTraits<Derived>::ProbTypeTag ProbTypeTag;

  /**@brief Datatype of container storing the probability values of random variable
   *
   * This is a Eigen::VectorType of length Dimension
   */
  typedef Eigen::Matrix<Scalar, Dimension, 1> DataType;

  /// Default Constructor
  CategoricalRandomVariable()
    :data_(init_data_) {
  }


  /** Construct from a DataType
   *
   * @param init_data of type DataType
   */
  CategoricalRandomVariable(const DataType& init_data)
  : data_(init_data) {
  }

  /// @return raw data stored in the node
  const DataType& getData() const {
    return data_;
  }

  /// sets the raw data to be stored in the node
  void setData(const DataType& new_data) {
    data_ = new_data;
  }

  /// sets the raw data to be stored in the node
  template<class ProbTypeFrom>
  void setData(const DataType& new_data, ProbTypeFrom type_from) {
    data_ = convertRandomVariable(new_data, ProbTypeTag(), type_from);
  }

  /// @return a const pointer to the data
  const Scalar * data() const {
    return data_.data();
  }

  /// @return probability of the RandomVraible
  DataType probability() const {
    return convertRandomVariable(data_, ProbabilityTag(), ProbTypeTag());
  }

  /// @return log probability of the RandomVraible
  DataType logProbability() const {
    return convertRandomVariable(data_, LogProbabilityTag(), ProbTypeTag());
  }

  /// @return logOddsRatio of the RandomVraible
  DataType logOddsRatio() const {
    return convertRandomVariable(data_, LogOddsTag(), ProbTypeTag());
  }

  /**@brief Set RandomVariable Probabilities
   *
   * @param new_prob of type DataType (in probability representation)
   */
  void setProbability(const DataType& new_prob ) {
    data_ = convertRandomVariable(new_prob, ProbTypeTag(), ProbabilityTag());
  }

  /// normalize the RandomVariable
  void normalize() {
    normalizeRandomVariable(data_, ProbTypeTag());
  }

  /**@brief Integrate an Measurement
   *
   * @param measurement (likelihood) of type DataType (in  same representation as ProbTypeTag )
   */
  void integrateMeasurement(const DataType& measurement) {
    multiplyRandomVariable(data_, measurement, ProbTypeTag());
    normalize();
  }

  /**@brief Integrate an Measurement without normalization
  *
  * @param measurement (likelihood) of type DataType (in  same representation as ProbTypeTag )
  */
  void integrateMeasurementLazy(const DataType& measurement) {
    multiplyRandomVariable(data_, measurement, ProbTypeTag());
  }

  /// Equal To operator
  bool operator==(const ThisType& other) const {
    return data_ == other.getData();
  }

  /// Not Equal operator
  bool operator!=(const ThisType& other) const {
    return data_ != other.getData();
  }

  /// Finish Lazy evaluation which just calls the normalization
  void finishLazyEvaluation() {
    normalize();
  }

private:

  /// Actual payload
  DataType data_;


  /// defaultInit() and init_data_ are used for faster default construction
  static DataType defaultInit() {
    DataType init_data(DataType::Constant(0.5 / (Dimension - 1)));
    init_data[0] = 0.5;
    init_data = convertRandomVariable(init_data, ProbTypeTag(), ProbabilityTag());
    return init_data;
  }

  static const DataType init_data_;



  // Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    static_assert(DataType::SizeAtCompileTime != Eigen::Dynamic, "Need to have Fixed Size Eigen Matrix");
    ar & boost::serialization::make_array(data_.data(), DataType::SizeAtCompileTime);
  }

public:
  /// This typedef determines if we need Eigen' specialized aligned new Operators
  typedef boost::mpl::int_< IsEigenNewAllignRequired<DataType>::value > EigenNeedsToAlign;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW_IF_REQUIRED(DataType)
};


template<typename Derived>
const typename CategoricalRandomVariable<Derived>::DataType
CategoricalRandomVariable<Derived>::init_data_ = CategoricalRandomVariable<Derived>::defaultInit();



/**@brief Base class of DiscreteRandomVariable for Binary Random Variables
 *
 * Specifically designed for discrete binary Random Variables. Simply stores
 * a scalar.
 *
 * This is kind of a policy class for the host class DiscreteRandomVariable.
 *
 * @ingroup DiscreteRandomVariables
 * @ingroup Payload
 */
template<typename Derived>
class BinaryRandomVariable {
  typedef BinaryRandomVariable<Derived> ThisType; ///< Convenience typedef of This Class

public:

  typedef typename RandomVariableTraits<Derived>::Scalar Scalar;  ///< scalar type of the data (e.g. float)

  enum {
    Dimension = RandomVariableTraits<Derived>::Dimension  ///< Dimension of the random variable
  };

  /// Tag of the RandomVariable
  typedef typename RandomVariableTraits<Derived>::ProbTypeTag ProbTypeTag;

  /// Datatype of container storing the probability values of random variable
  typedef Scalar DataType;

private:

  static_assert(Dimension == 2, "BinaryRandomVariable requires Dimension == 2");
  //TODO Make things work for other ProbabilityTypeTags
  static_assert((std::is_same<ProbTypeTag, LogOddsTag>::value), "BinaryRandomVariable only supports LogOddsTag");

public:

  /// Default Constructor
  BinaryRandomVariable()
    : data_(convertRandomVariable(0.0, ProbTypeTag(), LogOddsTag())) {
  }

  /**@brief Construct from a initial data
   *
   * @param init_data of type DataType
   */
  BinaryRandomVariable(const DataType& init_data)
    : data_(init_data) {
  }

  /// @return raw data stored in the node
  const DataType& getData() const {
    return data_;
  }

  /// sets the raw data to be stored in the node
  void setData(const DataType& new_data) {
    data_ = new_data;
  }

  /// @return a const pointer to the data
  const Scalar * data() const {
    return &data_;
  }

  /// @return probability of the RandomVraible
  DataType probability() const {
    return convertRandomVariable(data_, ProbabilityTag(), ProbTypeTag());
  }

  /// @return log probability of the RandomVraible
  DataType logProbability() const {
    return convertRandomVariable(data_, LogProbabilityTag(), ProbTypeTag());
  }

  /// @return logOddsRatio of the RandomVraible
  DataType logOddsRatio() const {
    return convertRandomVariable(data_, LogOddsTag(), ProbTypeTag());
  }

  /**@brief Set RandomVariable Probabilities
   *
   * @param new_prob of type DataType (in probability representation)
   */
  void setProbability(const DataType& new_prob ) {
    data_ = convertRandomVariable(new_prob, ProbTypeTag(), ProbabilityTag());
  }

  /// normalize the RandomVariable
  void normalize() {
  }

  /**@brief Integrate an Measurement
   *
   * @param measurement (likelihood) of type DataType (in  same representation as ProbTypeTag )
   */
  void integrateMeasurement(const DataType& measurement) {
    data_ += measurement;
  }

  /**@brief Integrate an Measurement without normalization
   *
   * For BinaryRandomVariable this is same as integrateMeasurement. since there is no
   * normalization involved
   *
   * @param measurement (likelihood) of type DataType (in  same representation as ProbTypeTag )
   */
  void integrateMeasurementLazy(const DataType& measurement) {
    data_ += measurement;
  }

  /// Equal To operator
  bool operator==(const ThisType& other) const {
    return data_ == other.getData();
  }

  /// Not Equal operator
  bool operator!=(const ThisType& other) const {
    return data_ != other.getData();
  }

private:

  /// Actual payload
  DataType data_;

  // Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & data_;
  }
};

}
 // end namespace Naksha

#include "Naksha/Common/BoostSerializationTraits.h"

BOOST_CLASS_IMPLEMENTATION_PP(Naksha::CategoricalRandomVariable, (class), object_serializable)
BOOST_CLASS_TRACKING_PP(Naksha::CategoricalRandomVariable, (class), track_never)

BOOST_CLASS_IMPLEMENTATION_PP(Naksha::BinaryRandomVariable, (class), object_serializable)
BOOST_CLASS_TRACKING_PP(Naksha::BinaryRandomVariable, (class), track_never)

#endif // NAKSHA_CATEGORICAL_RANDOM_VARIABLE_H_
