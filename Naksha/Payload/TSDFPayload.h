/**
 * @file TSDFPayload.h
 * @brief TSDFPayload
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_TSDF_PAYLOAD_H_
#define NAKSHA_TSDF_PAYLOAD_H_

#include "Naksha/Common/EigenBoostSerialization.h"

namespace Naksha {

/**
 * @brief TSDF( Truncated signed distance function) payload
 *
 * @ingroup Payload
 *
 */
struct TSDFPayload {
  using DataType = Eigen::Vector2f;

  TSDFPayload()
      : weighted_tsdf_(1.0f, 0.0f) {
  }

  inline void integrateMeasurement(const DataType& measurement) {
    weighted_tsdf_[0] = weighted_tsdf_.prod() + measurement.prod();
    weighted_tsdf_[1] += measurement[1];
    weighted_tsdf_[0] /= weighted_tsdf_[1];
  }

  inline float distance() const {return weighted_tsdf_[0];}
  inline float weight() const {return weighted_tsdf_[1];}

 private:
  DataType weighted_tsdf_;


  // Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    static_assert(DataType::SizeAtCompileTime != Eigen::Dynamic, "Need to have Fixed Size Eigen Matrix");
    ar & boost::serialization::make_array(weighted_tsdf_.data(), DataType::SizeAtCompileTime);
  }

};


}  // namespace Naksha

#include "Naksha/Common/BoostSerializationTraits.h"

BOOST_CLASS_IMPLEMENTATION(Naksha::TSDFPayload, object_serializable)
BOOST_CLASS_TRACKING(Naksha::TSDFPayload, track_never)

#endif // end NAKSHA_TSDF_PAYLOAD_H_
