/**
 * @file PayloadTraits.h
 * @brief PayloadTraits
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_PAYLOAD_TRAITS_H_
#define NAKSHA_PAYLOAD_TRAITS_H_

#include "Naksha/Payload/DiscreteRandomVariable.h"
#include "Naksha/Payload/ColoredPayload.h"

#include <type_traits>
#include <boost/mpl/has_xxx.hpp>

namespace Naksha {

/**@brief Check if a payload type has Color Information (derived from ColoredPayload)
 *
 * Example Usage:
 * HasColorInfo<int>::value // evaluates to false
 * HasColorInfo<int>::type // evaluates to false_type
 * HasColorInfo<ColoredPayload>::value // evaluates to true
 * HasColorInfo<ColoredPayload>::type // true_type
 */
template<class Payload>
struct HasColorInfo : std::is_base_of<ColoredPayload, Payload> {};


namespace detail {
BOOST_MPL_HAS_XXX_TRAIT_DEF(RVType)
}

/**@brief Traits class to check if a type has RandomVariable Information
 *
 * Currently this just checks if a type has RVType (defined in DiscreteRandomVariable)
 *
 * Example Usage:
 * HasRandomVariableInfo<ColoredPayload>::value // evaluates to false
 * HasRandomVariableInfo<ColoredPayload>::type // evaluates to false_type
 * HasRandomVariableInfo<ColoredRandomVariable>::value // evaluates to true
 * HasRandomVariableInfo<ColoredRandomVariable>::type // true_type
 *
 */
template<class Payload>
struct HasRandomVariableInfo : detail::has_RVType<Payload> {};

/**@brief Traits class to check if a type is DiscreteRandomVariable
 *
 *
 * Example Usage:
 * IsDiscreteRandomVariable<ColoredRandomVariable>::value // evaluates to false
 * IsDiscreteRandomVariable<ColoredRandomVariable>::type // evaluates to false_type
 * HasRandomVariableInfo<DiscreteRandomVariable>::value // evaluates to true
 * HasRandomVariableInfo<DiscreteRandomVariable>::type // true_type
 *
 */
template<class Payload>
struct IsDiscreteRandomVariable : std::false_type {};

template < int Dim, typename Scalar, class ProbTypeTag, template<class > class Base>
struct IsDiscreteRandomVariable<DiscreteRandomVariable<Dim, Scalar, ProbTypeTag, Base> > : std::true_type {};

}  // namespace Naksha

#endif // end NAKSHA_PAYLOAD_TRAITS_H_
