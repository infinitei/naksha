/**
 * @file DiscreteRandomVariable.h
 * @brief Discrete Random Variable types
 *
 * Basically stores a Eigen vector storing the
 * probabilities of the random variable taking
 * each discrete label. We have three different
 * types of DiscreteRandomVariable :
 *  - ProbabilityRandomVariable
 *  - LogProbabilityRandomVariable
 *  - LogOddsRandomVariable (Only for Binary RV)
 *
 * The primary class is DiscreteRandomVariable<Dimension, Scalar, ProbTypeTag>.
 * ProbTypeTag is the policy tag which can be either of :
 *  - ProbabilityTag
 *  - LogProbabilityTag
 *  - LogOddsTag
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_DISCRETE_RANDOM_VARIABLE_H_
#define NAKSHA_DISCRETE_RANDOM_VARIABLE_H_

#include "Naksha/Payload/CategoricalRandomVariable.h"

namespace Naksha {

/**
 * @brief Discrete Random Variable
 *
 * @tparam _Dimension of the Random Variable (>=2)
 * @tparam _Scalar some floating point type (e.g. float, double, etc)
 * @tparam _ProbTypeTag Probability Type Policy tag = (ProbabilityTag or LogProbabilityTag (Default) or LogOddsTag)
 * @tparam _BaseClass BaseClass for CRTP e.g. CategoricalRandomVariable, BinaryRandomVariable
 *
 * Example Instantiation of DiscreteRandomVariable:
 * @code
 *  typedef DiscreteRandomVariable<9, float> RVLP9f;  // 9 dimensional RandomVariable with LogProbabilityTag
 *  typedef DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> BLORF;  // A BinaryRandomVariable
 *  typedef DiscreteRandomVariable<11, double, ProbabilityTag> RVP11d;  // 11 dimensional RandomVariable with ProbabilityTag
 * @endcode
 *
 * @ingroup DiscreteRandomVariables
 * @ingroup Payload
 */
template<
  int _Dimension,
  typename _Scalar,
  class _ProbTypeTag = LogProbabilityTag,
  template<class > class _BaseClass = CategoricalRandomVariable>
class DiscreteRandomVariable:
    public _BaseClass<DiscreteRandomVariable<_Dimension, _Scalar, _ProbTypeTag, _BaseClass> > {

  static_assert(_Dimension > 1, "Random Variables cannot have less than 2 dimensions");

public:
  enum {
    Dimension = _Dimension  ///< Dimension of the Random Variable
  };

  /// Convenience typedef of the This Class
  typedef DiscreteRandomVariable<_Dimension, _Scalar, _ProbTypeTag, _BaseClass> RVType;

  /// Convenience typedef of the Base class
  typedef _BaseClass<RVType> Base;

  /// DataType of the raw data
  typedef typename Base::DataType DataType;

  /// Default Constructor
  DiscreteRandomVariable() : Base() {}

  /** Construct from a DataType
   *
   * @param init_data of type DataType
   */
  DiscreteRandomVariable(const DataType& init_data)
  : Base(init_data) {
  }

};

namespace detail {

template <typename T>
struct RVBaseTypeTag;

template <typename Derived>
struct RVBaseTypeTag <BinaryRandomVariable<Derived> > {
  typedef BinaryRandomVariableTag type;
};

template <typename Derived>
struct RVBaseTypeTag <CategoricalRandomVariable<Derived> > {
  typedef CategoricalRandomVariableTag type;
};

}

// RandomVariableTraits specialization for OccupancyNodeInterface:
template < int _Dimension, typename _Scalar, class _ProbTypeTag, template<class > class _BaseClass>
struct RandomVariableTraits<DiscreteRandomVariable<_Dimension, _Scalar, _ProbTypeTag, _BaseClass> > {
  typedef DiscreteRandomVariable<_Dimension, _Scalar, _ProbTypeTag, _BaseClass> RVType;
  typedef _Scalar Scalar;
  static const int Dimension = _Dimension;
  typedef _ProbTypeTag ProbTypeTag;
  typedef typename detail::RVBaseTypeTag<_BaseClass<RVType> >::type RVTypeTag;
};


} // end namespace Naksha

BOOST_CLASS_IMPLEMENTATION_PP(Naksha::DiscreteRandomVariable, (int)(class)(class)(template<class> class), object_serializable)
BOOST_CLASS_TRACKING_PP(Naksha::DiscreteRandomVariable, (int)(class)(class)(template<class> class), track_never)

#endif // NAKSHA_DISCRETE_RANDOM_VARIABLE_H_
