/**
 * @file MeasurementModel-impl.hpp
 * @brief MeasurementModel-impl
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_MEASUREMENT_MODEL_IMPL_HPP_
#define NAKSHA_MEASUREMENT_MODEL_IMPL_HPP_

#include <type_traits>

namespace Naksha {

namespace detail {

template<class RandomVariable, class Enable>
class InitMeasurementModel {

  typedef typename RandomVariable::DataType DataType;
  typedef typename RandomVariable::Scalar Scalar;
  typedef typename RandomVariable::ProbTypeTag ProbTypeTag;

public:
  static DataType init(const Scalar & value) {
    // create Probability based P(m|z)
    DataType meas_model = value;


    // Create Forward sensor model P(z|m) = K * P(m|z) / P(m)
//    const DataType prior = setDatatype(Scalar(0.5));
//    meas_model = meas_model.cwiseQuotient(prior);

    // Convert to required probability type
    convertRandomVariableInPlace(meas_model, ProbTypeTag(), ProbabilityTag());
    return meas_model;
  }
};


template<class RandomVariable>
class InitMeasurementModel<RandomVariable, ENABLE_IF_EIGEN_TYPE(typename RandomVariable::DataType) > {

  typedef typename RandomVariable::DataType DataType;
  typedef typename RandomVariable::Scalar Scalar;
  typedef typename RandomVariable::ProbTypeTag ProbTypeTag;

  typedef std::array< DataType, DataType::SizeAtCompileTime - 1> ArrayOfDataType;

public:
  static DataType init(const Scalar & value) {
    // create Probability based P(m|z)
    DataType meas_model = setDatatype(value);


    if (!std::is_same<ProbTypeTag, LogOddsTag>::value) {
      // Create Forward sensor model P(z|m) = K * P(m|z) / P(m)
      const DataType prior = setDatatype(Scalar(0.5));
      meas_model = meas_model.cwiseQuotient(prior);
    }

    // Convert to required probability type
    convertRandomVariableInPlace(meas_model, ProbTypeTag(), ProbabilityTag());
    return meas_model;
  }

  // Compute Semantic | Hit probabilities
  static ArrayOfDataType init(const Scalar& free_hit_prob, const Scalar& semantic_hit_prob) {

    static_assert(RandomVariable::Dimension > 2, "This requires a Hybrid Measurement Model");

    Scalar negate_semantic_hit_likelihood = (Scalar(1) - free_hit_prob - semantic_hit_prob) / (RandomVariable::Dimension -2);

    ArrayOfDataType semantic_hit;
    for (std::size_t i = 0; i < semantic_hit.size(); ++i) {

      DataType meas_model = DataType::Constant(negate_semantic_hit_likelihood);
      meas_model[0] = free_hit_prob;
      meas_model[i+1] = semantic_hit_prob;

      // Create Forward sensor model P(z|m) = K * P(m|z) / P(m)
      const DataType prior = setDatatype(Scalar(0.5));
      meas_model = meas_model.cwiseQuotient(prior);

      // Convert to required probability type
      convertRandomVariableInPlace(meas_model, ProbTypeTag(), ProbabilityTag());

      semantic_hit[i] = meas_model;
    }

    return semantic_hit;
  }

private:
  static DataType setDatatype(const Scalar & value) {
    DataType meas_model = DataType::Constant((Scalar(1) - value) / (RandomVariable::Dimension -1));
    meas_model[0] = value;
    return meas_model;
  }
};

} // end namespace detail
} // end namespace Naksha

#endif // NAKSHA_MEASUREMENT_MODEL_IMPL_HPP_
