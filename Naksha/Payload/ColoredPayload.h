/**
 * @file ColoredPayload.h
 * @brief ColoredPayload
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_COLORED_PAYLOAD_H_
#define NAKSHA_COLORED_PAYLOAD_H_

#include "Naksha/Common/EigenBoostSerialization.h"

namespace Naksha {

/**
 * @brief ColoredPayload containing color (texture) information
 *
 * @ingroup Payload
 */
class ColoredPayload {
public:
  typedef ColoredPayload ColoredPayloadType;  ///< Convenience typedef for ColoredPayload
  typedef Eigen::Matrix<unsigned char, 3, 1> DataType;  ///< DataType of underlying Eigen Data

  /// Default Constructor (Initializes as Black Colored)
  ColoredPayload()
      : color_(DataType::Zero()) {
  }

  /// Constructor (Initializes from a particular color)
  ColoredPayload(const DataType& init_color)
      : color_(init_color) {
  }

  /// Get color
  const DataType& getColor() const {
    return color_;
  }

  /// Set color
  void setColor(const DataType& new_color ) {
    color_ = new_color;
  }

  /// Set color
  void setColor(unsigned char r, unsigned char g, unsigned char b) {
    color_ = DataType(r, g, b);
  }

  /// has any color been integrated? (pure black is very unlikely...)
  bool isColorSet() const {
    return color_ != DataType::Zero();
  }

  /// Equal operator
  bool operator==(const ColoredPayloadType& other) const {
    return color_ == other.getColor();
  }

  /// Not Equal operator
  bool operator!=(const ColoredPayloadType& other) const {
    return color_ != other.getColor();
  }


private:
  DataType color_;

  // Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    static_assert(DataType::SizeAtCompileTime != Eigen::Dynamic, "Need to have Fixed Size Eigen Matrix");
    ar & boost::serialization::make_array(color_.data(), DataType::SizeAtCompileTime);
  }
};


} // end namespace Naksha

#include "Naksha/Common/BoostSerializationTraits.h"

BOOST_CLASS_IMPLEMENTATION(Naksha::ColoredPayload, object_serializable)
BOOST_CLASS_TRACKING(Naksha::ColoredPayload, track_never)

#endif // NAKSHA_COLORED_PAYLOAD_H_
