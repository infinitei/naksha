/**
 * @file SemanticLabels.h
 * @brief SemanticLabels
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_SEMANTIC_LABELS_H_
#define NAKSHA_SEMANTIC_LABELS_H_

namespace Naksha {

/// Namespace for 2D ImageLabels
namespace ImageLabels {

/// 2D Image Label Space
enum SemanticLabel {
  SKY = 0,         //!< SKY
  ROAD = 1,        //!< ROAD
  SIDEWALK = 2,    //!< SIDEWALK
  BUILDING = 3,    //!< BUILDING
  TREE = 4,        //!< TREE
  FENCE = 5,       //!< FENCE
  CAR = 6,         //!< CAR
  PEDESTRIAN = 7,  //!< PEDESTRIAN
  COLUMN_POLE = 8, //!< COLUMN_POLE
  BICYCLIST = 9,   //!< BICYCLIST
  SIGN_SYMBOL = 10,//!< SIGN_SYMBOL
  UNKNOWN = 11     //!< UNKNOWN
};

} // end namespace ImageSemanticLabel

/// Namespace for VoxelLabels
namespace VoxelLabels {

/// Voxel Semantic Label Space
enum SemanticLabel {
  FREE = 0,        //!< FREE
  ROAD = 1,        //!< ROAD
  SIDEWALK = 2,    //!< SIDEWALK
  BUILDING = 3,    //!< BUILDING
  TREE = 4,        //!< TREE
  FENCE = 5,       //!< FENCE
  CAR = 6,         //!< CAR
  PEDESTRIAN = 7,  //!< PEDESTRIAN
  COLUMN_POLE = 8, //!< COLUMN_POLE
  BICYCLIST = 9,   //!< BICYCLIST
  SIGN_SYMBOL = 10,//!< SIGN_SYMBOL
  UNKNOWN = 11,    //!< UNKNOWN
  SOLID = 100     //!< Any SOLID
};

} // end namespace VoxelSemanticLabel

} // namespace Naksha

#endif // NAKSHA_SEMANTIC_LABELS_H_
