/**
 * @file LabelToMeasurementMapping.h
 * @brief LabelToMeasurementMapping
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_LABEL_TO_MEASUREMENT_MAPPING_H_
#define NAKSHA_LABEL_TO_MEASUREMENT_MAPPING_H_

#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Payload/SemanticLabels.h"

namespace Naksha {

/**
 * @brief A static SemanticLabel to Measurement Model Map
 *
 * @ingroup MeasurementModels
 * @ingroup Payload
 */
template<class RandomVariable>
class MeasurementMap {
public:
  typedef MeasurementModel<RandomVariable> MeasurementModelType;  ///< typedef of the MeasurementModel type
  typedef typename MeasurementModelType::DataType MeasurementModelDataType;  ///< DataType of the MeasurementModel type

  template<int Label> struct LabelType {};

  /// Primary function
  template<int Label>
  static const MeasurementModelDataType& at() {
    return at(LabelType<Label>());
  }

private:
  template<int Label>
  static const MeasurementModelDataType& at(LabelType<Label>) {
    static_assert(IsEigenType<MeasurementModelDataType>::value, "Expects Eigen Types Only!!");
    static_assert((Label > 0) && (Label < RandomVariable::Dimension), "Out of Range Baby");
    return MeasurementModelType::SEMANTIC_HIT[Label - 1];
  }

  static const MeasurementModelDataType& at(LabelType<ImageLabels::SKY>) {
    return MeasurementModelType::MISS;
  }

  static const MeasurementModelDataType& at(LabelType<ImageLabels::UNKNOWN>) {
    return MeasurementModelType::HIT;
  }
};

} // end namespace Naksha

#endif // NAKSHA_LABEL_TO_MEASUREMENT_MAPPING_H_
