/**
 * @file PayloadMaker.h
 * @brief PayloadMaker
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_PAYLOAD_MAKER_H_
#define NAKSHA_PAYLOAD_MAKER_H_

#include "Naksha/Payload/DiscreteRandomVariable.h"
#include "Naksha/Payload/ColoredPayload.h"
#include "Naksha/Payload/TSDFPayload.h"

namespace Naksha {

/// An Empty Payload
struct EmptyPayload {
 private:
   // Boost serialization
   friend class boost::serialization::access;
   template<class Archive>
   void serialize(Archive & ar, const unsigned int version) {}
};


template <class T1, class T2>
class CombinePayloads2 : public T1, public T2 {
public:
  typedef CombinePayloads2<T1,T2> type;
  typedef typename T1::DataType T1DataType;
  typedef typename T2::DataType T2DataType;

  CombinePayloads2() {}

  CombinePayloads2(const T1DataType& t1_init_data, const T2DataType& t2_init_data)
      : T1(t1_init_data), T2(t2_init_data) {
  }

  explicit CombinePayloads2(const T1DataType& t1_init_data)
      : T1(t1_init_data) {
  }

  explicit CombinePayloads2(const T2DataType& t2_init_data)
      : T2(t2_init_data) {
  }

  bool operator==(const type& other) const {
    return (T1::operator==(static_cast<T1>(other))) && (T2::operator==(static_cast<T2>(other)));
  }

  bool operator!=(const type& other) const {
    return (T1::operator!=(static_cast<T1>(other))) || (T2::operator!=(static_cast<T2>(other)));
  }

private:
  /// Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & boost::serialization::base_object< T1 >(*this);
    ar & boost::serialization::base_object< T2 >(*this);
  }
};

/** ColoredRandomVariable
 * @tparam RandomVariable like DiscreteRandomVarible
 * @ingroup Payload
 */
template <class RandomVariable>
struct ColoredRandomVariable {
  typedef typename CombinePayloads2<RandomVariable, ColoredPayload>::type type;
};

} // end namespace Naksha

BOOST_CLASS_IMPLEMENTATION(Naksha::EmptyPayload, boost::serialization::object_serializable)
BOOST_CLASS_TRACKING(Naksha::EmptyPayload, boost::serialization::track_never)

BOOST_CLASS_IMPLEMENTATION_PP(Naksha::CombinePayloads2, (class)(class), object_serializable)
BOOST_CLASS_TRACKING_PP(Naksha::CombinePayloads2, (class)(class), track_never)

#endif // NAKSHA_PAYLOAD_MAKER_H_
