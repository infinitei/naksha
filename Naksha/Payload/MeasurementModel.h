/**
 * @file MeasurementModel.h
 * @brief Measurement Models
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_MEASEREMENT_MODEL_H_
#define NAKSHA_MEASEREMENT_MODEL_H_

#include "Naksha/Common/ProbabilityUtils.h"
#include <array>

namespace Naksha {

namespace detail {
template<class RandomVariable, class Enable = void>
class InitMeasurementModel;
}

/**@brief Binary Measurement Model i.e. either HIT or MISS
 *
 * BinaryMeasurementModel is the simplest measurement model
 * which can be either HIT or MISS. This class has been specialized
 * on RandomVariable type.
 *
 * @tparam RandomVariable type of Random Variable. See DiscreteRandomVariable.
 *
 * @ingroup MeasurementModels
 * @ingroup Payload
 */
template<class RandomVariable>
class BinaryMeasurementModel {
public:
  typedef typename RandomVariable::DataType DataType; ///< typedef of underlying Data Type (Same as RandomVariable's DataType)
  typedef typename RandomVariable::Scalar Scalar; ///< Scalar type of the underlying DataType

  static const DataType MISS; ///< Corresponds to a MISS measurement
  static const DataType HIT;  ///< Corresponds to a HIT measurement without semantic label

  static const Scalar free_hit_prob;       ///< P(Free|Hit)
  static const Scalar free_miss_prob ;      ///< P(Free|Miss)
};

// Initializations

template<class RandomVariable> const typename RandomVariable::Scalar
BinaryMeasurementModel<RandomVariable>::free_hit_prob = 0.3;       ///< P(Free|Hit)

template<class RandomVariable> const typename RandomVariable::Scalar
BinaryMeasurementModel<RandomVariable>::free_miss_prob = 0.6;      ///< P(Free|Miss)

template<class RandomVariable>
const typename RandomVariable::DataType BinaryMeasurementModel<RandomVariable>::MISS =
    detail::InitMeasurementModel<RandomVariable>::init(BinaryMeasurementModel<RandomVariable>::free_miss_prob);

template<class RandomVariable>
const typename RandomVariable::DataType BinaryMeasurementModel<RandomVariable>::HIT =
    detail::InitMeasurementModel<RandomVariable>::init(BinaryMeasurementModel<RandomVariable>::free_hit_prob);





/**@brief Hybrid Measurement Model
 *
 * HybridMeasurementModel measurement model is for multi-class mapping
 * which supports two different kind of measurement models either MISS
 * or HIT(Label). This class has been specialized
 * on RandomVariable type.
 *
 * @tparam RandomVariable type of Random Variable. See DiscreteRandomVariable.
 *
 * @ingroup Payload
 */
template<class RandomVariable>
class HybridMeasurementModel : public BinaryMeasurementModel<RandomVariable> {

  typedef BinaryMeasurementModel<RandomVariable> Base;

public:
  typedef typename RandomVariable::DataType DataType; ///< typedef of underlying Data Type (Same as RandomVariable's DataType)
  typedef typename RandomVariable::Scalar Scalar;  ///< Scalar type of the underlying DataType

  typedef std::array< DataType, DataType::SizeAtCompileTime - 1> ArrayOfDataType; ///< typdef for Array<DataType> to store semantic measurement models
  static const ArrayOfDataType SEMANTIC_HIT;  ///< Corresponds to a HIT measurement with semantic label

  static const Scalar semantic_hit_prob ;   ///< e.g P(Road|Z= Road,Hit)
};

// Initializations

template<class RandomVariable> const typename RandomVariable::Scalar
HybridMeasurementModel<RandomVariable>::semantic_hit_prob = 0.52;   ///< e.g P(Road|Z= Road,Hit)

template<class RandomVariable> const typename HybridMeasurementModel<RandomVariable>::ArrayOfDataType
HybridMeasurementModel<RandomVariable>::SEMANTIC_HIT =
    detail::InitMeasurementModel<RandomVariable>::init(HybridMeasurementModel<RandomVariable>::free_hit_prob, HybridMeasurementModel<RandomVariable>::semantic_hit_prob);


/**@brief Generic Measurement Model
 *
 * This is either HybridMeasurementModel or BinaryMeasurementModel based
 * on RandomVariable Type
 *
 * @tparam RandomVariable type of Random Variable. See DiscreteRandomVariable.
 *
 * @ingroup Payload
 */
template<class RandomVariable, class Enable = void>
class MeasurementModel : public BinaryMeasurementModel<RandomVariable> {
};

template<class RandomVariable>
class MeasurementModel<RandomVariable, ENABLE_IF_EIGEN_TYPE(typename RandomVariable::DataType) >
  : public HybridMeasurementModel<RandomVariable> {
};



} // namespace Naksha

#include "Naksha/Payload/MeasurementModel-impl.hpp"

#endif // NAKSHA_MEASEREMENT_MODEL_H_
