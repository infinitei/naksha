/**
 * @file ConvertPayload.h
 * @brief ConvertPayload
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_CONVERT_PAYLOAD_H_
#define NAKSHA_CONVERT_PAYLOAD_H_

#include "Naksha/Payload/PayloadTraits.h"
#include "Naksha/Payload/PayloadMaker.h"

namespace Naksha {

/**@brief Check if both payload types has Color Information (derived from ColoredPayload)
 *
 * Example Usage:
 * HasCompatibleColorInfo<int, ColoredPayload>::value // evaluates to false
 * HasCompatibleColorInfo<ColoredPayload, int>::type // evaluates to false_type
 * HasCompatibleColorInfo<ColoredPayload, ColoredRandomVariable>::value // evaluates to true
 * HasCompatibleColorInfo<ColoredPayload, ColoredRandomVariable>::type // true_type
 */
template<class PayloadA, class PayloadB>
struct HasCompatibleColorInfo : std::integral_constant<bool,
    HasColorInfo<PayloadA>::value && HasColorInfo<PayloadB>::value> {
};

template <class PayloadA, class PayloadB, class Enable = void>
struct HasCompatibleRandomVariableInfo : public std::false_type {
};

// TODO Remove the hard requirement for RVTypeTag to be same
template<class PayloadA, class PayloadB>
struct HasCompatibleRandomVariableInfo<PayloadA, PayloadB,
    typename std::enable_if<
        (RandomVariableTraits<typename PayloadA::RVType>::Dimension
            == RandomVariableTraits<typename PayloadB::RVType>::Dimension)
        && std::is_same<
                typename RandomVariableTraits<typename PayloadA::RVType>::RVTypeTag,
                typename RandomVariableTraits<typename PayloadB::RVType>::RVTypeTag>::value
        && std::is_same<
                typename RandomVariableTraits<typename PayloadA::RVType>::RVTypeTag,
                CategoricalRandomVariableTag
    >::value>::type> : public std::true_type {
};

namespace detail {

template<class ToPayload, class FromPayload>
inline void addColorInfoImpl(ToPayload& ret_payload, const FromPayload& from_payload,
                       std::true_type) {
  ret_payload.setColor(from_payload.getColor());
}

template<class ToPayload, class FromPayload>
inline void addColorInfoImpl(ToPayload& ret_payload, const FromPayload& from_payload,
                       std::false_type) {
}

template<class ToPayload, class FromPayload>
inline void addRVInfoImpl(ToPayload& to_payload, const FromPayload& from_payload,
                       std::true_type) {
  typedef typename RandomVariableTraits<typename FromPayload::RVType>::ProbTypeTag ProbTypeFrom;
  typedef typename RandomVariableTraits<typename ToPayload::RVType>::ProbTypeTag ProbTypeTo;
  typedef typename RandomVariableTraits<typename ToPayload::RVType>::Scalar ScalarTo;
  to_payload.setData(convertRandomVariable(from_payload.getData(), ProbTypeTo(), ProbTypeFrom())
          .template cast<ScalarTo>());
}

template<class ToPayload, class FromPayload>
inline void addRVInfoImpl(ToPayload& ret_payload, const FromPayload& from_payload,
                       std::false_type) {
}

}  // namespace detail

// set color data

template<class ToPayload, class FromPayload>
inline void addColorInfo(ToPayload& to_payload, const FromPayload& from_payload) {
  detail::addColorInfoImpl(to_payload, from_payload,
                           typename HasCompatibleColorInfo<ToPayload, FromPayload>::type());
}


template<class ToPayload, class FromPayload>
inline void addRVInfo(ToPayload& to_payload, const FromPayload& from_payload) {
  detail::addRVInfoImpl(to_payload, from_payload,
                        typename HasCompatibleRandomVariableInfo<ToPayload, FromPayload>::type());
}

template<class ReturnPayload, class FromPayload, class Enable = void>
struct ConvertPayload {
  static_assert((!std::is_same<ReturnPayload, FromPayload>::value),
                          "Specilaization Error. Should be handled by Different specialization");

  static ReturnPayload apply(const FromPayload& payload) {
    // TODO Use constructor itself to initialize ret_payload
    ReturnPayload ret_payload;

    // set RV data
    addRVInfo(ret_payload, payload);

    // set color data
    addColorInfo(ret_payload, payload);

    return ret_payload;
  }
};

/// Specialization of ConvertPayload when ReturnPayload and FromPayload are same
template <class Payload>
struct ConvertPayload<Payload, Payload> {
  static Payload apply(const Payload& payload) {
    return payload;
  }
};

/** Specialization for Payloads for which we can transfer only "Random Variable" information
 *  and ReturnPayload is a DiscreteRandomVariable
 */
template<class ReturnPayload, class FromPayload>
struct ConvertPayload<ReturnPayload, FromPayload,
    typename std::enable_if<
        !std::is_same<ReturnPayload, FromPayload>::value
            && IsDiscreteRandomVariable<ReturnPayload>::value
            && HasCompatibleRandomVariableInfo<ReturnPayload, FromPayload>::value>::type> {

  static ReturnPayload apply(const FromPayload& payload) {
    typedef typename RandomVariableTraits<typename FromPayload::RVType>::ProbTypeTag ProbTypeFrom;
    typedef typename RandomVariableTraits<typename ReturnPayload::RVType>::ProbTypeTag ProbTypeTo;
    typedef typename RandomVariableTraits<typename ReturnPayload::RVType>::Scalar ScalarTo;
    return ReturnPayload(
        convertRandomVariable(payload.getData(), ProbTypeTo(), ProbTypeFrom())
            .template cast<ScalarTo>());
  }
};

/**@brief Convert from one payload type to another
 *
 * @param payload of type FromPayload
 * @return  payload in type ReturnPayload
 */
template <class ReturnPayload, class FromPayload>
inline ReturnPayload convertPayload(const FromPayload& payload) {
  return ConvertPayload<ReturnPayload, FromPayload>::apply(payload);
}

}  // namespace Naksha



#endif // end NAKSHA_CONVERT_PAYLOAD_H_
