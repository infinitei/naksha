namespace Naksha {
/**
  \addtogroup Payload

  \brief Payload is stuff contained inside \ref Nodes.

This is probably the module to configured most by Naksha client codes. Payload can be anything from a empty class to custom color variables to
RandomVariables.

The library plans to implement common \ref Payload like DiscreteRandomVariable, TSDF,
ColoredRandomVariable, others or a combination of some of them.
 
PayloadMaker.h has several convenience functions and typedefs for generating \ref Payload types.
*/

/**
  \defgroup DiscreteRandomVariables
  
  \brief DiscreteRandomVariables are for representing binary/multi-label probability distributions

  A binary random variable is the most natuaral representation for standard occupancy mapping. For
  a multi-label problem like <a href="http://www.cc.gatech.edu/~akundu7/projects/JointSegRec/">Joint
  Semantic Segmentation and 3D Reconstruction from Monocular Video</a>, we can simply use a 
  categorical random variable.
  
  Look at host class DiscreteRandomVariable which is all you need to create a random variable payload.
  
  
  \ingroup Payload
*/

/**
  \defgroup MeasurementModels

  \brief Different measurement models to integrate into payload.
  
  MeasurementModels are used to generate a a particular measurement. Thise is closely tied to 
  \ref DiscreteRandomVariables, where MeasurementModels provide the measurement data for functions 
  like integrateMeasurement() of DiscreteRandomVariables.
  
  \ingroup Payload
*/

} // end namespace Naksha