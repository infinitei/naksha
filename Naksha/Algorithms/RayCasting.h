/**
 * @file RayCasting.h
 * @brief RayCasting
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_ALGORITHMS_RAYCASTING_H_
#define NAKSHA_ALGORITHMS_RAYCASTING_H_

#include "Naksha/Nodes/NodeUtils.h"
#include "Naksha/Algorithms/RayMarching.h"
#include <tuple>
#include <type_traits>
#include <boost/mpl/if.hpp>

namespace Naksha {

/**@brief Functor for Ray Casting to get information of Solid Voxel along a Ray
 *
 * This shoots a ray and finds the nearest voxel that satisfies some criteria
 * while performing some boundary checks for stopping the process.*
 *
 * Example Usage:
 * @code
 *  // Construct the functor for a map with default occupancy threshold
 *  RayCasting<MapType> ray_caster(&map);
 *
 *  bool hit; KeyType hit_key;  RayCasterType::VoxelNodePtrType hit_node;
 *  std::tie(hit, hit_key, hit_node) = ray_caster(start, end-start);
 *
 *  // Same as above but this time with some max_range check
 *  RayCasting<MapType>::ReturnType result = ray_caster(start, end-start, max_range);
 * @endcode
 *
 * @ingroup Algorithms
 */
template<class MapType, bool IgnroreUnknownVoxels = true>
class RayCasting {
 public:
  typedef typename MapType::TransformScalar Scalar;  ///< Convenience typedef for the Map's TransformScalar
  typedef typename MapType::KeyType KeyType;  ///< Convenience typedef for the Map's KeyType
  typedef typename MapType::VoxelNodeType VoxelNodeType;  ///< Convenience typedef for the Map's VoxelNodeType
  typedef typename
      boost::mpl::if_c<
        std::is_const<MapType>::value,
        const VoxelNodeType*,
        VoxelNodeType*
      >::type VoxelNodePtrType; ///< Return PtrType to a VoxelNode (constified for const MapTypes)
  typedef std::tuple<bool, KeyType, VoxelNodePtrType> ReturnType;  ///< ReturnType of RayCasting operation

  /**@brief Functor Constructor
   *
   * @param map
   * @param free_prob_thresh  occupancy threshold (defaults to 0.5)
   */
  RayCasting(MapType *const map, const Scalar free_prob_thresh = Scalar(0.5))
    : map_(map), occupancy_checker_(free_prob_thresh) {
  }

  /**@brief Functor operator() with a max range
   *
   * @param[in] ray_start               coordinate of ray origin
   * @param[in] ray_direction           ray direction (needs not be normalized)
   * @param[in] max_range               maximum range for ray. (should be +ve)
   *
   * @return tuple of boolean indicator for a successful solid voxel hit, Key of the last voxel and pointer to VoxelNode
   */
  template<class EigenPointTypeA, class EigenPointTypeB>
  ReturnType operator()(const EigenPointTypeA& ray_start,
                        const EigenPointTypeB& ray_direction,
                        const Scalar max_range) const;

  /**@brief Functor operator() with no max range checking
   *
   * @param[in] ray_start               coordinate of ray origin
   * @param[in] ray_direction           ray direction(need not be normalized)
   *
   * @return tuple of boolean indicator for a successful solid voxel hit, Key of the last voxel and pointer to VoxelNode
   */
  template<class EigenPointTypeA, class EigenPointTypeB>
  ReturnType operator()(const EigenPointTypeA& ray_start,
                        const EigenPointTypeB& ray_direction) const;


private:
  MapType *const map_;
  const IsNodeOccupied<VoxelNodeType> occupancy_checker_;
};

} // end namespace Naksha

#include "Naksha/Algorithms/RayCasting-impl.hpp"

#endif // NAKSHA_ALGORITHMS_RAYCASTING_H_
