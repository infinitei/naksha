/**
 * @file InsertMeasurements.h
 * @brief InsertMeasurements
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_ALGORITHMS_INSERT_MEASUREMENTS_H_
#define NAKSHA_ALGORITHMS_INSERT_MEASUREMENTS_H_

#include <map>
#include <vector>
#include "Naksha/Payload/MeasurementModel.h"

namespace Naksha {

/**@brief Integrates measurements from a depth scan
 *
 * This methods updates a map along a set of rays originating from sensor origin with
 * MISS measurements and user specified measurement at the voxels corresponding
 * to the observed depth (point cloud).
 *
 * This method additionally gives preference to HIT measurement compared to MISS measurement.
 * This prevents free voxels from eating up occupied voxels specially when the rays have
 * lesser angle of attack with surface.
 *
 * @param[in,out] map         some map to be updated
 * @param[in] sensor_origin   global coordinate of the sensor origin (e.g. camera center)
 * @param[in] point_cloud     global point coordinates of ray ends
 * @param[in] measurement     measurement to be integrated at voxels corresponding to point_cloud
 *
 *
 * Example Usage:
 * @code
 *  MapType map;  // MapType is your favorite Map
 *
 *  // Define your PointCloud type. All the following PointCloud types works.
 *  typedef std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Vector3d> > PointCloud;
 *  typedef Eigen::Matrix< float, 3, Eigen::Dynamic, Eigen::RowMajor> PointCloud;
 *
 *  // Fill out the camera center and point cloud
 *  Eigen::Vector3d camera_center = .... ;
 *  PointCloud point_cloud = ...;
 *
 *  // integrate the Depth Scan to the map
 *  insertDepthScan(map, camera_center, point_cloud, MeasurementModelType::HIT);
 * @endcode
 *
 * @ingroup Algorithms
 */
template<class MapType, class EigenPointType, class PointCloudType, class MeasurementType>
void insertDepthScan(MapType& map, const EigenPointType& sensor_origin,
                     const PointCloudType& point_cloud,
                     const MeasurementType& measurement);

/**@brief Integrates HIT measurements from a depth scan
 *
 * This methods updates a map along a set of rays originating from sensor origin with
 * MISS measurements and user specified measurement at the voxels corresponding
 * to the observed depth (point cloud).
 *
 * This method additionally gives preference to HIT measurement compared to MISS measurement.
 * This prevents free voxels from eating up occupied voxels specially when the rays have
 * lesser angle of attack with surface.
 *
 * @param[in,out] map         some map to be updated
 * @param[in] sensor_origin   global coordinate of the sensor origin (e.g. camera center)
 * @param[in] point_cloud     global point coordinates of ray ends
 *
 *
 * Example Usage:
 * @code
 *  MapType map;  // MapType is your favorite Map
 *
 *  // Define your PointCloud type. All the following PointCloud types works.
 *  typedef std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Vector3d> > PointCloud;
 *  typedef Eigen::Matrix< float, 3, Eigen::Dynamic, Eigen::RowMajor> PointCloud;
 *
 *  // Fill out the camera center and point cloud
 *  Eigen::Vector3d camera_center = .... ;
 *  PointCloud point_cloud = ...;
 *
 *  // integrate the Depth Scan to the map
 *  insertDepthScan(map, camera_center, point_cloud);
 * @endcode
 *
 * @ingroup Algorithms
 */
template<class MapType, class EigenPointType, class PointCloudType>
void insertDepthScan(MapType& map, const EigenPointType& sensor_origin,
                     const PointCloudType& point_cloud) {
  typedef MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;
  insertDepthScan(map, sensor_origin, point_cloud, MeasurementModelType::HIT);
}



/**@brief Integrates measurements from a depth scan with different measurement models for each ray
 *
 * This methods updates a map along a set of rays originating from sensor origin with
 * MISS measurements and user specified set of measurements at the voxels corresponding
 * to the observed depth (point cloud).
 *
 * @param[in,out] map           some map to be updated
 * @param[in] sensor_origin     global coordinate of the sensor origin (e.g. camera center)
 * @param[in] point_cloud       global point coordinates of the ray ends
 * @param[in] measurement_ids   vector of measurement indexes to be integrated at the voxel corresponding to each point in point_cloud
 * @param[in] measurement_map   a map from measurement_id to actual measurement
 *
 * @note point_cloud.size() == measurement_ids.size() i.e. # of measurements should be same as # of points with depth
 *
 * @ingroup Algorithms
 */
template<class MapType, class EigenPointType, class PointCloudType, class MeasurementIndex, class MeasurementType>
void insertDepthScan(MapType& map,
                     const EigenPointType& sensor_origin,
                     const PointCloudType& point_cloud,
                     const std::vector<MeasurementIndex>& measurement_ids,
                     const std::map<MeasurementIndex, MeasurementType> measurement_map);


/**@brief Integrates specific measurement along rays from a single sensor location
 *
 * This methods updates a map along a set of bounded rays with the user specified measurement.
 * The rays and their extent is determined from the input parameters sensor_origin and point_cloud
 *
 * @param[in,out] map         some map to be updated
 * @param[in] sensor_origin   global coordinate of the sensor origin (e.g. camera center)
 * @param[in] point_cloud     global point coordinates
 * @param[in] measurement     measurement to be integrated at voxels corresponding to point_cloud
 *
 * @ingroup Algorithms
 */
template<class MapType, class EigenPointType, class PointCloudType, class MeasurementType>
void insertMeasurementsAlongRays(MapType& map, const EigenPointType& sensor_origin,
                     const PointCloudType& point_cloud,
                     const MeasurementType& measurement);


/**@brief Simply Insert a point cloud into the map (No ray tracing etc.)
 *
 * This method simply tries to create a new voxel at each point of the point cloud
 *
 * @param[in,out] map       some map to be updated
 * @param[in] point_cloud   global point coordinates
 */
template<class MapType, class PointCloudType>
void insertPointCloud(MapType& map, const PointCloudType& point_cloud);

}  // namespace Naksha


#include "Naksha/Algorithms/InsertMeasurements-impl.hpp"

#endif // NAKSHA_ALGORITHMS_INSERT_MEASUREMENTS_H_
