/**
 * @file RayTracing.h
 * @brief RayTracing
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_RAY_TRACING_H_
#define NAKSHA_RAY_TRACING_H_

#include <vector>
#include <Eigen/Core>

namespace Naksha {

/**@brief Compute the keys lying on a 3D Ray
 *
 * This computes all the voxel lying on a ray from begin to end (excluding)
 *
 * This implements "A Faster Voxel Traversal Algorithm for Ray Tracing"
 * by John Amanatides and Andrew Woo, 1987
 *
 * @param[in] map           some \ref Maps type
 * @param[in] ray_start     coordinate of ray origin
 * @param[in] ray_end       coordinate of ray end
 * @param[out] ray_keys     Keys contained in the ray from origin till end i.e. [origin, end)
 * @return true if successful
 *
 * @note the voxel containing end is not included in the output ray_keys
 *
 * @ingroup Algorithms
 */
template<class MapType, class EigenPointTypeA, class EigenPointTypeB>
bool computeKeysOnRay(
    const MapType& map,
    const EigenPointTypeA& ray_start,
    const EigenPointTypeB& ray_end,
    std::vector<typename MapType::KeyType>& ray_keys);


/**@brief Compute the keys lying on a 3D Ray from discrete coordinates
 *
 * This computes all the voxel lying on a ray from start_key to end_key (excluding)
 *
 * @param[in]  map                    some \ref Maps type
 * @param[in]  start_key              discrete key of ray origin
 * @param[in]  end_key                discrete key of ray end
 * @param[in]  start_coord            coordinate of ray origin
 * @param[in]  normalized_direction   normalized direction vector
 * @param[out] ray_keys               Keys contained in the ray from origin till end i.e. [origin, end)
 *
 * @note the voxel containing end_key is not included in the output ray_keys
 *
 * @ingroup Algorithms
 */
template<class MapType, class EigenPointType, class DirectionType>
void computeKeysOnRay(
    const MapType& map,
    const typename MapType::KeyType& start_key,
    const typename MapType::KeyType& end_key,
    const EigenPointType& start_coord,
    const DirectionType& normalized_direction,
    std::vector<typename MapType::KeyType>& ray_keys);


/**@brief Compute the keys lying on a 3D Ray from discrete coordinates
 *
 * This computes all the voxel lying on a ray from start_key to end_key (excluding)
 *
 * @param[in]  key_coord_transformer  functor for key/coord conversion e.g. \ref KeyCoordTransformer
 * @param[in]  start_key              discrete key of ray origin
 * @param[in]  end_key                discrete key of ray end
 * @param[in]  start_coord            coordinate of ray origin
 * @param[in]  normalized_direction   normalized direction vector
 * @param[out] ray_keys               Keys contained in the ray from origin till end i.e. [origin, end)
 *
 * @note the voxel containing end_key is not included in the output ray_keys
 *
 * @ingroup Algorithms
 */
template<class KeyCordTransformer, class EigenPointType, class DirectionType>
void computeKeysOnRayWithKeyCordTransformer(
    const KeyCordTransformer& key_coord_transformer,
    const typename KeyCordTransformer::KeyType& start_key,
    const typename KeyCordTransformer::KeyType& end_key,
    const EigenPointType& start_coord,
    const DirectionType& normalized_direction,
    std::vector<typename KeyCordTransformer::KeyType>& ray_keys);



/**@brief Compute the Hit and Miss keys from a depth scan
 *
 * @param[in] map             some \ref Maps type
 * @param[in] sensor_origin   coordinate of sensor_origin
 * @param[in] point_cloud     PointCloud of the depth scan from sensor_origin
 * @return Hit and Miss keys
 *
 * @tparam HitKeysContainer   should be container of KeyType e.g. std::vector<KeyType>
 * @tparam MissKeysContainer  should be container of KeyType e.g. std::unordered_set<KeyType>
 *
 * Example Usage:
 * @code
 *  typedef MapType::KeyType KeyType;
 *  typedef std::vector<KeyType> KeyVector;
 *  typedef std::unordered_set<KeyType> KeySet;
 *
 *  // compute hit keys and miss keys
 *  KeyVector hit_keys;
 *  KeySet miss_keys;
 *  std::tie(hit_keys, miss_keys) = computeKeysFromDepthScan<KeyVector, KeySet>(map, sensor_origin, point_cloud);
 * @endcode
 *
 * @ingroup Algorithms
 */
template<class HitKeysContainer, class MissKeysContainer, class MapType,
    class EigenPointType, class PointCloudType>
std::pair<HitKeysContainer, MissKeysContainer>
computeKeysFromDepthScan(const MapType& map, const EigenPointType& sensor_origin,
                         const PointCloudType& point_cloud);

} // end namespace Naksha

#include "Naksha/Algorithms/RayTracing-impl.hpp"

#endif // NAKSHA_RAY_TRACING_H_
