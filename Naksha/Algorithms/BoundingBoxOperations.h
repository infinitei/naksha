/**
 * @file BoundingBoxOperations.h
 * @brief BoundingBoxOperations
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_BOUNDING_BOX_OPERATIONS_H_
#define NAKSHA_BOUNDING_BOX_OPERATIONS_H_

#include "Naksha/Algorithms/EraseIf.h"
#include <Eigen/Geometry>
#include <functional>

namespace Naksha {

/**@brief Predicate for checking if a key is inside some Bounding Box
 *
 * See \ref eraseVoxelsOutsideBbx and \ref eraseVoxelsInsideBbx
 * for example usage.
 *
 * @ingroup Algorithms
 */
template <typename KeyType>
struct IsKeyInsideBoundingBox : std::unary_function<KeyType, bool> {
  typedef typename KeyType::Scalar KeyScalar;
  typedef Eigen::AlignedBox<KeyScalar, KeyType::Dimension> KeyBoundingBox;

  /// Constructor from discrete keys corresponding to BBX extremes
  IsKeyInsideBoundingBox(const KeyType& min_key, const KeyType& max_key)
   : key_bbx(min_key.value, max_key.value) {
  }

  /**@brief Primary predicate operator
   *
   * @param key - discrete Key
   * @return true if key is inside the key_bbx
   */
  bool operator() (const KeyType& key) const {
    return key_bbx.contains(key.value);
  }

  KeyBoundingBox key_bbx;
};


/**@brief Culls a map with a provided bounding box
 *
 * This removes all voxels lying outside the input bounding box.
 *
 * @param[in,out] map - Naksha Map
 * @param[in] bbx_min - bounding box min point
 * @param[in] bbx_max - bounding box max point
 *
 * This is opposite to \ref eraseVoxelsInsideBbx.
 * See \ref EraseIf for a more genric erase functionality.
 *
 * @ingroup Algorithms
 */
template <class MapType, typename PointType>
void eraseVoxelsOutsideBbx(MapType& map, const PointType& bbx_min, const PointType& bbx_max) {
  typedef typename MapType::KeyType KeyType;
  IsKeyInsideBoundingBox<KeyType> predicate(map.keyFromCoord(bbx_min), map.keyFromCoord(bbx_max));
  EraseIfKey(map, std::not1(predicate));
}

/**@brief Erase all Voxels Inside the input bounding box.
 *
 * This is opposite to \ref eraseVoxelsOutsideBbx.
 *
 * @param[in,out] map - Naksha Map
 * @param[in] bbx_min - bounding box min point
 * @param[in] bbx_max - bounding box max point
 *
 * See \ref EraseIf for a more genric erase functionality.
 *
 * @ingroup Algorithms
 */
template <class MapType, typename PointType>
void eraseVoxelsInsideBbx(MapType& map, const PointType& bbx_min, const PointType& bbx_max) {
  typedef typename MapType::KeyType KeyType;
  IsKeyInsideBoundingBox<KeyType> functor(map.keyFromCoord(bbx_min), map.keyFromCoord(bbx_max));
  EraseIfKey(map, functor);
}



}  // namespace Naksha

#endif // end NAKSHA_BOUNDING_BOX_OPERATIONS_H_
