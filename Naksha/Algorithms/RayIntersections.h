/**
 * @file RayIntersections.h
 * @brief RayIntersections
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_RAY_BOUNDING_BOX_INTERSECTION_H_
#define NAKSHA_RAY_BOUNDING_BOX_INTERSECTION_H_

#include "Naksha/Common/PointCloudTraits.h"
#include <boost/assert.hpp>
#include <type_traits>

namespace Naksha {

/**
 * Functor for computing intersection of a Axis Aligned Bounding Box and a ray
 * originating from inside the bounding box.
 *
 * @tparam _Scalar    - Scalar type like float or double
 * @tparam _Dimension - Dimension which can be 2 or 3
 *
 * Usage Example:
 * @code
 *  typedef RayAABBIntersection<double, 3> RayBbxIntersectionFunctor;
 *
 *  // Construct from bbx_min, bbx_max, ray_origin
 *  // ray_origin should be inside the bounding box
 *  RayBbxIntersectionFunctor functor(Vector3d::Constant(-1000.0), Vector3d::Constant(1000.0), Vector3d::Zero());
 *
 *  //If we have Eigen::AlignedBox kind of BoundIngBox, we can also use that to
 *  //construct our functor
 *  AlignedBox3d bbx(Vector3d::Constant(-1000.0), Vector3d::Constant(1000.0));
 *  RayBbxIntersectionFunctor functor(bbx, Vector3d::Zero());
 *
 *  // We can call the functor on single ray
 *  Vector3d intersection = functor(Vector3d::(1,0,0));
 *
 *  // We can call the functor on multiple rays also
 *  Matrix<double, 3, Dynamic> rays(3, 10000);  // 10,000 rays
 *  // Fill rays
 *  ....
 *  Matrix<double, 3, Dynamic> intersections = functor(rays);
 *
 *  //We can use the functor with STL alogorithms like std::transform
 *  //So if we have a vector of 3D rays, we can do:
 *  std::vector<Vector3d> rays;
 *  // Fill rays
 *  ....
 *  std::transform(rays.begin(), rays.end(), rays.begin(), functor);
 *  // rays will now store the intersection ponts
 * @endcode
 *
 * @ingroup Algorithms
 */
template<typename _Scalar, int _Dimension>
class RayAABBIntersection {
 public:
  typedef _Scalar Scalar; ///< Scalar type float or diuble
  static const int Dimension = _Dimension;  ///< Dimension (2 or 3)
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType; ///< VectorType

  /**@brief construct from bbx extremities and ray origin
   *
   * @param bbx_min     - coordinate of bbx minimum
   * @param bbx_max     - coordinate of bbx maximum
   * @param ray_origin  - coordinate of ray origin
   */
  template<class BbxMinVectorType, class BbxMaxVectorType, class RayOriginVectorType>
  RayAABBIntersection(const BbxMinVectorType& bbx_min, const BbxMaxVectorType& bbx_max,
                      const RayOriginVectorType& ray_origin)
      : ray_origin_(ray_origin),
        bbx_min_to_ray_origin_(bbx_min - ray_origin),
        bbx_max_to_ray_origin_(bbx_max - ray_origin) {
    BOOST_ASSERT_MSG((bbx_min_to_ray_origin_.array() <= 0).all(), "Ray origin is outside Bounding Box");
    BOOST_ASSERT_MSG((bbx_max_to_ray_origin_.array() >= 0).all(), "Ray origin is outside Bounding Box");
  }

  /**@brief construct from a BoundingBox and ray origin
   *
   * @param bbx         - bounding box like Eigen::AlignedBox
   * @param ray_origin  - coordinate of ray origin
   */
  template<class BoundingBoxType, class RayOriginVectorType>
  RayAABBIntersection(const BoundingBoxType& bbx, const RayOriginVectorType& ray_origin)
      : ray_origin_(ray_origin),
        bbx_min_to_ray_origin_(bbx.min() - ray_origin),
        bbx_max_to_ray_origin_(bbx.max() - ray_origin) {
    BOOST_ASSERT_MSG(bbx.contains(ray_origin), "Ray origin is outside Bounding Box");
  }

  /**@brief primary functor operator
   *
   * @param rays - ray direction (a single ray or multiple rays)
   * @return  point of intersection(s) of ray(s) with bbx
   */
  template<class EigenPointCloudType>
  typename EigenPointCloudType::PlainObject operator()(const EigenPointCloudType& rays) const {
    return impl(rays, typename EigenPointCloudTraits<Dimension, EigenPointCloudType>::type());
  }


 private:
  template<class PointCloud>
  VectorType impl(const PointCloud& ray, SingleEigenFixedSizePointTag) const {
    static_assert(
        PointCloud::SizeAtCompileTime == Dimension,
        "Dimension of ray is wrong");
    // TODO The following assumption can probably be relaxed
    static_assert(
        (std::is_same<typename PointCloud::Scalar, Scalar>::value),
        "Assumption is that the RayDirectionType::Scalar == Scalar");

    BOOST_ASSERT_MSG(std::abs(ray.norm() - 1) <= std::numeric_limits<typename PointCloud::Scalar>::epsilon(),
                       "ray_direction is not normalized");

    const VectorType ray_inv = ray.cwiseInverse();
    const VectorType tmin = bbx_min_to_ray_origin_.cwiseProduct(ray_inv);
    const VectorType tmax = bbx_max_to_ray_origin_.cwiseProduct(ray_inv);
    return ray_origin_ + ray * tmin.cwiseMax(tmax).minCoeff();
  }

  template<class PointCloud>
  PointCloud impl(const PointCloud& rays, EigenPointCloudTag) const {
    // TODO The following assumption can probably be relaxed
    static_assert(
        (std::is_same<typename PointCloud::Scalar, Scalar>::value),
        "Assumption is that the PointCloud::Scalar == Scalar");

    PointCloud intersections(rays.rows(), rays.cols());
    for(typename PointCloud::Index i = 0; i < rays.cols(); ++i) {
      const VectorType& ray = rays.col(i);
      const VectorType ray_inv = ray.cwiseInverse();
      const VectorType tmin = bbx_min_to_ray_origin_.cwiseProduct(ray_inv);
      const VectorType tmax = bbx_max_to_ray_origin_.cwiseProduct(ray_inv);
      intersections.col(i) = ray_origin_ + ray * tmin.cwiseMax(tmax).minCoeff();
    }
    return intersections;
  }

  VectorType ray_origin_;
  VectorType bbx_min_to_ray_origin_;
  VectorType bbx_max_to_ray_origin_;
};




/**@brief Functor for finding key at intesection of ray and Map boundary
 *
 * This is similar to \ref RayAABBIntersection, but instead returns
 * a discrete key instead of coordinate of the intersection point.
 *
 * @ingroup Algorithms
 */
template<class MapType>
class RayMapBoundaryIntersection {
  typedef typename MapType::KeyCoordTransformerType KeyCoordTransformerType;
  typedef RayAABBIntersection<typename KeyCoordTransformerType::Scalar, MapType::Dimension> RayBbxFunctor;
 public:
  typedef typename MapType::KeyType KeyType;

  /**@brief construct from a map
   *
   * @param map         - some \ref Maps type
   * @param ray_origin  - ray origin coordinates (should be inside map's Maximal bbx)
   */
  template<class RayOriginVectorType>
  RayMapBoundaryIntersection(const MapType& map, const RayOriginVectorType& ray_origin)
   : ray_bbx_functor_(map.maximumBbx(), ray_origin),
     key_coord_transformer_(map.keyCoordTransformer()) {
  }

  /**
   * Returns Key at which a ray from inside the map hits the map's Maximal Bounding Box
   *
   * @param ray - ray direction vector
   * @return key corresponding intersection of the ray and map's Maximal Bounding Box
   */
  template<class RayDirectionType>
  KeyType operator()(const RayDirectionType& ray) const {
    return key_coord_transformer_(ray_bbx_functor_(ray));
  }

 private:
  RayBbxFunctor ray_bbx_functor_;
  KeyCoordTransformerType key_coord_transformer_;
};


/// This is an inplace version of \ref RayAABBIntersection
/// TODO Remove this method and make RayAABBIntersection itself inplace aware
template<typename _Scalar, int _Dimension>
class RayAABBIntersectionInPlace {
 public:
  typedef _Scalar Scalar; ///< Scalar type float or diuble
  static const int Dimension = _Dimension;  ///< Dimension (2 or 3)
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType; ///< VectorType

  /**@brief construct from bbx extremities and ray origin
   *
   * @param bbx_min     - coordinate of bbx minimum
   * @param bbx_max     - coordinate of bbx maximum
   * @param ray_origin  - coordinate of ray origin
   */
  template<class BbxMinVectorType, class BbxMaxVectorType, class RayOriginVectorType>
  RayAABBIntersectionInPlace(const BbxMinVectorType& bbx_min, const BbxMaxVectorType& bbx_max,
                      const RayOriginVectorType& ray_origin)
      : ray_origin_(ray_origin),
        bbx_min_to_ray_origin_(bbx_min - ray_origin),
        bbx_max_to_ray_origin_(bbx_max - ray_origin) {
    BOOST_ASSERT_MSG((bbx_min_to_ray_origin_.array() <= 0).all(), "Ray origin is outside Bounding Box");
    BOOST_ASSERT_MSG((bbx_max_to_ray_origin_.array() >= 0).all(), "Ray origin is outside Bounding Box");
  }

  /**@brief construct from a BoundingBox and ray origin
   *
   * @param bbx         - bounding box like Eigen::AlignedBox
   * @param ray_origin  - coordinate of ray origin
   */
  template<class BoundingBoxType, class RayOriginVectorType>
  RayAABBIntersectionInPlace(const BoundingBoxType& bbx, const RayOriginVectorType& ray_origin)
      : ray_origin_(ray_origin),
        bbx_min_to_ray_origin_(bbx.min() - ray_origin),
        bbx_max_to_ray_origin_(bbx.max() - ray_origin) {
    BOOST_ASSERT_MSG(bbx.contains(ray_origin), "Ray origin is outside Bounding Box");
  }

  /**@brief primary functor operator
   *
   * @param rays - ray direction (a single ray or multiple rays)
   * @return  point of intersection(s) of ray(s) with bbx
   */
  template<class EigenPointCloudType>
  void operator()(EigenPointCloudType& rays) const {
    // TODO The following assumption can probably be relaxed
    static_assert(
        (std::is_same<typename EigenPointCloudType::Scalar, Scalar>::value),
        "Assumption is that the PointCloud::Scalar == Scalar");

    for(typename EigenPointCloudType::Index i = 0; i < rays.cols(); ++i) {
      const VectorType ray_inv = rays.col(i).cwiseInverse();
      const VectorType tmin = bbx_min_to_ray_origin_.cwiseProduct(ray_inv);
      const VectorType tmax = bbx_max_to_ray_origin_.cwiseProduct(ray_inv);
      rays.col(i) = ray_origin_ + rays.col(i) * tmin.cwiseMax(tmax).minCoeff();
    }
  }


 private:
  VectorType ray_origin_;
  VectorType bbx_min_to_ray_origin_;
  VectorType bbx_max_to_ray_origin_;
};



}  // namespace Naksha

#endif // end NAKSHA_RAY_BOUNDING_BOX_INTERSECTION_H_
