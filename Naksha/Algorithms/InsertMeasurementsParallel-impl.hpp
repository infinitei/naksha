/**
 * @file InsertMeasurementsParallel-impl.hpp
 * @brief InsertMeasurementsParallel-impl
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_ALGORITHMS_INSERT_MEASUREMENTS_PRALLEL_IMPL_HPP_
#define NAKSHA_ALGORITHMS_INSERT_MEASUREMENTS_PRALLEL_IMPL_HPP_

#include "Naksha/Algorithms/RayTracing.h"
#include "Naksha/Keys/KeyHash.h"
#include <boost/assert.hpp>
#include <unordered_set>
#include <tuple>
#include "tbb/concurrent_unordered_set.h"
#include "tbb/parallel_for_each.h"
#include "tbb/parallel_for.h"

namespace Naksha {

template<class MapType, class PointType, class PointCloudType, class MeasurementType>
void insertDepthScanParallel(MapType& map, const PointType& sensor_origin,
                             const PointCloudType& point_cloud,
                             const MeasurementType& measurement) {
  typedef typename MapType::KeyType KeyType;
  typedef std::vector<KeyType> KeyVector;
  typedef KeyHash KeyHashType;
  typedef std::unordered_set<KeyType, KeyHashType> KeySet;

  typedef MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;

  // compute hit keys and miss keys
  KeyVector hit_keys;
  KeyVector miss_keys;
  std::tie(hit_keys, miss_keys) = computeKeysFromDepthScan<KeyVector, KeyVector>(
      map, sensor_origin, point_cloud);

  // Generate a set of unique keys
  KeySet unique_miss_keys(miss_keys.begin(), miss_keys.end());

  // Remove miss_keys also present in hit_keys
  for(const KeyType& key : hit_keys) {
    unique_miss_keys.erase(key);
  }

  // Now integrate MISS for miss_voxels
  tbb::parallel_for_each(unique_miss_keys.begin(), unique_miss_keys.end(),
    [&](const KeyType& key) {
    map.updateVoxelLazy(key, MeasurementModelType::MISS);
    }
  );

  // Now integrate user specified measurement for hit_voxels
  tbb::parallel_for_each(hit_keys.begin(), hit_keys.end(),
    [&](const KeyType& key) {
    map.updateVoxelLazy(key, measurement);
    }
  );
}

template<class MapType, class PointType, class PointCloudType, class MeasurementIndex, class MeasurementType>
void insertDepthScanParallel(MapType& map, const PointType& sensor_origin,
                             const PointCloudType& point_cloud,
                             const std::vector<MeasurementIndex>& measurement_ids,
                             const std::map<MeasurementIndex, MeasurementType> measurement_map) {

  typedef typename MapType::KeyType KeyType;
  typedef std::vector<KeyType> KeyVector;
  typedef std::pair<bool, KeyType> BoolKeyPairType;
  typedef std::vector<BoolKeyPairType> BoolKeyPairVector;
  typedef KeyHash KeyHashType;
  typedef std::unordered_set<KeyType, KeyHashType> KeySet;

  typedef MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;

  BOOST_ASSERT_MSG( point_cloud.size() == measurement_ids.size(), "# of measurements should be same as # of points with depth");

  // compute hit keys and miss keys
  BoolKeyPairVector hit_keys;
  KeyVector miss_keys;
  std::tie(hit_keys, miss_keys) = computeKeysFromDepthScan<BoolKeyPairVector, KeyVector>(
      map, sensor_origin, point_cloud);

  // Generate a set of unique keys
  KeySet unique_miss_keys(miss_keys.begin(), miss_keys.end());

  // Remove miss_keys also present in hit_keys
  for(const BoolKeyPairType& bool_key_pair : hit_keys) {
    unique_miss_keys.erase(bool_key_pair.second);
  }

  //TODO Since we are probably searching for keys sequentially, we can do better than re-initializing the search

  // Now integrate MISS for miss_voxels
  tbb::parallel_for_each(unique_miss_keys.begin(), unique_miss_keys.end(), [&](const KeyType& key) {
    map.updateVoxelLazy(key, MeasurementModelType::MISS);
  });

  // Now integrate user specified measurements for hit_voxels
  tbb::parallel_for(
      size_t(0),
      hit_keys.size(),
      size_t(1),
      [&](size_t i) {
        if(hit_keys[i].first)
          map.updateVoxelLazy(hit_keys[i].second, measurement_map.at(measurement_ids[i]));
      });

}


template<class MapType, class EigenPointType, class PointCloudType, class MeasurementType>
void insertMeasurementsAlongRaysParallel(MapType& map, const EigenPointType& sensor_origin,
                                         const PointCloudType& point_cloud,
                                         const MeasurementType& measurement) {
  typedef typename MapType::KeyType KeyType;
  typedef std::vector<KeyType> KeyVector;

  // compute hit keys and miss keys
  KeyVector hit_keys;
  KeyVector miss_keys;
  std::tie(hit_keys, miss_keys) = computeKeysFromDepthScan<KeyVector, KeyVector>(map, sensor_origin, point_cloud);

  //TODO Since we are probably searching for keys sequentially, we can do better than re-initializing the search
  // Now integrate MISS for miss_voxels
  tbb::parallel_for_each(miss_keys.begin(), miss_keys.end(),
                         [&](const KeyType& key) {
                           map.updateVoxelLazy(key, measurement);
                         });

  // Now integrate user specified measurement for hit_voxels
  tbb::parallel_for_each(hit_keys.begin(), hit_keys.end(),
                         [&](const KeyType& key) {
                           map.updateVoxelLazy(key, measurement);
                         });

}


template<class MapType, class PointCloudType>
void insertPointCloudParallel(MapType& map, const PointCloudType& point_cloud) {
  typedef typename MapType::KeyType KeyType;
  typedef typename PointCloudType::value_type PointType;

  tbb::parallel_for_each(point_cloud.begin(), point_cloud.end(),
                         [&](const PointType& point) {
                           if (map.insideMaximumBbx(point)) {
                             KeyType point_key = map.keyFromCoord(point);
                             map.insertNewVoxel(point_key);
                           }
                         });
}


}  // namespace Naksha


#endif // NAKSHA_ALGORITHMS_INSERT_MEASUREMENTS_PRALLEL_IMPL_HPP_
