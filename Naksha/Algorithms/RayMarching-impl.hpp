/**
 * @file RayMarching-impl.hpp
 * @brief RayMarching-impl
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_ALGORITHMS_RAYMARCHING_IMPL_HPP_
#define NAKSHA_ALGORITHMS_RAYMARCHING_IMPL_HPP_

#include <limits>
#include <cassert>

namespace Naksha {

template<class KeyType, typename Scalar>
template<typename AnyPointType>
RayMarching<KeyType, Scalar>::RayMarching(const KeyType& start_key,
    const AnyPointType& start_coord, const PointType start_voxel_center,
    const PointType direction, const Scalar voxel_size)
    : current_key_(start_key),
      direction_(direction),
      step_(DiscretePointType::Zero()),
      tMax_(PointType::Constant(std::numeric_limits<Scalar>::max())),
      tDelta_(PointType::Constant(std::numeric_limits<Scalar>::max())) {

  // assert that direction is already normalized
  assert(std::abs(direction_.norm() - 1) <= std::numeric_limits<Scalar>::epsilon());

  PointType voxel_corner = start_voxel_center;
  const Scalar half_voxel_legth = voxel_size / 2;

  for (int i = 0; i < KeyType::Dimension; ++i) {
    // compute step direction
    if (direction_[i] > 0.0) {
      step_[i] = 1;
      voxel_corner[i] += half_voxel_legth;
    }
    else if (direction_[i] < 0.0) {
      step_[i] = -1;
      voxel_corner[i] -= half_voxel_legth;
    }
  }

  for (int i = 0; i < KeyType::Dimension; ++i) {
    // compute tMax, tDelta
    if (step_[i] != 0) {
      tMax_[i] = (voxel_corner[i] - start_coord[i]) / direction[i];
      tDelta_[i] = voxel_size / std::abs(direction[i]);
    }
  }
}


template<class KeyType, typename Scalar>
inline void RayMarching<KeyType, Scalar>::traverse() {
  // Find the minimum coeff index in tMax_
  int min_dir;
  tMax_.minCoeff(&min_dir);

  // Move in direction set in min_dir
  current_key_[min_dir] += step_[min_dir];
  tMax_[min_dir] += tDelta_[min_dir];
}

}  // namespace Naksha

#endif // NAKSHA_ALGORITHMS_RAYMARCHING_IMPL_HPP_
