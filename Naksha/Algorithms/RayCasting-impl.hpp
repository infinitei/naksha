/**
 * @file RayCasting-impl.hpp
 * @brief RayCasting Implementation
 *
 * @author Abhijit Kundu
 */

#include <iostream>

namespace Naksha {

template<class MapType, bool IgnroreUnknownVoxels>
template<class EigenPointTypeA, class EigenPointTypeB>
typename RayCasting<MapType, IgnroreUnknownVoxels>::ReturnType
RayCasting<MapType, IgnroreUnknownVoxels>::operator ()(
    const EigenPointTypeA& start,
    const EigenPointTypeB& direction,
    const Scalar max_range) const {
  typedef RayMarching<KeyType, Scalar> RayMarcher;
  typedef typename RayMarcher::PointType PointType;

  KeyType start_key;
  if (!map_->keyFromCoord(start, start_key) ) {
    const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
    std::cout << "[computeRayCasting] ERROR: coordinate " << start.format(vecfmt) << " is out of bounds\n";
    return ReturnType(false, start_key, 0);
  }

  VoxelNodePtrType start_node = map_->search(start_key);
  if(start_node) {
    if(occupancy_checker_(*start_node)) {
      return ReturnType(true, start_key, start_node);
    }
  }
  else if(!IgnroreUnknownVoxels) {
    const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
    std::cout << "[computeRayCasting] Warning: Start voxel at " << start.format(vecfmt) << " is unknown.\n";
    return ReturnType(true, start_key, start_node);
  }

  //-----------Initialization Phase of [Amanatides and  Woo, 1987]-------------//
  RayMarcher ray_marcher(
        start_key, start,
        map_->template voxelCenter<PointType>(start_key),
        direction.normalized(),
        map_->resolution());

  // Assert that Max Range is positive
  assert(max_range > 0);
  Scalar max_range_squared = max_range * max_range;

  //-------------Traversal Phase of [Amanatides and  Woo, 1987]--------------//

  //primary traversal loop
  while (true) {

    // Traverse One Step
    ray_marcher.traverse();

    const KeyType& current_key = ray_marcher.currentkey();

    // TODO This search can be aided and improved by hint from previous search??
    VoxelNodePtrType current_node = map_->search(current_key);

    // Check if current key is within maximal bbx limit of the map
    if (map_->outsideMaximumBbx(current_key))
      return ReturnType(false, current_key, current_node);

    // Check for max range
    Scalar current_range_squared = (map_->template voxelCenter<PointType>(current_key) - start.template cast<Scalar>()).squaredNorm();
    if (current_range_squared > max_range_squared)
      return ReturnType(false, current_key, current_node);

    if (current_node) {
      // Check for occupancy
      if (occupancy_checker_(*current_node))
        return ReturnType(true, current_key, current_node);
    }
    else if (!IgnroreUnknownVoxels) {
      return ReturnType(false, current_key, current_node);
    }
  } // end primary traversal loop
}


template<class MapType, bool IgnroreUnknownVoxels>
template<class EigenPointTypeA, class EigenPointTypeB>
typename RayCasting<MapType, IgnroreUnknownVoxels>::ReturnType
RayCasting<MapType, IgnroreUnknownVoxels>::operator ()(
    const EigenPointTypeA& start,
    const EigenPointTypeB& direction) const {
  typedef RayMarching<KeyType, Scalar> RayMarcher;
  typedef typename RayMarcher::PointType PointType;

  KeyType start_key;
  if (!map_->keyFromCoord(start, start_key) ) {
    const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
    std::cout << "[computeRayCasting] ERROR: coordinate " << start.format(vecfmt) << " is out of bounds\n";
    return ReturnType(false, start_key, 0);
  }

  VoxelNodePtrType start_node = map_->search(start_key);
  if(start_node) {
    if(occupancy_checker_(*start_node)) {
      return ReturnType(true, start_key, start_node);
    }
  }
  else if(!IgnroreUnknownVoxels) {
    const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
    std::cout << "[computeRayCasting] Warning: Start voxel at " << start.format(vecfmt) << " is unknown.\n";
    return ReturnType(true, start_key, start_node);
  }

  //-----------Initialization Phase of [Amanatides and  Woo, 1987]-------------//
  RayMarcher ray_marcher(
        start_key, start,
        map_->template voxelCenter<PointType>(start_key),
        direction.normalized(),
        map_->resolution());

  //-------------Traversal Phase of [Amanatides and  Woo, 1987]--------------//

  //primary traversal loop
  while (true) {

    // Traverse One Step
    ray_marcher.traverse();

    const KeyType& current_key = ray_marcher.currentkey();

    // TODO This search can be aided and improved by hint from previous search??
    VoxelNodePtrType current_node = map_->search(current_key);

    // Check if current key is within maximal bbx limit of the map
    if (map_->outsideMaximumBbx(current_key))
      return ReturnType(false, current_key, current_node);

    if (current_node) {
      // Check for occupancy
      if (occupancy_checker_(*current_node))
        return ReturnType(true, current_key, current_node);
    }
    else if (!IgnroreUnknownVoxels) {
      return ReturnType(false, current_key, current_node);
    }
  } // end primary traversal loop
}


} // end namespace Naksha
