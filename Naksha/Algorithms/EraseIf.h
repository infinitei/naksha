/**
 * @file EraseIf.h
 * @brief EraseIf
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_ERASE_IF_H_
#define NAKSHA_ERASE_IF_H_

#include "Naksha/Maps/MapMaker.h"

namespace Naksha {

namespace detail {

/// Tag for Vector like Erase pattern
struct VectorEraseTag {
};

/// Tag for Associative container Erase pattern
struct AssociativeEraseTag {
};

/// Tag for container supporting Erase by key only
struct KeyOnlyEraseTag {
};


/// Default EraseTraits
template<class MapType>
struct EraseTraits {
  typedef AssociativeEraseTag type;
};

/// EraseTraits for HashedVectorDataStructure
template<
  int _Dimension,
  class _Payload,
  template<class > class _Interface,
  template<class, typename > class _Transform,
  template<class > class _NodeInterface>
struct EraseTraits<VolumetricMap<_Dimension, _Payload, HashedVectorDataStructure, _Interface, _Transform, _NodeInterface>> {
  typedef VectorEraseTag type;
};

/// EraseTraits for TreeDataStructure
template<
  int _Dimension,
  class _Payload,
  template<class > class _Interface,
  template<class, typename > class _Transform,
  template<class > class _NodeInterface>
struct EraseTraits<VolumetricMap<_Dimension, _Payload, TreeDataStructure, _Interface, _Transform, _NodeInterface>> {
  typedef KeyOnlyEraseTag type;
};


template<class MapType, class Predicate>
void EraseIfKeyHelper(MapType& map, Predicate pred, AssociativeEraseTag) {

  for (typename MapType::iterator it = map.begin(); it != map.end();) {
    const typename MapType::KeyType& key = it.key();
    if (pred(key))
      it = map.erase(it);
    else
      ++it;
  }
}

template<class MapType, class Predicate>
void EraseIfHelper(MapType& map, Predicate pred, AssociativeEraseTag) {
  for (typename MapType::iterator it = map.begin(); it != map.end();) {
    if (pred(*it))
      it = map.erase(it);
    else
      ++it;
  }
}

template<class MapType, class Predicate>
void EraseIfKeyHelper(MapType& map, Predicate pred, KeyOnlyEraseTag) {

  for (typename MapType::iterator it = map.begin(), end = map.end(); it != end; ++it) {
    const typename MapType::KeyType& key = it.key();
    if (pred(key)) {
      map.erase(key);
    }
  }
}

template<class MapType, class Predicate>
void EraseIfHelper(MapType& map, Predicate pred, KeyOnlyEraseTag) {

  for (typename MapType::iterator it = map.begin(), end = map.end(); it != end; ++it) {
    const typename MapType::KeyType& key = it.key();
    if (pred(*it)) {
      map.erase(key);
    }
  }
}


/// This uses the erase-remove-if idiom
template<class MapType, class Predicate>
void EraseIfKeyHelper(MapType& map, Predicate pred, VectorEraseTag) {
  typedef typename MapType::iterator MapIterator;

  // Do the std::remove_if logic
  MapIterator first = map.begin();
  MapIterator result = first;
  MapIterator last = map.end();

  while (first != last) {
    if (!pred(first.key())) {
      *result = *first;
      result.key() = first.key();
      ++result;
    }
    ++first;
  }

  // Now Do the erase logic
  map.erase(result, map.end());
}

} // end namespace detail


/**@brief Erase a voxel if the Predicate is true on the voxel
 *
 * @param map         - Naksha Maps type
 * @param voxel_pred  - predicate functor over voxel
 *
 * @ingroup Algorithms
 */
template<class MapType, class VoxelPredicate>
void EraseIf(MapType& map, VoxelPredicate voxel_pred) {
  detail::EraseIfHelper(map, voxel_pred, typename detail::EraseTraits<MapType>::type());
}



/**@brief Erase a voxel if the Predicate is true on the key
 *
 * @param map       - Naksha Maps type
 * @param key_pred  - predicate functor over key
 *
 * @ingroup Algorithms
 */
template<class MapType, class KeyPredicate>
void EraseIfKey(MapType& map, KeyPredicate key_pred) {
  detail::EraseIfKeyHelper(map, key_pred, typename detail::EraseTraits<MapType>::type());
}

}  // end namespace Naksha



#endif // end NAKSHA_ERASE_IF_H_
