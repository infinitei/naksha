/**
 * @file CompareMaps.h
 * @brief CompareMaps
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_COMPARE_MAPS_H_
#define NAKSHA_COMPARE_MAPS_H_

#include <iostream>
#include <type_traits>

namespace Naksha {

/**@brief Compare two maps
 *
 * mapA and mapB are considered equivalent id they both have
 * same resolution, size, and same set of keys and payload.
 *
 * @param[in] mapA
 * @param[in] mapB
 * @return true if the maps are equivalent
 *
 *
 * @tparam MapTypeA should be self deduced
 * @tparam MapTypeB should be self deduced
 *
 * This function requires the KeyType and PayLoad type to be same for both maps i.e.
 * MapTypeA::KeyType == MapTypeB::KeyType and MapTypeA::Payload == MapTypeB::Payload
 *
 * @ingroup Algorithms
 */
template<class MapTypeA, class MapTypeB>
bool compareMaps(const MapTypeA& mapA, const MapTypeB& mapB) {
  static_assert((std::is_same<typename MapTypeA::KeyType, typename MapTypeB::KeyType>::value),
                          "Cannot compare maps with different KeyTypes");
  static_assert((std::is_same<typename MapTypeA::Payload, typename MapTypeB::Payload>::value),
                          "Cannot compare maps with different Payload types");

  std::cout << "Comparing Maps .. " << std::flush;

  if (mapA.resolution() != mapB.resolution()) {
    std::cout << "Resolution MisMatch: " << mapA.resolution() << " != "
        << mapB.resolution() << std::endl;
    return false;
  }

  if (mapA.size() != mapB.size()) {
    std::cout << "Size MisMatch: " << mapA.size() << " != "
        << mapB.size() << std::endl;
    return false;
  }

  typename MapTypeA::const_iterator it_mapA = mapA.begin(), map1_end = mapA.end();
  for (; it_mapA != map1_end; ++it_mapA) {

    typename MapTypeB::const_iterator it_mapB = mapB.find(it_mapA.key());
    if (it_mapB == mapB.end()) {
      std::cout << "Key " << it_mapA.key() << " Not found in Map2." << std::endl;
      return false;
    }
    else {
      if (it_mapA->payload() != it_mapB->payload()) {
        std::cout << "Map Contents are not same at Key: " << it_mapA.key() << std::endl;
        return false;
      }

      if (it_mapA.key() != it_mapB.key()) {
        std::cout << "Weirdly Keys are not same!!" << std::endl;
        return false;
      }
    }
  }
  std::cout << " Done." << std::endl;
  return true;
}

} // end namespace Naksha

#endif // NAKSHA_COMPARE_MAPS_H_
