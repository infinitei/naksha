/**
 * @file RayMarching.h
 * @brief RayMarching
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_ALGORITHMS_RAYMARCHING_H_
#define NAKSHA_ALGORITHMS_RAYMARCHING_H_

namespace Naksha {


/**@brief RayMarching implementation
 *
 * This implements "A Faster Voxel Traversal Algorithm for Ray Tracing"
 * by John Amanatides and Andrew Woo, 1987
 *
 * @tparam KeyType    some \ref Keys type
 * @tparam Scalar     Scalar type for point coordinates (should be some floating point type)
 *
 * @ingroup Algorithms
 */

template <class KeyType, typename Scalar>
class RayMarching {
public:

  typedef Eigen::Matrix<Scalar, KeyType::Dimension, 1> PointType; ///< Point Type
  typedef Eigen::Matrix<int, KeyType::Dimension, 1> DiscretePointType;  ///< Discrete Point Type


  /**@brief Constructor which also does the Initialization Phase of [Amanatides and  Woo, 1987]
   *
   * @param[in] start_key           discrete key of ray origin
   * @param[in] start_coord         coordinate of ray origin
   * @param[in] start_voxel_center  coordinate of ray origin
   * @param[in] ray_direction       ray direction (needs to be normalized)
   * @param[in] voxel_size          length of a voxel side
   *
   * @note ray_direction needs to be normalized before i.e. ray_direction.norm() == 1
   */
  template<typename AnyPointType>
  RayMarching(const KeyType& start_key, const AnyPointType& start_coord,
      const PointType start_voxel_center, const PointType ray_direction,
      const Scalar voxel_size);

  /// This does One step of the Traversal Phase of [Amanatides and  Woo, 1987]
  inline void traverse();

  /// @returns current key
  const KeyType& currentkey() const {return current_key_;}

private:
  KeyType current_key_;
  PointType direction_;
  DiscretePointType step_;
  PointType tMax_;
  PointType tDelta_;
};

}  // namespace Naksha

#include "Naksha/Algorithms/RayMarching-impl.hpp"

#endif // NAKSHA_ALGORITHMS_RAYMARCHING_H_
