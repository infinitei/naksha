/**
 * @file RayTracing-impl.hpp
 * @brief RayTracing-impl
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_RAY_TRACING_IMPL_HPP_
#define NAKSHA_RAY_TRACING_IMPL_HPP_

#include "Naksha/Algorithms/RayMarching.h"
#include "Naksha/Algorithms/RayIntersections.h"

#include <type_traits>
#include <iterator>
#include <iostream>

namespace Naksha {

namespace detail {

template <class KeyContainer, class KeyType, class Enable = void>
struct KeyContainerHelper {
  inline static void add_hit_key(KeyContainer& key_container, const KeyType& key, const bool inside_map) {
    if (inside_map) {
      key_container.push_back(key);
    }
  }
};

template <class KeyContainer, class KeyType>
struct KeyContainerHelper<KeyContainer, KeyType, typename std::enable_if< std::is_same<typename KeyContainer::value_type, std::pair<bool, KeyType> >::value >::type> {
  inline static void add_hit_key(KeyContainer& key_container, const KeyType& key, const bool inside_map) {
    key_container.push_back(std::make_pair(inside_map, key));
  }
};

}  // namespace detail

template<class MapType, class EigenPointTypeA, class EigenPointTypeB>
bool computeKeysOnRay(
    const MapType& map,
    const EigenPointTypeA& start,
    const EigenPointTypeB& end,
    std::vector<typename MapType::KeyType>& ray_keys) {

  // ray of zero length
  if (start == end)
    return true;

  typedef typename MapType::KeyType KeyType;
  KeyType start_key, end_key;
  if (!map.keyFromCoord(start, start_key)  || !map.keyFromCoord(end, end_key)) {
    const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
    std::cout << "[computeKeysOnRay] WARNING: coordinates "
        << start.format(vecfmt) << " -> " << end.format(vecfmt) << " are out of bounds\n";
    return false;
  }

  computeKeysOnRay(map, start_key, end_key, start, (end - start).normalized(), ray_keys);

  return true;
}

template<class MapType, class EigenPointType, class DirectionType>
void computeKeysOnRay(
    const MapType& map,
    const typename MapType::KeyType& start_key,
    const typename MapType::KeyType& end_key,
    const EigenPointType& start_coord,
    const DirectionType& normalized_direction,
    std::vector<typename MapType::KeyType>& ray_keys) {

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::TransformScalar Scalar;
  typedef RayMarching<KeyType, Scalar> RayMarcher;
  typedef typename RayMarcher::PointType PointType;

  //-----------Initialization Phase of [Amanatides and  Woo, 1987]-------------//


  RayMarcher ray_marcher(
      start_key, start_coord,
      map.template voxelCenter<PointType>(start_key),
      normalized_direction.template cast<Scalar>(),
      map.resolution());

  ray_keys.push_back(ray_marcher.currentkey());

  //-------------Traversal Phase of [Amanatides and  Woo, 1987]--------------//

  //primary traversal loop
  bool continue_traversal = true;
  while (continue_traversal) {
    /// Traverse
    ray_marcher.traverse();

    const KeyType& current_key = ray_marcher.currentkey();

    // Add current Key to the list of ray keys
    ray_keys.push_back(current_key);

    // Check for ending criteria
    // We currently check if the adjacent cell has been reached
    // rather than directly checking current_key == end_key
    // The reason is because due to discretization the DDA ray may
    // never reach end_key cell
    // Way to check adjacency : end_key.value - current_key.value).cwiseAbs().maxCoeff() < 2

    // The following is a faster way of checking
    // the criteria (end_key.value - current_key.value).cwiseAbs().maxCoeff() < 2

    continue_traversal = false;
    for (int i = 0; i < MapType::Dimension; ++i) {
      if (std::abs(int(end_key.value[i]) - int(current_key[i])) > 1) {
        continue_traversal = true;
        break;
      }
    }

  } // end primary traversal loop
}

template<class KeyCordTransformer, class EigenPointType, class DirectionType>
void computeKeysOnRayWithKeyCordTransformer(
    const KeyCordTransformer& key_coord_transformer,
    const typename KeyCordTransformer::KeyType& start_key,
    const typename KeyCordTransformer::KeyType& end_key,
    const EigenPointType& start_coord,
    const DirectionType& normalized_direction,
    std::vector<typename KeyCordTransformer::KeyType>& ray_keys) {

  typedef typename KeyCordTransformer::KeyType KeyType;
  typedef typename KeyCordTransformer::Scalar Scalar;
  typedef RayMarching<KeyType, Scalar> RayMarcher;

  //-----------Initialization Phase of [Amanatides and  Woo, 1987]-------------//


  RayMarcher ray_marcher(
      start_key, start_coord,
      key_coord_transformer(start_key),
      normalized_direction.template cast<Scalar>(),
      key_coord_transformer.resolution());

  ray_keys.push_back(ray_marcher.currentkey());

  //-------------Traversal Phase of [Amanatides and  Woo, 1987]--------------//

  //primary traversal loop
  bool continue_traversal = true;
  while (continue_traversal) {
    /// Traverse
    ray_marcher.traverse();

    const KeyType& current_key = ray_marcher.currentkey();

    // Add current Key to the list of ray keys
    ray_keys.push_back(current_key);

    // Check for ending criteria
    // We currently check if the adjacent cell has been reached
    // rather than directly checking current_key == end_key
    // The reason is because due to discretization the DDA ray may
    // never reach end_key cell
    // Way to check adjacency : end_key.value - current_key.value).cwiseAbs().maxCoeff() < 2

    // The following is a faster way of checking
    // the criteria (end_key.value - current_key.value).cwiseAbs().maxCoeff() < 2

    continue_traversal = false;
    for (int i = 0; i < KeyType::Dimension; ++i) {
      if (std::abs(int(end_key.value[i]) - int(current_key[i])) > 1) {
        continue_traversal = true;
        break;
      }
    }

  } // end primary traversal loop
}

template<class HitKeysContainer, class MissKeysContainer, class MapType, class EigenPointType, class PointCloudType>
std::pair<HitKeysContainer, MissKeysContainer>
computeKeysFromDepthScan(const MapType& map, const EigenPointType& sensor_origin, const PointCloudType& point_cloud) {

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VectorType VectorType;
  typedef typename PointCloudType::const_iterator PointCloudIterator;
  typedef typename MapType::Transform::KeyCoordTransformerType KeyCoordTransformerType;
  typedef typename MapType::Transform::BoundingBox BoundingBox;
  typedef RayAABBIntersection<typename KeyCoordTransformerType::Scalar, MapType::Dimension> RayBbxIntersectionFunctor;

  static_assert(
          (std::is_same<typename HitKeysContainer::value_type, KeyType>::value)
          || (std::is_same<typename HitKeysContainer::value_type, std::pair<bool, KeyType> >::value),
          "HitKeysContainer should be a container of KeyType or std::pair<bool, KeyType>");

  static_assert(
        (std::is_same<typename MissKeysContainer::value_type, KeyType>::value),
        "MissKeysContainer should be a container of KeyType");

  // Initialize the Key Containers
  HitKeysContainer hit_keys;
  MissKeysContainer miss_keys;

  // get the map's key coord transformation functor
  const KeyCoordTransformerType key_coord_transformer = map.keyCoordTransformer();

  const BoundingBox& bbx = map.maximumBbx();

  const RayBbxIntersectionFunctor ray_bbx_intersection(bbx, sensor_origin);

  // check origin and return if outside maximal bbx
  if(! bbx.contains(sensor_origin))
    return std::make_pair(hit_keys, miss_keys);

  // reserve the Key Containers
  hit_keys.reserve(point_cloud.size());

  // TODO Try guessing an good upper limit for reserve
  // probably point_cloud.size() * average_depth / resolution * some constant
  miss_keys.reserve(10000 * point_cloud.size());

  // Get origin key
  KeyType origin_key = key_coord_transformer(sensor_origin);

  // iterate over each ray
  for (PointCloudIterator point_it = point_cloud.begin(), end = point_cloud.end();
      point_it != end; ++point_it) {

    // Compute Ray Direction
    const VectorType direction = (*point_it - sensor_origin).normalized();

    // Determine Ray end key by checking for Map's Maximal Bounding Box
    bool inside_map = bbx.contains(*point_it);
    KeyType end_key = inside_map ? key_coord_transformer(*point_it) : key_coord_transformer(ray_bbx_intersection(direction));

    // Accumulate all miss_keys on this ray
    computeKeysOnRayWithKeyCordTransformer(key_coord_transformer, origin_key, end_key, sensor_origin, direction, miss_keys);

    // Accumulate hit keys
    detail::KeyContainerHelper<HitKeysContainer, KeyType>::add_hit_key(hit_keys, end_key, inside_map);
  }

  return std::make_pair(hit_keys, miss_keys);
}

} // end namespace Naksha

#endif // NAKSHA_RAY_TRACING_IMPL_HPP_
