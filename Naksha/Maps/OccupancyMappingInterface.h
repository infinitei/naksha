/**
 * @file OccupancyMappingInterface.h
 * @brief 
 * 
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_OCCUPANCY_MAPPING_INTERFACE_H_
#define NAKSHA_OCCUPANCY_MAPPING_INTERFACE_H_

#include "Naksha/Maps/MappingInterfaceBase.h"
#include <Eigen/Core>
#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>

namespace Naksha {

/**@brief Simple Mapping Interface Policy class
 *
 * @ingroup MapInterface
 * @ingroup Maps
 */
template <class DerivedMap>
class OccupancyMappingInteface : public MappingInterfaceBase<DerivedMap> {

  typedef MappingInterfaceBase<DerivedMap> Base;

public:
  typedef typename MapTraits<DerivedMap>::KeyType KeyType;  ///< Typedef for \ref Keys type of this map
  typedef typename MapTraits<DerivedMap>::VoxelNodeType VoxelNodeType; ///< Typedef for VoxelNode Type

  using Base::derived;

  /// @name Constructors
  /// @{

  /// Default constructor
  OccupancyMappingInteface() {}

  /// @}

  /// @name Integrate Measurements to Map voxels
  /// @{

  /**
   * @brief Update Voxel with a specific Measurement using Key
   *
   * @param key Discrete Key of the Node that is to be updated
   * @param measurement of MeasurementType T supported by the voxel
   * @return pointer to the updated Voxel Node
   */
  template <typename T>
  VoxelNodeType* updateVoxel(const KeyType& key, const T& measurement) {
    // Search and Add if required
    VoxelNodeType* voxel = derived().addNewVoxel(key);

    voxel->insertMeasurement(measurement);
    return voxel;
  }

  /**
   * @brief Update Voxel with a specific Measurement using point coordinate
   *
   * Looks up the Key corresponding to the coordinate and then calls udpateNode() with it.
   *
   * @param coord coordinate of the Node that is to be uMeasModelpdated (Eigen Vector)
   * @param measurement of MeasurementModel
   * @return pointer to the updated Voxel Node
   */
  template<typename DerivedVector, typename T>
  VoxelNodeType* updateVoxel(const Eigen::MatrixBase<DerivedVector>& coord,
      const T& measurement) {
    KeyType key;
    if (!derived().keyFromCoord(coord, key))
      return NULL;
    return updateVoxel(key, measurement);
  }


  /**
   * @brief Lazy Update Voxel with a specific Measurement using Key
   *
   * @param key Discrete Key of the Node that is to be updated
   * @param measurement of MeasurementType T supported by the voxel
   * @return pointer to the updated Voxel Node
   *
   * @note This does lazy update (Should call update Map later)
   */
  template <typename T>
  VoxelNodeType* updateVoxelLazy(const KeyType& key, const T& measurement) {
    // Search and Add if required
    VoxelNodeType* voxel = derived().addNewVoxel(key);

    voxel->insertMeasurementLazy(measurement);
    return voxel;
  }

  /**
   * This attempts to create a new default constructed VoxelNode
   * at location key
   *
   * @param[in] key discrete location
   * @return ptr to created VoxelNode (or existing one)
   */
  VoxelNodeType* insertNewVoxel(const KeyType& key) {
    return derived().addNewVoxel(key);
  }

  /**
   * This attempts to create a new VoxelNode with initialization
   * at location key
   *
   * @param[in] key discrete location
   * @param[in] init initialization data for VoxelNode constructor
   * @return ptr to created VoxelNode (or existing one)
   */
  template<typename T>
  VoxelNodeType* insertNewVoxel(const KeyType& key, const T& init) {
    return derived().addNewVoxel(key, init);
  }


  /// Finish lazy evaluation of the map
  void finishLazyEvaluation() {
    for(typename DerivedMap::iterator voxel_it = derived().begin(), voxel_end = derived().end(); voxel_it != voxel_end; ++voxel_it ) {
      voxel_it->finishLazyEvaluation();
    }
  }

  /// @}

private:
  /// Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
  }
};

} // end namespace Naksha

#include "Naksha/Maps/OccupancyMappingInterface-impl.hpp"

#endif // NAKSHA_OCCUPANCY_MAPPING_INTERFACE_H_
