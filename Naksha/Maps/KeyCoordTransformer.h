/**
 * @file KeyCoordTransformer.h
 * @brief Functor for Key/Coord Transformations
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_KEY_FROM_COORD_FUNCTOR_H_
#define NAKSHA_KEY_FROM_COORD_FUNCTOR_H_

#include "Naksha/Common/EigenTraits.h"
#include <boost/serialization/access.hpp>
#include <type_traits>
#include <functional>
#include <cmath>

namespace Naksha {

/**@brief Functor for Key/Coord Transformations
 *
 * @tparam KeyType Type of Key
 * @tparam Scalar Scalar Type usually either a float or double
 *
 * @par Example:
 * @code{.cpp}
    // Construct the functor by passing the map resolution
    const KeyCoordTransformer<Key, float> key_coord_transformer(map_resolution);

    // conversion of a coordinate to discrete key with no map bound check
    const Vector3f coord1(7, 3, 7);
    Key key1 = key_coord_transformer(coord1);

    // conversion of a coordinate to discrete key with map bound check
    const Vector3f coord2(7, 3, 7);
    Key key2;
    bool inside = key_coord_transformer(coord2, key2);

    // Get voxel center coordinate for a particular discrete key
    Vector3f center = key_coord_transformer(key);
    Vector3d center = key_coord_transformer.template operator()<Vector3d>(key);

    // create a bunch of voxel centers from a vector of keys
   @endcode
 *
 * @ingroup MapTransform
 * @ingroup Maps
 */
template<typename _KeyType, typename _Scalar>
class KeyCoordTransformer {
  /// Scalar Type of Key (should be Integral type)
  typedef typename _KeyType::Scalar KeyScalar;

 public:
  typedef _KeyType KeyType;
  typedef _Scalar Scalar;

  /// Preferred VectorType (point type) for the map
  typedef Eigen::Matrix<Scalar, KeyType::Dimension, 1> VectorType;


  /**@brief Constructor for Coord to Key conversion Functor
   *
   * @param[in] resolution  map resolution
   */
  KeyCoordTransformer(const Scalar resolution)
      : resolution_(resolution),
        inv_resolution_(1. / resolution),
        max_key_radius_(std::numeric_limits<KeyScalar>::max() / 2 + 1) {
  }

  /**
   * @brief Get a discrete Key address for a coordinate
   *
   * @param[in] coord position/coordinate in continuous domain
   * @return discrete Key for the voxel containing coord
   */
  template<typename EigenPointType>
  KeyType operator()(const EigenPointType& coord) const {
    static_assert(
        !(std::is_same<EigenPointType, KeyType>::value),
        "Bad overload. Input argument cannot be a KeyType");
    static_assert(
        IsEigenType<EigenPointType>::value,
        "EigenPointType is not of Eigen Type");
    static_assert(
        (
            EigenPointType::RowsAtCompileTime == static_cast<int>(KeyType::Dimension)
            || EigenPointType::RowsAtCompileTime == Eigen::Dynamic
        ) &&
        (
            EigenPointType::ColsAtCompileTime == 1
            || EigenPointType::ColsAtCompileTime == Eigen::Dynamic
        )
        ,"Coord Vector Type Size is Wrong");
    typedef typename EigenPointType::Scalar PointScalar;
    using std::floor;
    return KeyType(((coord * inv_resolution_).unaryExpr(std::ptr_fun<PointScalar, PointScalar>(floor)).template cast<int>().array() + max_key_radius_).template cast<KeyScalar>());
  }


  /**
   * @brief Get a discrete Key address for a coordinate (Check map limits)
   *
   * @param[in] coord position/coordinate in continuous domain
   * @param[out] key discrete Key for the voxel containing coord
   * @return true if coord lies inside possible map size
   *
   * @note This function does a checking if coord lies inside possible map size
   */
  template<typename EigenPointType>
  bool operator()(const EigenPointType& coord, KeyType& key) const {
    static_assert(IsEigenType<EigenPointType>::value, "EigenPointType is not of Eigen Type");
    static_assert(
        (
            EigenPointType::RowsAtCompileTime == static_cast<int>(KeyType::Dimension)
            || EigenPointType::RowsAtCompileTime == Eigen::Dynamic
        ) &&
        (
            EigenPointType::ColsAtCompileTime == 1
            || EigenPointType::ColsAtCompileTime == Eigen::Dynamic
        )
        ,"Coord Vector Type Size is Wrong");
    typedef typename EigenPointType::Scalar PointScalar;
    using std::floor;
    const Eigen::Matrix<int, KeyType::Dimension, 1> adjusted_coord = (coord * inv_resolution_).unaryExpr(std::ptr_fun<PointScalar, PointScalar>(floor)).template cast<int>().array() + max_key_radius_;
    if((adjusted_coord.minCoeff() < 0) || (adjusted_coord.maxCoeff() >= 2 * max_key_radius_))
      return false;
    key.value = adjusted_coord.template cast<KeyScalar>();
    return true;
  }

  template<typename EigenPointType>
  EigenPointType operator()(const KeyType& key) const {
    static_assert(IsEigenType<EigenPointType>::value, "EigenPointType is not of Eigen Type");
    typedef typename EigenPointType::Scalar EigenPointTypeScalar;
    static_assert(
        std::is_floating_point<EigenPointTypeScalar>::value,
        "Return Vector Scalar is not floating point type");
    return ((key.value.template cast<int>().array() - max_key_radius_).template cast<EigenPointTypeScalar>().array() + 0.5) * resolution_;
  }

  VectorType operator()(const KeyType& key) const {
    return ((key.value.template cast<int>().array() - max_key_radius_).template cast<Scalar>().array() + 0.5) * resolution_;
  }

  Scalar resolution() const {return resolution_;}

 private:
  const Scalar resolution_;
  const Scalar inv_resolution_;
  const int max_key_radius_;

  /// Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & const_cast<Scalar &>(resolution_);
    ar & const_cast<Scalar &>(inv_resolution_);
    ar & const_cast<int &>(max_key_radius_);
  }
};

}  // namespace Naksha

#endif // end NAKSHA_KEY_FROM_COORD_FUNCTOR_H_
