/**
 * @file HybridMappingInterface.h
 * @brief Hybrid Mapping Interface
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_HYBRID_MAPPING_INTEFACE_H_
#define NAKSHA_HYBRID_MAPPING_INTEFACE_H_

#include "Naksha/Maps/OccupancyMappingInterface.h"
#include "Naksha/Common/unordered_set_serialization.h"
#include "Naksha/Keys/KeyHash.h"

namespace Naksha {

/**@brief This Inteface Policy class simply adds Change Detection over OccupancyMappingInteface
 *
 * This Inteface policy class derives from OccupancyMappingInteface, and simply adds chage detection
 * capability.
 *
 * @ingroup MapInterface
 * @ingroup Maps
 */
template<class DerivedMap>
class HybridMappingInterface: public OccupancyMappingInteface<DerivedMap> {

  typedef OccupancyMappingInteface<DerivedMap> Base;
  typedef KeyHash KeyHashType; ///< Typedef for KeyHash type used by this map

public:
  typedef typename Base::KeyType KeyType; ///< Typedef for \ref Keys type of this map
  typedef typename Base::VoxelNodeType VoxelNodeType; ///< Typedef for VoxelNode Type
  typedef boost::unordered_set<KeyType, KeyHashType> KeySet;  ///< Unordered set of Keys
  typedef KeySet ChangedKeys; ///< Set of Changed Keys

  /// Constructor
  HybridMappingInterface()
    : use_change_detection_(false) {}

  /// @name Change Detection
  /// @{

  /// Const reference to all the changed keys
  const ChangedKeys& changedKeys() const {return changed_keys_;}

  /// track or ignore changes while inserting scans (default: ignore)
  void enableChangeDetection(const bool enable = true) {
    use_change_detection_ = enable;
  }

  /**@brief Reset the set of changed keys.
   * Call this after you obtained all changed nodes.
   */
  void resetChangeDetection() {
    changed_keys_.clear();
  }

  /// @}

  /// @name Integrate Measurements to Map voxels
  /// @{

  /**
   * @brief Update Voxel with a specific Measurement using Key
   *
   * @param key         - Discrete Key of the Node that is to be updated
   * @param measurement - the measurement to be integrated into the voxel
   * @return pointer to the updated Voxel Node
   */
  template<typename T>
  VoxelNodeType* updateVoxel(const KeyType& key, const T& measurement) {
    VoxelNodeType* voxel = Base::updateVoxel(key, measurement);
    if(use_change_detection_ && voxel)
      changed_keys_.insert(key);
    return voxel;
  }

  using Base::derived;

  /**
   * @brief Update Voxel with a specific Measurement using point coordinate
   *
   * Looks up the Key corresponding to the coordinate and then
   * calls updateVoxel(key, measurement) with it.
   *
   * @param coord       - coordinate of the Node that is to be updated
   * @param measurement - the measurement to be integrated into the voxel
   * @return pointer to the updated Voxel Node
   */
  template<typename DerivedVector, typename T>
  VoxelNodeType* updateVoxel(const Eigen::MatrixBase<DerivedVector>& coord,
      const T& measurement) {
    KeyType key;
    if (!derived().keyFromCoord(coord, key))
      return NULL;

    VoxelNodeType* voxel = Base::updateVoxel(key, measurement);
    if(use_change_detection_ && voxel)
         changed_keys_.insert(key);

    return voxel;
  }

  /**
   * @brief Lazy Update Voxel with a specific Measurement using Key
   *
   * @param key         - Discrete Key of the Node that is to be updated
   * @param measurement - the measurement to be integrated into the voxel
   * @return pointer to the updated Voxel Node
   *
   * @note This does lazy update (Should call update Map later)
   */
  template<typename T>
  VoxelNodeType* updateVoxelLazy(const KeyType& key, const T& measurement) {
    VoxelNodeType* voxel = Base::updateVoxelLazy(key, measurement);
    if (use_change_detection_ && voxel)
      changed_keys_.insert(key);
    return voxel;
  }


  /**
   * This attempts to create a new default constructed VoxelNode
   * at location key
   *
   * @param[in] key  discrete location
   * @return ptr to created VoxelNode (or existing one)
   */
  VoxelNodeType* insertNewVoxel(const KeyType& key) {
    VoxelNodeType* voxel_node;
    bool new_voxel;
    std::tie(voxel_node, new_voxel) = derived().emplace(key);
    if (use_change_detection_ && new_voxel)
      changed_keys_.insert(key);
    return voxel_node;
  }

  /**
   * This attempts to create a new VoxelNode with initilization
   * at location key
   *
   * @param[in] key discrete location
   * @param[in] init initialization data for VoxelNode constructor
   * @return ptr to created VoxelNode (or existing one)
   */
  template<typename T>
  VoxelNodeType* insertNewVoxel(const KeyType& key, const T& init) {
    VoxelNodeType* voxel_node;
    bool new_voxel;
    std::tie(voxel_node, new_voxel) = derived().emplace(key, init);
    if (use_change_detection_ && new_voxel)
      changed_keys_.insert(key);
    return voxel_node;
  }

  /// @}


  /// TODO Remove this function Enable/Disable BoundingBox Limit Checking
  void enableMaximumBbxChecking(const bool enable = true) {
//    use_maximum_bbx_checking_ = enable;
  }

protected:
  bool use_change_detection_; ///< Use Change Detection
  ChangedKeys changed_keys_; ///< Set of keys that changed since last resetChangeDetection

private:
  /// Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & boost::serialization::base_object< Base >(*this);
    ar & use_change_detection_;
    ar & changed_keys_;
  }
};

} // end namespace Naksha

#endif // NAKSHA_HYBRID_MAPPING_INTEFACE_H_
