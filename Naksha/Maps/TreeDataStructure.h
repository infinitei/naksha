/**
 * @file TreeDataStructure.h
 * @brief Tree DataStructure policy
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_TREE_DATA_STRUCTURE_H_
#define NAKSHA_TREE_DATA_STRUCTURE_H_

#include "Naksha/Maps/MapTraits.h"
#include "Naksha/Keys/KeyUtils.h"
#include "Naksha/Maps/TreeIterator.h"

#include <boost/serialization/access.hpp>
#include <boost/scoped_ptr.hpp>

namespace Naksha {

/**@brief Tree DataStructure Policy for creating OctreeMap or QuadtreeMap
 *
 * This class implements a tree datastructure policy, which stores the
 * voxels in a hierarchical tree.
 *
 * This class is intended to be uses as DataStructure Policy class
 * for higher level mapping class like VolumetricMap.
 *
 * @tparam _KeyType         Type of Key
 * @tparam _VoxelNodeType   Type of Voxel Node with non-zero childs
 *
 * @see UnorderedMapDataStructure, HashedVectorDataStructure for another MapDatastructure.
 *
 * @ingroup MapDatastructure
 * @ingroup Maps
 */
template<class _KeyType, class _VoxelNodeType>
class TreeDataStructure {

public:
  typedef _KeyType KeyType; ///< Typedef for \ref Keys type of this map
  typedef _VoxelNodeType VoxelNodeType; ///< Typedef for VoxelNode Type

  typedef typename std::size_t size_type;  ///< size_type

  typedef LeafIterator<VoxelNodeType, KeyType> iterator;  ///< iterator over voxels (LeafIterator)
  typedef LeafIterator<VoxelNodeType const, KeyType> const_iterator; ///< const iterator over voxels (LeafIterator)

  typedef NodeIterator<VoxelNodeType, KeyType> node_iterator;  ///< Node Iterator based NodeIterator
  typedef NodeIterator<VoxelNodeType const, KeyType> node_const_iterator; ///< Const version of NodeIterator

private:
  static_assert(VoxelNodeType::MaxNumOfChilds == (1 << KeyType::Dimension), "Number of Children needs to be 2^Dimension");


public:

  /// @name Constructors
  /// @{

  /// Default Constructor
  TreeDataStructure()
    : tree_size_(0) {}

  /// @}

  /// @name Size and capacity
  /// @{

  /// Returns the number voxels (leaf nodes)
  size_type size() const { return numberOfVoxels(); }

  /// Compute the number of voxels (This is == # of leaf nodes in TreeDataStructure)
  size_type numberOfVoxels() const;

  /// Compute the number of nodes (Total # of nodes including inner nodes)
  size_type numberOfNodes() const;

  /**@brief reserves storage
   * Increase the capacity of the container to a value that's greater
   * or equal to new_cap
   *
   * @param new_cap   - new capacity to reserve for the container
   *
   * @note This function does nothing as of now. It exists to satisfy
   * the Datastructure Policy concept.
   */
  void reserve(size_type new_cap) {
     //TODO Implement this
  }

  /// @}


  /// @name Lookup with Keys
  /// @anchor LookupWithKeys
  /// @{

  /**@brief Search for a Voxel at a key
   * @param[in] key of type KeyType
   * @return ptr to VoxelNodeType (Null if not found)
   */
  const VoxelNodeType* search(const KeyType& key) const;

  /**@brief Search for a Voxel at a key (non const version)
   * @param[in] key of type KeyType
   * @return ptr to VoxelNodeType (Null if not found)
   */
  VoxelNodeType* search(const KeyType& key);

  /**@brief Find a for a Voxel at a key
   *
   * This is same as search() but instead returns a iterator instead of
   * pointer. Also since we use fat iterators, this function may have a
   * little overhead over search(). But function this gives a very generic
   * STL like interface.
   *
   * @param[in] key of type KeyType
   * @return const_iterator to the voxel (evaluates to end() if no found)
   */
  const_iterator find(const KeyType& key) const;

  /// @}

  /// @name Modifiers
  /// @{

  /**
   * This attempts to create a new default constructed VoxelNode
   * at location key
   *
   * @param[in] key discrete location
   * @return ptr to created VoxelNode (or existing one)
   */
  VoxelNodeType* addNewVoxel(const KeyType& key);

  /**
   * This attempts to create a new VoxelNode with initialization
   * at location key
   *
   * @param[in] key   discrete location key
   * @param[in] init  initialization data for VoxelNode constructor
   * @return ptr to created VoxelNode (or existing one)
   */
  template <typename T>
  VoxelNodeType* addNewVoxel(const KeyType& key, const T& init);

  /**@brief Add a default constructed NewVoxel
   *
   * @param[in] key
   * @return Returns a pair consisting ptr to created VoxelNode (or existing one) and a bool denoting whether the insertion took place.
   */
  std::pair<VoxelNodeType*, bool> emplace(const KeyType& key);

  /**@brief This attempts to create a new VoxelNode with initialization
   *
   * @param[in] key   discrete location key
   * @param[in] init  initialization data for VoxelNode constructor
   * @return Returns a pair consisting ptr to created VoxelNode (or existing one) and a bool denoting whether the insertion took place.
   */
  template<typename T>
  std::pair<VoxelNodeType*, bool> emplace(const KeyType& key, const T& init);

  /// clears the contents
  void clear() {
    root_.reset(); // releasing the root ptr will cause the required chain reaction
  }

  /**@brief Erase all elements with key equivalent to key
   *
   * @param key of type KeyType
   * @return the number of elements erased.
   */
  size_type erase(const KeyType& key);

  /// Delete a Node at a particular key (same as erase)
  size_type deleteNode(const KeyType& key);

  /// @}

  /// @name Iterators
  /// @{

  /// @return beginning of the voxel iterator (only leaf nodes of the tree)
  iterator begin() {return iterator(root_.get());}
  /// @return beginning of the voxel iterator (only leaf nodes of the tree) const version
  const_iterator begin() const {return const_iterator(root_.get());}
  /// @return beginning of the voxel iterator (only leaf nodes of the tree) const version
  const_iterator cbegin() const {return const_iterator(root_.get());}

  /// @return end of the voxel node iterator
  iterator end() {return iterator();}
  /// @return end of the voxel node iterator (const version)
  const_iterator end() const {return const_iterator();}
  /// @return end of the voxel node iterator (const version)
  const_iterator cend() const {return const_iterator();}

  /// @return beginning of the node iterator (both inner & leaf nodes of the tree)
  node_iterator begin_node() {return node_iterator(root_.get());}
  /// @return beginning of the node iterator (both inner & leaf nodes of the tree) (const version)
  node_const_iterator begin_node() const {return node_const_iterator(root_.get());}
  /// @return beginning of the node iterator (both inner & leaf nodes of the tree) (const version)
  node_const_iterator cbegin_node() const {return node_const_iterator(root_.get());}

  /// @return end of the node iterator
  node_iterator end_node() {return node_iterator();}
  /// @return end of the node iterator (const version)
  node_const_iterator end_node() const {return node_const_iterator();}
  /// @return end of the node iterator (const version)
  node_const_iterator cend_node() const {return node_const_iterator();}

  /// @}


  /// @name Octree DataStructure specific
  /// @{

  /// Return Ptr to Root node
  const VoxelNodeType* rootNode() const {return root_.get();}
  /// Return modifiable Ptr to Root node
  VoxelNodeType* rootNode() {return root_.get();}

  /// Reset the root node ptr
  void resetRootNode(VoxelNodeType* p = 0) {
    root_.reset(p);
  }

  /// Get the max Tree Depth (TODO: Currently hardcoded be 16)
  static unsigned int maxTreeDepth() {return max_tree_depth_;}

  /// Update Inner Nodes TODO: Implement this
  void updateInnerNodes() {}

  /// @}

private:
  typedef boost::scoped_ptr<VoxelNodeType> VoxelNodeTypePtr; ///< Typedef for VoxelNode Ptr
  VoxelNodeTypePtr root_; ///< Ptr to root VoxelNode

  static const unsigned int max_tree_depth_ = 16; ///< Maximum Tree Depth

  size_type tree_size_; ///< total number of nodes in tree


  /// Some recursive functions
  void numberOfNodesRecursive(const VoxelNodeType& node, size_type& num_nodes) const;
  size_type numberOfVoxelsRecursive(const VoxelNodeType& parent) const;

  /// Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & root_;
  }

};


template <
  class Payload,
  template<typename > class NodeInterface
>
struct MapVoxelNode<2, Payload, TreeDataStructure, NodeInterface > {
  typedef VoxelNode<Payload, NodeInterface, QuadtreeNode> type;
};

template <
  class Payload,
  template<typename > class NodeInterface
>
struct MapVoxelNode<3, Payload, TreeDataStructure, NodeInterface > {
  typedef VoxelNode<Payload, NodeInterface, OctreeNode> type;
};

} // namespace Naksha

#include "Naksha/Maps/TreeDataStructure-impl.hpp"

#endif // NAKSHA_TREE_DATA_STRUCTURE_H_
