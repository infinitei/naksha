/**
 * @file MapTraits.h
 * @brief Map Traits class
 *
 * Map Traits class helps in exposing Derived class
 * typedefs to Base class (e.g in CRTP)
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_MAP_TRAITS_H_
#define NAKSHA_MAP_TRAITS_H_

#include "Naksha/Nodes/NodeTypedefs.h"

namespace Naksha {

/// Traits class for \ref Maps
template <typename Derived>
struct MapTraits;


/// Helper class to get VoxelNode type
template <
  int Dimension,
  class Payload,
  template<class, class > class _Datastructure,
  template<typename > class NodeInterface
>
struct MapVoxelNode {
  typedef VoxelNode<Payload, NodeInterface, NoChildPolicy> type;
};

} // namespace Naksha

#endif // NAKSHA_NODE_TRAITS_H_
