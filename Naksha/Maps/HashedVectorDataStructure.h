/**
 * @file HashedVectorDataStructure.h
 * @brief HashedVectorDataStructure
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_HASHED_VECTOR_DATA_STRUCTURE_H_
#define NAKSHA_HASHED_VECTOR_DATA_STRUCTURE_H_

#include "Naksha/Common/MakeAlignedContainer.h"
#include "Naksha/Common/Zip2Iterator.h"
#include "Naksha/Keys/KeyHash.h"

#include <type_traits>
#include <boost/assert.hpp>
#include <boost/serialization/vector.hpp>

namespace Naksha {

/**@brief Hashed Vector based DataStructure Policy
 *
 * This class stores the voxels and keys in a std::vector
 * It also has a KeyToVoxeLookup table which is realized by some unordered
 * associative container for fast association of a key to voxel.
 *
 * @tparam _KeyType         Type of Key
 * @tparam _VoxelNodeType   Type of Voxel Node
 *
 * @see TreeDataStructure, UnorderedMapDataStructure for another MapDatastructure.
 *
 * @ingroup MapDatastructure
 * @ingroup Maps
 */
template<class _KeyType, class _VoxelNodeType>
class HashedVectorDataStructure {

public:
  typedef _KeyType KeyType; ///< Typedef for \ref Keys type of this map
  typedef _VoxelNodeType VoxelNodeType; ///< Typedef for VoxelNode Type

  typedef KeyHash KeyHashType; ///< KeyHash Type

  typedef typename MakeStdVector<VoxelNodeType>::type VoxelContainer;  ///< VoxelContainer
  typedef std::vector<KeyType> KeyContainer;  ///< Key container
  typedef typename VoxelContainer::size_type size_type;  ///< size_type

  typedef Zip2Iterator<typename KeyContainer::iterator, typename VoxelContainer::iterator> iterator;  ///< iterator over voxel nodes
  typedef Zip2Iterator<typename KeyContainer::const_iterator, typename VoxelContainer::const_iterator> const_iterator;  ///< const iterator over voxel nodes

  typedef boost::unordered_map<KeyType, size_type, KeyHashType> KeyToVoxeLookup; ///< Key To Voxel Lookup container

  /// @name Constructors
  /// @{

  /// Default Constructor
  HashedVectorDataStructure() {}

  /// @}

  /// @name Size and capacity
  /// @{

  /// @return number of voxels
  size_type size() const {
    BOOST_ASSERT_MSG(_voxels.size() == _keys.size(), "# of voxels != # of keys");
    BOOST_ASSERT_MSG(_key_lookup.size() == _keys.size(), "KeyToVoxeLookup size is wrong");
    return _voxels.size();
  }

  /// @return number of voxels (same as size)
  size_type numberOfVoxels() const {
    return _voxels.size();
  }

  /**@brief reserves storage
   * Increase the capacity of the container to a value that's greater
   * or equal to new_cap
   *
   * @param new_cap
   */
  void reserve(size_type new_cap) {
    _voxels.reserve(new_cap);
    _keys.reserve(new_cap);
    _key_lookup.reserve(new_cap);
  }

  /// @}

  /// @name Lookup with Keys
  /// @anchor LookupWithKeys
  /// @{

  /** Search for a voxel at a key
   *
   * @param[in] key
   * @return ptr to VoxelNodeType (Null if not found)
   */
  const VoxelNodeType* search(const KeyType& key) const {
    typename KeyToVoxeLookup::const_iterator it = _key_lookup.find(key);
    if (it != _key_lookup.end())
      return _voxels.data() + it->second;
    else
      return 0; //TODO use nullptr c++11
  }

  /** Search for a voxel at a key (non const version)
   *
   * @param[in] key
   * @return ptr to VoxelNodeType (Null if not found)
   */
  VoxelNodeType* search(const KeyType& key) {
    typename KeyToVoxeLookup::const_iterator it = _key_lookup.find(key);
    if (it != _key_lookup.end())
      return _voxels.data() + it->second;
    else
      return 0; //TODO use nullptr c++11
  }

  /** Search for a voxel at a key
   *
   * @param[in] key discrete location key to look for
   * @return iterator to the corresponding voxel or end() if not found
   */
  const_iterator find(const KeyType& key) const {
    typename KeyToVoxeLookup::const_iterator it = _key_lookup.find(key);
    if (it != _key_lookup.end())
      return cbegin() + it->second;
    else
      return cend();
  }

  /** Search for a voxel at a key (non const version)
   *
   * @param[in] key discrete location key to look for
   * @return iterator to the corresponding voxel or end() if not found
   */
  iterator find(const KeyType& key) {
    typename KeyToVoxeLookup::const_iterator it = _key_lookup.find(key);
    if (it != _key_lookup.end())
      return iterator(_keys.begin() + it->second, _voxels.begin() + it->second);
    else
      return end();
  }


  /// @}


  /// @name Modifiers
  /// @{

  /**
   * This attempts to create a new default constructed VoxelNode
   * at location key
   *
   * @param[in] key discrete location
   * @return ptr to created VoxelNode (or existing one)
   */
  VoxelNodeType* addNewVoxel(const KeyType& key) {
    typedef std::pair<typename KeyToVoxeLookup::iterator, bool> KeyToVoxeLookupInsertReturnType;
    typedef typename KeyToVoxeLookup::value_type KeyToVoxeLookupValueType;
    KeyToVoxeLookupInsertReturnType insert_ret = _key_lookup.insert(KeyToVoxeLookupValueType(key, _voxels.size()));
    if(insert_ret.second) {
      // New Voxel
      _voxels.emplace_back();
      _keys.push_back(key);
      BOOST_ASSERT_MSG(_voxels.size() == _keys.size(), "# of voxels != # of keys");
      BOOST_ASSERT_MSG(_key_lookup.size() == _keys.size(), "KeyToVoxeLookup size is wrong");
      return &(_voxels.back());
    }
    else {
      // Existing voxel
      return &(_voxels[insert_ret.first->second]);
    }
  }

  /**
   * This attempts to create a new VoxelNode with initialization
   * at location key
   *
   * @param[in] key discrete location
   * @param[in] init initialization data for VoxelNode constructor
   * @return ptr to created VoxelNode (or existing one)
   */
  template<typename T>
  VoxelNodeType* addNewVoxel(const KeyType& key, const T& init) {
    typedef std::pair<typename KeyToVoxeLookup::iterator, bool> KeyToVoxeLookupInsertReturnType;
    typedef typename KeyToVoxeLookup::value_type KeyToVoxeLookupValueType;
    KeyToVoxeLookupInsertReturnType insert_ret = _key_lookup.insert(KeyToVoxeLookupValueType(key, _voxels.size()));
    if(insert_ret.second) {
      // New Voxel
      _voxels.emplace_back(init);
      _keys.push_back(key);
      BOOST_ASSERT_MSG(_voxels.size() == _keys.size(), "# of voxels != # of keys");
      BOOST_ASSERT_MSG(_key_lookup.size() == _keys.size(), "KeyToVoxeLookup size is wrong");
      return &(_voxels.back());
    }
    else {
      // Existing voxel
      return &(_voxels[insert_ret.first->second]);
    }
  }


  /**@brief Add a default constructed NewVoxel
   *
   * @param[in] key
   * @return Returns a pair consisting ptr to created VoxelNode (or existing one) and a bool denoting whether the insertion took place.
   */
  std::pair<VoxelNodeType*, bool> emplace(const KeyType& key) {
    typedef std::pair<typename KeyToVoxeLookup::iterator, bool> KeyToVoxeLookupInsertReturnType;
    typedef typename KeyToVoxeLookup::value_type KeyToVoxeLookupValueType;
    KeyToVoxeLookupInsertReturnType insert_ret = _key_lookup.insert(KeyToVoxeLookupValueType(key, _voxels.size()));
    if(insert_ret.second) {
      // New Voxel
      _voxels.emplace_back();
      _keys.push_back(key);
      BOOST_ASSERT_MSG(_voxels.size() == _keys.size(), "# of voxels != # of keys");
      BOOST_ASSERT_MSG(_key_lookup.size() == _keys.size(), "KeyToVoxeLookup size is wrong");
      return std::make_pair(&(_voxels.back()), true);
    }
    else {
      // Existing voxel
      return std::make_pair(_voxels.data() + insert_ret.first->second, false);
    }
  }

  /**@brief This attempts to create a new VoxelNode with initialization
   *
   * @param[in] key   discrete location key
   * @param[in] init  initialization data for VoxelNode constructor
   * @return Returns a pair consisting ptr to created VoxelNode (or existing one) and a bool denoting whether the insertion took place.
   */
  template<typename T>
  std::pair<VoxelNodeType*, bool> emplace(const KeyType& key, const T& init) {
    typedef std::pair<typename KeyToVoxeLookup::iterator, bool> KeyToVoxeLookupInsertReturnType;
    typedef typename KeyToVoxeLookup::value_type KeyToVoxeLookupValueType;
    KeyToVoxeLookupInsertReturnType insert_ret = _key_lookup.insert(KeyToVoxeLookupValueType(key, _voxels.size()));
    if(insert_ret.second) {
      // New Voxel
      _voxels.emplace_back(init);
      _keys.push_back(key);
      BOOST_ASSERT_MSG(_voxels.size() == _keys.size(), "# of voxels != # of keys");
      BOOST_ASSERT_MSG(_key_lookup.size() == _keys.size(), "KeyToVoxeLookup size is wrong");
      return std::make_pair(&(_voxels.back()), true);
    }
    else {
      // Existing voxel
      return std::make_pair(_voxels.data() + insert_ret.first->second, false);
    }
  }

  /// clears the contents
  void clear() {
    _voxels.clear();
    _keys.clear();
    _key_lookup.clear();
  }

  /**@brief Erase voxel with the provided key
   *
   * @param key of type KeyType
   * @return the number of elements erased.
   */
  size_type erase(const KeyType& key) {
    typename KeyToVoxeLookup::iterator it = _key_lookup.find(key);
    if (it != _key_lookup.end()) {
      _key_lookup.erase(it);
      _voxels.erase(_voxels.begin() + it->second);
      _keys.erase(_keys.begin() + it->second);
      // Now update _key_lookup for displaced keys
      for (size_type i = it->second; i < _keys.size(); ++i) {
        _key_lookup[_keys[i]] = i;
      }
      return 1;
    }
    else
      return 0;
  }

  iterator erase(iterator first, iterator last) {
    iterator ret_iterator(
        _keys.erase(first.keyIterator(), last.keyIterator()),
        _voxels.erase(first.valueIterator(), last.valueIterator()));
    generateKeyLookup();
    return ret_iterator;
  }

  /// @}

  /// @name Iterators
  /// @{


  /// @return begin iterator
  iterator begin() {return iterator(_keys.begin(),_voxels.begin());}
  /// @return begin const_iterator
  const_iterator begin() const {return const_iterator(_keys.begin(),_voxels.begin());}

  /// @return end iterator
  iterator end() {return iterator(_keys.end(),_voxels.end());}
  /// @return end const_iterator
  const_iterator end() const {return const_iterator(_keys.end(),_voxels.end());}

  /// @return begin const_iterator
  const_iterator cbegin() const {return const_iterator(_keys.cbegin(),_voxels.cbegin());}
  /// @return end const_iterator
  const_iterator cend() const {return const_iterator(_keys.cend(),_voxels.cend());}

  /// @}

  /// @name Inner datastructure
  /// @{

  /// const access to the keys
  const KeyContainer& keys() const {return _keys;}

  /// const access to the voxels
  const VoxelContainer& voxels() const {return _voxels;}

  /// const access to the key to voxel lookup
  const KeyToVoxeLookup& keyToVoxeLookup() const {return _key_lookup;}

  /// @}

private:
  static_assert((std::is_same<typename KeyContainer::size_type, size_type>::value), "size_type of KeyContainer and VoxelContainer is not same");
  static_assert((std::is_same<typename KeyToVoxeLookup::size_type, size_type>::value), "size_type of KeyToVoxeLookup and VoxelContainer is not same");

  VoxelContainer _voxels;
  KeyContainer _keys;
  KeyToVoxeLookup _key_lookup;

  void generateKeyLookup() {
    _key_lookup.clear();
    _key_lookup.reserve(_keys.size());
    for(size_type i = 0; i< _keys.size(); ++i) {
      _key_lookup[_keys[i]] = i;
    }
    BOOST_ASSERT_MSG(_key_lookup.size() == _keys.size(), "KeyToVoxeLookup size is wrong");
  }

  /// Boost serialization
  friend class boost::serialization::access;

  template<class Archive>
  void save(Archive & ar, const unsigned int version) const {
    ar & _voxels;
    ar & _keys;
  }

  template<class Archive>
  void load(Archive & ar, const unsigned int version) {
    ar & _voxels;
    ar & _keys;
    generateKeyLookup();

    BOOST_ASSERT_MSG(_voxels.size() == _keys.size(), "# of voxels != # of keys");
    BOOST_ASSERT_MSG(_key_lookup.size() == _keys.size(), "KeyToVoxeLookup size is wrong");
  }

  BOOST_SERIALIZATION_SPLIT_MEMBER()

};

}  // namespace Naksha

#endif // NAKSHA_HASHED_VECTOR_DATA_STRUCTURE_H_
