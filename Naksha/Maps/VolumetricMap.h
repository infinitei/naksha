/**
 * @file VolumetricMap.h
 * @brief Volumetric Map
 * 
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_VOLUMETRIC_MAP_H_
#define NAKSHA_VOLUMETRIC_MAP_H_

#include "Naksha/Maps/MapTraits.h"
#include "Naksha/Maps/MapCommonBase.h"
#include "Naksha/Keys/Key.h"

#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>

namespace Naksha {

/**
 * @brief Volumetric Map Class
 *
 * This is the host class for several Map policies. Naksha users should try to use
 * this class instead of trying to use the policy classes like
 * UnorderedMapDataStructure, TreeDataStructure, etc.
 *
 * @tparam _Dimension either 2 or 3 for 2D or 3D map
 * @tparam _Payload should be of some \ref Payload type.
 * @tparam _Datastructure \ref MapDatastructure. See UnorderedMapDataStructure, HashedVectorDataStructure, TreeDataStructure, etc
 * @tparam _Interface \ref MapInterface e.g. HybridMappingInterface, etc
 * @tparam _Transform \ref MapTransform e.g MapTransform
 * @tparam _NodeInterface \ref NodeInterface e.g OccupancyNodeInterface (default), RVOccupancyNodeInterface
 *
 * Example of VolumetricMap with UnorderedMapDataStructure policy. See VolumetricMapExampleInstantionA:
 * @code
 *  typedef DiscreteRandomVariable<2, float, ProbabilityTag> Payload;     // Create your Payload type
 *  typedef VolumetricMap<3,Payload,UnorderedMapDataStructure,OccupancyMappingInteface,MapTransform> MapType;  // MapType
 * @endcode
 *
 * Example of VolumetricMap with TreeDataStructure policy. See VolumetricMapExampleInstantionB:
 * @code
 *  typedef DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> Payload;     // Create your Payload type
 *  typedef VolumetricMap<3,Payload,TreeDataStructure,OccupancyMappingInteface,MapTransform> MapType;  // MapType
 * @endcode
 *
 * @ingroup Maps
 */
template<
  int _Dimension,
  class _Payload,
  template<class, class > class _Datastructure,
  template<class > class _Interface,
  template<class, typename > class _Transform,
  template<typename > class _NodeInterface = OccupancyNodeInterface >
class VolumetricMap :
    public _Datastructure< Key<_Dimension>, typename MapVoxelNode<_Dimension, _Payload, _Datastructure, _NodeInterface>::type >,
    public _Interface< VolumetricMap<_Dimension, _Payload, _Datastructure, _Interface, _Transform, _NodeInterface> >,
    public _Transform< Key<_Dimension>, double >,
    public MapCommonBase {

  typedef VolumetricMap<_Dimension, _Payload, _Datastructure, _Interface, _Transform, _NodeInterface> MapType;  ///< Convenience typdef of this Map type

public:
  typedef _Payload Payload; ///< Typedef of the \ref Payload Type of this Map
  typedef Key<_Dimension> KeyType;  ///< Typedef for \ref Keys type of this map
  typedef typename MapVoxelNode<_Dimension, _Payload, _Datastructure, _NodeInterface>::type VoxelNodeType;  ///< Typedef for voxel \ref Nodes type of this map
  typedef _Datastructure< KeyType, VoxelNodeType > Datastructure;  ///< Datastructure policy. See UnorderedMapDataStructure, TreeDataStructure, HashedVectorDataStructure, etc
  typedef _Interface< MapType > Interface;  ///< Interface policy e.g. HybridMappingInterface, etc
  typedef _Transform< KeyType, double > Transform;  ///< Transform policy e.g Map Transform
  typedef typename Transform::Scalar TransformScalar; ///< Scalar Type of Transform

  static const int Dimension = _Dimension; ///< Map dimension either 2(2D map) or 3 (3D map)

  /// @name Constructors
  /// @{

  /**@brief Construct a Volumetric Map
   *
   * @param resolution map resolution defaults to 0.1
   */
  VolumetricMap(const TransformScalar& resolution = 0.1)
      : Transform(resolution) {
  }

  /**@brief Construct a Volumetric Map with size hint
   *
   * @param map_size_hint size hint for the DataStructure policy (It my call reserve)
   * @param resolution map resolution defaults to 0.1
   */
  VolumetricMap(const std::size_t map_size_hint, const TransformScalar& resolution = 0.1)
        : Datastructure(map_size_hint),
          Transform(resolution) {
    }

  /// @}

  /// @name Lookup with Coordinates
  /// @{

  using Datastructure::search;

  /**@brief Search for a Voxel at a coordinate
   *
   * This simply finds the discrete key corresponding to the input
   * coordinate and then searches for the key. If already have
   * the key, the version of search taking key as argument
   * (see \ref LookupWithKeys) is faster.
   *
   * @param[in] coord   coordinate of some point
   * @return ptr to VoxelNodeType at coord (Null if not found)
   */
  template<typename EigenPointType>
  const VoxelNodeType* search(const Eigen::MatrixBase<EigenPointType>& coord) const {
    KeyType key;
    if(this->keyFromCoord(coord, key)) {
      return search(key);
    }
    else
      return 0;
  }

  /**@brief Search for a Voxel at a coordinate (Non const version)
   *
   * This simply finds the discrete key corresponding to the input
   * coordinate and then searches for the key. If already have
   * the key, the version of search taking key as argument
   * (see \ref LookupWithKeys) is faster.
   *
   * @param[in] coord   coordinate of some point
   * @return ptr to VoxelNodeType at coord (Null if not found)
   */
  template<typename EigenPointType>
  VoxelNodeType* search(const Eigen::MatrixBase<EigenPointType>& coord) {
    KeyType key;
    if(this->keyFromCoord(coord, key)) {
      return search(key);
    }
    else
      return 0;
  }


  using Datastructure::find;

  /**@brief Search for a voxel at a coordinate
   *
   * This simply finds the discrete key corresponding to the input
   * coordinate and then searches for the key. If already have
   * the key, the version of find taking key as argument
   * (see \ref LookupWithKeys) is faster.
   *
   * @param[in] coord   coordinate of some point
   * @return const_iterator to the corresponding voxel or end() if not found
   *
   */
  template<typename EigenPointType>
  typename Datastructure::iterator
  find(const Eigen::MatrixBase<EigenPointType>& coord) {
    KeyType key;
    if(keyFromCoord(coord, key)) {
      return find(key);
    }
    else
      return this->end();
  }

  /**@brief Search for a voxel at a coordinate (const Version)
   *
   * This simply finds the discrete key corresponding to the input
   * coordinate and then searches for the key. If already have
   * the key, the version of find taking key as argument
   * (see \ref LookupWithKeys) is faster.
   *
   * @param[in] coord   coordinate of some point
   * @return const_iterator to the corresponding voxel or end() if not found
   *
   */
  template<typename EigenPointType>
  typename Datastructure::const_iterator
  find(const Eigen::MatrixBase<EigenPointType>& coord) const {
    KeyType key;
    if(keyFromCoord(coord, key)) {
      return find(key);
    }
    else
      return this->end();
  }

  /// @}

private:
  static_assert(Dimension == 2 || Dimension == 3, "You can either have 2D or 3D Map");
  static_assert(Dimension == Transform::KeyType::Dimension, "Map Dimension & Key Dimension should be same");

  /// Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & boost::serialization::base_object< Datastructure >(*this);
    ar & boost::serialization::base_object< Interface >(*this);
    ar & boost::serialization::base_object< Transform >(*this);
    ar & boost::serialization::base_object< MapCommonBase >(*this);
  }

};

template<
  int _Dimension,
  class _Payload,
  template<class, class > class _Datastructure,
  template<class > class _Interface,
  template<class, typename > class _Transform,
  template<class > class _NodeInterface>
struct MapTraits<VolumetricMap<_Dimension, _Payload, _Datastructure, _Interface, _Transform, _NodeInterface> > {
  static const int Dimension = _Dimension;
  typedef _Payload Payload;
  typedef VolumetricMap<_Dimension, _Payload, _Datastructure, _Interface, _Transform, _NodeInterface> MapType;
  typedef Key<_Dimension> KeyType;
  typedef typename MapVoxelNode<_Dimension, _Payload, _Datastructure, _NodeInterface>::type VoxelNodeType;
};

} // end namespace Naksha

#endif // NAKSHA_VOLUMETRIC_MAP_H_
