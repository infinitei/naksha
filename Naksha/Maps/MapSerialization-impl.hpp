/**
 * @file MapSerialization-impl.hpp
 * @brief MapSerialization-impl
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_MAP_SERIALIZATION_IMPL_HPP_
#define NAKSHA_MAP_SERIALIZATION_IMPL_HPP_


namespace Naksha {
namespace detail {

template <class MapType>
bool serializeMapCommonBase(const MapCommonBase* map_base_ptr, const std::string& filename) {
  typedef boost::archive::binary_oarchive Archive;
  std::ofstream ofs(filename.c_str(), std::ios::out | std::ios::binary);
  if (!ofs.is_open()) {
    std::cout << "Error: Could not Open " << filename << std::endl;
    return false;
  }
  { // use scope to ensure archive goes out of scope before stream
    Archive oa(ofs);
    oa.template register_type<MapType>();
    oa << map_base_ptr;
  }
  ofs.close();
  std::cout << "Serialized Map as " << filename << " FileSize = "
      << boost::filesystem::file_size(filename) / 1048576. << " MB." << std::endl;
  return true;
}

template <class MapType>
struct SerializeMapImpl {
  static bool save(const MapType& map, const std::string& filename) {
    std::cout << "Serializing Naksha Map (Ref Version).... " << std::endl;
    MapType* map_ptr = const_cast<MapType*>(&map);
    MapCommonBase* map_base_ptr = dynamic_cast<MapCommonBase*>(map_ptr);
    if (!map_base_ptr) {
      std::cout << "Upcast from MapType to MapCommonBase was not possible\n";
      return false;
    }
    return serializeMapCommonBase<MapType>(map_base_ptr, filename);
  }
};

template <class MapType>
struct SerializeMapImpl<MapType*> {
  static bool save(const MapType* map, const std::string& filename) {
    std::cout << "Serializing Naksha Map (Ptr Version).... " << std::endl;
    MapType* map_ptr = const_cast<MapType*>(map);
    MapCommonBase* map_base_ptr = dynamic_cast<MapCommonBase*>(map_ptr);
    if (!map_base_ptr) {
      std::cout << "Upcast from MapType to MapCommonBase was not possible\n";
      return false;
    }
    return serializeMapCommonBase<MapType>(map_base_ptr, filename);
  }
};



} // end namespace detail
} // end namespace Naksha

#endif // NAKSHA_MAP_SERIALIZATION_IMPL_HPP_
