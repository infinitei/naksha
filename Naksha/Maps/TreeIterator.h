/**
 * @file TreeIterator.h
 * @brief TreeIterator
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_TREE_ITERATOR_H_
#define NAKSHA_TREE_ITERATOR_H_

#include <boost/iterator/iterator_facade.hpp>
#include <type_traits>
#include <boost/utility/enable_if.hpp>
#include <stack>
#include <vector>

namespace Naksha {

/// Element on the internal recursion stack of the iterator
template<class NodeType, class KeyType>
struct TreeIteratorBaseStackItem {
  /// Constructor
  TreeIteratorBaseStackItem(NodeType* init_node, const KeyType& int_Key, unsigned char init_depth)
    :node(init_node), key(int_Key), depth(init_depth) {}

  NodeType* node;           ///< ptr to node
  KeyType key;             ///< key of the node
  unsigned char depth;    ///< tree depth of the node
};

/// Base class for Tree Iterators like NodeIterator, LeafIteratoror
template<class Derived, class NodeType, class KeyType>
class TreeIteratorBase: public boost::iterator_facade<
      Derived                            // Derived
    , NodeType                           // Value
    , boost::forward_traversal_tag        // CategoryOrTraversal
> {

private:

  typedef boost::iterator_facade<
      Derived                             // Derived
    , NodeType                            // Value
    , boost::forward_traversal_tag         // CategoryOrTraversal
  > super_t;

protected:

public:


  typedef typename super_t::value_type value_type;  ///< Value type of the iterator == (const striped) NodeType

  typedef TreeIteratorBaseStackItem<value_type, KeyType> StackItem; ///< Iterator Stack Item Type.
  typedef std::stack<StackItem, std::vector<StackItem> > StackType; ///< Iterator Stack Type.

  /// Default Constructor
  TreeIteratorBase() {}

  /// Constructor from Root Node Ptr
  explicit TreeIteratorBase(value_type* root_node_ptr) {
    if(root_node_ptr) {
      typedef typename KeyType::DataType KeyDataType;
      StackItem s(root_node_ptr, KeyType(KeyDataType::Constant(32768)), 0);
      stack_.push(s);
    }
  }

//  /// Constructor from Other Iterators
//  template<class OtherDerived>
//  TreeIteratorBase(OtherDerived const& other)
//    : stack_(other.stack()) {}

  /// Constructor from Stack
  explicit TreeIteratorBase(const StackType& stack)
      : stack_(stack) {}

  /// @return depth of the current node
  unsigned depth() const {
    return unsigned(stack_.top().depth);
  }

  /// @return the key of the current node
  const KeyType& key() const {
    return stack_.top().key;
  }

  /// @return true if the current node is a leaf, i.e. has no children
  bool isLeafNode() const {
    return (!stack_.top().node->hasAnyChildren());
  }

  /// return a const ref to the iterator's underlying stack
  const StackType& stack() const {return stack_;}


protected:

  /// A single step of depth-first tree traversal
  void singleIncrementDepthFirst() {
    StackItem top = stack_.top();
    stack_.pop();

    unsigned short int center_offset_key = 32768 >> (top.depth + 1);
    // push on stack in reverse order
    for (int i = NodeType::MaxNumOfChilds - 1; i >= 0; --i) {
      if (top.node->childExistsAt(i)) {
        StackItem s(top.node->childPtr(i),
            computeChildKey(i, center_offset_key, top.key), top.depth + 1);
        stack_.push(s);
      }
    }
  }

  /// Recursion stack.
  StackType stack_;

private:
  friend class boost::iterator_core_access;
  template <class,class, class> friend class TreeIteratorBase;

  template<class OtherDerived, class OtherNodeType, class OtherKeyType>
  bool equal(TreeIteratorBase<OtherDerived, OtherNodeType, OtherKeyType> const& other) const {
    return (stack_.size() == other.stack_.size()
        && (stack_.size() == 0
            || (stack_.size() > 0
                && (stack_.top().node == other.stack_.top().node
                    && stack_.top().depth == other.stack_.top().depth
                    && stack_.top().key == other.stack_.top().key))));
  }

  NodeType& dereference() const {
    return *stack_.top().node;
  }

  void increment() {
    if (!stack_.empty())
      singleIncrementDepthFirst();
  }

};

/**@brief Node Iterator Class (iterates all Tree Nodes)
 *
 * @tparam NodeType Type of Node
 * @tparam KeyType Type of Key
 */
template<class NodeType, class KeyType>
class NodeIterator: public TreeIteratorBase<
        NodeIterator<NodeType, KeyType>     // Derived
      , NodeType                             // NodeType
      , KeyType                              // KeyType
> {

  struct enabler {};  // a private type avoids misuse

  typedef TreeIteratorBase<
        NodeIterator<NodeType, KeyType>     // Derived
      , NodeType                             // NodeType
      , KeyType                              // KeyType
   > BaseIterator;

public:

  /// Value type of the iterator == (const striped) NodeType
  typedef typename BaseIterator::value_type value_type;

  /// Iterator Stack Type
  typedef typename BaseIterator::StackType StackType;


  /// Default Constructor
  NodeIterator()
    : BaseIterator() {}

  /// Constructor from Root Node Ptr
  explicit NodeIterator(value_type* root_node_ptr)
    : BaseIterator(root_node_ptr) {
  }

  /// Constructor from Other Iterators
  template<class OtherNodeType, class OtherKeyType>
  NodeIterator(NodeIterator<OtherNodeType, OtherKeyType> const& other,
      typename boost::enable_if<
        std::is_convertible<OtherNodeType*, NodeType*>
      , enabler>::type = enabler()
      )
        : BaseIterator(other.stack()) {
  }

  /// Constructor from Stack
  explicit NodeIterator(const StackType& stack)
    : BaseIterator(stack) {
  }

};


/**@brief Leaf Iterator Class (iterates only Leaf Nodes)
 *
 * @tparam NodeType Type of Node
 * @tparam KeyType Type of Key
 */
template<class NodeType, class KeyType>
class LeafIterator: public TreeIteratorBase<
        LeafIterator<NodeType, KeyType>     // Derived
      , NodeType                             // NodeType
      , KeyType                              // KeyType
> {

  struct enabler {};  // a private type avoids misuse

  typedef TreeIteratorBase<
        LeafIterator<NodeType, KeyType>     // Derived
      , NodeType                             // NodeType
      , KeyType                              // KeyType
   > BaseIterator;

public:

  /// Value type of the iterator == (const striped) NodeType
  typedef typename BaseIterator::value_type value_type;

  /// Iterator Stack Type
  typedef typename BaseIterator::StackType StackType;

  /// Default Constructor
  LeafIterator()
    : BaseIterator() {}

  /// Constructor from Root Node Ptr
  explicit LeafIterator(value_type* root_node_ptr)
    : BaseIterator(root_node_ptr) {
    if (this->stack_.size() > 0) {
      // skip forward to next leaf node
      this->stack_.push(this->stack_.top());
      increment();
    }
  }

  /// Constructor from Other Iterators
  template<class OtherNodeType, class OtherKeyType>
  LeafIterator(LeafIterator<OtherNodeType, OtherKeyType> const& other,
      typename boost::enable_if<
        std::is_convertible<OtherNodeType*, NodeType*>
      , enabler>::type = enabler()
      )
        : BaseIterator(other.stack_) {
  }

  /// Constructor from Stack
  explicit LeafIterator(const StackType& stack)
    : BaseIterator(stack) {
  }

private:
  friend class boost::iterator_core_access;
  template <class,class> friend class LeafIterator;

  void increment() {
    if (!this->stack_.empty()) {
      this->stack_.pop();

      while (!this->stack_.empty() && this->stack_.top().node->hasAnyChildren()) {
        this->singleIncrementDepthFirst();
      }
    }
  }
};


}  // end namespace Naksha

#endif // NAKSHA_TREE_ITERATOR_H_
