/**
 * @file MapTransform.h
 * @brief MapTransform class does all index space to world space conversions
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_MAP_TRANSFORM_H_
#define NAKSHA_MAP_TRANSFORM_H_

#include "Naksha/Maps/KeyCoordTransformer.h"
#include <Eigen/Geometry>
#include <boost/config.hpp>

namespace Naksha {

/**
 * @brief MapTransform class does all index space to world space conversions
 *
 * This serves as Transform policy class for Volumetric Map
 *
 * @tparam _KeyType Type of Key
 * @tparam _Scalar Scalar Type usually either a float or double
 *
 * @ingroup MapTransform
 * @ingroup Maps
 */

template<class _KeyType, typename _Scalar = double>
class MapTransform {

public:

  /// Key Type
  typedef _KeyType KeyType;

  /// Map dimension either 2(2D map) or 3 (3D map)
  static const int Dimension = KeyType::Dimension;

  /// Scalar Type of Key (should be Integral type)
  typedef typename KeyType::Scalar KeyScalar;

  /// Scalar Type usually either a float or double
  typedef _Scalar Scalar;

  /// BoundingBox is just a Eigen::AlignedBox
  typedef Eigen::AlignedBox<Scalar, Dimension> BoundingBox;

  /// KeyBoundingBox is just a Eigen::AlignedBox
  typedef Eigen::AlignedBox<KeyScalar, Dimension> KeyBoundingBox;

  /// Preferred VectorType (point type) for the map
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType;

  /// Functor for key <---- coordinate conversion
  typedef KeyCoordTransformer<KeyType, Scalar> KeyCoordTransformerType;


  /// @name Constructors
  /// @{

  /**@brief Default Constructor
   * @param[in] resolution of the map i.e. size of a voxel
   */
  MapTransform(const Scalar& resolution = 0.1);


  /// @}

  /// @name Coordinate/Key conversions
  /// @{

  /// Returns Map resolution
  Scalar resolution() const {return resolution_;}


  /// Return Functor for converting coordinates to discrete Keys
  const KeyCoordTransformerType keyCoordTransformer() const {
    return key_coord_transformer_;
  }

  /**
   * @brief Get a discrete Key address for a coordinate
   *
   * @param[in] coord position/coordinate in continuous domain
   * @return discrete Key for the voxel containing coord
   *
   * @par Example:
   @code
     const Vector3d pos(7, 0, 7);
     Key key = map.keyFromCoord(pos);
   @endcode
   */
  template<typename PointType>
  inline KeyType keyFromCoord(const PointType& coord) const {
    return key_coord_transformer_(coord);
  }

  /**
   * @brief Get a discrete Key address for a coordinate (Check map limits)
   *
   * @param[in] coord position/coordinate in continuous domain
   * @param[out] key discrete Key for the voxel containing coord
   * @return true if coord lies inside possible map size
   *
   * @note This function does a checking if coord lies inside possible map size
   * @par Example:
   @code{.cpp}
     const Vector3f pos(7, 3, 7);
     Key key;
     bool inside = map.keyFromCoord(pos, key);
   @endcode
   */
  template<typename PointType>
  inline bool keyFromCoord(const PointType& coord, KeyType& key) const {
    return key_coord_transformer_(coord, key);
  }


  /**
   * @brief Get Voxel Center coordinate from a Key
   *
   * @param[in] key input voxel key
   * @return coordinate of voxel center as Eigen Vector
   * @par Example:
   @code{.cpp}
     Vector3f center = map.voxelCenter<Vector3f>(key);
     Vector3d center = map.voxelCenter<Vector3d>(key);
     Vector3d center = map.voxelCenter(key);  // C++11 and MapTransform::Scalar = double
   @endcode
   */
#ifdef BOOST_NO_CXX11_FUNCTION_TEMPLATE_DEFAULT_ARGS
  template<typename T>
#else
  template<typename T = Eigen::Matrix<Scalar, Dimension, 1> >
#endif
  inline T voxelCenter(const KeyType& key) const {
#ifdef _MSC_VER
    return key_coord_transformer_.operator()<T>(key);
#else
    return key_coord_transformer_.template operator()<T>(key);
#endif    
  }

  /// @}


  /// @name BoundingBox related methods
  /// @{

  /**@brief Returns a const reference to the Maximum BoundingBox of the map.
   *
   * This usually set by user or default constructed to store the
   * Largest possible bbx. Use currentBbx() to get the map's current
   * bounding box.
   *
   * The return type is BoundingBox which is simply Eigen::AlignedBox.
   * Use this to get other functionalities like contains(), volume()
   *
   * @return Maximum BoundingBox of the map
   */
  const BoundingBox& maximumBbx() const { return maximum_bbx_;}

  /**@brief Returns a const reference to the Maximum KeyBoundingBox of the map.
   *
   * This usually set by user or default constructed to store the
   * Largest possible bbx. Use currentBbx() to get the map's current
   * bounding box.
   *
   * The return type is KeyBoundingBox which is simply Eigen::AlignedBox.
   * Use this to get other functionalities like contains(), volume()
   *
   * @return Maximum KeyBoundingBox of the map
   */
  const KeyBoundingBox& maximumKeyBbx() const { return maximum_key_bbx_;}

  /// @return Min Key of MaximumKeyBoundingBox
  KeyType maximumKeyBbxMin() const { return KeyType(maximum_key_bbx_.min());}

  /// @return Max Key of MaximumKeyBoundingBox
  KeyType maximumKeyBbxMax() const { return KeyType(maximum_key_bbx_.max());}

  /**@brief Check if a Coordinate is inside maximum BoundingBox
   * @param coord coordinate
   * @return true if coord is contained in maximum BoundingBox
   */
  template<typename CoordType>
  bool insideMaximumBbx(const CoordType& coord) const {
    return maximum_bbx_.contains(coord);
  }

  /**@brief Check if a key is inside maximum BoundingBox
   * @param key Discrete KeyType key to be checked
   * @return true if key is contained in maximum BoundingBox
   */
  bool insideMaximumBbx(const KeyType& key) const {
    return maximum_key_bbx_.contains(key.value);
  }

  /**@brief Check if a key is inside maximum BoundingBox
  * @param key Discrete KeyType key to be checked
  * @return true if key is contained in maximum BoundingBox
  */
  bool outsideMaximumBbx(const KeyType& key) const {
    return (maximum_key_bbx_.min().array() > key.value.array()).any() || (maximum_key_bbx_.max().array() < key.value.array()).any();
  }

  /// Set the Map's Maximum BBX from Current Map bbx
  void setMaximumBbxFromCurrentBbx() {
    maximum_bbx_ = current_bbx_;
    maximum_key_bbx_ = current_key_bbx_;
  }

  /**@brief Set the Map's Maximal BBX from min and max coordinates
   *
   * @param[in] min minimum coord of the bounding box
   * @param[in] max maximum coord of the bounding box
   *
   * @return true if successful i.e. input coordinates are within range
   */
  template<typename VectorType1, typename VectorType2>
  bool setMaximumBbx(const VectorType1& min, const VectorType2& max) {
    maximum_bbx_ = BoundingBox(min, max);
    KeyType min_key, max_key;
    if(keyFromCoord(min, min_key) && keyFromCoord(max, max_key)) {
      maximum_key_bbx_.min() = min_key.value;
      maximum_key_bbx_.max() = max_key.value;
      return true;
    }
    else {
      //  TODO Show error?
      return false;
    }
  }


  /**@brief Set the Map's Maximal BBX from min and max keys
   *
   * @param[in] min_key minimum key of the Maximal bounding box
   * @param[in] max_key maximum key of the Maximal bounding box
   */
  void setMaximumBbx(const KeyType& min_key, const KeyType& max_key) {
    maximum_key_bbx_.min() = min_key.value;
    maximum_key_bbx_.max() = max_key.value;
    Scalar half_voxel_length = resolution_ / 2;
    typedef typename BoundingBox::VectorType BbxVectorType;
    maximum_bbx_.min() = voxelCenter<BbxVectorType>(min_key.value) - BbxVectorType::Constant(half_voxel_length);
    maximum_bbx_.max() = voxelCenter<BbxVectorType>(max_key.value) + BbxVectorType::Constant(half_voxel_length);
  }

  /**@brief Returns a const reference to the Current BoundingBox of the map.
   *
   * This is modified as the map grows bigger. Use maximumBbx() to get the
   * (user-set) hard limit of map's bounding box.
   *
   * The return type is BoundingBox which is simply Eigen::AlignedBox.
   * Use this to get other functionalities like contains(), volume()
   *
   * @return Current BoundingBox of the map
   */
  const BoundingBox& currentBbx() const { return current_bbx_;}

  /**@brief Returns a const reference to the Current KeyBoundingBox of the map.
   *
   * This is modified as the map grows bigger. Use maximumBbx() to get the
   * (user-set) hard limit of map's bounding box.
   *
   * The return type is KeyBoundingBox which is simply Eigen::AlignedBox.
   * Use this to get other functionalities like contains(), volume()
   *
   * @return Current KeyBoundingBox of the map
   */
  const KeyBoundingBox& currentKeyBbx() const { return current_key_bbx_;}

  /// @}

private:
  const Scalar resolution_;

  const KeyCoordTransformerType key_coord_transformer_;

  BoundingBox maximum_bbx_; //< Maximum Bounding Box of the Map
  KeyBoundingBox maximum_key_bbx_;  //< Maximum Key Bounding Box of the Map

  /// Current Bounding Box of the Map
  BoundingBox current_bbx_; //< Current Bounding Box of the Map
  KeyBoundingBox current_key_bbx_;  //< Current Key Bounding Box of the Map

  /// Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & const_cast<Scalar &>(resolution_);
    ar & const_cast<KeyCoordTransformerType &>(key_coord_transformer_);

    //TODO: Serialize other members like BBX
  }

};

} // end namespace Naksha

#include "Naksha/Maps/MapTransform-impl.hpp"

#endif // NAKSHA_MAP_TRANSFORM_H_
