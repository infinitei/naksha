/**
 * @file MappingInterfaceBase.h
 * @brief Mapping Interface Base class header
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_MAPPING_INTERFACE_BASE_H_
#define NAKSHA_MAPPING_INTERFACE_BASE_H_

#include "Naksha/Maps/MapTraits.h"

namespace Naksha {

/**@brief Base class for Mapping Inteface Policies
 *
 * Other interface policies like OccupancyMappingInteface derives
 * from this class.
 *
 * @ingroup MapInterface
 * @ingroup Maps
 */
template <class DerivedMap>
class MappingInterfaceBase {
protected:

  /** \returns a reference to the derived object */
  DerivedMap& derived() { return *static_cast<DerivedMap*>(this); }

  /** \returns a const reference to the derived object */
  const DerivedMap& derived() const { return *static_cast<const DerivedMap*>(this); }

  /** \returns a const reference to the derived object */
  inline const DerivedMap& const_derived() const { return *static_cast<const DerivedMap*>(this); }
};

} // end namespace Naksha

#endif // NAKSHA_MAPPING_INTERFACE_BASE_H_
