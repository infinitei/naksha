/**
 * @file MapMaker.h
 * @brief MapMaker
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_MAP_MAKER_H_
#define NAKSHA_MAP_MAKER_H_

#include "Naksha/Maps/VolumetricMap.h"
#include "Naksha/Maps/UnorderedMapDataStructure.h"
#include "Naksha/Maps/ConcurrentUnorderedMapDataStructure.h"
#include "Naksha/Maps/TreeDataStructure.h"
#include "Naksha/Maps/HashedVectorDataStructure.h"
#include "Naksha/Maps/HybridMappingInterface.h"
#include "Naksha/Maps/MapTransform.h"

namespace Naksha {

/**
 * @brief Traits to check if a Payload is Binary Random Variable (Dimension 2)
 *
 * @tparam Payload any \ref Payload types
 */
template<class Payload>
struct IsBinaryRV : std::integral_constant<bool, Payload::RVType::Dimension == 2> {
};

///////////////////////////////////////////////////////////////////////////////

template<class Payload, class Enable = void>
struct UnorderedMap3D {
  typedef VolumetricMap< 3,
                Payload,
                UnorderedMapDataStructure,
                HybridMappingInterface,
                MapTransform > type;
};

template<class Payload>
struct UnorderedMap3D<Payload, typename std::enable_if< IsBinaryRV<Payload>::value >::type > {
  typedef VolumetricMap< 3,
                Payload,
                UnorderedMapDataStructure,
                OccupancyMappingInteface,
                MapTransform > type;
};

///////////////////////////////////////////////////////////////////////////////

template<class Payload, class Enable = void>
struct OctreeMap {
  typedef VolumetricMap< 3,
                Payload,
                TreeDataStructure,
                HybridMappingInterface,
                MapTransform > type;
};

template<class Payload>
struct OctreeMap<Payload, typename std::enable_if< IsBinaryRV<Payload>::value >::type > {
  typedef VolumetricMap< 3,
                Payload,
                TreeDataStructure,
                OccupancyMappingInteface,
                MapTransform > type;
};

///////////////////////////////////////////////////////////////////////////////

template<class Payload, class Enable = void>
struct HashedVectorMap {
  typedef VolumetricMap< 3,
                Payload,
                HashedVectorDataStructure,
                HybridMappingInterface,
                MapTransform > type;
};

template<class Payload>
struct HashedVectorMap<Payload, typename std::enable_if< IsBinaryRV<Payload>::value >::type > {
  typedef VolumetricMap< 3,
                Payload,
                HashedVectorDataStructure,
                OccupancyMappingInteface,
                MapTransform > type;
};


} // namespace Naksha

#endif // NAKSHA_MAP_MAKER_H_
