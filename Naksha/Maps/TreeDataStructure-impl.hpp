/**
 * @file TreeDataStructure-impl.hpp
 * @brief TreeDataStructure-impl
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_TREE_DATA_STRUCTURE_IMPL_HPP_
#define NAKSHA_TREE_DATA_STRUCTURE_IMPL_HPP_

#define UNUSED(x) (void)(x)

namespace Naksha {

////////////////////////////// Number of Nodes //////////////////////////////
template<class KeyType, class VoxelNodeType>
typename TreeDataStructure<KeyType, VoxelNodeType>::size_type
TreeDataStructure<KeyType, VoxelNodeType>::numberOfNodes() const {
  size_type num_nodes (0); // root node
  if (root_) {
    ++num_nodes;
    numberOfNodesRecursive(*root_, num_nodes);
  }
  return num_nodes;
}

template<class KeyType, class VoxelNodeType>
void TreeDataStructure<KeyType, VoxelNodeType>::numberOfNodesRecursive(
    const VoxelNodeType& node, size_type& num_nodes) const {
  if (node.hasAnyChildren()) {
    for (unsigned int i = 0; i < VoxelNodeType::MaxNumOfChilds; ++i) {
      if (node.childExistsAt(i)) {
        ++num_nodes;
        numberOfNodesRecursive(node.child(i), num_nodes);
      }
    }
  }
}

//////////////////////////// Number of Voxels //////////////////////////////

template<class KeyType, class VoxelNodeType>
typename TreeDataStructure<KeyType, VoxelNodeType>::size_type
TreeDataStructure<KeyType, VoxelNodeType>::numberOfVoxels() const {
  return root_ ? numberOfVoxelsRecursive(*root_) : 0;
}

template<class KeyType, class VoxelNodeType>
typename TreeDataStructure<KeyType, VoxelNodeType>::size_type
TreeDataStructure<KeyType, VoxelNodeType>::numberOfVoxelsRecursive(
    const VoxelNodeType& parent) const {

  // Check if this is a leaf node and terminate if true
  if (!parent.hasAnyChildren())
    return 1;

  // Recurse for children of this node and compute the sume
  size_type leafs_in_children_branches = 0;
  for (unsigned int i = 0; i <  VoxelNodeType::MaxNumOfChilds; ++i) {
    if (parent.childExistsAt(i)) {
      leafs_in_children_branches += numberOfVoxelsRecursive(parent.child(i));
    }
  }
  return leafs_in_children_branches;
}

/////////////////////////////// Search ////////////////////////////////////////

template<class KeyType, class VoxelNodeType>
const VoxelNodeType*
TreeDataStructure<KeyType, VoxelNodeType>::search(const KeyType& key) const {
  if (!root_)
    return 0;

  VoxelNodeType* curr_node = root_.get();

  // follow down the tree
  for (int depth_diff = (max_tree_depth_ - 1); depth_diff >= 0; --depth_diff) {
    const unsigned int child_index = computeChildIndex(key, depth_diff);
    if (curr_node->childExistsAt(child_index)) {
      curr_node = curr_node->childPtr(child_index);
    }
    else {
      // we expected a child but did not get it
      // is the current node a leaf already?
      if (!curr_node->hasAnyChildren()) {
        return curr_node;
      }
      else {
        // it is not, search failed
        return 0;
      }
    }
  } // end for
  return curr_node;
}

template<class KeyType, class VoxelNodeType>
VoxelNodeType*
TreeDataStructure<KeyType, VoxelNodeType>::search(const KeyType& key) {
  if (!root_)
    return 0;

  VoxelNodeType* curr_node = root_.get();

  // follow down the tree
  for (int depth_diff = (max_tree_depth_ - 1); depth_diff >= 0; --depth_diff) {
    const unsigned int child_index = computeChildIndex(key, depth_diff);
    if (curr_node->childExistsAt(child_index)) {
      curr_node = curr_node->childPtr(child_index);
    }
    else {
      // we expected a child but did not get it
      // is the current node a leaf already?
      if (!curr_node->hasAnyChildren()) {
        return curr_node;
      }
      else {
        // it is not, search failed
        return 0;
      }
    }
  } // end for
  return curr_node;
}

template<class KeyType, class VoxelNodeType>
typename TreeDataStructure<KeyType, VoxelNodeType>::const_iterator
Naksha::TreeDataStructure<KeyType, VoxelNodeType>::find(const KeyType& key) const {
  if (!root_)
    return end();

  typedef typename const_iterator::StackType StackType;
  typedef typename const_iterator::StackItem StackItem;

  StackType stack;

  VoxelNodeType* curr_node = root_.get();
  typedef typename KeyType::DataType KeyDataType;
  stack.push(StackItem(curr_node, KeyType(KeyDataType::Constant(32768)), 0));  //TODO remove this hard coded value

  // follow down the tree
  for (int depth_diff = (max_tree_depth_ - 1); depth_diff >= 0; --depth_diff) {
    const unsigned int child_index = computeChildIndex(key, depth_diff);

    if (!stack.empty()) {
      StackItem top = stack.top();
      stack.pop();
      unsigned short int center_offset_key = 32768 >> (top.depth + 1);
      for (int i = VoxelNodeType::MaxNumOfChilds - 1; i >= (int) child_index; --i) {
        if (top.node->childExistsAt(i)) {
          StackItem s(top.node->childPtr(i),
              computeChildKey(i, center_offset_key, top.key), top.depth + 1);
          stack.push(s);
        }
      }
    }

    if (curr_node->childExistsAt(child_index)) {
      curr_node = curr_node->childPtr(child_index);
    }
    else {
      // we expected a child but did not get it
      // is the current node a leaf already?
      if (!curr_node->hasAnyChildren()) {
        return const_iterator(stack);
      }
      else {
        // it is not, search failed
        return end();
      }
    }
  } // end for
  return const_iterator(stack);
}


/////////////////////////////// AddNewVoxel//////////////////////////////////
template<class KeyType, class VoxelNodeType>
typename TreeDataStructure<KeyType, VoxelNodeType>::VoxelNodeType*
TreeDataStructure<KeyType, VoxelNodeType>::addNewVoxel(const KeyType& key) {
  if (!root_) {
    root_.reset(new VoxelNodeType());
    ++tree_size_;
  }

  VoxelNodeType* curr_node = root_.get();

  for (int depth_diff = (max_tree_depth_ - 1); depth_diff >= 0; --depth_diff ) {
    const unsigned int child_index = computeChildIndex(key, depth_diff);
    if (!curr_node->childExistsAt(child_index)) {
      const bool child_created  = curr_node->createChild(child_index);
      assert(child_created); UNUSED(child_created);
      ++tree_size_;
    }
    curr_node = curr_node->childPtr(child_index);
  }

  return curr_node;
}

template<class KeyType, class VoxelNodeType>
template<typename T>
VoxelNodeType*
TreeDataStructure<KeyType, VoxelNodeType>::addNewVoxel(const KeyType& key, const T& init) {
  if (!root_) {
    root_.reset(new VoxelNodeType(init));
    ++tree_size_;
  }

  VoxelNodeType* curr_node = root_.get();

  for (int depth_diff = (max_tree_depth_ - 1); depth_diff >= 0; --depth_diff ) {
    const unsigned int child_index = computeChildIndex(key, depth_diff);
    if (!curr_node->childExistsAt(child_index)) {
      const bool child_created  = curr_node->createChild(child_index, init);
      assert(child_created); UNUSED(child_created);
      ++tree_size_;
    }
    curr_node = curr_node->childPtr(child_index);
  }

  return curr_node;
}

////////////////////////////////// emplace ////////////////////////////////////

template<class KeyType, class VoxelNodeType>
std::pair<VoxelNodeType*, bool>
TreeDataStructure<KeyType, VoxelNodeType>::emplace(const KeyType& key) {
  if (!root_) {
    root_.reset(new VoxelNodeType());
    ++tree_size_;
  }

  VoxelNodeType* curr_node = root_.get();
  bool child_created = false;
  for (int depth_diff = (max_tree_depth_ - 1); depth_diff >= 0; --depth_diff ) {
    const unsigned int child_index = computeChildIndex(key, depth_diff);
    if (!curr_node->childExistsAt(child_index)) {
      child_created  = curr_node->createChild(child_index);
      assert(child_created);
      ++tree_size_;
    }
    curr_node = curr_node->childPtr(child_index);
  }

  return std::make_pair(curr_node, child_created);
}

template<class KeyType, class VoxelNodeType>
template<typename T>
std::pair<VoxelNodeType*, bool>
TreeDataStructure<KeyType, VoxelNodeType>::emplace(const KeyType& key, const T& init) {
  if (!root_) {
    root_.reset(new VoxelNodeType(init));
    ++tree_size_;
  }

  VoxelNodeType* curr_node = root_.get();
  bool child_created = false;
  for (int depth_diff = (max_tree_depth_ - 1); depth_diff >= 0; --depth_diff ) {
    const unsigned int child_index = computeChildIndex(key, depth_diff);
    if (!curr_node->childExistsAt(child_index)) {
      child_created  = curr_node->createChild(child_index, init);
      assert(child_created);
      ++tree_size_;
    }
    curr_node = curr_node->childPtr(child_index);
  }

  return std::make_pair(curr_node, child_created);
}



/////////////////////////////// Erase/deleteNode //////////////////////////////

template<class KeyType, class VoxelNodeType>
typename TreeDataStructure<KeyType, VoxelNodeType>::size_type
TreeDataStructure<KeyType, VoxelNodeType>::erase(const KeyType& key) {
  return deleteNode(key);
}

template<class KeyType, class VoxelNodeType>
typename TreeDataStructure<KeyType, VoxelNodeType>::size_type
TreeDataStructure<KeyType, VoxelNodeType>::deleteNode(const KeyType& key) {
  if (!root_)
    return 0;

  typedef std::vector<VoxelNodeType*> NodePtrs;
  typedef std::vector<unsigned int> ChildIndices;

  NodePtrs nodes;
  ChildIndices child_indices;

  nodes.reserve(max_tree_depth_);
  child_indices.reserve(max_tree_depth_);

  nodes.push_back(root_.get());

  // follow down the tree
  for (int depth_diff = (max_tree_depth_ - 1); depth_diff >= 0; --depth_diff) {
    const unsigned int child_index = computeChildIndex(key, depth_diff);
    const typename NodePtrs::value_type& curr_node = nodes.back();
    if (curr_node->childExistsAt(child_index)) {
      nodes.push_back(curr_node->childPtr(child_index));
      child_indices.push_back(child_index);
    }
    else {
      // we expected a child but did not get it
      // is the current node a leaf already?
      if (!curr_node->hasAnyChildren()) {
        break;
      }
      else {
        // it is not, search failed
        return 0;
      }
    }
  } // end for

  nodes.pop_back();
  assert(nodes.size()== child_indices.size());

  typename NodePtrs::reverse_iterator node_it = nodes.rbegin();
  typename ChildIndices::reverse_iterator index_it = child_indices.rbegin();
  for (; (node_it != nodes.rend()) && (index_it != child_indices.rend()); ++node_it, ++index_it) {
    typename NodePtrs::value_type& curr_node = *node_it;
    curr_node->deleteChild(*index_it);
    if (curr_node->hasAnyChildren())
      break;
  }

  if(!root_->hasAnyChildren()) {
    root_.reset();
  }
  return 1;
}

} // namespace Naksha

#undef UNUSED
#endif // NAKSHA_TREE_DATA_STRUCTURE_IMPL_HPP_
