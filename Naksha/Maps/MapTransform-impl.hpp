/**
 * @file MapTransform-impl.hpp
 * @brief Implementation of Transform
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_MAP_TRANSFORM_IMPL_HPP_
#define NAKSHA_MAP_TRANSFORM_IMPL_HPP_

namespace Naksha {

template<class KeyType, typename Scalar>
MapTransform<KeyType, Scalar>::MapTransform(const Scalar& resolution)
  : resolution_(resolution),
    key_coord_transformer_(resolution) {

  // Initialize the maximal Bounding Box
  typedef typename KeyType::DataType KeyDataType;
  KeyType min_key(KeyDataType::Constant(std::numeric_limits<KeyScalar>::min() + 1));
  KeyType max_key(KeyDataType::Constant(std::numeric_limits<KeyScalar>::max() - 1));
  setMaximumBbx(min_key, max_key);
}

}// end namespace Naksha

#include "Naksha/Common/BoostSerializationTraits.h"

BOOST_CLASS_IMPLEMENTATION_PP(Naksha::MapTransform, (class)(class), object_serializable)
BOOST_CLASS_TRACKING_PP(Naksha::MapTransform, (class)(class), track_never)

#endif // NAKSHA_MAP_TRANSFORM_IMPL_HPP_
