/**
 * @file UnorderedMapDataStructure.h
 * @brief Simple Map (unordered_map) based DataStructure
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_UNORDERED_MAP_DATA_STRUCTURE_H_
#define NAKSHA_UNORDERED_MAP_DATA_STRUCTURE_H_

#include "Naksha/Keys/KeyHash.h"
#include "Naksha/Common/MakeAlignedContainer.h"
#include "Naksha/Common/unordered_map_serialization.h"
#include "Naksha/Common/TransformMapIterator.h"
#include <boost/function.hpp>


namespace Naksha {

/**@brief Unordered map based DataStructure Policy
 *
 * This class uses an unordered associative container as datastructure
 * policy to store voxels of a Map. UnorderedMapDataStructure currently uses
 * boost::unordered_map<KeyType, VoxelNodeType> as the underlying
 * datastructure, where KeyType is some \ref Keys and VoxelNodeType is
 * OccupancyNodeNoChild with some \ref Payload.
 *
 * This class is intended to be uses as DataStructure Policy class
 * for higher level mapping class like VolumetricMap. Either use
 * OctreeMapDataStructure as the DataStructure policy template parameter
 * or use some typedefs like GridMap3D.
 *
 * @tparam _KeyType         Type of Key
 * @tparam _VoxelNodeType   Type of Voxel Node
 *
 * @see TreeDataStructure, HashedVectorDataStructure for another MapDatastructure.
 *
 * @ingroup MapDatastructure
 * @ingroup Maps
 */
template<class _KeyType, class _VoxelNodeType>
class UnorderedMapDataStructure {

public:

  typedef _KeyType KeyType; ///< Typedef for \ref Keys type of this map
  typedef _VoxelNodeType VoxelNodeType; ///< Typedef for VoxelNode Type
  typedef KeyHash KeyHashType; ///< KeyHash Type
  typedef typename MakeBoostUnorderedMap<KeyType, VoxelNodeType, KeyHashType>::type DataType; ///< Underlying Data Type

  typedef typename DataType::size_type size_type;  ///< size_type

  typedef TransformMapIterator<typename DataType::iterator> iterator; ///< iterator over voxel nodes
  typedef TransformMapIterator<typename DataType::const_iterator> const_iterator; ///< const iterator over voxel nodes


  /// @name Constructors
  /// @{

  /// Default Constructor
  explicit UnorderedMapDataStructure() {
  }

  /// @}

  /// @name Size and capacity
  /// @{

  /// @return number of voxels (size of the HashMap)
  size_type size() const {return map_data_.size();}

  /// @return number of voxels (same as size)
  size_type numberOfVoxels() const {return map_data_.size();}

  /**@brief reserves storage
   * Increase the capacity of the container to a value that's greater
   * or equal to new_cap
   *
   * @param new_cap
   */
  void reserve(size_type new_cap) {
    map_data_.reserve(new_cap);
  }

  /// @}


  /// @name Lookup with Keys
  /// @anchor LookupWithKeys
  /// @{

  /** Search for a voxel at a key
   *
   * @param[in] key
   * @return ptr to VoxelNodeType (Null if not found)
   */
  const VoxelNodeType* search(const KeyType& key) const {
    typename DataType::const_iterator it = map_data_.find(key);
    if (it != map_data_.end())
      return &(it->second);
    else
      return 0;
  }

  /** Search for a voxel at a key (non const version)
   *
   * @param[in] key
   * @return ptr to VoxelNodeType (Null if not found)
   */
  VoxelNodeType* search(const KeyType& key) {
    typename DataType::iterator it = map_data_.find(key);
    if (it != map_data_.end())
      return &(it->second);
    else
      return 0;
  }

  /** Search for a voxel at a key
   *
   * @param[in] key to look for
   * @return iterator to the corresponding voxel or end() if not found
   */
  iterator find(const KeyType& key) {
    return iterator(map_data_.find(key));
  }

  /** Search for a voxel at a key (const version)
   *
   * @param[in] key to look for
   * @return const_iterator to the corresponding voxel or end() if not found
   */
  const_iterator find(const KeyType& key) const {
    return const_iterator(map_data_.find(key));
  }

  /// @}

  /// @name Modifiers
  /// @{

  /**
   * This attempts to create a new default constructed VoxelNode
   * at location key
   *
   * @param[in] key discrete location
   * @return ptr to created VoxelNode (or existing one)
   */
  VoxelNodeType* addNewVoxel(const KeyType& key) {
    return &map_data_[key];
  }

  /**
   * This attempts to create a new VoxelNode with initialization
   * at location key
   *
   * @param[in] key discrete location
   * @param[in] init initialization data for VoxelNode constructor
   * @return ptr to created VoxelNode (or existing one)
   */
  template<typename T>
  VoxelNodeType* addNewVoxel(const KeyType& key, const T& init) {
    std::pair<typename DataType::iterator, bool> ret = map_data_.emplace(key, init);
    return &(ret.first->second);
  }

  /**@brief Add a default constructed NewVoxel
   *
   * @param[in] key
   * @return Returns a pair consisting ptr to created VoxelNode (or existing one) and a bool denoting whether the insertion took place.
   */
  std::pair<VoxelNodeType*, bool> emplace(const KeyType& key) {
    std::pair<typename DataType::iterator, bool> ret = map_data_.emplace(key, VoxelNodeType());
    return  std::make_pair(&(ret.first->second), ret.second);
  }

  /**@brief This attempts to create a new VoxelNode with initialization
   *
   * @param[in] key   discrete location key
   * @param[in] init  initialization data for VoxelNode constructor
   * @return Returns a pair consisting ptr to created VoxelNode (or existing one) and a bool denoting whether the insertion took place.
   */
  template<typename T>
  std::pair<VoxelNodeType*, bool> emplace(const KeyType& key, const T& init) {
    std::pair<typename DataType::iterator, bool> ret = map_data_.emplace(key, init);
    return  std::make_pair(&(ret.first->second), ret.second);
  }


  /// clears the contents
  void clear() {
    map_data_.clear();
  }

  /**@brief Erase all elements with key equivalent to key
   *
   * @param key of type KeyType
   * @return the number of elements erased.
   */
  size_type erase(const KeyType& key) {
    return map_data_.erase(key);
  }

  /// erase at some iterator position
  iterator erase(const const_iterator& iter) {
    return iterator(map_data_.erase(iter.base()));
  }

  /// @}

  /// @name Iterators
  /// @{


  /// @return begin iterator
  iterator begin() {return iterator(map_data_.begin());}
  /// @return begin const_iterator
  const_iterator begin() const {return const_iterator(map_data_.begin());}

  /// @return end iterator
  iterator end() {return iterator(map_data_.end());}
  /// @return end const_iterator
  const_iterator end() const {return const_iterator(map_data_.end());}

  /// @return begin const_iterator
  const_iterator cbegin() const {return const_iterator(map_data_.cbegin());}
  /// @return end const_iterator
  const_iterator cend() const {return const_iterator(map_data_.cend());}

  /// @}

  /// @name UnorderedMap DataStructure specific
  /// @{

  /// Return const reference to Underlying Data
  const DataType& data() const { return map_data_;}
  /// Return reference to Underlying Data
  DataType& data() { return map_data_;}

  /// @}

private:
  DataType map_data_;


  /// Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & map_data_;
  }
};

} // end namespace Naksha
#endif // end NAKSHA_UNORDERED_MAP_DATA_STRUCTURE_H_
