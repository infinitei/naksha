/**
 * @file MapCommonBase.h
 * @brief MapCommonBase
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_MAPCOMMON_BASE_H_
#define NAKSHA_MAPCOMMON_BASE_H_

#include <boost/serialization/access.hpp>

namespace Naksha {

class MapCommonBase {
public:
  MapCommonBase() {}
  virtual ~MapCommonBase() {}

private:
  // Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
  }

};

} // end namespace Naksha

#endif // NAKSHA_MAPCOMMON_BASE_H_
