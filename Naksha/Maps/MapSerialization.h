/**
 * @file MapSerialization.h
 * @brief MapSerialization
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_MAP_SERIALIZATION_H_
#define NAKSHA_MAP_SERIALIZATION_H_

#include "Naksha/Common/SerializationHelper.h"
#include <boost/serialization/export.hpp>
#include <boost/filesystem.hpp>

namespace Naksha {

// Fwd Declaration of SaveMapImpl
namespace detail {
template <class MapType>
struct SerializeMapImpl;
} // end namespace detail

// Fwd Declaration of MapCommonBase
class MapCommonBase;

/**@brief serialize a Naksha Map
 *
 * @param[in] map (Either by Reference or Pointer)
 * @param[in] filename to save to
 * @return true if successfully saved
 *
 * @ingroup Maps
 */
template<class MapType>
bool serializeMap(const MapType& map, const std::string& filename) {
  return detail::SerializeMapImpl<MapType>::save(map, filename);
}

/**@brief deSerialize (Load) a Naksha Map
 *
 * @param[in] filename to deSerialize from
 * @return ptr to MapType. (NULL if unsuccesfull)
 *
 * @tparam MapType
 *
 * @ingroup Maps
 */
template<class MapType>
MapType* deSerializeMap(const std::string& filename) {
  std::cout << "Deserializing Naksha Map .... " << std::endl;

  MapCommonBase* map_base_ptr;

  typedef boost::archive::binary_iarchive Archive;
  std::ifstream ifs(filename.c_str(), std::ios::in | std::ios::binary);
  if (!ifs.is_open()) {
    std::cout << "Error: Could not Open " << filename << std::endl;
    return 0;
  }
  { // use scope to ensure archive goes out of scope before stream
    Archive ia(ifs);
    ia.template register_type<MapType>();
    ia >> map_base_ptr;
  }
  ifs.close();
  std::cout << "Loaded Map from " << filename << " FileSize = "
      << boost::filesystem::file_size(filename) / 1048576. << " MB." << std::endl;

  MapType* map_ptr = dynamic_cast<MapType*>(map_base_ptr);
  if(!map_ptr) {
    std::cout << "DownCast from MapCommonBase to MapType was not possible\n";
  }
  return map_ptr;
}

} // end namespace Naksha

#include "Naksha/Maps/MapSerialization-impl.hpp"

#endif // NAKSHA_MAP_SERIALIZATION_H_
