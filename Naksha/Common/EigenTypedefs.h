/**
 * @file EigenTypedefs.h
 * @brief Useful Eigen typedefs
 *
 * @author Abhijit Kundu
 */

#ifndef EIGEN_TYPEDEFS_H
#define EIGEN_TYPEDEFS_H

#include <Eigen/Geometry>
#include <Eigen/StdVector>



#include <vector>
#include <list>
#include <map>

// TODO: Use b for all unsigned char Eigen matrices
namespace Eigen {
  typedef Matrix<unsigned char, 3, 1> Vector3b;
  typedef Matrix<unsigned short int, 3, 1> Vector3s;
}

/*
 * Templated typedefs of Eigen Transform for SE3
 * Currently uses the Struct hack (No need for this in C++11)
 * 
 * Usage example:
 *  SE3<float>::type my_float_se3;
 *  typedef SE3<double>::type SE3d
 * 
 */
template <typename T>
struct EigenSE3 {
  typedef Eigen::Transform< T, 3, Eigen::AffineCompact > type;
};

typedef EigenSE3<double>::type SE3d;
typedef EigenSE3<float>::type SE3f;

/*
 * Templated typedefs of Eigen STL containers
 * Currently uses the Struct hack (No need for this in C++11)
 * 
 * Usage example:
 *  EigenAligned<Vector2d>::std_vector my_vector_of_Vector2d
 *  EigenAligned<Matrxix4d>::std_list my_list_of_Matrix4d
 * 
 */
template <typename T>
struct EigenAligned {
  typedef std::vector<T, Eigen::aligned_allocator<T> > std_vector;
  typedef std::list<T, Eigen::aligned_allocator<T> > std_list;
};

/*
 * Templated typedefs of Eigen Matrix for storing 3D point cloud data
 * Currently uses the Struct hack (No need for this in C++11)
 *
 */

template <typename T>
struct EigenPointCloudT {
  typedef Eigen::Matrix< T, 3, Eigen::Dynamic, Eigen::RowMajor> type;
};

typedef EigenPointCloudT<double>::type EigenPointCloudd;
typedef EigenPointCloudT<float>::type EigenPointCloudf;

/*

 * Naksha Typedefs


typedef RAW_DATA_TYPE RawDataType;

// These definitions should change based on RawDataType i.e double/float
typedef Eigen::Matrix<RawDataType, 2, 1> Vector2;
typedef Eigen::Matrix<RawDataType, 3, 1> Vector3;
typedef Eigen::Matrix<RawDataType, 4, 1> Vector4;
typedef Eigen::Matrix<RawDataType, 1, 3> RowVector3;

typedef Eigen::Matrix<RawDataType, 3, 3> Matrix3;
typedef Eigen::Transform<RawDataType,3,Eigen::Affine> Affine3;
typedef Eigen::Transform<RawDataType,3,Eigen::Isometry> Isometry3;
typedef Eigen::Quaternion<RawDataType> Quat;
typedef Eigen::Matrix<RawDataType, 4, 4> Matrix4;
typedef Eigen::AlignedBox<RawDataType, 3> Bbx3;

typedef EigenSE3<RawDataType>::type Pose3;
typedef EigenPointCloudT<RawDataType>::type EigenPointCloud;

typedef Eigen::Vector3b EigenColor;

typedef EigenAligned<Vector2>::std_vector Vec2DPoints;
typedef EigenAligned<Vector3>::std_vector Vec3DPoints;
typedef EigenAligned<EigenColor>::std_vector VecColors;
typedef Eigen::Matrix<RawDataType, 3, 4> ProjectionMatrix;*/

#endif // EIGEN_TYPEDEFS_H





