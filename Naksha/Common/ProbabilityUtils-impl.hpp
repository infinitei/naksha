/**
 * @file ProbabilityUtils-impl.hpp
 * @brief 
 * 
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_PROBABILITY_UTILS_IMPL_HPP_
#define NAKSHA_PROBABILITY_UTILS_IMPL_HPP_

namespace Naksha {

namespace detail {
/////////////////////   RVConversionImpl     ////////////////////////////

//! The macro is used for enable_if if T is an floating point type
#define ENABLE_IF_FLOATING_TYPE(T)\
    typename std::enable_if< std::is_floating_point<T>::value >::type

/// A completely Generic Implementation
template<class T, class ProbTypeTo, class ProbTypeFrom, class Enable>
struct RVConversionImpl {
  typedef T ReturnType;
  static ReturnType convert(const T& value) {
    return value;
  }
};

/// same <-- same (Any T)
template<class T, class ProbType>
struct RVConversionImpl<T, ProbType, ProbType> {
  typedef T ReturnType;
  static ReturnType convert(const T& value) {
    return value;
  }
};


/// LogProbability <-- Probability
template<class T, class Enable>
struct RVConversionImpl<T, LogProbabilityTag, ProbabilityTag, Enable > {
  typedef T ReturnType;
  static ReturnType convert(const T& prob) {
    return std::log(prob);
  }
};
/// LogProbability <-- Probability (specialized for Eigen Types)
template<class T>
struct RVConversionImpl<T, LogProbabilityTag, ProbabilityTag, ENABLE_IF_EIGEN_TYPE(T) > {
  typedef typename T::PlainObject ReturnType;
  static ReturnType convert(const T& prob) {
    return prob.array().log();
  }
};

/// LogOdds <-- Probability
template<class T, class Enable>
struct RVConversionImpl<T, LogOddsTag, ProbabilityTag, Enable > {
  typedef T ReturnType;
  static ReturnType convert(const T& prob) {
    return logOddsFromProb(prob);
  }
};
/// LogOdds <-- Probability (specialized for Eigen Types)
template<class T>
struct RVConversionImpl<T, LogOddsTag, ProbabilityTag, ENABLE_IF_EIGEN_TYPE(T) > {
  typedef typename T::PlainObject ReturnType;
  static ReturnType convert(const T& prob) {
    return prob.cwiseQuotient(T::Ones() - prob).array().log();
  }
};


/// Probability <-- LogProbability
template<class T, class Enable>
struct RVConversionImpl<T, ProbabilityTag, LogProbabilityTag, Enable > {
  typedef T ReturnType;
  static ReturnType convert(const T& log_prob) {
    return std::exp(log_prob);
  }
};
/// Probability <-- LogProbability (specialized for Eigen Types)
template<class T>
struct RVConversionImpl<T, ProbabilityTag, LogProbabilityTag, ENABLE_IF_EIGEN_TYPE(T) > {
  typedef typename T::PlainObject ReturnType;
  static typename T::PlainObject convert(const T& log_prob) {
    return log_prob.array().exp();
  }
};


/// LogOdds <-- LogProbability
template<class T, class Enable>
struct RVConversionImpl<T, LogOddsTag, LogProbabilityTag, Enable> {
  typedef T ReturnType;
  static ReturnType convert(const T& log_prob) {
    return convertRandomVariable(
        convertRandomVariable(log_prob, ProbabilityTag(), LogProbabilityTag()),
        LogOddsTag(), ProbabilityTag());
  }
};



/// Probability <-- LogOdds
template<class T, class Enable>
struct RVConversionImpl<T, ProbabilityTag, LogOddsTag, Enable> {
  typedef T ReturnType;
  static ReturnType convert(const T& log_odds) {
    return probFromLogOdds(log_odds);
  }
};
/// Probability <-- LogOdds (specialized for Eigen Types)
template<class T>
struct RVConversionImpl<T, ProbabilityTag, LogOddsTag, ENABLE_IF_EIGEN_TYPE(T)> {
  typedef typename T::PlainObject ReturnType;
  static ReturnType convert(const T& log_odds) {
    return T::Ones().array()- (log_odds.array().exp() + T::Ones().array()).cwiseInverse();
  }
};


/// LogProbability <-- LogOdds
template<class T, class Enable>
struct RVConversionImpl<T, LogProbabilityTag, LogOddsTag, Enable> {
  typedef T ReturnType;
  static ReturnType convert(const T& log_odds) {
    return std::log(probFromLogOdds(log_odds));
  }
};
/// LogProbability <-- LogOdds (specialized for Eigen Types)
template<class T>
struct RVConversionImpl<T, LogProbabilityTag, LogOddsTag, ENABLE_IF_EIGEN_TYPE(T)> {
  typedef typename T::PlainObject ReturnType;
  static ReturnType convert(const T& log_odds) {
    return (T::Ones().array()- (log_odds.array().exp() + T::Ones().array()).cwiseInverse()).log();
  }
};

/////////////////   RVNormalizeMultiplyImpl  ///////////////////////////

// A completely Generic Implementation
template<class T, class ProbType, class Enable>
struct RVNormalizeMultiplyImpl {
  static void multiply( T& value, const T& meas ) {
  }
  static void normalize(T& value) {
  }
};


// Specialization for ProbabilityTag + EigenTypes
template<class T>
struct RVNormalizeMultiplyImpl<T, ProbabilityTag, ENABLE_IF_EIGEN_TYPE(T) > {

  static void multiply(T const& value, const T& prob) {
    const_cast<T&>(value) = value.cwiseProduct(prob);
  }

  static void normalize(T const& value) {
    const_cast<T&>(value) /= value.sum();
  }
};


// Specialization for LogProbabilityTag + Eigen Types
template<class T>
struct RVNormalizeMultiplyImpl<T, LogProbabilityTag, ENABLE_IF_EIGEN_TYPE(T) > {

  static void multiply(T const& value, const T& log_prob) {
    const_cast<T&>(value) += log_prob;
  }

  static void normalize(T const& value) {
    const_cast< T& >(value).array() -= logSumExp(value);
  }
};


// Specialization for LogOddsTag + Eigen Types
template<class T>
struct RVNormalizeMultiplyImpl<T, LogOddsTag, ENABLE_IF_EIGEN_TYPE(T) > {
  static void multiply(T const& value, const T& log_odds) {
    const_cast<T&>(value) += log_odds;
  }

  static void normalize(T const& value) {
  }
};

//////////////////////////////////////////////////////////////////////////


//////////////////////////// modeOfRandomVariable ///////////////////////

template<class T>
struct RVModeImpl<T, ProbabilityTag, ENABLE_IF_FLOATING_TYPE(T) > {
  static int mode( const T& value ) {
    if (value < static_cast<T>(0.5))
      return 0;
    else
      return 1;
  }
};

template<class T>
struct RVModeImpl<T, LogProbabilityTag, ENABLE_IF_FLOATING_TYPE(T) > {
  static int mode( const T& value ) {
    if (value < static_cast<T>(-0.69314718056)) // std::log(0.5) = -0.69314718056
      return 0;
    else
      return 1;
  }
};

template<class T>
struct RVModeImpl<T, LogOddsTag, ENABLE_IF_FLOATING_TYPE(T) > {
  static int mode( const T& value ) {
    if (value < 0)  // logOdds(0.5) = 0
      return 0;
    else
      return 1;
  }
};


// Specialization for EigenTypes
template<class T, class ProbType>
struct RVModeImpl<T, ProbType, ENABLE_IF_EIGEN_TYPE(T) > {
  static int mode( const T& value ) {
    int mode_label;
    value.maxCoeff(&mode_label);
    return mode_label;
  }
};

} // end namespace detail

} // end namespace Naksha


#endif // NAKSHA_PROBABILITY_UTILS_IMPL_HPP_
