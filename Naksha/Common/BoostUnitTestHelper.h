/**
 * @file BoostUnitTestHelper.h
 * @brief BoostUnitTestHelper
 *
 * @author Abhijit Kundu
 */

#ifndef BOOST_UNIT_TEST_HELPER_H_
#define BOOST_UNIT_TEST_HELPER_H_


#define CHECK_CLOSE_EIGEN_MATRIX(aa, bb, tolerance) { \
    BOOST_REQUIRE_EQUAL(aa.size(), bb.size()); \
    for (std::size_t i = 0, size = aa.size(); i < size; ++i) { \
      BOOST_CHECK_CLOSE(*(aa.data() + i), *(bb.data() + i), tolerance); \
    } \
}

#endif // BOOST_UNIT_TEST_HELPER_H_
