/**
 * @file EigenUtils.h
 * @brief Useful Eigen utils
 *
 * @author Abhijit Kundu
 */

#ifndef EIGEN_UTILS_H
#define EIGEN_UTILS_H

#include <Eigen/Core>
#include <fstream>


namespace Eigen { 

template <typename Derived>
inline std::istream& operator>>(std::istream& is, Eigen::MatrixBase<Derived> const & rhs) {
  typedef typename Derived::Scalar Scalar;  
  
  Scalar data[rhs.size()];
  
  for(int i = 0; i < rhs.size(); ++i)
        is >> data[i];
  const_cast< Eigen::MatrixBase<Derived>& >(rhs) =  Eigen::Map< Eigen::Matrix<Scalar, Dynamic, Dynamic, RowMajor> > (data, rhs.rows(), rhs.cols());
  
  return is;
}

/**
 * makeSkewSymmetric() creates a skew-symmetric matrix (3x3) from vector (3)
 * @param vec vector of length 3
 * @param out 3x3 skew symmetric matrix
 */
template <typename Derived>
inline Matrix<typename Derived::Scalar, 3, 3> makeSkewSymmetric(const MatrixBase<Derived>& vec)
{
  EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(Derived, 3);
  Matrix<typename Derived::Scalar, 3, 3> out;
  out <<     0, -vec[2],  vec[1],
          vec[2],     0, -vec[0],
         -vec[1],  vec[0],     0;

  return out;
}

}// namespace Eigen

#endif // EIGEN_UTILS_H





