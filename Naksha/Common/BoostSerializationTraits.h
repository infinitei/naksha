/**
 * @file BoostSerializationTraits.h
 * @brief BoostSerializationTraits
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_BOOST_SERIALIZATION_TRAITS_H_
#define NAKSHA_BOOST_SERIALIZATION_TRAITS_H_

#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>
#include <boost/serialization/is_bitwise_serializable.hpp>

#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/arithmetic/sub.hpp>
#include <boost/preprocessor/seq/enum.hpp>
#include <boost/preprocessor/seq/transform.hpp>
#include <boost/preprocessor/seq/size.hpp>
#include <boost/preprocessor/repetition/enum_params.hpp>


#define PARAM_NAME param

#define PARAM(Index) BOOST_PP_CAT(PARAM_NAME, Index)

#define PARAM_DESCRIPTION(Index, Data, ParamType) \
    ParamType PARAM(BOOST_PP_SUB(Index, 2))

#define BOOST_IS_BITWISE_SERIALIZABLE_PP(TemplateClass, Params) \
namespace boost {                                     \
namespace serialization {                             \
template \
    < \
        BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_TRANSFORM(PARAM_DESCRIPTION,, Params)) \
    > \
struct is_bitwise_serializable \
    < \
        TemplateClass \
            < \
                BOOST_PP_ENUM_PARAMS(BOOST_PP_SEQ_SIZE(Params), PARAM_NAME) \
            > \
    > \
    : boost::mpl::true_ {}; \
}} \
/**/


#define BOOST_CLASS_IMPLEMENTATION_PP(TemplateClass, Params, E) \
namespace boost {                                     \
namespace serialization {                             \
template \
    < \
        BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_TRANSFORM(PARAM_DESCRIPTION,, Params)) \
    > \
struct implementation_level_impl \
    < \
        const TemplateClass \
            < \
                BOOST_PP_ENUM_PARAMS(BOOST_PP_SEQ_SIZE(Params), PARAM_NAME) \
            > \
    > { \
    typedef mpl::integral_c_tag tag;                                        \
    typedef mpl::int_< E > type;                            \
    BOOST_STATIC_CONSTANT(                                                   \
        int,                                                                 \
        value = implementation_level_impl::type::value                            \
    );                                                                       \
};                                                                           \
}} \
/**/


// The STATIC_ASSERT is prevents one from setting tracking for a primitive type.
// This almost HAS to be an error.  Doing this will effect serialization of all
// char's in your program which is almost certainly what you don't want to do.
// If you want to track all instances of a given primitive type, You'll have to
// wrap it in your own type so its not a primitive anymore.  Then it will compile
// without problem.
#define BOOST_CLASS_TRACKING_PP(TemplateClass, Params, E) \
namespace boost {                                     \
namespace serialization {                             \
template \
    < \
        BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_TRANSFORM(PARAM_DESCRIPTION,, Params)) \
    > \
struct tracking_level \
    < \
        TemplateClass \
            < \
                BOOST_PP_ENUM_PARAMS(BOOST_PP_SEQ_SIZE(Params), PARAM_NAME) \
            > \
    > { \
    typedef mpl::integral_c_tag tag;                                        \
    typedef mpl::int_< E > type;                                            \
    BOOST_STATIC_CONSTANT(                                                   \
        int,                                                                 \
        value = tracking_level::type::value                                   \
    );                                                                       \
     /* tracking for a class  */              \
    BOOST_STATIC_ASSERT((                    \
        mpl::greater<                        \
            /* that is a prmitive */         \
            implementation_level  \
            < \
              TemplateClass \
                < \
                  BOOST_PP_ENUM_PARAMS(BOOST_PP_SEQ_SIZE(Params), PARAM_NAME) \
                > \
            >, \
            mpl::int_<primitive_type>        \
        >::value                             \
    ));                                      \
};                                           \
}} \
/**/

#endif // NAKSHA_BOOST_SERIALIZATION_TRAITS_H_
