/**
 * @file EigenTraits.h
 * @brief EigenTraits
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_EIGEN_TRAITS_H_
#define NAKSHA_EIGEN_TRAITS_H_

#include <Eigen/Core>

#include <boost/mpl/has_xxx.hpp>
#include <boost/mpl/and.hpp>

namespace Naksha {

//! The macro is used for enable_if if T is an Eigen Type
#define ENABLE_IF_EIGEN_TYPE(T)\
    typename std::enable_if< IsEigenType<T>::value >::type

//! The macro enable Eigen new() when required. T can be any type
#define EIGEN_MAKE_ALIGNED_OPERATOR_NEW_IF_REQUIRED(T)\
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW_IF(IsEigenNewAllignRequired<T>::value)

namespace detail {
BOOST_MPL_HAS_XXX_TRAIT_DEF(Scalar)
BOOST_MPL_HAS_XXX_TRAIT_DEF(Index)
BOOST_MPL_HAS_XXX_TRAIT_DEF(StorageKind)
}

/**
 * @brief Traits for checking if T is indeed an Eigen Type
 * @tparam T any Type
 *
 * Example Usage:
 * IsEigenType<int>::value // evaluates to false
 * IsEigenType<int>::type // evaluates to false_type
 * IsEigenType<Eigen::Vector2d>::value // evaluates to true
 * IsEigenType<Eigen::Vector2d>::type // true_type
 */
template<typename T>
struct IsEigenType:
    boost::mpl::and_<
      detail::has_Scalar<T>,
      detail::has_Index<T>,
      detail::has_StorageKind<T> > {
};

/**@brief Traits for checking if T requires the Eigen's Aligned new Operator
 * @tparam T any Type
 *
 * Example Usage:
 * IsEigenNewAllignRequired<int>::value // false
 * IsEigenNewAllignRequired<Vector3d>::value // false
 * IsEigenNewAllignRequired<Vector4d>::value // true
 * IsEigenNewAllignRequired<DiscreteRandomVariable<4, double>>::value // true
 */
template<class T, class Enable = void>
struct IsEigenNewAllignRequired {
  static const bool value = false;
};

/// Speciallization of IsEigenNewAllignRequired
template<class T>
struct IsEigenNewAllignRequired<T, ENABLE_IF_EIGEN_TYPE(T)> {
  typedef typename T::Scalar Scalar;
  static const bool value = (((T::SizeAtCompileTime) != Eigen::Dynamic)
      && ((sizeof(Scalar) * (T::SizeAtCompileTime)) % 16 == 0));
};

} // namespace Naksha

#endif // NAKSHA_EIGEN_TRAITS_H_
