/**
 * @file EigenBoostSerialization.h
 * @brief Boost Serialization of Eigen objects
 *
 * @author Abhijit Kundu
 */

#ifndef EIGEN_BOOST_SERIALIZATION_H_
#define EIGEN_BOOST_SERIALIZATION_H_

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/serialization/nvp.hpp>
#include <boost/serialization/array.hpp>
#include <boost/serialization/split_free.hpp>

// Forward declare Eigen block type
namespace Eigen {
 template <typename XprType, int BlockRows, int BlockCols, bool InnerPanel> class Block;
}


namespace Eigen {
namespace internal {
/**
 * Simple traits class to check for Eigen Block Type
 */
template <typename T>
struct is_eigen_block_type { static const bool value = false; };

/**
 * Simple traits class to check for Eigen Block Type (Specialization)
 */
template <typename XprType, int BlockRows, int BlockCols, bool InnerPanel>
struct is_eigen_block_type<Block<XprType, BlockRows, BlockCols, InnerPanel> > {
  static const bool value = true;
};

} // end namespace internal
} // end namespace Eigen


namespace boost {
namespace serialization {

template<class Archive, class Derived>
void save(Archive& ar, const Eigen::DenseBase<Derived>& m, const unsigned int) {
  m.derived().eval();
  const Eigen::Index rows =  m.derived().rows(), cols =  m.derived().cols();
  ar & BOOST_SERIALIZATION_NVP(rows);
  ar & BOOST_SERIALIZATION_NVP(cols);

  if (Eigen::internal::is_eigen_block_type < Derived > ::value) {
    // Handle special case of block matrix types
    typename Derived::PlainObject plain_object =  m.derived();
    ar & make_nvp("data", make_array(plain_object.data(), plain_object.size()));
  } else {
    ar & make_nvp("data", make_array( m.derived().data(), m.derived().size()));
  }
}


template<class Archive, class Derived>
void load(Archive& ar, Eigen::DenseBase<Derived>& m, const unsigned int) {
  Eigen::Index rows, cols;
  ar & BOOST_SERIALIZATION_NVP(rows);
  ar & BOOST_SERIALIZATION_NVP(cols);
  if (rows != m.derived().rows() || cols != m.derived().cols())
    m.derived().resize(rows, cols);
  ar & make_nvp("data", make_array(m.derived().data(), m.derived().size()));
}

template<class Archive, typename _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
void serialize(Archive & ar, Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols>& m, const unsigned int file_version) {
  split_free(ar, m, file_version);
}

template<class Archive, typename _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
void serialize(Archive & ar, Eigen::Array<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols>& m, const unsigned int file_version) {
  split_free(ar, m, file_version);
}

template<class Archive, typename XprType, int BlockRows, int BlockCols, bool InnerPanel>
void serialize(Archive & ar, Eigen::Block<XprType, BlockRows, BlockCols, InnerPanel>& m, const unsigned int file_version) {
  split_free(ar, m, file_version);
}

template<class Archive, typename VectorType, int Size>
void serialize(Archive & ar, Eigen::VectorBlock<VectorType, Size>& m, const unsigned int file_version) {
  split_free(ar, m, file_version);
}


//Boost Serialization of Eigen Transform class
template<class Archive, typename _Scalar, int _Dim, int _Mode, int _Options>
void serialize(Archive & ar, Eigen::Transform<_Scalar, _Dim, _Mode, _Options>& t, const unsigned int file_version) {
  // serialize data
  ar & make_nvp("TransformMatrix", t.matrix());
}

//Boost Serialization of Eigen Transform class
template<class Archive, typename _Scalar, int _AmbientDim>
void serialize(Archive & ar, Eigen::AlignedBox<_Scalar, _AmbientDim>& bbx, const unsigned int file_version) {
  // serialize data
  ar & make_nvp("min", bbx.min());
  ar & make_nvp("max", bbx.max());
}

}} // end namespace boost serialization


#endif // EIGEN_BOOST_SERIALIZATION_H_
