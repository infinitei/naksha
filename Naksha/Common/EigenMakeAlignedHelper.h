/**
 * @file EigenMakeAlignedHelper.h
 * @brief EigenMakeAlignedHelper
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_EIGEN_MAKE_ALIGNED_HELPER_H_
#define NAKSHA_EIGEN_MAKE_ALIGNED_HELPER_H_

#include <Eigen/Core>
#include <boost/mpl/has_xxx.hpp>
#include <boost/serialization/access.hpp>
#include "Naksha/Common/BoostSerializationTraits.h"

namespace Naksha {

namespace detail {
  BOOST_MPL_HAS_XXX_TRAIT_DEF(EigenNeedsToAlign)
}

template<typename T, class Enable = void>
class EigenMakeAlignedHelper {
public:
  typedef boost::mpl::int_<0> EigenNeedsToAlign;

private:
  // Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
  }
};

template< typename T >
class EigenMakeAlignedHelper< T, typename std::enable_if< detail::has_EigenNeedsToAlign<T>::value >::type > {
public:
  typedef typename T::EigenNeedsToAlign EigenNeedsToAlign;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW_IF(EigenNeedsToAlign::value)

private:
  // Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
  }
};

} // end namespace Naksha


namespace boost {
namespace serialization {

template<typename T, class Enable>
struct implementation_level<Naksha::EigenMakeAlignedHelper<T, Enable> >
{
    typedef mpl::integral_c_tag tag;
    typedef mpl::int_<object_serializable> type;
    BOOST_STATIC_CONSTANT(
        int,
        value = implementation_level::type::value
    );
};

template<typename T, class Enable>
struct tracking_level<Naksha::EigenMakeAlignedHelper<T, Enable> >
{
    typedef mpl::integral_c_tag tag;
    typedef mpl::int_<track_never> type;
    BOOST_STATIC_CONSTANT(
        int,
        value = tracking_level::type::value
    );
};

template<typename T, class Enable>
struct is_bitwise_serializable< Naksha::EigenMakeAlignedHelper<T, Enable> > : mpl::true_ {};


} // end namespce serialization
} // end namespce boost

#endif // NAKSHA_EIGENMAKEALIGNEDHELPER_H_
