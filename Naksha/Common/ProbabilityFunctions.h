/**
 * @file ProbabilityFunctions.h
 * @brief ProbabilityFunctions
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_PROBABILITY_FUNCTIONS_H_
#define NAKSHA_PROBABILITY_FUNCTIONS_H_

#include "Naksha/Common/EigenTraits.h"
#include <cmath>
#include <type_traits>

namespace Naksha {

struct FolatingPointTag { };///< Scalar Tag for standard types e.g double, float, int
struct EigenTypeTag { }; ///< Eigen Type Tag for Eigen types like Eigen::Vector3d, Eigen::Array2i


//! The macro is used for enable_if if T is an floating point type
#define ENABLE_IF_FLOATING_TYPE(T)\
    typename std::enable_if< std::is_floating_point<T>::value >::type

template <class T, class Enable = void>
struct DataTraits;


template <class T>
struct DataTraits<T, ENABLE_IF_EIGEN_TYPE(T) >{
  typedef EigenTypeTag Tag;
  typedef typename T::PlainObject ReturnType;
};


template <class T>
struct DataTraits<T, ENABLE_IF_FLOATING_TYPE(T) >{
  typedef FolatingPointTag Tag;
  typedef T ReturnType;
};

namespace detail {

template<typename Scalar>
inline Scalar logitImpl(const Scalar& p, FolatingPointTag) {
  return std::log(p / (1. - p));
}

template<class Derived>
inline typename Derived::PlainObject logitImpl(const Eigen::MatrixBase<Derived>& p, EigenTypeTag) {
  return (p.array() / (1. - p.array())).log();
}

template<class Derived>
inline typename Derived::PlainObject logitImpl(const Eigen::ArrayBase<Derived>& p, EigenTypeTag) {
  using namespace std;
  return (p / (1. - p.array())).log();
}


template<typename Scalar>
inline Scalar sigmoidImpl(const Scalar& x, FolatingPointTag) {
  return 1. / (1. + std::exp(-x));
}

template<class Derived>
inline typename Derived::PlainObject sigmoidImpl(const Eigen::MatrixBase<Derived>& x, EigenTypeTag) {
  return (1. + (-x).array().exp()).inverse();
}

template<class Derived>
inline typename Derived::PlainObject sigmoidImpl(const Eigen::ArrayBase<Derived>& x, EigenTypeTag) {
  using namespace std;
  return (1. + exp(-x)).inverse();
}


}  // namespace detail


/**@brief Functor to compute logit transform (aka log-odds)
 * This is inverse of sigmoid function
 * See http://en.wikipedia.org/wiki/Logit
 */
struct Logit {
  template<typename T>
  typename DataTraits<T>::ReturnType operator()(const T& p) const {
    return detail::logitImpl(p, typename DataTraits<T>::Tag());
  }
};

/**@brief Compute logit transform (aka log-odds)
 * This is inverse of Sigmoid
 * See http://en.wikipedia.org/wiki/Logit
 */
template<typename T>
inline typename DataTraits<T>::ReturnType logit(const T& p) {
  return detail::logitImpl(p, typename DataTraits<T>::Tag());
}


/**@brief Functor to compute sigmoid function (logistic function)
 * This inverse of Logit
 * See http://en.wikipedia.org/wiki/Logistic_function
 */
struct Sigmoid {
  template<typename T>
  typename DataTraits<T>::ReturnType operator()(const T& x) const {
    return detail::sigmoidImpl(x, typename DataTraits<T>::Tag());
  }
};

/**@brief Free function to compute sigmoid function (logistic function)
 * This inverse of logit function
 * See http://en.wikipedia.org/wiki/Logistic_function
 */
template<typename T>
inline typename DataTraits<T>::ReturnType sigmoid(const T& x) {
  return detail::sigmoidImpl(x, typename DataTraits<T>::Tag());
}

}  // namespace Naksha

#endif // end NAKSHA_PROBABILITY_FUNCTIONS_H_
