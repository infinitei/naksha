/**
 * @file MakeAlignedContainer.h
 * @brief Make Aligned Containers (STL)
 *
 * Storing fixed sized Eigen types in STL containers may need using
 * Eigen's alligned_allocator.
 *
 * See http://eigen.tuxfamily.org/dox/group__TopicStlContainers.html
 *
 * This header provides simple structs to make STL/Boost conatainers
 * which uses Eigen's alligned_allocator when needed.
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_STD_VECTOR_H_
#define NAKSHA_STD_VECTOR_H_

#include "Naksha/Common/EigenTraits.h"
#include "Naksha/Common/EigenMakeAlignedHelper.h"

#include <Eigen/StdVector>
#include <boost/unordered_map.hpp>

namespace Naksha {

/**@brief Make std::vector<> with Eigen's alligned_allocator if needed
 *
 *  - MakeStdVector<int>::type will give std::vector<int, std::allocator<int>> *
 *  - MakeStdVector<Eigen::Vectord3f>::type will give std::vector<Eigen::Vectord3f, std::allocator<Eigen::Vectord3f>>
 *  - MakeStdVector<Eigen::Vectord4f>::type will give std::vector<Eigen::Vectord4f, Eigen::aligned_allocator<Eigen::Vectord3f>>
 */
template <class ValueType, class Enable = void>
struct MakeStdVector {
  typedef std::vector<ValueType> type;
};

template <class ValueType>
struct MakeStdVector<ValueType, typename std::enable_if< IsEigenNewAllignRequired<ValueType>::value >::type> {
  typedef std::vector<ValueType, Eigen::aligned_allocator<ValueType> > type;
};




/// Make boost::unordered_map<> with Eigen's alligned_allocator if needed
template<
  class Key,
  class Mapped,
  class Hash = boost::hash<Key>,
  class Pred = std::equal_to<Key>,
  class Enable = void
  >
struct MakeBoostUnorderedMap {
  typedef boost::unordered_map<Key, Mapped, Hash, Pred> type;
};

template<
  class Key,
  class Mapped,
  class Hash,
  class Pred
  >
struct MakeBoostUnorderedMap<Key, Mapped, Hash, Pred, typename std::enable_if< IsEigenNewAllignRequired<Mapped>::value >::type> {
  typedef boost::unordered_map<Key, Mapped, Hash, Pred, Eigen::aligned_allocator<std::pair<Key const, Mapped> > > type;
};

/// Specialization of IsEigenNewAllignRequired
template<class T>
struct IsEigenNewAllignRequired<T, typename std::enable_if< detail::has_EigenNeedsToAlign<T>::value >::type> {
  static const bool value = T::EigenNeedsToAlign::value;
};

}  // namespace Naksha



#endif // end NAKSHA_STD_VECTOR_H_
