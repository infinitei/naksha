/**
 * @file TransformMapIterator.h
 * @brief TransformMapIterator
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_TRANSFORM_MAP_ITERATOR_H_
#define NAKSHA_TRANSFORM_MAP_ITERATOR_H_

#include <boost/iterator/iterator_adaptor.hpp>
#include <type_traits>

namespace Naksha {

namespace detail {

template <class Iterator, class Enable = void>
struct TransformMapIteratorHelper {
  typedef typename Iterator::value_type::second_type& reference;
};

template<class Iterator>
struct TransformMapIteratorHelper<Iterator,
    typename std::enable_if<
        std::is_const<typename boost::remove_pointer<typename Iterator::pointer>::type>::value >::type> {
  typedef const typename Iterator::value_type::second_type& reference;
};

} // end namespace detail

/**
 * @brief Transform a Map like (Associate Container) Iterators
 *
 * @tparam Iterator Base Associate Container Iterator
 */
template<class Iterator>
class TransformMapIterator: public boost::iterator_adaptor<
    TransformMapIterator<Iterator>                   // Derived
    , Iterator                                       // Base
    , typename Iterator::value_type::second_type    // Value
    , typename Iterator::iterator_category          // CategoryOrTraversal
    , typename detail::TransformMapIteratorHelper<Iterator>::reference   // Reference
    , typename Iterator::difference_type            // Difference
> {

private:
  struct enabler {};  // a private type avoids misuse

  typedef boost::iterator_adaptor<
      TransformMapIterator<Iterator>                   // Derived
      , Iterator                                       // Base
      , typename Iterator::value_type::second_type    // Value
      , typename Iterator::iterator_category          // CategoryOrTraversal
      , typename detail::TransformMapIteratorHelper<Iterator>::reference   // Reference
      , typename Iterator::difference_type            // Difference
  > super_t;

public:
  /// Default Constructor
  TransformMapIterator()
      : super_t() {
  }

  /// Constructor from Base Iterator
  explicit TransformMapIterator(Iterator const& x)
      : super_t(x) {
  }

  /// Constructor from Other kind of Iterators
  template <class OtherIterator>
  TransformMapIterator(
      TransformMapIterator<OtherIterator> const& other
        , typename boost::enable_if<
              std::is_convertible<OtherIterator,Iterator>
            , enabler
          >::type = enabler()
      )
        : super_t(other.base()) {}

  /// Returns Key value
  const typename Iterator::value_type::first_type&
  key() const {
    return this->base()->first;
  }

  /// Returns mapped value
  const typename Iterator::value_type::second_type&
  mapped() const {
    return this->base()->second;
  }

private:
  friend class boost::iterator_core_access;

  typename super_t::reference
  dereference() const {
    return this->base()->second;
  }
};



} // end namespace Naksha

#endif // NAKSHA_TRANSFORM_MAP_ITERATOR_H_
