/**
 * @file ProbabilityUtils.h
 * @brief Probability Utils
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_PROBABILITY_UTILS_H_
#define NAKSHA_PROBABILITY_UTILS_H_

#include "Naksha/Common/EigenTraits.h"

namespace Naksha {

/**@brief Get logodds value from probability
 * @tparam Scalar some floating point type (e.g. float,double)
 * @param[in] probability value
 * @return logodds value
 */
template<typename Scalar>
inline Scalar logOddsFromProb(const Scalar& probability){
  return std::log(probability/(1-probability));
}

/**@brief Get probability from logodds value
 * @tparam Scalar some floating point type (e.g. float,double)
 * @param[in] logodds value
 * @return probability value
 */
template<typename Scalar>
inline Scalar probFromLogOdds(const Scalar& logodds){
  return 1. - ( 1. / (1. + std::exp(logodds)));
}


/**@brief Get logSumExp of all entries of Eigen MatrixBase types
 * @param mat matrix of log probabilities
 * @return logSumExp of all entries
 */
template <typename Derived>
inline typename Derived::Scalar logSumExp(const Eigen::MatrixBase<Derived>& mat) {
  typedef typename Derived::Scalar Scalar;
  Scalar max = mat.maxCoeff();
  return max + std::log((mat.array() - max).array().exp().sum());
}


/** @defgroup ProbabilityTypeTags
 *
 *  @brief Probability Type Tags for different representations of Probability
 *
 *  Probabilities may be represented in dofferent formats like LogProbability
 *  or LogOdds, or direct Probability formats. These tags help with static
 *  tag dispatching.
 *  @{
 */

struct ProbabilityTag { };  ///< Probability Tag
struct LogProbabilityTag { }; ///< Log Probability Tag
struct LogOddsTag { };  ///< Log Odds Tag

/** @} */ // end of Probability Type Tags

namespace detail {

template<class T, class ProbTypeTo, class ProbTypeFrom, class Enable = void>
struct RVConversionImpl;

template<class T, class ProbType, class Enable = void>
struct RVNormalizeMultiplyImpl;

template<class T, class ProbType, class Enable = void>
struct RVModeImpl;

}

/**@brief Convert a RandomVariable between different \ref ProbabilityTypeTags
 *
 * @param[in] value can be a Scalar type or Eigen types currently in type_from \ref ProbabilityTypeTags
 * @param[in] type_to \ref ProbabilityTypeTags tag to convert to
 * @param[in] type_from \ref ProbabilityTypeTags tag to convert from
 * @return converted RandomVariable with type_to \ref ProbabilityTypeTags
 */
template<class T, class ProbTypeTo, class ProbTypeFrom>
typename detail::RVConversionImpl<T, ProbTypeTo, ProbTypeFrom>::ReturnType
convertRandomVariable(const T& value, ProbTypeTo type_to, ProbTypeFrom type_from) {
  return detail::RVConversionImpl<T, ProbTypeTo, ProbTypeFrom>::convert(value);
}

/**@brief Convert a RandomVariable In-Place between different \ref ProbabilityTypeTags
 *
 * @param[in,out] value can be a Scalar type or Eigen types currently in type_from \ref ProbabilityTypeTags
 * @param[in] type_to \ref ProbabilityTypeTags to convert to
 * @param[in] type_from ProbabilityTypeTags to convert from
 */
template<class T, class ProbTypeTo, class ProbTypeFrom>
void
convertRandomVariableInPlace(T const& value, ProbTypeTo type_to, ProbTypeFrom type_from) {
  const_cast<T&>(value) = detail::RVConversionImpl<T, ProbTypeTo, ProbTypeFrom>::convert(value);
}



/**@brief Mutiply two RandomVariables
 *
 * @param[in,out] value can be a Scalar type or Eigen types
 * @param[in] meas can be a Scalar type or Eigen types
 * @param[in] prob_type_tag \ref ProbabilityTypeTags tag
 */
template<class T, class ProbType>
void multiplyRandomVariable( T const& value, const T& meas, ProbType prob_type_tag) {
  detail::RVNormalizeMultiplyImpl<T, ProbType>::multiply(value, meas);
}

/**@brief Normalize a RandomVariable
 *
 * @param[in,out] value can be a Scalar type or Eigen types
 * @param[in] prob_type_tag \ref ProbabilityTypeTags tag
 */
template<class T, class ProbType>
void normalizeRandomVariable( T const& value, ProbType prob_type_tag) {
  detail::RVNormalizeMultiplyImpl<T, ProbType>::normalize(value);
}


/**@brief Compute mode of a RandomVariable
 *
 * @param[in] value can be a Scalar type or Eigen types
 * @param[in] prob_type_tag \ref ProbabilityTypeTags tag
 * @return mode of the RandomVariable
 */
template<class T, class ProbType>
int modeOfRandomVariable( const T& value, ProbType prob_type_tag ) {
  return detail::RVModeImpl<T, ProbType>::mode(value);
}

} // end namespace Naksha

#include "Naksha/Common/ProbabilityUtils-impl.hpp"

#endif // PROBABILITY_UTILS_H_
