/**
 * @file Zip2Iterator.h
 * @brief Zip2Iterator
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_ZIP2_ITERATOR_H_
#define NAKSHA_ZIP2_ITERATOR_H_

#include <boost/iterator/iterator_facade.hpp>


namespace Naksha {

template<class KeyIterator, class ValueIterator>
class Zip2Iterator : public boost::iterator_facade<
  Zip2Iterator<KeyIterator, ValueIterator>      // Derived
  , typename ValueIterator::value_type          // Value
  , typename ValueIterator::iterator_category   // CategoryOrTraversal
  , typename ValueIterator::reference           // Reference
  , typename ValueIterator::difference_type     // Difference
  > {

  typedef boost::iterator_facade<
      Zip2Iterator<KeyIterator, ValueIterator>    // Derived
    , typename ValueIterator::value_type          // Value
    , typename ValueIterator::iterator_category   // CategoryOrTraversal
    , typename ValueIterator::reference           // Reference
    , typename ValueIterator::difference_type     // Difference
  > super_t;

  template<class K, class V>
  friend class Zip2Iterator;

 public:

  /// Constructor from a key_iterator and value_iterator
  Zip2Iterator(KeyIterator const& key_iterator, ValueIterator const& value_iterator)
   : _key_iterator(key_iterator), _value_iterator(value_iterator) {
  }

  /// Constructor from another zip iterator
  template<class K, class V>
  Zip2Iterator(Zip2Iterator<K,V> const& other)
      : _key_iterator(other._key_iterator),
        _value_iterator(other._value_iterator) {
  }

  /// Returns Key value
  typename KeyIterator::reference key() const {
    return *_key_iterator;
  }

  /// Returns Key Iterator
  const KeyIterator&  keyIterator() const {
    return _key_iterator;
  }

  /// Returns Value Iterator
  const ValueIterator&  valueIterator() const {
    return _value_iterator;
  }


 private:
  friend class boost::iterator_core_access;

  typename super_t::reference dereference() const {
    return *_value_iterator;
  }

  template<class K, class V>
  bool equal(Zip2Iterator<K,V> const& other) const {
    return (_key_iterator == other._key_iterator) && (_value_iterator == other._value_iterator);
  }

  void increment() {
    ++_key_iterator;
    ++_value_iterator;
  }

  void decrement() {
    --_key_iterator;
    --_value_iterator;
  }

  void advance(typename ValueIterator::difference_type n) {
    std::advance(_key_iterator, n);
    std::advance(_value_iterator, n);
  }

  template<class OtherZipIterator>
  typename ValueIterator::difference_type distance_to(OtherZipIterator const& other) const {
    return other._value_iterator - _value_iterator;
  }

  KeyIterator _key_iterator;
  ValueIterator _value_iterator;
};

}  // end namespace Naksha

#endif // end NAKSHA_ZIP2_ITERATOR_H_
