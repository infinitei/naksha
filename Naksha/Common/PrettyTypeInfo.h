/**
 * @file PrettyTypeInfo.h
 * @brief PrettyTypeInfo
 *
 * @author Abhijit Kundu
 */

#ifndef PRETTY_TYPE_INFO_H_
#define PRETTY_TYPE_INFO_H_

#include <string>
#include <typeinfo>
#ifdef __GNUG__
#include <cstdlib>
#include <memory>
#include <cxxabi.h>

struct handle {
    char* p;
    handle(char* ptr) : p(ptr) { }
    ~handle() { std::free(p); }
};

static inline std::string demangleTypeInfo(const std::type_info&ti) {

    int status = -4; // some arbitrary value to eliminate the compiler warning

    handle result( abi::__cxa_demangle(ti.name(), NULL, NULL, &status) );

    return (status==0) ? result.p : ti.name() ;
}

#else

// does nothing if not g++
static inline std::string demangleTypeInfo(const std::type_info&ti) {
    return ti.name();
}

#endif

static inline std::string removeNS(const std::string & source,
    const std::string & namespace_) {
  if (namespace_ == "")
    return source;

  std::string dst = source;
  const std::string ns = namespace_ + "::";
  std::size_t position = source.find(ns);
  while (position != std::string::npos) {
    dst.erase(position, ns.length());
    position = dst.find(ns, position + 1);
  }
  return dst;
}

static inline std::string setTemplateExpansion(const std::string& input_string, const int level, const std::string& replace_str = std::string()) {
  if (level < 0)
    return input_string;

  std::string return_string = input_string;

  typedef std::string::iterator IteratorType;
  int curr_level = 0;
  IteratorType starting_it = return_string.begin();

  while (true) {

    IteratorType replace_start_it = return_string.end();
    for (IteratorType it = starting_it; it != return_string.end(); ++it) {
      if (*it == '<') {
        if (curr_level == level) {
          replace_start_it = it;
          break;
        }
        ++curr_level;
      } else if (*it == '>') {
        --curr_level;
      }
    }

    if (replace_start_it != return_string.end()) {
      // Now find the closing >
      IteratorType replace_end_it = return_string.end();
      for (IteratorType it = replace_start_it + 1; it != return_string.end(); ++it) {
        if (*it == '>') {
          if (curr_level == level) {
            replace_end_it = it;
            break;
          }
          --curr_level;
        } else if (*it == '<') {
          ++curr_level;
        }
      }

      if (replace_end_it != return_string.end()) {
        std::string::size_type start_pos = replace_start_it - return_string.begin();
        return_string.replace(replace_start_it, replace_end_it + 1, replace_str);
        starting_it = return_string.begin() + start_pos + replace_str.length();
        continue;
      }
    }

    break;
  }

  return return_string;
}

static inline std::string demangle_and_remove_ns(const std::type_info& ti, const std::string& ns ="") {
  return removeNS(demangleTypeInfo(ti), ns);
}

static inline std::string pretty_type(const std::type_info& ti, const std::string& ns ="", const int level = -1) {
  return removeNS(setTemplateExpansion(demangleTypeInfo(ti), level, "<...>"), ns);
}

#define PRETTY_TYPE_INFO(T) demangleTypeInfo(typeid(T))
#define PRETTY_TYPE_INFO_REMOVE_NS(Type, Ns) demangle_and_remove_ns(typeid(Type),Ns)
#define PRETTY_TYPE(Type, Ns, level) pretty_type(typeid(Type), Ns, level)

#endif // PRETTY_TYPE_INFO_H_
