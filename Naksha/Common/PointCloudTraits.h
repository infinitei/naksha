/**
 * @file PointCloudTraits.h
 * @brief PointCloudTraits
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_POINT_CLOUD_TRAITS_H_
#define NAKSHA_POINT_CLOUD_TRAITS_H_

#include "Naksha/Common/EigenTraits.h"
#include <vector>

namespace Naksha {

/// Tag for PointCloud of type std::vector<EigenType>
struct StdVectorOfEigenPointsTag {
};

/// Tag for Point Cloud based on Eigen
struct EigenPointCloudTag {
};

/// Tag for Single Fixed Size Point e.g. Vector3d, Vector4f
struct SingleEigenFixedSizePointTag : EigenPointCloudTag {
};

/// Tag for Mutiple Fixed Size Points e.g Eigen::Matrix<double, 3, Eigen::Dynamic>
struct MultiEigenFixedSizePointsTag : EigenPointCloudTag {
};

/**@brief Traits class for PointCloud
 *
 * The resulting type can be either
 *  - StdVectorOfEigenPointsTag
 *  - EigenPointCloudTag
 *
 */
template <class PointCloud, class Enable = void>
struct PointCloudTraits;

template <typename V, typename A >
struct PointCloudTraits<std::vector<V, A> , ENABLE_IF_EIGEN_TYPE(V) > {
  typedef StdVectorOfEigenPointsTag type;
};

template <typename PointCloud>
struct PointCloudTraits<PointCloud, ENABLE_IF_EIGEN_TYPE(PointCloud)> {
  typedef EigenPointCloudTag type;
};


/**@brief Traits class for EigenPointCloud
 *
 * The resulting type can be either
 *  - SingleEigenFixedSizePointTag
 *  - MultiEigenFixedSizePointsTag
 *  - EigenPointCloudTag
 *
 */
template <int Dimension, class PointCloud, class Enable = void>
struct EigenPointCloudTraits;

template <int Dimension, class PointCloud>
struct EigenPointCloudTraits<Dimension, PointCloud, typename std::enable_if<PointCloud::RowsAtCompileTime == Dimension && PointCloud::ColsAtCompileTime == 1>::type > {
  typedef SingleEigenFixedSizePointTag type;
};

template <int Dimension, class PointCloud>
struct EigenPointCloudTraits<Dimension, PointCloud, typename std::enable_if<PointCloud::RowsAtCompileTime == Dimension && PointCloud::ColsAtCompileTime != 1>::type > {
  typedef MultiEigenFixedSizePointsTag type;
};

template <int Dimension, class PointCloud>
struct EigenPointCloudTraits<Dimension, PointCloud, typename std::enable_if<PointCloud::RowsAtCompileTime == Eigen::Dynamic>::type > {
  typedef EigenPointCloudTag type;
};


}  // namespace Naksha



#endif // end NAKSHA_POINT_CLOUD_TRAITS_H_
