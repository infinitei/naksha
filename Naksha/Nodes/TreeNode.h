/**
 * @file TreeNode.h
 * @brief Defines base class for Tree Nodes e.g. Octree or Quadtree
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_TREE_NODE_H_
#define NAKSHA_TREE_NODE_H_

#include <array>
#include <boost/scoped_ptr.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/scoped_ptr.hpp>

namespace Naksha {

/**
 * @brief Base class for Tree type ChildPolicy classes like OctreeNode & QuadtreeNode
 *
 * This is meant to create ChildPolicy classes for Nodes of Tree like data-structure.
 *
 * You probably will use OctreeNode & QuadtreeNode as ChildPolicy param
 * for your Node.
 *
 * @tparam Derived Node type (CRTP)
 * @tparam Number of Child nodes (Default 8 (Quadtree))
 *
 * @ingroup NodeChildPolicy
 * @ingroup Nodes
 */
template <typename Derived, int MaxNumOfChildren = 8>
class TreeNodeBase {

  typedef boost::scoped_ptr<Derived> DerivedPtr;  ///< Typedef for DerivedNode Ptr

public:

  enum {
    MaxNumOfChilds = MaxNumOfChildren ///< Maximum number of Children (e.g 8 for Octree)
  };

  TreeNodeBase() {}

  /**@brief Check if a particular child exists
   * @param child_index index of child (throws is out of range)
   * @return true if the i-th child exists
   */
  inline bool childExistsAt(unsigned int child_index) const;

  /// @return true if the node has at least one child
  inline bool hasAnyChildren() const;

  /// initialize a child with default payload value
  inline bool createChild(unsigned int child_index);

  /// initialize a child with provided payload value
  template<typename T>
  inline bool createChild(unsigned int child_index, const T& initPayload);

  /// Deletes a child of the node
  inline void deleteChild(unsigned int child_index);

  /// @return a const pointer to a child
  const Derived* childPtr(unsigned int child_index) const {
    BOOST_ASSERT_MSG(child_index < MaxNumOfChildren, "Trying to access Ghost Child??");
    return ptrToChildNodePointersArray_->operator[](child_index).get();
  }

  /// @return a modifiable pointer to a child (throws if child_index is out of range)
  Derived* childPtr(unsigned int child_index) {
    BOOST_ASSERT_MSG(child_index < MaxNumOfChildren, "Trying to access Ghost Child??");
    return ptrToChildNodePointersArray_->operator[](child_index).get();
  }

  /// @return a const reference to a child (throws if child_index is out of range)
  const Derived& child(unsigned int child_index) const {
    BOOST_ASSERT_MSG(child_index < MaxNumOfChildren, "Trying to access Ghost Child??");
    return * ptrToChildNodePointersArray_->operator[](child_index);
  }

  /// @return a modifiable reference to a child (throws if child_index is out of range)
  Derived& child(unsigned int child_index) {
    BOOST_ASSERT_MSG(child_index < MaxNumOfChildren, "Trying to access Ghost Child??");
    return * ptrToChildNodePointersArray_->operator[](child_index);
  }

  /// update this node from the children
  void updateFromChildren() {
    static_cast<Derived*>(this)->updateFromChildren();
  }

protected:

  ///TODO Consider using boost ptr array here
  /// Typedef for ChildNodePointersArray
  typedef std::array<DerivedPtr, MaxNumOfChildren> ChildNodePointersArray;

  /// We store a Pointer to array of ChildNodePointersArray
  boost::scoped_ptr<ChildNodePointersArray> ptrToChildNodePointersArray_;

  /// Allocate child pointer array only when needed
  inline void allocChildPointersArray() {
    ptrToChildNodePointersArray_.reset(new ChildNodePointersArray);
  }

private:
  static_assert(MaxNumOfChildren > 0, "Max Number of Child Nodes in a Tree should be Positive");

  // Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
      ar & ptrToChildNodePointersArray_;
  }
};

/**@brief Octree (8 children)  ChildPolicy for Node
 * @tparam Derived Node type (CRTP)
 *
 * @ingroup NodeChildPolicy
 * @ingroup Nodes
 */
template <typename Derived>
class OctreeNode : public TreeNodeBase<Derived, 8> {
  typedef TreeNodeBase<Derived, 8> Base;

private:
  // Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & boost::serialization::base_object< Base >(*this);
  }
};

/**@brief Quadtree (4 children)  ChildPolicy for Node
 * @tparam Derived Node type (CRTP)
 *
 * @ingroup NodeChildPolicy
 * @ingroup Nodes
 */
template <typename Derived>
class QuadtreeNode : public TreeNodeBase<Derived, 4> {
  typedef TreeNodeBase<Derived, 4> Base;

private:
  // Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & boost::serialization::base_object< Base >(*this);
  }
};

} // namespace Naksha

#include "Naksha/Nodes/TreeNode-impl.hpp"

#endif // NAKSHA_TREE_NODE_H_
