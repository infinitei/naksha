/**
 * @file RVOccupancyNodeInterface-impl.hpp
 *
 * Implementation header for RVOccupancyNodeInterface
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_OCCUPANCY_OCTREE_NODE_IMPL_HPP_
#define NAKSHA_OCCUPANCY_OCTREE_NODE_IMPL_HPP_

namespace Naksha {

template<typename Payload>
RVOccupancyNodeInterface<Payload>::RVOccupancyNodeInterface()
    : Base(), num_of_observations_(0) {
}

template<typename Payload>
template<typename T>
RVOccupancyNodeInterface<Payload>::RVOccupancyNodeInterface(const T& init)
    : Base(init), num_of_observations_(0) {
}

template<typename Payload>
void RVOccupancyNodeInterface<Payload>::updateFromChildren() {
  typedef typename Base::Payload::DataType DataType;
  DataType mean(DataType::Zero());
  int num_childs_present = 0;
  for (unsigned int i = 0; i < 8; ++i) {
    if (this->childExistsAt(i)) {
      mean += this->child(i)->payload().probability();
      num_childs_present++;
    }
  }
  if (num_childs_present){
    // Change Node only if num_childs_present is +ve
    mean /= num_childs_present;
    //normalize
    mean /= mean.sum();

    this->payload_.setProbability(mean);
  }
}

} // namespace Naksha

BOOST_CLASS_IMPLEMENTATION_PP(Naksha::RVOccupancyNodeInterface, (class), object_serializable)
BOOST_CLASS_TRACKING_PP(Naksha::RVOccupancyNodeInterface, (class), track_never)
//BOOST_IS_BITWISE_SERIALIZABLE_PP(Naksha::RVOccupancyNodeInterface, (class))


#endif // NAKSHA_OCCUPANCY_OCTREE_NODE_IMPL_HPP_
