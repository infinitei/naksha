/**
 * @file VoxelNode.h
 * @brief VoxelNode class
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_VOXEL_NODE_H_
#define NAKSHA_VOXEL_NODE_H_

#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>

namespace Naksha {

/**@brief VoxelNode depicts a voxel in 3D(2D) space
 *
 * VoxelNode is designed to serve the purpose of representing voxels in the \ref Maps.
 * Typically this will be leaf nodes of a Tree Datastructure based \ref Maps.
 *
 * This class is host class for several policies like Payload, InterfacePolicy and ChildPolicy.
 *
 * @tparam _Payload any \ref Payload type
 * @tparam _InterfacePolicy Interface Policy e.g. OccupancyNodeInterface or RVOccupancyNodeInterface
 * @tparam _ChildPolicy ChildPolicy which can be OctreeNode (8 childs), NoChildPolicy (0 childs), etc.
 *
 * @ingroup Nodes
 */

template<
  class _Payload,
  template<class > class _InterfacePolicy,
  template<class > class _ChildPolicy
         >
class VoxelNode:
    public _InterfacePolicy<_Payload>,
    public _ChildPolicy<VoxelNode<_Payload, _InterfacePolicy, _ChildPolicy> > {

public:
  typedef _InterfacePolicy<_Payload> Interface; ///< Interface Policy e.g. OccupancyNodeInterface or RVOccupancyNodeInterface
  typedef _ChildPolicy<VoxelNode<_Payload, _InterfacePolicy, _ChildPolicy> > ChildPolicy; ///< ChildPolicy which can be OctreeNode (8 childs), NoChildPolicy (0 childs), etc.
  typedef _Payload Payload; ///< Typedef of the \ref Payload Type of this Map

  /// Default VoxelNode Constructor
  VoxelNode() : Interface() {}

  /**@brief VoxelNode Constructor with Payload init data
   *
   * This assumes exists constructor like Payload(T init)
   * So T can be the Payload itself of like Payload::DataType
   * in case of DiscreteRandomVariables
   *
   * @tparam T Payload initializer type
   * @param init of type T used to initialize the payload
   */
  template<typename T>
  VoxelNode(const T& init) : Interface(init) {}

private:
  /// Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & boost::serialization::base_object< Interface >(*this);
    ar & boost::serialization::base_object< ChildPolicy >(*this);
  }

};

} // end namespace Naksha

#include "Naksha/Common/BoostSerializationTraits.h"

BOOST_CLASS_IMPLEMENTATION_PP(Naksha::VoxelNode, (class)(template<class>class)(template<class>class), object_serializable)
BOOST_CLASS_TRACKING_PP(Naksha::VoxelNode, (class)(template<class > class)(template<class > class), track_never)

#endif // NAKSHA_VOXEL_NODE_H_
