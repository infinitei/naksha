/**
 * @file TreeNode-impl.hpp
 * @brief Tree Node Base Implementation
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_TREE_NODE_IMPL_HPP_
#define NAKSHA_TREE_NODE_IMPL_HPP_

#include <boost/assert.hpp>
#include <iostream>

namespace Naksha {

template<typename Derived, int MaxNumOfChildren>
inline bool TreeNodeBase<Derived, MaxNumOfChildren>::createChild(unsigned int child_index) {
  if(!ptrToChildNodePointersArray_)
    allocChildPointersArray();
  BOOST_ASSERT_MSG(ptrToChildNodePointersArray_, "Child pointers Array has Not been Allocated");

  if(!(child_index < MaxNumOfChildren)) {
    std::cout << "[WARNING] Trying to create more than MAX child" <<std::endl;
    return false;
  }

  if (!(*ptrToChildNodePointersArray_)[child_index]) {
    (*ptrToChildNodePointersArray_)[child_index].reset(new Derived());
    return true;
  }
  std::cout << "[WARNING] Attempt to Replace child" << std::endl;
  return false;
}


template<typename Derived, int MaxNumOfChildren>
template<typename T>
inline bool TreeNodeBase<Derived, MaxNumOfChildren>::createChild(unsigned int child_index, const T& initPayload) {
  if(!ptrToChildNodePointersArray_)
    allocChildPointersArray();
  BOOST_ASSERT_MSG(ptrToChildNodePointersArray_, "Child pointers Array has Not been Allocated");

  if (!(child_index < MaxNumOfChildren)) {
    std::cout << "[WARNING] Trying to create more than MAX child" << std::endl;
    return false;
  }

  if(! (*ptrToChildNodePointersArray_)[child_index]) {
    (*ptrToChildNodePointersArray_)[child_index].reset(new Derived(initPayload));
    return true;
  }
  std::cout << "[WARNING] Attempt to Replace child" << std::endl;
  return false;
}

template<typename Derived, int MaxNumOfChildren>
inline bool TreeNodeBase<Derived, MaxNumOfChildren>::childExistsAt(unsigned int child_index) const {
  BOOST_ASSERT_MSG(child_index < MaxNumOfChildren, "Trying to access Ghost Child??");
  if (ptrToChildNodePointersArray_ && ptrToChildNodePointersArray_->operator[](child_index))
    return true;
  else
    return false;
}

template<typename Derived, int MaxNumOfChildren>
inline bool TreeNodeBase<Derived, MaxNumOfChildren>::hasAnyChildren() const {
  if (!ptrToChildNodePointersArray_)
    return false;
  // return true if at-least one of the child node pointers is not null
  for (typename ChildNodePointersArray::const_iterator it = ptrToChildNodePointersArray_->begin();
      it != ptrToChildNodePointersArray_->end(); ++it) {
    if (*it)
      return true;
  }
  return false;
}

template<typename Derived, int MaxNumOfChildren>
inline void TreeNodeBase<Derived, MaxNumOfChildren>::deleteChild(unsigned int child_index) {
  BOOST_ASSERT_MSG(child_index < MaxNumOfChildren, "Trying to access Ghost Child??");
  BOOST_ASSERT_MSG(ptrToChildNodePointersArray_.get() != 0, "Child pointers Array has Not been Allocated");
  ptrToChildNodePointersArray_->operator[](child_index).reset();
}


} // namespace Naksha

#include "Naksha/Common/BoostSerializationTraits.h"

BOOST_CLASS_IMPLEMENTATION_PP(Naksha::OctreeNode, (class), object_serializable)
BOOST_CLASS_TRACKING_PP(Naksha::OctreeNode, (class), track_never)

BOOST_CLASS_IMPLEMENTATION_PP(Naksha::QuadtreeNode, (class), object_serializable)
BOOST_CLASS_TRACKING_PP(Naksha::QuadtreeNode, (class), track_never)

BOOST_CLASS_IMPLEMENTATION_PP(Naksha::TreeNodeBase, (class)(int), object_serializable)
BOOST_CLASS_TRACKING_PP(Naksha::TreeNodeBase, (class)(int), track_never)
//BOOST_IS_BITWISE_SERIALIZABLE_PP(Naksha::TreeNodeBase, (class)(int))

BOOST_CLASS_IMPLEMENTATION_PP(boost::scoped_ptr, (class), object_serializable)
BOOST_CLASS_TRACKING_PP(boost::scoped_ptr, (class), track_never)

#endif // NAKSHA_TREE_NODE_IMPL_HPP_
