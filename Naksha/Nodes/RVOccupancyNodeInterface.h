/**
 * @file RVOccupancyNodeInterface.h
 * @brief RandomVariable Occupancy Node Interface
 *
 * Derives from OccupancyNodeInterface
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_RV_OCCUPANCY_NODE_INTERFACE_H_
#define NAKSHA_RV_OCCUPANCY_NODE_INTERFACE_H_

#include "Naksha/Nodes/OccupancyNodeInterface.h"

namespace Naksha {

/**
 * @brief Node InterfacePolicy class for RandomVariable type Payload
 *
 * @tparam Payload RandomVariable \ref Payload types
 *
 * @ingroup NodeInterface
 * @ingroup Nodes
 */
template<typename Payload>
class RVOccupancyNodeInterface: public OccupancyNodeInterface<Payload> {

  typedef OccupancyNodeInterface<Payload> Base; ///< Convenience typedef to Base class

public:

  /// Default Node Constructor (default constructs the payload)
  RVOccupancyNodeInterface();

  /**@brief Node Initializer from Payload init data
   *
   * This assumes exists constructor like Payload(T init)
   * So T can be the Payload itself of like Payload::DataType
   * in case of DiscreteRandomVariables
   *
   * @tparam T Payload initializer type
   * @param init of type T used to initialize the payload
   */
  template<typename T>
  RVOccupancyNodeInterface(const T& init);

  /// update this node from the average (mean) Probability value of children
  void updateFromChildren();

  /**
   * Insert a new measurement and increment the observation count
   * @tparam T MeasurementType supported by Payload
   * @param measurement new measurement to be integrated
   */
  template<typename T>
  inline void insertMeasurement(const T& measurement) {
    ++num_of_observations_;
    Base::insertMeasurement(measurement);
  }

  /**
   * Insert a new measurement and increment the observation count
   * @tparam T MeasurementType supported by Payload
   * @param measurement new measurement to be integrated
   */
  template<typename T>
  inline void insertMeasurementLazy(const T& measurement) {
    ++num_of_observations_;
    Base::insertMeasurementLazy(measurement);
  }

  /**\brief Returns number of observations
   * i.e. number of times insertMeasurement() has been called.
   * @return number of observations (int)
   */
  inline std::size_t numOfObservations() const {
    return num_of_observations_;
  }

protected:
  std::size_t num_of_observations_; ///< number of times insertMeasurement() has been called.

private:
  // Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & boost::serialization::base_object< Base >(*this);
    ar & num_of_observations_;
  }

};

} // namespace Naksha

#include "Naksha/Nodes/RVOccupancyNodeInterface-impl.hpp"

#endif // NAKSHA_RV_OCCUPANCY_NODE_INTERFACE_H_
