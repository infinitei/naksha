/**
 * @file OccupancyNodeInterface.h
 * @brief Occupancy Node Interface class
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_OCCUPANCY_NODE_INTERFACE_H_
#define NAKSHA_OCCUPANCY_NODE_INTERFACE_H_

#include "Naksha/Common/EigenMakeAlignedHelper.h"

#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/type_traits/has_equal_to.hpp>

namespace Naksha {

/**
 * @brief Occupancy Node InterfacePolicy class
 * @tparam Payload \ref Any Payload types
 *
 * @ingroup NodeInterface
 * @ingroup Nodes
 */
template <typename Payload>
class OccupancyNodeInterface : public EigenMakeAlignedHelper<Payload> {

public:

  /// Default Node Constructor (default constructs the payload)
  OccupancyNodeInterface() : payload_() {}

  /**@brief Node Initializer from Payload init data
   *
   * This assumes exists constructor like Payload(T init)
   * So T can be the Payload itself of like Payload::DataType
   * in case of DiscreteRandomVariables
   *
   * @tparam T Payload initializer type
   * @param init of type T used to initialize the payload
   */
  template<typename T>
  OccupancyNodeInterface(const T& init)  : payload_(init) {}

  /// Returns the payload
  const Payload& payload() const {return payload_;}

  /// Returns the payload
  Payload& payload() {return payload_;}

  /// Sets the payload
  void setPayload(const Payload& payload) {payload_ =  payload;}

  /// Equals operator, compares if the stored value is identical
  bool operator==(const OccupancyNodeInterface& rhs) const {
    static_assert(
        (boost::has_equal_to<Payload,Payload>::value),
        "Incompatible Payload: Needs equal to operator");
    return rhs.payload() == payload_;
  }

  /**@brief Insert a new measurement to this node
   *
   * This function simply calls payload's integrateMeasurement()
   *
   * @tparam T MeasurementType supported by Payload
   * @param measurement new measurement to be integrated
   */
  template<typename T>
  inline void insertMeasurement(const T& measurement) {
    payload_.integrateMeasurement(measurement);
  }

  /**@brief Insert a new measurement to node lazily
   *
   * This function simply calls payload's integrateMeasurementLazy()
   *
   * @tparam T MeasurementType supported by Payload
   * @param measurement new measurement to be integrated
   */
  template<typename T>
  inline void insertMeasurementLazy(const T& measurement) {
    payload_.integrateMeasurementLazy(measurement);
  }

  /// Finish lazy evaluation of the node
  inline void finishLazyEvaluation() {
    payload_.finishLazyEvaluation();
  }


protected:
  /// Payload
  Payload payload_;

private:
  // Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    typedef EigenMakeAlignedHelper<Payload> Base;
    ar & boost::serialization::base_object< Base >(*this);
    ar & payload_;
  }
};

} // namespace Naksha

BOOST_CLASS_IMPLEMENTATION_PP(Naksha::OccupancyNodeInterface, (class), object_serializable)
BOOST_CLASS_TRACKING_PP(Naksha::OccupancyNodeInterface, (class), track_never)
//BOOST_IS_BITWISE_SERIALIZABLE_PP(Naksha::OccupancyNodeInterface, (class))

#endif // NAKSHA_OCCUPANCY_NODE_INTERFACE_H_
