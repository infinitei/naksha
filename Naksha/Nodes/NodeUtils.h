/**
 * @file NodeUtils.h
 * @brief NodeUtils
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_NODE_UTILS_H_
#define NAKSHA_NODE_UTILS_H_

#include "Naksha/Payload/PayloadUtils.h"

namespace Naksha {

/**@brief Functor for checking Is Node Occupied
 *
 * This functionality required Node::Payload is of RandomVariable Type.
 * Also assumes the first element of the data corresponds to Occupancy/Free
 *
 * See IsPayloadOccupied to check if a payload is occupied.
 *
 * Example Usage:
 * @code
 *  MapType map;
 *  ......
 *  typedef MapType::VoxelNodeType VoxelNodeType;
 *  // Count the number of occupied voxels
 *  size_t count = std::count_if(map.begin(), map.end(), IsNodeOccupied<VoxelNodeType>());
 * @endcode
 * @ingroup Nodes
 */
template<class Node>
class IsNodeOccupied: public IsPayloadOccupied<typename Node::Payload> {
public:
  typedef typename Node::Payload Payload;
  typedef IsPayloadOccupied<Payload> Base;
  typedef typename Base::Scalar Scalar;

  IsNodeOccupied(const Scalar free_prob_thresh = Scalar(0.5))
      : Base(free_prob_thresh) {
  }

  typedef Node argument_type;
  typedef bool result_type;

  bool operator()(const Node& node) const {
    return Base::operator ()(node.payload());
  }
};


/**@brief Functor to get Mode Label of the Node Payload
 *
 * This functor computes mode and is generic version for both
 * ModeOfNodeOccChecked and modeOfNode
 *
 * @tparam Node Node Type
 * @tparam OccupancyCheck If true (default: false) returns mode after occupancy check
 * @tparam ReturnType Defaults to int
 *
 *
 * Example Usage:
 * @code
 *  MapType map;
 *  ......
 *
 *  int mode_sum(0), mode_occ_cheked_sum(0);
 *  for(MapType::const_iterator it = map.voxel_begin(); it!= map.voxel_end(); ++it) {
 *    typedef MapType::VoxelNodeType VoxelNodeType;
 *    // Use standard ModeOfNode functor
 *    ModeOfNode<VoxelNodeType> node_mode;
 *    mode_occ_cheked_sum += node_mode(*it);
 *
 *    // Use ModeOfNode functor with occupancy check
 *    ModeOfNode<VoxelNodeType, true> node_mode_occ_cheked;
 *    mode_occ_cheked_sum += node_mode_occ_cheked(*it);
 *  }
 * @endcode
 *
 * @ingroup Nodes
 */
template<class Node, bool OccupancyCheck = false, typename ReturnType = int>
class ModeOfNode;



/// Specialization of ModeOfNode for NO OccupancyCheck
template<class Node, typename ReturnType>
class ModeOfNode<Node, false, ReturnType> {
public:
  typedef Node argument_type;
  typedef ReturnType result_type;

  ReturnType operator()(const Node& node) const {
    return static_cast<ReturnType>(modeOfPayload(node.payload()));
  }

};

/// Specialization of ModeOfNode for OccupancyCheck
template<class Node, typename ReturnType>
class ModeOfNode<Node, true, ReturnType>: public ModeOfPayloadOccChecked<typename Node::Payload, ReturnType> {
public:
  typedef typename Node::Payload Payload;
  typedef ModeOfPayloadOccChecked<Payload, ReturnType> Base;
  typedef typename Base::Scalar Scalar;

  ModeOfNode(const Scalar free_prob_thresh = Scalar(0.5))
      : Base(free_prob_thresh) {
  }

  typedef Node argument_type;
  typedef ReturnType result_type;

  ReturnType operator()(const Node& node) const {
    return Base::operator ()(node.payload());
  }
};


/**@brief Get Mode Label of the Node Payload
 *
 * @param[in] node of RV type
 * @return mode label of the RV
 *
 * Consider using the more generic ModeOfNode functor.
 *
 * @ingroup Nodes
 */
template <class Node>
int modeOfNode(const Node& node) {
  return modeOfPayload(node.payload());
}


/**@brief Functor to get Mode Label of the Node Payload after checking for occupancy
 *
 * Unlike mode this should output to a non Free
 * label even if Free is the actual mode. Also assumes
 * the first element of the data corresponds to Occupancy/Free
 *
 * Consider using the more generic ModeOfNode functor.
 * This functor is same as specialization ModeOfNode<Node, true, ReturnType>.
 *
 *
 * @ingroup Nodes
 */
template<class Node, typename ReturnType = int>
class ModeOfNodeOccChecked: public ModeOfPayloadOccChecked<typename Node::Payload, ReturnType> {
public:
  typedef typename Node::Payload Payload;
  typedef ModeOfPayloadOccChecked<Payload, ReturnType> Base;
  typedef typename Base::Scalar Scalar;

  ModeOfNodeOccChecked(const Scalar free_prob_thresh = Scalar(0.5))
      : Base(free_prob_thresh) {
  }

  typedef Node argument_type;
  typedef ReturnType result_type;

  ReturnType operator()(const Node& node) const {
    return Base::operator ()(node.payload());
  }
};


} // namespace Naksha


#endif // NAKSHA_NODE_UTILS_H_
