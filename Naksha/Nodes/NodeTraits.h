/**
 * @file NodeTraits.h
 * @brief Node Traits class
 *
 * Node Traits class helps in exposing Derived class
 * typedefs to Base class (e.g in CRTP)
 *
 * @author Abhijit Kundu
 */


#ifndef NAKSHA_NODE_TRAITS_H_
#define NAKSHA_NODE_TRAITS_H_

namespace Naksha {

// Common Node traits class template:
template <typename Derived>
struct NodeTraits;

} // namespace Naksha

#endif // NAKSHA_NODE_TRAITS_H_
