/**
 * @file NodeTypedefs.h
 * @brief NodeTypedefs
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_NODE_TYPEDEFS_H_
#define NAKSHA_NODE_TYPEDEFS_H_

#include "Naksha/Nodes/VoxelNode.h"

// Payloads
#include "Naksha/Payload/DiscreteRandomVariable.h"
// Interfaces
#include "Naksha/Nodes/RVOccupancyNodeInterface.h"
// ChildPolicy
#include "Naksha/Nodes/TreeNode.h"

namespace Naksha {

/**
 * Templated typedefs of Occupancy Tree Nodes
 * Currently uses the Struct hack (No need for this in C++11)
 *
 * Usage example:
 * OccupancyTreeNode<RV>::type my_RV_occupancy_octree_node;
 * OccupancyTreeNode<RV, 3>::type my_RV_occupancy_octree_node;
 * OccupancyTreeNode<RV, 2>::type my_RV_occupancy_quadtree_node; *
 * OccupancyTreeNode<RV, 3, OccupancyNodeInterface>::type my_occupancy_octree_node;
 *
 * @ingroup Nodes
 */
template<typename Payload, int Dimension = 3, template<class > class InterfacePolicy = RVOccupancyNodeInterface>
struct OccupancyTreeNode;

template<typename Payload, template<class > class InterfacePolicy>
struct OccupancyTreeNode<Payload, 3, InterfacePolicy> {
  typedef VoxelNode<Payload, InterfacePolicy, OctreeNode> type;  ///< VoxelNode type

//  static_assert(Dimension == 3, "You have to have 3D Map for OctreeNode");
};

/// Specialization of OccupancyTreeNode for 2D
template<typename Payload, template<class > class InterfacePolicy>
struct OccupancyTreeNode <Payload, 2, InterfacePolicy> {
  typedef VoxelNode<Payload, RVOccupancyNodeInterface, QuadtreeNode> type;  ///< VoxelNode type
};


/**
 * @brief ChildPolicy class for SimpleNode with no Child *
 * @tparam Derived derived node
 *
 * @ingroup Nodes
 */
template <typename Derived>
class NoChildPolicy {
private:
  // Boost serialization
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
  }
};

/**
 * Templated typedefs of Occupancy Nodes with No children
 * Currently uses the Struct hack (No need for this in C++11)
 *
 * Usage example:
 * OccupancyNodeNoChild<RV>::type my_RV_occupancy_octree_node;
 * OccupancyNodeNoChild<RV, OccupancyNodeInterface>::type my_occupancy_octree_node;
 *
 * @ingroup Nodes
 */
template<typename Payload, template<class > class InterfacePolicy = RVOccupancyNodeInterface>
struct OccupancyNodeNoChild {
  typedef VoxelNode<Payload, InterfacePolicy, NoChildPolicy> type;
};

} // end namespace Naksha

BOOST_CLASS_IMPLEMENTATION_PP(Naksha::NoChildPolicy, (class), object_serializable)
BOOST_CLASS_TRACKING_PP(Naksha::NoChildPolicy, (class), track_never)
//BOOST_IS_BITWISE_SERIALIZABLE_PP(Naksha::NoChildPolicy, (class))

#endif // NAKSHA_NODE_TYPEDEFS_H_
