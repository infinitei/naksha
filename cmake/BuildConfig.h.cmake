/**
 * @file BuildConfig.h
 * @brief This is auto generated by CMake to define various Build Config globals
 *
 * @author Abhijit Kundu
 */

#ifndef NAKSHA_BUILD_CONFIG_H_
#define NAKSHA_BUILD_CONFIG_H_

#include <string>
#include <iostream>

namespace Naksha {

/// Namespace for Build Configuration of Naksha
namespace Build {

/// @name Build Configuration global constants
/// @{

static const std::string compiler_type = "@CMAKE_CXX_COMPILER_ID@ @CMAKE_CXX_COMPILER_VERSION@";
static const std::string build_type = "@CMAKE_BUILD_TYPE@";
static const std::string build_type_flags = "@CMAKE_BUILD_TYPE_FLAGS@";
static const std::string compile_flags = "@CMAKE_CXX_FLAGS@";

/// @}

/**@brief Prints Build Configuration
 *
 * Calling this function will print some like:
 *
 * ## Build Configuration:
    - Compiler Type:  GNU 4.8.2
    - Build Type:     Release
    - Build Flags:    -O3 -DNDEBUG -march=native -funroll-loops -ffast-math
 */
inline void printBuildConfig() {
  std::cout << "## Build Configuration:\n";
  std::cout << "  - Compiler Type:  " << compiler_type << "\n";
  std::cout << "  - Build Type:     " << build_type << "\n";
  std::cout << "  - Build Flags:    " << build_type_flags << "\n";
  std::cout << "  - Compile Flags:  " << compile_flags << "\n";
}

} // end namespace Build
} // end namespace Naksha

#endif // NAKSHA_BUILD_CONFIG_H_

