###################### InstallProjectConfig  ###########################

# ... for the build tree
set(CONF_INCLUDE_DIRS "${PROJECT_SOURCE_DIR}" "${Boost_INCLUDE_DIRS}" "${EIGEN_INCLUDE_DIRS}")
configure_file( ${PROJECT_CMAKE_DIR}/${PROJECT_NAME}Config.cmake.in
                ${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake
                @ONLY
              )

# ... for the install tree
set(CONF_INCLUDE_DIRS "${INSTALL_INCLUDE_DIR}" "${Boost_INCLUDE_DIRS}" "${EIGEN_INCLUDE_DIRS}")
configure_file( ${PROJECT_CMAKE_DIR}/${PROJECT_NAME}Config.cmake.in
                ${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${PROJECT_NAME}Config.cmake
                @ONLY
              )


# Install the ${PROJECT_NAME}Config.cmake and ${PROJECT_NAME}ConfigVersion.cmake
install(FILES ${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${PROJECT_NAME}Config.cmake
  DESTINATION ${INSTALL_CMAKE_DIR}
  COMPONENT   Devel
  )

if(CMAKE_SUPPORTS_INTERFACE_TARGET)
  # Add all targets to the build-tree export set
  export( TARGETS Naksha
    FILE "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Targets.cmake")

  # Install the export set for use with the install-tree
  install(EXPORT ${PROJECT_NAME}Targets
    DESTINATION ${INSTALL_CMAKE_DIR}
    COMPONENT Devel
    )
endif()