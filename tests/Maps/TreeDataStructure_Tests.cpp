/**
 * @file TreeDataStructure_Tests.cpp
 * @brief TreeDataStructure Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/TreeDataStructure.h"
#include "Naksha/Nodes/NodeTypedefs.h"
#include "Naksha/Keys/Key.h"

#include <boost/test/unit_test.hpp>
#include <type_traits>

namespace Naksha {

template
<
  int _Dimension,
  class _Payload,
  template<class, class > class _Datastructure,
  template<typename > class _NodeInterface = RVOccupancyNodeInterface
>
class CustomMap: public _Datastructure<Key<_Dimension>, typename MapVoxelNode<_Dimension, _Payload, _Datastructure, _NodeInterface>::type > {
public:
  typedef CustomMap<_Dimension, _Payload, _Datastructure, _NodeInterface> This;
  typedef Key<_Dimension> KeyType;
  typedef typename MapVoxelNode<_Dimension, _Payload, _Datastructure, _NodeInterface>::type VoxelNodeType;
  typedef _Datastructure<KeyType, VoxelNodeType> Datastructure;
};

} // end namespace Naksha


using namespace Naksha;
using namespace std;

BOOST_AUTO_TEST_CASE(check_voxelNode_child_policy) {
  typedef float Payload;
  typedef CustomMap<2,Payload,TreeDataStructure> MapType2D;
  typedef CustomMap<3,Payload,TreeDataStructure> MapType3D;

  static_assert(
      (std::is_same<
        VoxelNode<Payload, RVOccupancyNodeInterface, OctreeNode>,
        MapType3D::VoxelNodeType
      >::value),
      "VoxelNodeType for MapType3D is not correct");

  static_assert(
      (std::is_same<
        VoxelNode<Payload, RVOccupancyNodeInterface, QuadtreeNode>,
        MapType2D::VoxelNodeType
      >::value),
      "VoxelNodeType for MapType3D is not correct");
}

template <class MapType>
void simple_float_payload_test() {
  typedef typename MapType::KeyType KeyType;
  typedef typename KeyType::DataType KeyDataType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;

  MapType map;

  VoxelNodeType* voxel = map.search(KeyDataType::Zero());

  BOOST_CHECK_EQUAL(voxel != 0, false);

  map.addNewVoxel(KeyDataType::Zero());
  map.addNewVoxel(KeyDataType::Ones(), 10);

  BOOST_CHECK_EQUAL(map.numberOfVoxels(), 2);
  BOOST_CHECK_EQUAL(map.size(), 2);

  voxel = map.search(KeyDataType::Zero());
  BOOST_CHECK_EQUAL(voxel != 0, true);

  voxel = map.search(KeyDataType::Ones());
  BOOST_CHECK_EQUAL(voxel->payload(), 10);
}

BOOST_AUTO_TEST_CASE(Simple_float_payload) {
  simple_float_payload_test< CustomMap<2,float,TreeDataStructure> >();
  simple_float_payload_test< CustomMap<3,float,TreeDataStructure> >();
}

BOOST_AUTO_TEST_CASE(addNewVoxel) {

  typedef DiscreteRandomVariable<2, float> Payload;
  typedef Payload::DataType PayloadDataType;
  typedef CustomMap<3,Payload,TreeDataStructure> MapType;
  typedef MapType::KeyType KeyType;
  typedef MapType::VoxelNodeType VoxelNodeType;

  MapType map;

  VoxelNodeType* voxel;
  voxel = map.addNewVoxel(KeyType(0,0,0), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));

  voxel = map.addNewVoxel(KeyType(0,0,1), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));

  voxel = map.addNewVoxel(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel->payload().probability(), Eigen::Vector2f(0.5f,0.5f));

  BOOST_CHECK_EQUAL(map.numberOfVoxels(), 3);
  BOOST_CHECK_EQUAL(map.size(), 3);
}

BOOST_AUTO_TEST_CASE(searchAPI) {
  typedef float Payload;
  typedef CustomMap<3,Payload,TreeDataStructure> MapType;
  typedef MapType::KeyType KeyType;
  typedef MapType::VoxelNodeType VoxelNodeType;

  MapType map;

  BOOST_CHECK_EQUAL(map.size(), 0);

  VoxelNodeType* voxel = map.search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, false);
  voxel = map.search(KeyType(0,0,1));
  BOOST_CHECK_EQUAL(voxel != 0, false);

  // Add a new voxel at KeyType(0,0,0)
  map.addNewVoxel(KeyType(0,0,0), 10);
  BOOST_CHECK_EQUAL(map.size(), 1);
  voxel = map.search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK_EQUAL(voxel->payload(), 10);
  voxel = map.search(KeyType(0,0,1));
  BOOST_CHECK_EQUAL(voxel != 0, false);


  // Add a new voxel at KeyType(0,0,1)
  map.addNewVoxel(KeyType(0,0,1));
  BOOST_CHECK_EQUAL(map.size(), 2);
  voxel = map.search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK_EQUAL(voxel->payload(), 10);
  voxel = map.search(KeyType(0,0,1));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  voxel->setPayload(11);
  BOOST_CHECK_EQUAL(voxel->payload(), 11);


  // Try Adding a new voxel at KeyType(0,0,1)
  voxel = map.addNewVoxel(KeyType(0,0,1));
  BOOST_CHECK_EQUAL(voxel->payload(), 11);



  voxel = map.search(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel == 0, true);
  voxel = map.addNewVoxel(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  voxel = map.search(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel != 0, true);

  BOOST_CHECK_EQUAL(map.size(), 3);

  // Verify the search and addNew return pointers
  BOOST_CHECK_EQUAL(map.addNewVoxel(KeyType(0,0,2)), map.search(KeyType(0,0,2)));

  BOOST_CHECK_EQUAL(map.size(), 3);

  voxel = map.search(KeyType(0,0,1));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK_EQUAL(voxel->payload(), 11);
  voxel->setPayload(20);
  BOOST_CHECK_EQUAL(voxel->payload(), 20);

}

BOOST_AUTO_TEST_CASE(TreeStructureCheck) {
  typedef float Payload;
  typedef CustomMap<3,Payload,TreeDataStructure> MapType;
  typedef MapType::KeyType KeyType;
  typedef MapType::VoxelNodeType VoxelNodeType;

  MapType map;

  BOOST_CHECK_EQUAL(map.numberOfVoxels(), 0);
  BOOST_CHECK_EQUAL(map.numberOfNodes(), 0);

  map.addNewVoxel(KeyType(0,0,0));

  BOOST_CHECK_EQUAL(map.numberOfNodes(), MapType::maxTreeDepth() + 1);
  BOOST_CHECK_EQUAL(map.numberOfVoxels(), 1);


  map.addNewVoxel(KeyType(0,0,1), 10);

  BOOST_CHECK_EQUAL(map.numberOfVoxels(), 2);
  BOOST_CHECK_EQUAL(map.size(), 2);

  int depth = 0;
  VoxelNodeType * node = map.rootNode();
  while (node->hasAnyChildren()) {
    for (unsigned int i = 0; i < VoxelNodeType::MaxNumOfChilds; ++i) {
      if(node->childExistsAt(i)) {
        ++depth;
        node = node->childPtr(i);
        break;
      }
    }
  }

  BOOST_CHECK_EQUAL(depth, MapType::maxTreeDepth());


}

BOOST_AUTO_TEST_CASE(TreeIteratorTest) {
  typedef float Payload;
  typedef CustomMap<3,Payload,TreeDataStructure> MapType;
  typedef MapType::KeyType KeyType;
  typedef KeyType::DataType KeyDataType;

  BOOST_TEST_MESSAGE( "\nTesting TreeIterator:" );

  MapType map;

  map.addNewVoxel(KeyType(0,0,1), 10);

  {
    MapType::node_iterator it = map.begin_node();
    MapType::node_iterator end = map.end_node();

    for (size_t expected_depth = 0; expected_depth < (MapType::maxTreeDepth() + 1); ++it, ++expected_depth) {

      BOOST_CHECK_EQUAL(it != end, true);

      BOOST_TEST_MESSAGE( "Node Details: "
          " Depth= " << it.depth() <<
          " Key= " << it.key() <<
          " Payload= " << it->payload() <<
          " IsLeafNode= " << boolalpha <<it.isLeafNode());

      BOOST_CHECK_EQUAL(it->payload(), 10);
      BOOST_CHECK_EQUAL(it.depth(), expected_depth);

      KeyDataType expect_key_value;
      bool expected_leaf = false;
      if(expected_depth < MapType::maxTreeDepth())
        expect_key_value = KeyDataType::Constant(32768 / std::pow(2,expected_depth));
      else {
        expect_key_value = KeyDataType(0,0,1);
        expected_leaf = true;
      }

      BOOST_CHECK_EQUAL(it.key().value, expect_key_value);
      BOOST_CHECK_EQUAL(it.isLeafNode(), expected_leaf);
    }
  }
}

BOOST_AUTO_TEST_CASE(LeafIteratorTests) {
  typedef float Payload;
  typedef CustomMap<3,Payload,TreeDataStructure> MapType;
  typedef MapType::KeyType KeyType;
  typedef KeyType::DataType KeyDataType;

  BOOST_TEST_MESSAGE( "\nTesting LeafIterator:" );

  MapType map;

  map.addNewVoxel(KeyType(0,0,0));
  map.addNewVoxel(KeyType(0,0,1), 10);
  map.addNewVoxel(KeyType(0,1,1), 20);
  map.addNewVoxel(KeyType(1,1,1), 30);
  map.addNewVoxel(KeyType(2,1,1), 40);

  {
    MapType::iterator it = map.begin();
    MapType::iterator end = map.end();

    size_t count = 0;
    float payload_sum = 0;
    KeyDataType key_sum = KeyDataType::Zero();
    for (; it != end; ++it) {

    BOOST_TEST_MESSAGE( "Node Details: "
        " Depth= " << it.depth() <<
        " Key= " << it.key() <<
        " Payload= " << it->payload() <<
        " IsLeafNode= " << boolalpha <<it.isLeafNode());


      BOOST_CHECK_EQUAL(it.isLeafNode(), true);
      BOOST_CHECK_EQUAL(it.depth(), MapType::maxTreeDepth());

      ++count;
      payload_sum += it->payload();
      key_sum += it.key().value;
    }

    BOOST_CHECK_EQUAL(count, 5);
    BOOST_CHECK_EQUAL(payload_sum, 100);
    BOOST_CHECK_EQUAL(key_sum, KeyDataType(3, 3, 4));
  }
}

BOOST_AUTO_TEST_CASE(findAPI) {
  typedef float Payload;
  typedef CustomMap<3, Payload, TreeDataStructure> MapType;
  typedef MapType::KeyType KeyType;
  typedef MapType::const_iterator VoxelIt;

  BOOST_TEST_MESSAGE("\nTesting findAPI:");

  MapType map;

  VoxelIt voxel_it = map.find(KeyType(0, 0, 0));
  BOOST_CHECK(voxel_it == map.end());

  map.addNewVoxel(KeyType(0, 0, 0), 10);

  voxel_it = map.find(KeyType(0, 0, 0));
  BOOST_CHECK(voxel_it != map.end());
  BOOST_CHECK_EQUAL(voxel_it->payload(), 10);

  voxel_it = map.find(KeyType(0, 0, 2));
  BOOST_CHECK(voxel_it == map.end());
  map.addNewVoxel(KeyType(0, 0, 2), 11);
  voxel_it = map.find(KeyType(0, 0, 2));
  BOOST_CHECK(voxel_it != map.end());
  BOOST_CHECK_EQUAL(voxel_it->payload(), 11);

  BOOST_CHECK(*map.addNewVoxel(KeyType(0, 0, 2)) == *map.find(KeyType(0, 0, 2)));
  BOOST_CHECK_EQUAL(map.size(), 2);
}

BOOST_AUTO_TEST_CASE(findIteratorTest) {
  typedef float Payload;
  typedef CustomMap<3, Payload, TreeDataStructure> MapType;
  typedef MapType::KeyType KeyType;

  typedef MapType::const_iterator VoxelIt;
  typedef MapType::node_const_iterator NodeIt;

  BOOST_TEST_MESSAGE("\nTesting findIteratorTest ");

  MapType map;

  BOOST_CHECK_EQUAL(map.size(), 0);

  map.addNewVoxel(KeyType(0, 0, 0), 1);
  map.addNewVoxel(KeyType(1, 0, 0), 2);
  map.addNewVoxel(KeyType(2, 0, 0), 3);
  map.addNewVoxel(KeyType(3, 0, 0), 4);

  map.addNewVoxel(KeyType(0, 5, 0), 5);
  map.addNewVoxel(KeyType(0, 8, 0), 6);
  map.addNewVoxel(KeyType(0, 8, 5), 7);
  map.addNewVoxel(KeyType(0, 8, 9), 8);

  BOOST_TEST_MESSAGE("Node Iterator Contents");
  NodeIt it_spl;
  for (NodeIt it = map.begin_node(); it != map.end_node(); ++it) {
    BOOST_TEST_MESSAGE( "Node Details: "
        " Depth= " << it.depth() <<
        " Key= " << it.key() <<
        " Payload= " << it->payload() <<
        " IsLeafNode= " << boolalpha <<it.isLeafNode());
    if (it.key() == KeyType(0, 0, 0))
      it_spl = it;
  }

  BOOST_TEST_MESSAGE("Node Iterator Stack at Key(0,0,0):");
  {
    BOOST_TEST_MESSAGE( "Stack Size = " << it_spl.stack().size() );
    NodeIt::StackType dump = it_spl.stack();
    for (; !dump.empty(); dump.pop())
      BOOST_TEST_MESSAGE( dump.top().key << ' ' << (int) dump.top().depth );

  }

  BOOST_TEST_MESSAGE("Voxel Iterator Stack at Key(0,0,0):");


  VoxelIt::StackType expected_stack;
  {
    VoxelIt voxel_it = map.begin();
    BOOST_TEST_MESSAGE("Stack Size = " << voxel_it.stack().size() );
    VoxelIt::StackType dump = voxel_it.stack();
    expected_stack = dump;
    for (; !dump.empty(); dump.pop())
      BOOST_TEST_MESSAGE(dump.top().key << ' ' << (int) dump.top().depth);
  }

  BOOST_TEST_MESSAGE("Find Iterator Stack at Key(0,0,0):");
  VoxelIt::StackType stack;
  {
    VoxelIt voxel_it = map.find(KeyType(0, 0, 0));
    BOOST_CHECK_EQUAL(voxel_it->payload(), 1);
    BOOST_TEST_MESSAGE( "Stack Size = " << voxel_it.stack().size() );
    VoxelIt::StackType dump = voxel_it.stack();
    stack = dump;
    for (; !dump.empty(); dump.pop())
      BOOST_TEST_MESSAGE( dump.top().key << ' ' << (int) dump.top().depth );
  }

  BOOST_CHECK_EQUAL(stack.size(), expected_stack.size());
  for (; !stack.empty(); stack.pop(), expected_stack.pop()) {

    BOOST_CHECK_EQUAL(stack.empty(), expected_stack.empty());
    BOOST_CHECK_EQUAL(stack.top().key, expected_stack.top().key);
    BOOST_CHECK_EQUAL(stack.top().depth, expected_stack.top().depth);

  }

  BOOST_TEST_MESSAGE("===================");

  {
    VoxelIt it = map.find(KeyType(0, 0, 0));
    BOOST_CHECK_EQUAL(it->payload(), 1);

    Payload sum = Payload();
    for (; it != map.end(); ++it) {
      sum += it->payload();

      BOOST_TEST_MESSAGE( "Node Details: "
          " Depth= " << it.depth() <<
          " Key= " << it.key() <<
          " Payload= " << it->payload() <<
          " IsLeafNode= " << boolalpha <<it.isLeafNode());
    }
    BOOST_CHECK_EQUAL(sum, 36);
  }
}


