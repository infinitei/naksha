/**
 * @file UnorderedMapDataStructure_Tests.cpp
 * @brief UnorderedMapDataStructure Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/UnorderedMapDataStructure.h"
#include "Naksha/Nodes/NodeTypedefs.h"
#include "Naksha/Keys/Key.h"

#include <boost/test/unit_test.hpp>

namespace Naksha {

template
<
  int _Dimension,
  class _Payload,
  template<class, class > class _Datastructure,
  template<typename > class _NodeInterface = RVOccupancyNodeInterface
>
class CustomMap: public _Datastructure<Key<_Dimension>,  VoxelNode<_Payload, _NodeInterface, NoChildPolicy> > {
public:
  typedef CustomMap<_Dimension, _Payload, _Datastructure, _NodeInterface> This;
  typedef Key<_Dimension> KeyType;
  typedef VoxelNode<_Payload, _NodeInterface, NoChildPolicy> VoxelNodeType;
  typedef _Datastructure<KeyType, VoxelNodeType> Datastructure;

  CustomMap() : Datastructure(){};
};

} // end namespace Naksha


using namespace Naksha;
using namespace std;

BOOST_AUTO_TEST_CASE(Constructor1) {
  typedef float Payload;
  typedef CustomMap<3,Payload,UnorderedMapDataStructure> MapType;
  typedef MapType::KeyType KeyType;
  typedef MapType::VoxelNodeType VoxelNodeType;

  MapType map;

  map.data()[KeyType(0,0,0)] = VoxelNodeType(10);
  map.data()[KeyType(0,0,1)] = VoxelNodeType(11);

  VoxelNodeType* voxel = map.search(KeyType(0,0,0));

  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK_EQUAL(voxel->payload(), 10);
  BOOST_CHECK_EQUAL(map.data().size(), 2);
  BOOST_CHECK(map.data().at(KeyType(0,0,1)) == VoxelNodeType(11));
}

BOOST_AUTO_TEST_CASE(Constructor2) {
  typedef float Payload;
  typedef CustomMap<3,Payload,UnorderedMapDataStructure> MapType;
  typedef MapType::KeyType KeyType;
  typedef MapType::VoxelNodeType VoxelNodeType;

  MapType map;

  map.reserve(2);

  map.data()[KeyType(0,0,0)] = VoxelNodeType(10);
  map.data()[KeyType(0,0,1)] = VoxelNodeType(11);

  VoxelNodeType* voxel = map.search(KeyType(0,0,0));

  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK_EQUAL(voxel->payload(), 10);
  BOOST_CHECK_EQUAL(map.data().size(), 2);
  BOOST_CHECK(map.data().at(KeyType(0,0,1)) == VoxelNodeType(11));
}

BOOST_AUTO_TEST_CASE(addNewVoxel) {

  typedef DiscreteRandomVariable<2, float> Payload;
  typedef Payload::DataType PayloadDataType;
  typedef CustomMap<3,Payload,UnorderedMapDataStructure> MapType;
  typedef MapType::KeyType KeyType;
  typedef MapType::VoxelNodeType VoxelNodeType;

  MapType map;

  VoxelNodeType* voxel;
  voxel = map.addNewVoxel(KeyType(0,0,0), VoxelNodeType(PayloadDataType(0.5f,0.5f)));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));

  voxel = map.addNewVoxel(KeyType(0,0,1), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));

  voxel = map.addNewVoxel(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel->payload().probability(), Eigen::Vector2f(0.5f,0.5f));

  BOOST_CHECK_EQUAL(map.data().size(), 3);

}

BOOST_AUTO_TEST_CASE(emplace_new_voxel) {

  typedef DiscreteRandomVariable<2, float> Payload;
  typedef CustomMap<3,Payload,UnorderedMapDataStructure> MapType;
  typedef MapType::KeyType KeyType;
  typedef MapType::VoxelNodeType VoxelNodeType;

  MapType map;

  VoxelNodeType* voxel_node;
  bool new_voxel;
  std::tie(voxel_node, new_voxel) = map.emplace(KeyType(0,0,0));
//  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));

//  voxel = map.addNewVoxel(KeyType(0,0,1), PayloadDataType(0.5f,0.5f));
//  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
//
//  voxel = map.addNewVoxel(KeyType(0,0,2));
//  BOOST_CHECK_EQUAL(voxel->payload().probability(), Eigen::Vector2f(0.5f,0.5f));
//
//  BOOST_CHECK_EQUAL(map.data().size(), 3);

}

BOOST_AUTO_TEST_CASE(searchAPI) {
  typedef float Payload;
  typedef CustomMap<3,Payload,UnorderedMapDataStructure> MapType;
  typedef MapType::KeyType KeyType;
  typedef MapType::VoxelNodeType VoxelNodeType;

  MapType map;

  map.addNewVoxel(KeyType(0,0,0), VoxelNodeType(10));
  map.data()[KeyType(0,0,1)] = VoxelNodeType(11);

  VoxelNodeType* voxel = map.search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK_EQUAL(voxel->payload(), 10);
  BOOST_CHECK_EQUAL(map.data().size(), 2);
  BOOST_CHECK(map.data().at(KeyType(0,0,1)) == VoxelNodeType(11));

  voxel = map.search(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel == 0, true);
  voxel = map.addNewVoxel(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  voxel = map.search(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel != 0, true);

  BOOST_CHECK_EQUAL(map.addNewVoxel(KeyType(0,0,2)), map.search(KeyType(0,0,2)));
}

BOOST_AUTO_TEST_CASE(findAPI) {
  typedef float Payload;
  typedef CustomMap<3,Payload,UnorderedMapDataStructure> MapType;
  typedef MapType::KeyType KeyType;
  typedef MapType::VoxelNodeType VoxelNodeType;

  typedef MapType::const_iterator VoxelIt;

  MapType map;

  VoxelIt voxel_it = map.find(KeyType(0,0,0));
  BOOST_CHECK(voxel_it == map.end());

  map.addNewVoxel(KeyType(0,0,0), VoxelNodeType(10));
  map.data()[KeyType(0,0,1)] = VoxelNodeType(11);

  voxel_it = map.find(KeyType(0,0,0));
  BOOST_CHECK(voxel_it != map.end());
  BOOST_CHECK_EQUAL(voxel_it->payload(), 10);
  BOOST_CHECK_EQUAL(map.data().size(), 2);
  BOOST_CHECK(map.data().at(KeyType(0,0,1)) == VoxelNodeType(11));

  voxel_it = map.find(KeyType(0,0,2));
  BOOST_CHECK(voxel_it == map.end());
  map.addNewVoxel(KeyType(0,0,2));
  voxel_it = map.find(KeyType(0,0,2));
  BOOST_CHECK(voxel_it != map.end());

//  BOOST_CHECK_EQUAL(map.insert(KeyType(0,0,2)), map.find(KeyType(0,0,2)));
}

BOOST_AUTO_TEST_CASE(eraseAPI) {
  typedef float Payload;
  typedef CustomMap<3,Payload,UnorderedMapDataStructure> MapType;
  typedef MapType::KeyType KeyType;
  typedef MapType::VoxelNodeType VoxelNodeType;

  typedef MapType::iterator VoxelIt;

  MapType map;
  map.addNewVoxel(KeyType(0,0,0), VoxelNodeType(10));
  map.addNewVoxel(KeyType(0,0,1), VoxelNodeType(11));
  map.addNewVoxel(KeyType(0,0,2), VoxelNodeType(12));
  map.addNewVoxel(KeyType(0,0,3), VoxelNodeType(13));
  map.addNewVoxel(KeyType(0,0,4), VoxelNodeType(14));
  map.addNewVoxel(KeyType(0,0,5), VoxelNodeType(14));

  BOOST_CHECK_EQUAL(map.data().size(), 6);

  map.erase(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(map.data().size(), 5);


  VoxelIt voxel_it = map.find(KeyType(0,0,4));
  BOOST_CHECK_EQUAL(voxel_it.base()->second.payload(), 14);
  BOOST_CHECK(voxel_it.base() != map.end().base());
  BOOST_CHECK(voxel_it != map.end());

  map.erase(voxel_it);
  BOOST_CHECK_EQUAL(map.data().size(), 4);

  // Loop and erase
  for (auto it = map.begin(); it != map.end();) {
    it = map.erase(it);
  }
  BOOST_CHECK_EQUAL(map.data().size(), 0);
}


