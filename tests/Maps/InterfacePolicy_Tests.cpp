/**
 * @file InterfacePolicy_Tests.cpp
 * @brief InterfacePolicy_Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/HybridMappingInterface.h"
#include "Naksha/Maps/VolumetricMap.h"
#include "Naksha/Maps/UnorderedMapDataStructure.h"
#include "Naksha/Maps/TreeDataStructure.h"
#include "Naksha/Maps/HashedVectorDataStructure.h"
#include "Naksha/Maps/MapTransform.h"
#include "Naksha/Payload/DiscreteRandomVariable.h"
#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Keys/DenseKeyIterator.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;

typedef DiscreteRandomVariable<3, float, LogProbabilityTag> RVLP3f;
typedef VolumetricMap<3,RVLP3f,UnorderedMapDataStructure,OccupancyMappingInteface,MapTransform> UnorderedMapOIRVLP3f;
typedef VolumetricMap<3,RVLP3f,UnorderedMapDataStructure,HybridMappingInterface,MapTransform> UnorderedMapHIRVLP3f;
typedef VolumetricMap<3,RVLP3f,TreeDataStructure,OccupancyMappingInteface,MapTransform> OctreeMapOIRVLP3f;
typedef VolumetricMap<3,RVLP3f,TreeDataStructure,HybridMappingInterface,MapTransform> OctreeMapHIRVLP3f;
typedef VolumetricMap<3,RVLP3f,HashedVectorDataStructure,OccupancyMappingInteface,MapTransform> HashedVectorMapOIRVLP3f;
typedef VolumetricMap<3,RVLP3f,HashedVectorDataStructure,HybridMappingInterface,MapTransform> HashedVectorMapHIRVLP3f;


typedef boost::mpl::list<
      UnorderedMapOIRVLP3f
    , UnorderedMapHIRVLP3f
    , OctreeMapOIRVLP3f
    , OctreeMapHIRVLP3f
    , HashedVectorMapOIRVLP3f
    , HashedVectorMapHIRVLP3f
> MapTypes3f;

BOOST_AUTO_TEST_CASE_TEMPLATE(updateVoxelAPI, MapType, MapTypes3f) {
  Vector3f hit_sensor_model (0.3f,0.35f,0.35f);
  Vector3f miss_sensor_model (0.7f,0.15f,0.15f);

  hit_sensor_model = hit_sensor_model.cwiseQuotient(Vector3f(0.5,0.25,0.25));
  miss_sensor_model = miss_sensor_model.cwiseQuotient(Vector3f(0.5,0.25,0.25));
  hit_sensor_model = hit_sensor_model.array().log();
  miss_sensor_model = miss_sensor_model.array().log();

  typedef typename MapType::Payload Payload;
  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::Datastructure::VoxelNodeType VoxelNodeType;

  BOOST_TEST_MESSAGE( "\nTesting updateVoxelAPI with :" );
  BOOST_TEST_MESSAGE( " MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map(0.1);

  KeyType key(0,0,0);
  const IOFormat vecfmt(StreamPrecision, DontAlignCols, ", ", ", ", "", "", "[", "]");

  VoxelNodeType* voxel;
  voxel = map.addNewVoxel(key);
  const Payload& payload = voxel->payload();

  BOOST_TEST_MESSAGE( payload.probability().format(vecfmt) << "   Sum = " <<  payload.probability().sum() );
  map.updateVoxel(key, hit_sensor_model);
  BOOST_TEST_MESSAGE( payload.probability().format(vecfmt) << "   Sum = " <<  payload.probability().sum() );
  map.updateVoxel(key, hit_sensor_model);
  BOOST_TEST_MESSAGE( payload.probability().format(vecfmt) << "   Sum = " << payload.probability().sum() );
  map.updateVoxel(key, miss_sensor_model);
  BOOST_TEST_MESSAGE( payload.probability().format(vecfmt) << "   Sum = " << payload.probability().sum() );
  map.updateVoxel(key, miss_sensor_model);
  BOOST_TEST_MESSAGE( payload.probability().format(vecfmt) << "   Sum = " << payload.probability().sum() );

  BOOST_CHECK_EQUAL(payload.probability(), Vector3f(0.5f,0.25f,0.25f));


  // Update voxel on a uninitialized node
  key = KeyType(0,0,1);
  voxel = map.search(key);
  BOOST_CHECK_EQUAL(voxel == 0, true);
  map.updateVoxel(key, hit_sensor_model);
  voxel = map.search(key);
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK(voxel->payload().probability().isApprox(Vector3f(0.3f,0.35f,0.35f)));
}


BOOST_AUTO_TEST_CASE_TEMPLATE(insertNewVoxelAPI, MapType, MapTypes3f) {


  typedef typename MapType::Payload Payload;
  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::Datastructure::VoxelNodeType VoxelNodeType;
  typedef typename Payload::DataType PayloadDataType;

  BOOST_TEST_MESSAGE( "\nTesting updateVoxelAPI with :" );
  BOOST_TEST_MESSAGE( " MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );


  MapType map(0.1);

  VoxelNodeType* voxel_node;

  BOOST_CHECK_EQUAL(map.size(), 0);

  VoxelNodeType default_voxel_node;

  // test emplace with default node contructor
  voxel_node = map.insertNewVoxel(KeyType(0,0,0));
  BOOST_CHECK(voxel_node != 0);
  BOOST_CHECK(*voxel_node == default_voxel_node);
  BOOST_CHECK_EQUAL(voxel_node->payload().getData(), default_voxel_node.payload().getData());

  BOOST_CHECK_EQUAL(map.size(), 1);

  /// adding new voxel at existing key should not change anything
  voxel_node = map.insertNewVoxel(KeyType(0,0,0));
  BOOST_CHECK(voxel_node != 0);
  BOOST_CHECK(*voxel_node == default_voxel_node);
  BOOST_CHECK_EQUAL(voxel_node->payload().getData(), default_voxel_node.payload().getData());

  BOOST_CHECK_EQUAL(map.size(), 1);

  // test emplace with init data
  voxel_node = map.insertNewVoxel(KeyType(0,0,1), PayloadDataType::Zero());
  BOOST_CHECK(voxel_node != 0);
  BOOST_CHECK_EQUAL(voxel_node->payload().getData(), PayloadDataType::Zero());

  BOOST_CHECK_EQUAL(map.size(), 2);

  /// adding new voxel at existing key should not change anything
  voxel_node = map.insertNewVoxel(KeyType(0,0,1), PayloadDataType::Ones());
  BOOST_CHECK(voxel_node != 0);
  BOOST_CHECK_EQUAL(voxel_node->payload().getData(), PayloadDataType::Zero());

  BOOST_CHECK_EQUAL(map.size(), 2);
}

typedef boost::mpl::list<
    UnorderedMapHIRVLP3f,
    OctreeMapHIRVLP3f
> ChangeDetectionMapTypes3f;

BOOST_AUTO_TEST_CASE_TEMPLATE(change_detection_Tests, MapType, ChangeDetectionMapTypes3f) {
  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;

  BOOST_TEST_MESSAGE( "\nTesting change_detection_Tests with :" );
  BOOST_TEST_MESSAGE( " MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map(0.1);
  map.enableChangeDetection();

  BOOST_CHECK_EQUAL(map.size(), 0);

  VoxelNodeType* voxel;
  KeyType key1(100, 100, 100);
  voxel = map.updateVoxel(key1, MeasurementModelType::HIT);
  BOOST_CHECK(voxel != 0);
  BOOST_CHECK_EQUAL(map.size(), 1);
  BOOST_CHECK_EQUAL(map.changedKeys().size(), 1);
  BOOST_CHECK(map.changedKeys().find(key1) != map.changedKeys().end());

  KeyType key2(300, 400, 700);
  voxel = map.updateVoxel(key2, MeasurementModelType::MISS);
  BOOST_CHECK(voxel != 0);
  BOOST_CHECK_EQUAL(map.size(), 2);
  BOOST_CHECK_EQUAL(map.changedKeys().size(), 2);
  BOOST_CHECK(map.changedKeys().find(key1) != map.changedKeys().end());
  BOOST_CHECK(map.changedKeys().find(key2) != map.changedKeys().end());

  voxel = map.updateVoxel(key2, MeasurementModelType::HIT);
  BOOST_CHECK(voxel != 0);
  BOOST_CHECK_EQUAL(map.size(), 2);
  BOOST_CHECK_EQUAL(map.changedKeys().size(), 2);
  BOOST_CHECK(map.changedKeys().find(key2) != map.changedKeys().end());

  map.resetChangeDetection();
  BOOST_CHECK_EQUAL(map.changedKeys().size(), 0);

  voxel = map.updateVoxel(key2, MeasurementModelType::HIT);
  BOOST_CHECK(voxel != 0);
  BOOST_CHECK_EQUAL(map.size(), 2);
  BOOST_CHECK_EQUAL(map.changedKeys().size(), 1);
  BOOST_CHECK(map.changedKeys().find(key2) != map.changedKeys().end());

  map.resetChangeDetection();
  BOOST_CHECK_EQUAL(map.changedKeys().size(), 0);


  KeyType key3(200, 200, 200);
  voxel = map.insertNewVoxel(key3);
  BOOST_CHECK(voxel != 0);
  BOOST_CHECK_EQUAL(map.size(), 3);
  BOOST_CHECK_EQUAL(map.changedKeys().size(), 1);
  BOOST_CHECK(map.changedKeys().find(key3) != map.changedKeys().end());

  map.resetChangeDetection();
  BOOST_CHECK_EQUAL(map.changedKeys().size(), 0);

  voxel = map.insertNewVoxel(key3);
  BOOST_CHECK(voxel != 0);
  BOOST_CHECK_EQUAL(map.size(), 3);
  BOOST_CHECK_EQUAL(map.changedKeys().size(), 0);
  BOOST_CHECK(map.changedKeys().find(key3) == map.changedKeys().end());
}
