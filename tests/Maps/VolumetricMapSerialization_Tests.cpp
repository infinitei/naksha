/**
 * @file VolumetricMapSerialization_Tests.cpp
 * @brief VolumetricMap Serialization Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Maps/MapSerialization.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/serialization/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

using namespace Naksha;
using namespace Eigen;

typedef DiscreteRandomVariable<2, float> RVLP2f;
typedef VolumetricMap<3,RVLP2f,UnorderedMapDataStructure,OccupancyMappingInteface,MapTransform> UnorderedMapRVLP2f;
typedef VolumetricMap<3,RVLP2f,TreeDataStructure,OccupancyMappingInteface,MapTransform> OctreeMapRVLP2f;
typedef VolumetricMap<3,RVLP2f,HashedVectorDataStructure,OccupancyMappingInteface,MapTransform> HashedVectorMapRVLP2f;

typedef boost::mpl::list<
      UnorderedMapRVLP2f
    , OctreeMapRVLP2f
    , HashedVectorMapRVLP2f
> MapTypesRVLP2f;

BOOST_AUTO_TEST_CASE_TEMPLATE(VolumetricMap_Serializtion, MapType, MapTypesRVLP2f) {

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef typename MapType::Payload Payload;
  typedef typename Payload::DataType PayloadDataType;

  MapType map(0.2);

  VoxelNodeType* voxel;
  voxel = map.addNewVoxel(KeyType(0,0,0), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map.search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, true);

  voxel = map.addNewVoxel(KeyType(0,0,1), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));

  serializeBinary(map, "map.naksha");

  MapType map_in;
  deSerializeBinary(map_in, "map.naksha");

  BOOST_CHECK_EQUAL(map_in.resolution(), 0.2);

  voxel = map_in.addNewVoxel(KeyType(0,0,0), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map_in.search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, true);

  voxel = map_in.addNewVoxel(KeyType(0,0,1), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map_in.search(KeyType(0,0,1));
  BOOST_CHECK(voxel->payload() == Payload(PayloadDataType(0.5f,0.5f)));

  BOOST_CHECK_EQUAL(map.keyFromCoord(Vector3d(5.1, 4.9, 5.0)), map_in.keyFromCoord(Vector3d(5.1, 4.9, 5.0)));

#ifdef BOOST_NO_CXX11_FUNCTION_TEMPLATE_DEFAULT_ARGS
  BOOST_CHECK_EQUAL(map.template voxelCenter<Vector3d>(KeyType(32818, 3, 1)), map_in.template voxelCenter<Vector3d>(KeyType(32818, 3, 1)));
#else
  BOOST_CHECK_EQUAL(map.voxelCenter(KeyType(32818, 3, 1)), map_in.voxelCenter(KeyType(32818, 3, 1)));
#endif
}

BOOST_AUTO_TEST_CASE_TEMPLATE(VolumetricMap_SharedPtr_Serializtion, MapType, MapTypesRVLP2f) {

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef typename MapType::Payload Payload;
  typedef typename Payload::DataType PayloadDataType;
  typedef boost::shared_ptr<MapType> MapPtr;

  MapPtr map;
  map = boost::make_shared<MapType>(0.2);

  VoxelNodeType* voxel;
  voxel = map->addNewVoxel(KeyType(0,0,0), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map->search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, true);

  voxel = map->addNewVoxel(KeyType(0,0,1), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));

  serializeBinary(map, "map_ptr.naksha");

  MapPtr map_in;
  deSerializeBinary(map_in, "map_ptr.naksha");

  BOOST_CHECK_EQUAL(map_in->resolution(), 0.2);

  voxel = map_in->addNewVoxel(KeyType(0,0,0), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map_in->search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, true);

  voxel = map_in->addNewVoxel(KeyType(0,0,1), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map_in->search(KeyType(0,0,1));
  BOOST_CHECK(voxel->payload() == Payload(PayloadDataType(0.5f,0.5f)));

  BOOST_CHECK_EQUAL(map->keyFromCoord(Vector3d(5.1, 4.9, 5.0)), map_in->keyFromCoord(Vector3d(5.1, 4.9, 5.0)));

#ifdef BOOST_NO_CXX11_FUNCTION_TEMPLATE_DEFAULT_ARGS
  BOOST_CHECK_EQUAL(map->template voxelCenter<Vector3d>(KeyType(32818, 3, 1)), map_in->template voxelCenter<Vector3d>(KeyType(32818, 3, 1)));
#else
  BOOST_CHECK_EQUAL(map->voxelCenter(KeyType(32818, 3, 1)), map_in->voxelCenter(KeyType(32818, 3, 1)));
#endif
}


BOOST_AUTO_TEST_CASE_TEMPLATE(VolumetricMap_BasePtr_Serializtion, MapType, MapTypesRVLP2f) {

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef typename MapType::Payload Payload;
  typedef typename Payload::DataType PayloadDataType;
  typedef boost::shared_ptr<MapType> MapPtr;

  MapPtr map;
  map = boost::make_shared<MapType>(0.2);

  VoxelNodeType* voxel;
  voxel = map->addNewVoxel(KeyType(0,0,0), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map->search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, true);

  voxel = map->addNewVoxel(KeyType(0,0,1), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));

  serializeMap(map.get(), "map_ptr.naksha");

  MapPtr map_in (deSerializeMap<MapType>("map_ptr.naksha"));

  BOOST_CHECK_EQUAL(map_in->resolution(), 0.2);

  voxel = map_in->addNewVoxel(KeyType(0,0,0), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map_in->search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, true);

  voxel = map_in->addNewVoxel(KeyType(0,0,1), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map_in->search(KeyType(0,0,1));
  BOOST_CHECK(voxel->payload() == Payload(PayloadDataType(0.5f,0.5f)));

  BOOST_CHECK_EQUAL(map->keyFromCoord(Vector3d(5.1, 4.9, 5.0)), map_in->keyFromCoord(Vector3d(5.1, 4.9, 5.0)));

#ifdef BOOST_NO_CXX11_FUNCTION_TEMPLATE_DEFAULT_ARGS
  BOOST_CHECK_EQUAL(map->template voxelCenter<Vector3d>(KeyType(32818, 3, 1)), map_in->template voxelCenter<Vector3d>(KeyType(32818, 3, 1)));
#else
  BOOST_CHECK_EQUAL(map->voxelCenter(KeyType(32818, 3, 1)), map_in->voxelCenter(KeyType(32818, 3, 1)));
#endif
}
