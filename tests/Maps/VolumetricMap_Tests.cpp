/**
 * @file VolumetricMap_Tests.cpp
 * @brief VolumetricMap_Tests
 *
 * @author Abhijit Kundu
 */
#include "Naksha/Maps/VolumetricMap.h"

#include "Naksha/Payload/DiscreteRandomVariable.h"
#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Maps/UnorderedMapDataStructure.h"
#include "Naksha/Maps/ConcurrentUnorderedMapDataStructure.h"
#include "Naksha/Maps/TreeDataStructure.h"
#include "Naksha/Maps/HashedVectorDataStructure.h"
#include "Naksha/Maps/HybridMappingInterface.h"
#include "Naksha/Maps/MapTransform.h"
#include "Naksha/Keys/DenseKeyIterator.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>
#include <boost/mpl/joint_view.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;

typedef DiscreteRandomVariable<2, float> RVLP2f;
typedef VolumetricMap<3,RVLP2f,UnorderedMapDataStructure,OccupancyMappingInteface,MapTransform> UnorderedMapRVLP2f;
typedef VolumetricMap<3,RVLP2f,TreeDataStructure,OccupancyMappingInteface,MapTransform> OctreeMapRVLP2f;
typedef VolumetricMap<3,RVLP2f,HashedVectorDataStructure,OccupancyMappingInteface,MapTransform> HashedVectorMapRVLP2f;
typedef VolumetricMap<3,RVLP2f,ConcurrentUnorderedMapDataStructure,OccupancyMappingInteface,MapTransform> ConcurrentUnorderedMapRVLP2f;

typedef boost::mpl::list<
      UnorderedMapRVLP2f
    , OctreeMapRVLP2f
    , HashedVectorMapRVLP2f
    , ConcurrentUnorderedMapRVLP2f
> MapTypes3D_RVLP2f;

typedef boost::mpl::list<
      VolumetricMap<2,RVLP2f,UnorderedMapDataStructure,HybridMappingInterface,MapTransform>
    , VolumetricMap<2,RVLP2f,TreeDataStructure,OccupancyMappingInteface,MapTransform>
    , VolumetricMap<2,RVLP2f,HashedVectorDataStructure,OccupancyMappingInteface,MapTransform>
    , VolumetricMap<2,RVLP2f,ConcurrentUnorderedMapDataStructure,OccupancyMappingInteface,MapTransform>
> MapTypes2D_RVLP2f;

typedef boost::mpl::joint_view<MapTypes3D_RVLP2f, MapTypes2D_RVLP2f> MapTypes_RVLP2f;

typedef DiscreteRandomVariable<3, float, LogProbabilityTag> RVLP3f;
typedef VolumetricMap<3,RVLP3f,UnorderedMapDataStructure,OccupancyMappingInteface,MapTransform> UnorderedMapRVLP3f;
typedef VolumetricMap<3,RVLP3f,TreeDataStructure,OccupancyMappingInteface,MapTransform> OctreeMapRVLP3f;
typedef VolumetricMap<3,RVLP3f,HashedVectorDataStructure,OccupancyMappingInteface,MapTransform> HashedVectorMapRVLP3f;
typedef VolumetricMap<3,RVLP3f,ConcurrentUnorderedMapDataStructure,OccupancyMappingInteface,MapTransform> ConcurrentUnorderedMapRVLP3f;

typedef boost::mpl::list<
      UnorderedMapRVLP3f
    , OctreeMapRVLP3f
    , HashedVectorMapRVLP3f
    , ConcurrentUnorderedMapRVLP3f
> MapTypes3D_RVLP3f;


BOOST_AUTO_TEST_CASE_TEMPLATE(basic_construction_and_usage, MapType, MapTypes_RVLP2f) {
  BOOST_TEST_MESSAGE( "\nRunning basic_construction_and_usage test with :" );
  BOOST_TEST_MESSAGE( " MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map(0.45);
  BOOST_CHECK_EQUAL(map.resolution(), 0.45);

  typedef Eigen::Matrix<double, MapType::Dimension, 1> VectorType;
  VectorType end_point = VectorType::Constant(5.0);
  map.updateVoxel(end_point, MapType::Payload::DataType::Ones());

}

BOOST_AUTO_TEST_CASE_TEMPLATE(DatastrcutureAPI, MapType, MapTypes3D_RVLP2f) {

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::Datastructure::VoxelNodeType VoxelNodeType;
  typedef typename MapType::Payload::DataType PayloadDataType;

  BOOST_TEST_MESSAGE( "\nTesting DatastrcutureAPI with :" );
  BOOST_TEST_MESSAGE( " MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map(0.1);

  BOOST_CHECK_EQUAL(map.size(), 0);

  VoxelNodeType* voxel;
  voxel = map.addNewVoxel(KeyType(0,0,0), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map.search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, true);

  BOOST_CHECK_EQUAL(map.size(), 1);

  voxel = map.addNewVoxel(KeyType(0,0,1), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map.search(KeyType(0,0,1));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK(voxel->payload().getData() == Eigen::Vector2f(0.5f,0.5f));

  BOOST_CHECK_EQUAL(map.size(), 2);
  map.addNewVoxel(KeyType(0,0,1), PayloadDataType(0.1f,0.5f));
  BOOST_CHECK(voxel->payload().getData() == Eigen::Vector2f(0.5f,0.5f));
  BOOST_CHECK_EQUAL(map.size(), 2);

  voxel = map.search(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel == 0, true);

  voxel = map.addNewVoxel(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK_EQUAL(voxel->payload().probability(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map.search(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel != 0, true);

  BOOST_CHECK_EQUAL(map.size(), 3);
  BOOST_CHECK_EQUAL(map.addNewVoxel(KeyType(0,0,2)), map.search(KeyType(0,0,2)));
}


BOOST_AUTO_TEST_CASE_TEMPLATE(updateVoxel, MapType, MapTypes3D_RVLP3f) {
  Vector3f hit_sensor_model (0.3f,0.35f,0.35f);
  Vector3f miss_sensor_model (0.7f,0.15f,0.15f);

  hit_sensor_model = hit_sensor_model.cwiseQuotient(Vector3f(0.5,0.25,0.25));
  miss_sensor_model = miss_sensor_model.cwiseQuotient(Vector3f(0.5,0.25,0.25));
  hit_sensor_model = hit_sensor_model.array().log();
  miss_sensor_model = miss_sensor_model.array().log();

  typedef typename MapType::Payload Payload;
  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::Datastructure::VoxelNodeType VoxelNodeType;

  BOOST_TEST_MESSAGE( "\nTesting updateVoxel with :" );
  BOOST_TEST_MESSAGE( " MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );


  MapType map(0.1);

  KeyType key(0,0,0);
  const IOFormat vecfmt(StreamPrecision, DontAlignCols, ", ", ", ", "", "", "[", "]");

  VoxelNodeType* voxel;
  voxel = map.addNewVoxel(key);
  const Payload& payload = voxel->payload();

  BOOST_TEST_MESSAGE( payload.probability().format(vecfmt) << "   Sum = " <<  payload.probability().sum() );
  map.updateVoxel(key, hit_sensor_model);
  BOOST_TEST_MESSAGE( payload.probability().format(vecfmt) << "   Sum = " <<  payload.probability().sum() );
  map.updateVoxel(key, hit_sensor_model);
  BOOST_TEST_MESSAGE( payload.probability().format(vecfmt) << "   Sum = " << payload.probability().sum() );
  map.updateVoxel(key, miss_sensor_model);
  BOOST_TEST_MESSAGE( payload.probability().format(vecfmt) << "   Sum = " << payload.probability().sum() );
  map.updateVoxel(key, miss_sensor_model);
  BOOST_TEST_MESSAGE( payload.probability().format(vecfmt) << "   Sum = " << payload.probability().sum() );

  BOOST_CHECK_EQUAL(payload.probability(), Vector3f(0.5f,0.25f,0.25f));


  // Update voxel on a unintialized node
  key = KeyType(0,0,1);
  voxel = map.search(key);
  BOOST_CHECK_EQUAL(voxel == 0, true);
  map.updateVoxel(key, hit_sensor_model);
  voxel = map.search(key);
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK(voxel->payload().probability().isApprox(Vector3f(0.3f,0.35f,0.35f)));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterators_basic, MapType, MapTypes_RVLP2f) {
  BOOST_TEST_MESSAGE( "\nTesting iterators_basic with :" );
  BOOST_TEST_MESSAGE( " MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map(0.45);
  BOOST_CHECK_EQUAL(map.resolution(), 0.45);

  typedef typename MapType::KeyType KeyType;
  typedef typename KeyType::DataType KeyDataType;

  const KeyType min_bbx_key(KeyDataType::Constant(0));
  const KeyType max_bbx_key(KeyDataType::Constant(3));
  for (const KeyType& key : DenseKeyRange(min_bbx_key, max_bbx_key)) {
    map.addNewVoxel(key);
  }

  {
    typedef typename MapType::iterator MapVoxelIt;
    for(MapVoxelIt it = map.begin(); it != map.end(); ++it) {
      BOOST_CHECK(it->payload().probability().isApprox(Vector2f(0.5f,0.5f)));
    }
  }

}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterators_full, MapType, MapTypes3D_RVLP3f){
  Vector3f HIT (0.3f,0.35f,0.35f);
  Vector3f MISS (0.7f,0.15f,0.15f);

  HIT = HIT.cwiseQuotient(Vector3f(0.5,0.25,0.25));
  MISS = MISS.cwiseQuotient(Vector3f(0.5,0.25,0.25));
  HIT = HIT.array().log();
  MISS = MISS.array().log();

  typedef typename MapType::KeyType KeyType;

  BOOST_TEST_MESSAGE( "\nTesting iterators_full with :" );
  BOOST_TEST_MESSAGE( " MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map(0.1);

  const KeyType min_bbx_key(0, 0, 0);
  const KeyType max_bbx_key(3, 3, 3);
  for (const KeyType& key : DenseKeyRange(min_bbx_key, max_bbx_key)) {
    map.updateVoxel(key, HIT);
  }

  {
    typedef typename MapType::iterator MapVoxelIt;
    BOOST_TEST_MESSAGE( "==========   Using iterator   =============" );
    for(MapVoxelIt it = map.begin(); it != map.end(); ++it) {
      const IOFormat vecfmt(StreamPrecision, DontAlignCols, ", ", ", ", "", "", "[", "]");
      BOOST_TEST_MESSAGE( it->payload().probability().format(vecfmt) );
      BOOST_CHECK(it->payload().probability().isApprox(Vector3f(0.3f,0.35f,0.35f)));
    }
  }

  {
    typedef typename MapType::const_iterator MapVoxelIt;
    BOOST_TEST_MESSAGE( "======== Using const_iterator ============" );
    for(MapVoxelIt it = map.begin(); it != map.end(); ++it) {
      const IOFormat vecfmt(StreamPrecision, DontAlignCols, ", ", ", ", "", "", "[", "]");
      BOOST_TEST_MESSAGE( it->payload().probability().format(vecfmt) );
      BOOST_CHECK(it->payload().probability().isApprox(Vector3f(0.3f,0.35f,0.35f)));
    }
  }

  {
    BOOST_TEST_MESSAGE( "==== Using Range based For loop ==========" );
    for (typename MapType::VoxelNodeType& voxel : map) {
      const IOFormat vecfmt(StreamPrecision, DontAlignCols, ", ", ", ", "", "", "[", "]");
      BOOST_TEST_MESSAGE( voxel.payload().probability().format(vecfmt) );
      BOOST_CHECK(voxel.payload().probability().isApprox(Vector3f(0.3f,0.35f,0.35f)));
    }
  }

}

BOOST_AUTO_TEST_CASE_TEMPLATE(MapInterfaceBase_Tests, MapType, MapTypes3D_RVLP2f) {

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef typename MapType::Payload::DataType PayloadDataType;

  BOOST_TEST_MESSAGE( "\nTesting MapInterfaceBase with :" );
  BOOST_TEST_MESSAGE( " MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map(1.0);

  BOOST_CHECK_EQUAL(map.size(), 0);

  VoxelNodeType* voxel;
  KeyType key_in(100, 100, 100);
  BOOST_TEST_MESSAGE( "key_in = " << key_in );
  voxel = map.addNewVoxel(key_in, PayloadDataType(0.5f,0.5f));
  Eigen::Vector3f coord = map.template voxelCenter<Eigen::Vector3f>(key_in);
  BOOST_TEST_MESSAGE( "coord = " << coord.transpose() );
  KeyType key_out = map.keyFromCoord(coord);
  BOOST_TEST_MESSAGE( "key_out = " << key_out );
  BOOST_CHECK_EQUAL(key_in, key_out);

  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map.search(coord);
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK_EQUAL(map.size(), 1);
}



typedef VolumetricMap<3,RVLP2f,UnorderedMapDataStructure,HybridMappingInterface,MapTransform> ChangeDetectionUnorderedMapRVLP2f;
typedef VolumetricMap<3,RVLP2f,TreeDataStructure,HybridMappingInterface,MapTransform> ChangeDetectionOctreeMapRVLP2f;
typedef VolumetricMap<3,RVLP2f,HashedVectorDataStructure,HybridMappingInterface,MapTransform> ChangeDetectionHashedVectorMapRVLP2f;
typedef VolumetricMap<3,RVLP2f,ConcurrentUnorderedMapDataStructure,HybridMappingInterface,MapTransform> ChangeDetectionConcurrentUnorderedMapRVLP2f;


typedef boost::mpl::list<
      ChangeDetectionUnorderedMapRVLP2f
    , ChangeDetectionOctreeMapRVLP2f
    , ChangeDetectionHashedVectorMapRVLP2f
    , ChangeDetectionConcurrentUnorderedMapRVLP2f
> ChangeDetectionMapTypes2f;

BOOST_AUTO_TEST_CASE_TEMPLATE(change_detection_Tests, MapType, ChangeDetectionMapTypes2f) {
  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;

  BOOST_TEST_MESSAGE( "\nTesting change_detection_Tests with :" );
  BOOST_TEST_MESSAGE( " MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map(0.1);
  map.enableChangeDetection();

  BOOST_CHECK_EQUAL(map.size(), 0);

  VoxelNodeType* voxel;
  KeyType key1(100, 100, 100);
  voxel = map.updateVoxel(key1, MeasurementModelType::HIT);
  BOOST_CHECK(voxel != 0);
  BOOST_CHECK_EQUAL(map.size(), 1);
  BOOST_CHECK_EQUAL(map.changedKeys().size(), 1);
  BOOST_CHECK(map.changedKeys().find(key1) != map.changedKeys().end());

  KeyType key2(300, 400, 700);
  voxel = map.updateVoxel(key2, MeasurementModelType::MISS);
  BOOST_CHECK(voxel != 0);
  BOOST_CHECK_EQUAL(map.size(), 2);
  BOOST_CHECK_EQUAL(map.changedKeys().size(), 2);
  BOOST_CHECK(map.changedKeys().find(key1) != map.changedKeys().end());
  BOOST_CHECK(map.changedKeys().find(key2) != map.changedKeys().end());

  map.resetChangeDetection();
  BOOST_CHECK_EQUAL(map.changedKeys().size(), 0);

  KeyType key3(100, 233, 567);
  voxel = map.insertNewVoxel(key3);
}
