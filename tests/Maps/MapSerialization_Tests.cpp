/**
 * @file MapSerialization_Tests.cpp
 * @brief Map Serialization Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapSerialization.h"
#include "Naksha/Maps/MapMaker.h"

#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Nodes/NodeUtils.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Algorithms/CompareMaps.h"

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>
#include <boost/mpl/joint_view.hpp>


// Mapping works for Naksha and HybridOctree
template<class MapType>
void mapping(MapType& map) {
  const int occ_span = 20;
  const int free_span = 40;

  typedef Naksha::MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;

  for (int x = -occ_span; x < occ_span; ++x) {
    for (int y = -occ_span; y < occ_span; ++y) {
      for (int z = -occ_span; z < occ_span; ++z) {
        const Eigen::Vector3f endpoint((float) x * 0.05f, (float) y * 0.05f,
            (float) z * 0.05f);

        map.updateVoxel(endpoint, MeasurementModelType::HIT); // integrate 'occupied' measurement
      }
    }
  }

  // insert some measurements of free cells
  for (int x = -free_span; x < free_span; ++x) {
    for (int y = -free_span; y < free_span; ++y) {
      for (int z = -free_span; z < free_span; ++z) {
        const Eigen::Vector3f endpoint((float) x * 0.02f - 1.0f, (float) y * 0.02f - 1.0f,
            (float) z * 0.02f - 1.0f);

        map.updateVoxel(endpoint, MeasurementModelType::MISS); // integrate 'free' measurement
      }
    }
  }
}


using namespace Naksha;

typedef DiscreteRandomVariable<9, float> RV9f;
typedef DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> BLORF;

typedef ColoredRandomVariable<RV9f>::type ColoredRV9f;
typedef ColoredRandomVariable<BLORF>::type ColoredBLORF;

typedef UnorderedMap3D<ColoredRV9f>::type UnorderedMap3DColoredRV9f;
typedef UnorderedMap3D<ColoredBLORF>::type UnorderedMap3DColoredBLORF;
typedef UnorderedMap3D<BLORF>::type UnorderedMap3DBLORF;

typedef OctreeMap<ColoredRV9f>::type OctreeMapColoredRV9f;
typedef OctreeMap<ColoredBLORF>::type OctreeMapColoredBLORF;
typedef OctreeMap<BLORF>::type OctreeMapBLORF;

typedef HashedVectorMap<ColoredRV9f>::type HashedVectorMapColoredRV9f;
typedef HashedVectorMap<ColoredBLORF>::type HashedVectorMapColoredBLORF;
typedef HashedVectorMap<BLORF>::type HashedVectorMapBLORF;

typedef boost::mpl::list<
    UnorderedMap3DColoredRV9f,
    UnorderedMap3DColoredBLORF,
    UnorderedMap3DBLORF
  > UnorderedMapTypes;

typedef boost::mpl::list<
    OctreeMapColoredRV9f,
    OctreeMapColoredBLORF,
    OctreeMapBLORF
  > OctreeMapTypes;

typedef boost::mpl::list<
    HashedVectorMapColoredRV9f,
    HashedVectorMapColoredBLORF,
    HashedVectorMapBLORF
  > HashedVectorMapTypes;

template<class MapType>
void testMapSerialization() {
  typedef typename MapType::VoxelNodeType VoxelNodeType;

  BOOST_TEST_MESSAGE("\n\nRunning MapSerializationTest with :");
  BOOST_TEST_MESSAGE(" MapType = " << PRETTY_TYPE(MapType, "Naksha", 1));
  BOOST_TEST_MESSAGE(" VoxelNodeType = " << PRETTY_TYPE(VoxelNodeType, "Naksha", 2));
  BOOST_TEST_MESSAGE(" IsBinaryRV = " << std::boolalpha << IsBinaryRV<typename MapType::Payload>::value );

  typedef boost::shared_ptr<MapType> MapTypePtr;

  MapTypePtr map = boost::make_shared<MapType>(0.1);

  mapping(*map);

  serializeMap(*map, "maptype.naksha");

  MapTypePtr map_in(deSerializeMap<MapType>("maptype.naksha"));

  // Compare
  BOOST_CHECK_EQUAL(compareMaps(*map, *map_in), true);

  size_t solid_voxel_count_map = std::count_if(map->begin(), map->end(), IsNodeOccupied<VoxelNodeType>());
  size_t solid_voxel_count_map_in = std::count_if(map_in->begin(), map_in->end(), IsNodeOccupied<VoxelNodeType>());

  BOOST_CHECK_EQUAL(solid_voxel_count_map, solid_voxel_count_map_in);
  BOOST_CHECK_EQUAL(solid_voxel_count_map_in, 7488);
  BOOST_CHECK_EQUAL(map_in->size(), 11584);
}


BOOST_AUTO_TEST_CASE_TEMPLATE(UnorderedMapSerializationTest, MapType, UnorderedMapTypes) {
  testMapSerialization<MapType>();
}

BOOST_AUTO_TEST_CASE_TEMPLATE(OctreeMapSerializationTest, MapType, OctreeMapTypes) {
  testMapSerialization<MapType>();
}

BOOST_AUTO_TEST_CASE_TEMPLATE(HashedVectorMapSerializationTest, MapType, HashedVectorMapTypes) {
  testMapSerialization<MapType>();
}

