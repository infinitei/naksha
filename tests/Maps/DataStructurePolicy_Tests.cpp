/**
 * @file DataStructurePolicy_Tests.cpp
 * @brief Unit Tests for DataStructure Policy classes e.g. TreeDataStructure, HashedVectorDataStructure, etc.
 * 
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/TreeDataStructure.h"
#include "Naksha/Maps/UnorderedMapDataStructure.h"
#include "Naksha/Maps/HashedVectorDataStructure.h"
#include "Naksha/Maps/ConcurrentUnorderedMapDataStructure.h"
#include "Naksha/Nodes/NodeTypedefs.h"
#include "Naksha/Keys/Key.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>
#include <boost/mpl/joint_view.hpp>

namespace Naksha {

template
<
  int _Dimension,
  class _Payload,
  template<class, class > class _Datastructure,
  template<typename > class _NodeInterface = RVOccupancyNodeInterface
>
class CustomMap: public _Datastructure< Key<_Dimension>, typename MapVoxelNode<_Dimension, _Payload, _Datastructure, _NodeInterface>::type > {
public:
  typedef CustomMap<_Dimension, _Payload, _Datastructure, _NodeInterface> This;
  typedef Key<_Dimension> KeyType;
  typedef typename MapVoxelNode<_Dimension, _Payload, _Datastructure, _NodeInterface>::type VoxelNodeType;
  typedef _Payload Payload;
  typedef _Datastructure<KeyType, VoxelNodeType> Datastructure;
};

} // end namespace Naksha


using namespace Naksha;
using namespace std;

typedef CustomMap<3,float,UnorderedMapDataStructure> UnorderedMapf;
typedef CustomMap<3,float,TreeDataStructure> OctreeMapf;
typedef CustomMap<3,float,HashedVectorDataStructure> HashedVectorMapf;
typedef CustomMap<3,float,ConcurrentUnorderedMapDataStructure> ConcurrentUnorderedMapf;

typedef boost::mpl::list<
      UnorderedMapf
    , OctreeMapf
    , HashedVectorMapf
    , ConcurrentUnorderedMapf
> MapTypesf;

typedef DiscreteRandomVariable<2, float> RVLP2f;
typedef CustomMap<3,RVLP2f,UnorderedMapDataStructure> UnoderedMapRVLP2f;
typedef CustomMap<3,RVLP2f,TreeDataStructure> OctreeMapRVLP2f;
typedef CustomMap<3,RVLP2f,HashedVectorDataStructure> HashedVectorRVLP2f;
typedef CustomMap<3,RVLP2f,ConcurrentUnorderedMapDataStructure> ConcurrentUnoderedMapRVLP2f;

typedef boost::mpl::list<
    UnoderedMapRVLP2f
    , OctreeMapRVLP2f
    , HashedVectorRVLP2f
    , ConcurrentUnoderedMapRVLP2f
> MapTypesRVLP2f;

typedef DiscreteRandomVariable<4, double> AlignedRV;
typedef boost::mpl::list<
      UnorderedMapf
    , OctreeMapf
    , HashedVectorMapf
    , UnoderedMapRVLP2f
    , OctreeMapRVLP2f
    , HashedVectorRVLP2f
    , CustomMap<3,AlignedRV,UnorderedMapDataStructure>
    , CustomMap<3,AlignedRV,TreeDataStructure>
    , CustomMap<3,AlignedRV,HashedVectorDataStructure>
    , CustomMap<3,AlignedRV,ConcurrentUnorderedMapDataStructure>
> MapTypes;


BOOST_AUTO_TEST_CASE_TEMPLATE(Constructors, MapType, MapTypes) {

  BOOST_TEST_MESSAGE( "\nTesting Constructors with :" );
  BOOST_TEST_MESSAGE( " MapType = " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map;
  MapType* map_ptr = new MapType();

  BOOST_CHECK(map_ptr != 0);

  BOOST_TEST_MESSAGE( "Done" );
}

BOOST_AUTO_TEST_CASE_TEMPLATE(clear_and_size, MapType, MapTypes) {
  typedef typename MapType::KeyType KeyType;
  typedef typename KeyType::Scalar KeyScalar;

  BOOST_TEST_MESSAGE( "\nTesting clear_and_size with :" );
  BOOST_TEST_MESSAGE( " MapType = " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map;

  BOOST_CHECK_EQUAL(map.numberOfVoxels(), 0);
  BOOST_CHECK_EQUAL(map.size(), 0);

  typename MapType::size_type num_of_voxels = 0;
  for (KeyScalar x = 0; x < 20; ++x )
    for (KeyScalar y = 0; y < 20; ++y )
      for (KeyScalar z = 0; z < 20; ++z ) {
        map.addNewVoxel(KeyType(x,y,z));
        ++num_of_voxels;
        BOOST_CHECK_EQUAL(map.numberOfVoxels(), num_of_voxels);
      }

  BOOST_CHECK_EQUAL(map.numberOfVoxels(), 20*20*20);
  BOOST_CHECK_EQUAL(map.size(), 20*20*20);

  map.clear();

  BOOST_CHECK_EQUAL(map.numberOfVoxels(), 0);
  BOOST_CHECK_EQUAL(map.size(), 0);

  BOOST_TEST_MESSAGE( "Done" );
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reserve_operations, MapType, MapTypes) {
  typedef typename MapType::KeyType KeyType;
  typedef typename KeyType::Scalar KeyScalar;

  BOOST_TEST_MESSAGE( "\nTesting reserve_operations with :" );
  BOOST_TEST_MESSAGE( " MapType = " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map;

  BOOST_CHECK_EQUAL(map.numberOfVoxels(), 0);
  BOOST_CHECK_EQUAL(map.size(), 0);

  map.reserve(20*20*20);

  for (KeyScalar x = 0; x < 20; ++x )
    for (KeyScalar y = 0; y < 20; ++y )
      for (KeyScalar z = 0; z < 20; ++z ) {
        map.addNewVoxel(KeyType(x,y,z));
      }

  BOOST_CHECK_EQUAL(map.numberOfVoxels(), 20*20*20);
  BOOST_CHECK_EQUAL(map.size(), 20*20*20);

  BOOST_TEST_MESSAGE( "Done" );
}

BOOST_AUTO_TEST_CASE_TEMPLATE(addNewVoxel_and_search, MapType, MapTypesf) {
  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;

  BOOST_TEST_MESSAGE( "\nTesting addNewVoxel_and_search with :" );
  BOOST_TEST_MESSAGE( " MapType = " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map;

  BOOST_CHECK_EQUAL(map.size(), 0);

  VoxelNodeType* voxel = map.search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, false);
  voxel = map.search(KeyType(0,0,1));
  BOOST_CHECK_EQUAL(voxel != 0, false);

  // Add a new voxel at KeyType(0,0,0)
  map.addNewVoxel(KeyType(0,0,0), 10);
  BOOST_CHECK_EQUAL(map.size(), 1);
  voxel = map.search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK_EQUAL(voxel->payload(), 10);
  voxel = map.search(KeyType(0,0,1));
  BOOST_CHECK_EQUAL(voxel != 0, false);


  // Add a new voxel at KeyType(0,0,1)
  map.addNewVoxel(KeyType(0,0,1));
  BOOST_CHECK_EQUAL(map.size(), 2);
  voxel = map.search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK_EQUAL(voxel->payload(), 10);
  voxel = map.search(KeyType(0,0,1));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  voxel->setPayload(11);
  BOOST_CHECK_EQUAL(voxel->payload(), 11);


  // Try Adding a new voxel at KeyType(0,0,1)
  voxel = map.addNewVoxel(KeyType(0,0,1));
  BOOST_CHECK_EQUAL(voxel->payload(), 11);

  voxel = map.search(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel == 0, true);
  voxel = map.addNewVoxel(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  voxel = map.search(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel != 0, true);

  BOOST_CHECK_EQUAL(map.size(), 3);

  // Verify the search and addNew return pointers
  BOOST_CHECK_EQUAL(map.addNewVoxel(KeyType(0,0,2)), map.search(KeyType(0,0,2)));

  BOOST_CHECK_EQUAL(map.size(), 3);

  BOOST_TEST_MESSAGE( "Done" );
}

BOOST_AUTO_TEST_CASE_TEMPLATE(addNewVoxel_and_search_ver2, MapType, MapTypesRVLP2f){

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef typename MapType::Payload Payload;
  typedef typename Payload::DataType PayloadDataType;

  BOOST_TEST_MESSAGE( "\nTesting addNewVoxel_and_search_ver2 with :" );
  BOOST_TEST_MESSAGE( " MapType = " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map;

  BOOST_CHECK_EQUAL(map.size(), 0);

  VoxelNodeType* voxel;
  voxel = map.addNewVoxel(KeyType(0,0,0), PayloadDataType(0.5f,0.5f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map.search(KeyType(0,0,0));
  BOOST_CHECK_EQUAL(voxel != 0, true);

  BOOST_CHECK_EQUAL(map.size(), 1);

  voxel = map.addNewVoxel(KeyType(0,0,1), PayloadDataType(0.5f,0.3f));
  BOOST_CHECK_EQUAL(voxel->payload().getData(), Eigen::Vector2f(0.5f,0.3f));
  voxel = map.search(KeyType(0,0,1));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK(voxel->payload().getData() == Eigen::Vector2f(0.5f,0.3f));

  /// adding new voxel at existing key should not change anything
  BOOST_CHECK_EQUAL(map.size(), 2);
  map.addNewVoxel(KeyType(0,0,1), PayloadDataType(0.1f,0.5f));
  BOOST_CHECK(voxel->payload().getData() == Eigen::Vector2f(0.5f,0.3f));
  BOOST_CHECK_EQUAL(map.size(), 2);

  voxel = map.search(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel == 0, true);

  voxel = map.addNewVoxel(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK_EQUAL(voxel->payload().probability(), Eigen::Vector2f(0.5f,0.5f));
  voxel = map.search(KeyType(0,0,2));
  BOOST_CHECK_EQUAL(voxel != 0, true);

  BOOST_CHECK_EQUAL(map.size(), 3);
  BOOST_CHECK_EQUAL(map.addNewVoxel(KeyType(0,0,2)), map.search(KeyType(0,0,2)));

  BOOST_TEST_MESSAGE( "Done" );
}

BOOST_AUTO_TEST_CASE_TEMPLATE(emplace_tests, MapType, MapTypesRVLP2f){

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef typename MapType::Payload Payload;
  typedef typename Payload::DataType PayloadDataType;

  BOOST_TEST_MESSAGE( "\nTesting addNewVoxel_and_search_ver2 with :" );
  BOOST_TEST_MESSAGE( " MapType = " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map;

  VoxelNodeType* voxel_node;
  bool new_voxel;

  BOOST_CHECK_EQUAL(map.size(), 0);

  VoxelNodeType default_voxel_node;

  // test emplace with default node constructor
  std::tie(voxel_node, new_voxel) = map.emplace(KeyType(0,0,0));
  BOOST_CHECK(*voxel_node == default_voxel_node);
  BOOST_CHECK_EQUAL(voxel_node->payload().getData(), default_voxel_node.payload().getData());
  BOOST_CHECK_EQUAL(new_voxel, true);

  BOOST_CHECK_EQUAL(map.size(), 1);

  /// adding new voxel at existing key should not change anything
  std::tie(voxel_node, new_voxel) = map.emplace(KeyType(0,0,0));
  BOOST_CHECK(voxel_node != 0);
  BOOST_CHECK(*voxel_node == default_voxel_node);
  BOOST_CHECK_EQUAL(voxel_node->payload().getData(), default_voxel_node.payload().getData());
  BOOST_CHECK_EQUAL(new_voxel, false);

  BOOST_CHECK_EQUAL(map.size(), 1);

  // test emplace with init data
  std::tie(voxel_node, new_voxel) = map.emplace(KeyType(0,0,1), PayloadDataType(0.5f,0.3f));
  BOOST_CHECK_EQUAL(voxel_node->payload().getData(), Eigen::Vector2f(0.5f,0.3f));
  BOOST_CHECK_EQUAL(new_voxel, true);

  BOOST_CHECK_EQUAL(map.size(), 2);

  /// adding new voxel at existing key should not change anything
  std::tie(voxel_node, new_voxel) = map.emplace(KeyType(0,0,1), PayloadDataType(0.1f,0.5f));
  BOOST_CHECK_EQUAL(voxel_node->payload().getData(), Eigen::Vector2f(0.5f,0.3f));
  BOOST_CHECK_EQUAL(new_voxel, false);

  BOOST_CHECK_EQUAL(map.size(), 2);

  BOOST_TEST_MESSAGE( "Done" );
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterators, MapType, MapTypesf){

  typedef typename MapType::KeyType KeyType;
  typedef typename KeyType::DataType KeyDataType;

  BOOST_TEST_MESSAGE( "\nTesting iterators with :" );
  BOOST_TEST_MESSAGE( " MapType = " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );


  MapType map;

  BOOST_CHECK_EQUAL(map.size(), 0);

  map.addNewVoxel(KeyType(0,0,0), 1);
  map.addNewVoxel(KeyType(1,0,0), 2);
  map.addNewVoxel(KeyType(2,0,0), 3);
  map.addNewVoxel(KeyType(3,0,0), 4);

  map.addNewVoxel(KeyType(0,5,0), 5);
  map.addNewVoxel(KeyType(0,8,0), 6);
  map.addNewVoxel(KeyType(0,8,5), 7);
  map.addNewVoxel(KeyType(0,8,9), 8);

  BOOST_CHECK_EQUAL(map.size(), 8);

  BOOST_TEST_MESSAGE("Voxel Contents:");

  size_t count = 0;
  float payload_sum = 0;
  KeyDataType key_sum = KeyDataType::Zero();
  typedef typename MapType::const_iterator VoxelIt;
  for(VoxelIt it= map.begin(); it != map.end(); ++it) {
    BOOST_TEST_MESSAGE(" Key = " << it.key() << " Payload = " << it->payload());
    ++count;
    payload_sum += it->payload();
    key_sum += it.key().value;
  }

  BOOST_CHECK_EQUAL(count, 8);
  BOOST_CHECK_EQUAL(payload_sum, 36);
  BOOST_CHECK_EQUAL(key_sum, KeyDataType(6, 29, 14));

  BOOST_TEST_MESSAGE( "Done" );
}

BOOST_AUTO_TEST_CASE_TEMPLATE(findAPI, MapType, MapTypesf) {
  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::const_iterator VoxelIt;

  BOOST_TEST_MESSAGE( "\nTesting findAPI with :" );
  BOOST_TEST_MESSAGE( " MapType = " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );


  MapType map;

  VoxelIt voxel_it = map.find(KeyType(0,0,0));
  BOOST_CHECK(voxel_it == map.end());

  map.addNewVoxel(KeyType(0,0,0), 10);

  voxel_it = map.find(KeyType(0,0,0));
  BOOST_CHECK(voxel_it != map.end());
  BOOST_CHECK_EQUAL(voxel_it->payload(), 10);

  voxel_it = map.find(KeyType(0,0,2));
  BOOST_CHECK(voxel_it == map.end());
  map.addNewVoxel(KeyType(0,0,2), 11);
  voxel_it = map.find(KeyType(0,0,2));
  BOOST_CHECK(voxel_it != map.end());
  BOOST_CHECK_EQUAL(voxel_it->payload(), 11);

//  BOOST_CHECK_EQUAL(map.insert(KeyType(0,0,2)), map.find(KeyType(0,0,2)));

 BOOST_CHECK(*map.addNewVoxel(KeyType(0,0,2)) == *map.find(KeyType(0,0,2)));

 BOOST_CHECK_EQUAL(map.size(), 2);
 BOOST_TEST_MESSAGE( "Done" );
}

BOOST_AUTO_TEST_CASE_TEMPLATE(erase_operations, MapType, MapTypes) {
  typedef typename MapType::KeyType KeyType;
  typedef typename KeyType::Scalar KeyScalar;

  BOOST_TEST_MESSAGE( "\nTesting erase_operations with :" );
  BOOST_TEST_MESSAGE( " MapType = " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map;

  BOOST_CHECK_EQUAL(map.numberOfVoxels(), 0);
  BOOST_CHECK_EQUAL(map.size(), 0);

  for (KeyScalar x = 0; x < 20; ++x )
    for (KeyScalar y = 0; y < 20; ++y )
      for (KeyScalar z = 0; z < 20; ++z ) {
        BOOST_REQUIRE(map.erase(KeyType(x,y,z)) == 0);
        map.addNewVoxel(KeyType(x,y,z));
      }


  typename MapType::size_type num_of_voxels = 20*20*20;

  BOOST_CHECK_EQUAL(map.numberOfVoxels(), num_of_voxels);

  for (KeyScalar x = 0; x < 10; ++x )
    for (KeyScalar y = 0; y < 10; ++y )
      for (KeyScalar z = 0; z < 10; ++z ) {
        BOOST_REQUIRE(map.erase(KeyType(x,y,z)) == 1);
        --num_of_voxels;
        BOOST_CHECK_EQUAL(map.numberOfVoxels(), num_of_voxels);
        BOOST_CHECK(map.find(KeyType(x,y,z)) == map.end());
      }

  BOOST_CHECK_EQUAL(map.numberOfVoxels(), num_of_voxels);

  typedef typename MapType::const_iterator MapConstIterator;
  for(MapConstIterator it = map.begin(); it!= map.end(); ++it) {
    MapConstIterator voxel = map.find(it.key());
    BOOST_REQUIRE(voxel == it);
  }

  BOOST_TEST_MESSAGE( "Done" );
}


template<class MapType>
void checkConstMapOperations(const MapType& map) {
  BOOST_CHECK_EQUAL(map.size(), 2);
  BOOST_CHECK_EQUAL(map.numberOfVoxels(), 2);

  typedef typename MapType::KeyType KeyType;

  typedef typename MapType::const_iterator VoxelIt;

  VoxelIt voxel_it = map.find(KeyType(0, 0, 0));
  BOOST_CHECK(voxel_it != map.end());
  BOOST_CHECK_EQUAL(voxel_it->payload(), 10);

  voxel_it = map.find(KeyType(0,0,2));
  BOOST_CHECK(voxel_it != map.end());
  BOOST_CHECK_EQUAL(voxel_it->payload(), 11);

  typedef typename MapType::VoxelNodeType VoxelNodeType;

  const VoxelNodeType* voxel = map.search(KeyType(0, 0, 0));
  BOOST_CHECK_EQUAL(voxel != 0, true);
  BOOST_CHECK_EQUAL(voxel->payload(), 10);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(const_map_operations, MapType, MapTypesf){

typedef typename MapType::KeyType KeyType;

  BOOST_TEST_MESSAGE( "\nTesting const_map_operations with :" );
  BOOST_TEST_MESSAGE( " MapType = " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map;

  map.addNewVoxel(KeyType(0,0,0), 10);
  map.addNewVoxel(KeyType(0,0,2), 11);

  checkConstMapOperations(map);
}
