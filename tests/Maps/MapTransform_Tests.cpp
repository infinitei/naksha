/**
 * @file MapTransform_Tests.cpp
 * @brief MapTransform Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapTransform.h"
#include "Naksha/Keys/Key.h"

#include <type_traits>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

#include <typeinfo>

using namespace Naksha;
using namespace Eigen;
using namespace std;

typedef Key<3> Key3D;
typedef Key<2> Key2D;

typedef MapTransform<Key3D> MapTransform3d;
typedef MapTransform<Key3D, float> MapTransform3f;
typedef MapTransform<Key2D> MapTransform2d;

BOOST_AUTO_TEST_CASE(CompileTimeChecks) {
  static_assert(MapTransform3d::Dimension == 3, "Should have been 3");
  static_assert(MapTransform3f::Dimension == 3, "Should have been 3");
  static_assert(MapTransform2d::Dimension == 2, "Should have been 2");

  static_assert(MapTransform3d::BoundingBox::AmbientDimAtCompileTime == 3, "Should have been 3");
  static_assert(MapTransform2d::BoundingBox::AmbientDimAtCompileTime == 2, "Should have been 2");

  static_assert((std::is_same<MapTransform3d::Scalar, double>::value), "Problem with Scalar type");
  static_assert((std::is_same<MapTransform3f::Scalar, float>::value), "Problem with Scalar type");
}

typedef boost::mpl::list<
    Key3D,
    Key2D
> KeyTypes;

BOOST_AUTO_TEST_CASE_TEMPLATE(Constructors, KeyType, KeyTypes) {
  MapTransform<KeyType> map1(0.1);
  BOOST_CHECK_EQUAL(map1.resolution(), 0.1);

  MapTransform<KeyType, float > map2(0.1f);
  BOOST_CHECK_EQUAL(map2.resolution(), 0.1f);
}

typedef boost::mpl::list<
    MapTransform3d,
    MapTransform3f,
    MapTransform2d
> MapTransformTypes;

BOOST_AUTO_TEST_CASE_TEMPLATE(MaximumBbxInit, MapTransformType, MapTransformTypes) {
  typedef typename MapTransformType::KeyType KeyType;
  typedef typename KeyType::Scalar KeyScalar;
  typedef typename MapTransformType::KeyBoundingBox KeyBoundingBox;
  typedef Eigen::Matrix<KeyScalar, MapTransformType::Dimension, 1> VectorType;

  MapTransformType map_tfm(0.1);

  IOFormat vecfmt(StreamPrecision, DontAlignCols, ", ", ", ", "", "", "[", "]");
  BOOST_TEST_MESSAGE( "maximumKeyBbx = "
      << map_tfm.maximumKeyBbx().min().format(vecfmt) << " --- "
      << map_tfm.maximumKeyBbx().max().format(vecfmt) );
  BOOST_TEST_MESSAGE( "maximumBbx = "
      << map_tfm.maximumBbx().min().format(vecfmt) << " --- "
      << map_tfm.maximumBbx().max().format(vecfmt) );

  VectorType min(VectorType::Constant(1));
  VectorType max(VectorType::Constant(std::numeric_limits<KeyScalar>::max() - 1));
  KeyBoundingBox expected(min, max);
  BOOST_CHECK_EQUAL(map_tfm.maximumKeyBbx().min(), expected.min());
  BOOST_CHECK_EQUAL(map_tfm.maximumKeyBbx().max(), expected.max());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(keyFromCoord, MapTransformType, MapTransformTypes) {
  typedef typename MapTransformType::KeyType KeyType;

  MapTransformType map_tfm(0.1);

  VectorXf pos;
  pos.setLinSpaced(MapTransformType::Dimension, 0,3);

  KeyType key1 = map_tfm.keyFromCoord(pos);

  KeyType key2;
  bool inside = map_tfm.keyFromCoord(pos, key2);

  BOOST_CHECK_EQUAL(inside, true);
  BOOST_CHECK_EQUAL(key1, key2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(voxelCenterT, MapTransformType, MapTransformTypes) {
  typedef typename MapTransformType::KeyType KeyType;
  MapTransformType map_tfm(0.1);

  cout << "MapTransform::Dim = " << MapTransformType::Dimension
      << " MapTransform::Scalar = " << typeid(typename MapTransformType::Scalar).name() << endl;

  IOFormat VectorFormat(StreamPrecision, DontAlignCols, ", ", ", ", "", "", "[", "]");

  VectorXf gt_pos;
  gt_pos.setLinSpaced(MapTransformType::Dimension, 1,3);
  cout << "gt_pos = " << gt_pos.format(VectorFormat) << endl;

  const KeyType key = map_tfm.keyFromCoord(gt_pos);

  typedef Eigen::Matrix<float, MapTransformType::Dimension, 1>  Vecf;
  typedef Eigen::Matrix<double, MapTransformType::Dimension, 1>  Vecd;

  Vecf centerf = map_tfm.template voxelCenter<Vecf>(key);
  Vecd centerd = map_tfm.template voxelCenter<Vecd>(key);

  cout << "centerf = " << centerf.format(VectorFormat) << endl;
  cout << "centerd = " << centerd.format(VectorFormat) << endl;

  BOOST_CHECK_EQUAL(centerd.template cast<float>().isApprox(centerf), true);
}

BOOST_AUTO_TEST_CASE(VerifyTransforms) {
  MapTransform3d map_tfm(0.1);
  typedef MapTransform3d::KeyType KeyType;
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(Vector3d(5.0, 5.0, 5.0)), KeyType(32818, 32818, 32818));
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(Vector3d(5.1, 4.9, 5.0)), KeyType(32819, 32817, 32818));
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(Vector3d(5.02, 4.87, 5.0)), KeyType(32818, 32816, 32818));

#ifdef BOOST_NO_CXX11_FUNCTION_TEMPLATE_DEFAULT_ARGS
  BOOST_CHECK(map_tfm.voxelCenter<Vector3d>(KeyType(32818, 32818, 32818)).isApprox(Vector3d(5.05, 5.05, 5.05)));
#else
  BOOST_CHECK(map_tfm.voxelCenter(KeyType(32818, 32818, 32818)).isApprox(Vector3d(5.05, 5.05, 5.05)));
#endif
}

BOOST_AUTO_TEST_CASE(keyFromCoord_Tests) {
  BOOST_TEST_MESSAGE( "\nRunning  keyFromCoord Tests");

  typedef MapTransform3d::KeyType KeyType;
  typedef MapTransform3d::VectorType CoordType;

  const double resolution = 0.1;
  MapTransform3d map_tfm(resolution);

  CoordType coord = CoordType(0, 0, 0);
  KeyType expected_key = KeyType(32768, 32768, 32768);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord + CoordType::Constant(0.05)), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord + CoordType::Constant(0.09)), expected_key);

  coord = CoordType(-0.05, -0.05, -0.05);
  expected_key = KeyType(32767, 32767, 32767);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord - CoordType::Constant(0.05)), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord + CoordType::Constant(0.04)), expected_key);

  coord = CoordType(100, 0, 0);
  expected_key = KeyType(33768, 32768, 32768);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord + CoordType::Constant(0.05)), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord + CoordType::Constant(0.09)), expected_key);

  coord = CoordType(100, 0, 100);
  expected_key = KeyType(33768, 32768, 33768);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord + CoordType::Constant(0.05)), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord + CoordType::Constant(0.09)), expected_key);

  coord = CoordType(30, 0, 100);
  expected_key = KeyType(33068, 32768, 33768);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord + CoordType::Constant(0.05)), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord + CoordType::Constant(0.09)), expected_key);

  coord = CoordType(-3276.75, -3276.75, -3276.75);
  expected_key = KeyType(0, 0, 0);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord - CoordType::Constant(0.05)), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord + CoordType::Constant(0.04)), expected_key);

  coord = CoordType(-3266.75, -3266.75, -3266.75);
  expected_key = KeyType(100, 100, 100);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord - CoordType::Constant(0.05)), expected_key);
  BOOST_CHECK_EQUAL(map_tfm.keyFromCoord(coord + CoordType::Constant(0.04)), expected_key);
}

BOOST_AUTO_TEST_CASE(VoxelCenter_Tests) {
  BOOST_TEST_MESSAGE( "\nRunning  VoxelCenter Tests");

  typedef MapTransform3d::KeyType KeyType;
  typedef MapTransform3d::VectorType CoordType;

  const double resolution = 0.1;
  MapTransform3d map_tfm(resolution);

  CoordType coord = map_tfm.voxelCenter<CoordType>(KeyType(32768, 32768, 32768));
  BOOST_CHECK(coord.isApprox(CoordType(0.05, 0.05, 0.05)));

  coord = map_tfm.voxelCenter<CoordType>(KeyType(32767, 32767, 32767));
  BOOST_CHECK(coord.isApprox(CoordType(-0.05, -0.05, -0.05)));

  coord = map_tfm.voxelCenter<CoordType>(KeyType(33768, 32768, 32768));
  BOOST_CHECK(coord.isApprox(CoordType(100, 0, 0) + CoordType::Constant(0.05)));

  coord = map_tfm.voxelCenter<CoordType>(KeyType(33768, 32768, 33768));
  BOOST_CHECK(coord.isApprox(CoordType(100, 0, 100) + CoordType::Constant(0.05)));

  coord = map_tfm.voxelCenter<CoordType>(KeyType(33068, 32768, 33768));
  BOOST_CHECK(coord.isApprox(CoordType(30, 0, 100) + CoordType::Constant(0.05)));

  coord = map_tfm.voxelCenter<CoordType>(KeyType(0, 0, 0));
  BOOST_CHECK(coord.isApprox(CoordType(-3276.75, -3276.75, -3276.75)));

  coord = map_tfm.voxelCenter<CoordType>(KeyType(100, 100, 100));
  BOOST_CHECK(coord.isApprox(CoordType(-3266.75, -3266.75, -3266.75)));
}



BOOST_AUTO_TEST_CASE(Inversibility) {
  BOOST_TEST_MESSAGE( "\nRunning  Inversibility Tests i.e key == keyFromCoord(voxelCenter(key))");

  MapTransform3d map_tfm(0.1);
  typedef MapTransform3d::KeyType KeyType;
  typedef MapTransform3d::VectorType CoordType;

  typedef std::vector<KeyType> KeysContainer;
  KeysContainer keys;
  keys.push_back(KeyType(0, 0, 0));
  keys.push_back(KeyType(100, 100, 100));
  keys.push_back(KeyType(999, 1000, 2000));
  keys.push_back(KeyType(32768, 32768, 32768));
  keys.push_back(KeyType(62768, 62768, 62768));

  for (KeysContainer::const_iterator it = keys.begin(); it!=keys.end(); ++it) {

    const KeyType& key_in = *it;
    CoordType coord = map_tfm.voxelCenter<CoordType>(key_in);
    KeyType key_out = map_tfm.keyFromCoord(coord);

    BOOST_TEST_MESSAGE( "\nkey_in = " << key_in );
    BOOST_TEST_MESSAGE( "voxelCenter(key_in) = " << coord.transpose() );
    BOOST_TEST_MESSAGE( "key_out = " << key_out );

    BOOST_CHECK_EQUAL(key_in, key_out);
  }
}
