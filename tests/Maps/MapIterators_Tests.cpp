/**
 * @file MapIterators_Tests.cpp
 * @brief MapIterators_Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Keys/DenseKeyIterator.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>
#include <boost/mpl/joint_view.hpp>

using namespace Naksha;
using namespace Eigen;

typedef DiscreteRandomVariable<2, float> RVLP2f;

typedef VolumetricMap<3,RVLP2f,UnorderedMapDataStructure,OccupancyMappingInteface,MapTransform> UnorderedMapRVLP2f;
typedef VolumetricMap<3,RVLP2f,TreeDataStructure,OccupancyMappingInteface,MapTransform> OctreeMapRVLP2f;
typedef VolumetricMap<3,RVLP2f,HashedVectorDataStructure,OccupancyMappingInteface,MapTransform> HashedVectorMapRVLP2f;

typedef boost::mpl::list<
      UnorderedMapRVLP2f
    , OctreeMapRVLP2f
    , HashedVectorMapRVLP2f
> MapTypes3D_RVLP2f;

typedef boost::mpl::list<
      VolumetricMap<2,RVLP2f,UnorderedMapDataStructure,HybridMappingInterface,MapTransform>
    , VolumetricMap<2,RVLP2f,TreeDataStructure,OccupancyMappingInteface,MapTransform>
    , VolumetricMap<2,RVLP2f,HashedVectorDataStructure,OccupancyMappingInteface,MapTransform>
> MapTypes2D_RVLP2f;

typedef boost::mpl::joint_view<MapTypes3D_RVLP2f, MapTypes2D_RVLP2f> MapTypes_RVLP2f;

typedef DiscreteRandomVariable<3, float, LogProbabilityTag> RVLP3f;
typedef VolumetricMap<3,RVLP3f,UnorderedMapDataStructure,OccupancyMappingInteface,MapTransform> UnorderedMapRVLP3f;
typedef VolumetricMap<3,RVLP3f,TreeDataStructure,OccupancyMappingInteface,MapTransform> OctreeMapRVLP3f;
typedef VolumetricMap<3,RVLP3f,HashedVectorDataStructure,OccupancyMappingInteface,MapTransform> HashedVectorMapRVLP3f;

typedef boost::mpl::list<
      UnorderedMapRVLP3f
    , OctreeMapRVLP3f
    , HashedVectorMapRVLP3f
> MapTypes3D_RVLP3f;


BOOST_AUTO_TEST_CASE_TEMPLATE(iterators_basic, MapType, MapTypes_RVLP2f) {
  BOOST_TEST_MESSAGE( "\nTesting iterators_basic with :" );
  BOOST_TEST_MESSAGE( " MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map(0.45);
  BOOST_CHECK_EQUAL(map.resolution(), 0.45);

  typedef typename MapType::KeyType KeyType;
  typedef typename KeyType::DataType KeyDataType;

  const KeyType min_bbx_key(KeyDataType::Constant(0));
  const KeyType max_bbx_key(KeyDataType::Constant(3));
  for (const KeyType& key : DenseKeyRange(min_bbx_key, max_bbx_key)) {
    map.addNewVoxel(key);
  }

  {
    typedef typename MapType::iterator MapVoxelIt;
    for(MapVoxelIt it = map.begin(); it != map.end(); ++it) {
      BOOST_CHECK(it->payload().probability().isApprox(Vector2f(0.5f,0.5f)));
    }
  }
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterators_full, MapType, MapTypes3D_RVLP3f){
  Vector3f HIT (0.3f,0.35f,0.35f);
  Vector3f MISS (0.7f,0.15f,0.15f);

  HIT = HIT.cwiseQuotient(Vector3f(0.5,0.25,0.25));
  MISS = MISS.cwiseQuotient(Vector3f(0.5,0.25,0.25));
  HIT = HIT.array().log();
  MISS = MISS.array().log();

  typedef typename MapType::KeyType KeyType;

  BOOST_TEST_MESSAGE( "\nTesting iterators_full with :" );
  BOOST_TEST_MESSAGE( " MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map(0.1);

  const KeyType min_bbx_key(0, 0, 0);
  const KeyType max_bbx_key(3, 3, 3);
  for (const KeyType& key : DenseKeyRange(min_bbx_key, max_bbx_key)) {
    map.updateVoxel(key, HIT);
  }

  {
    typedef typename MapType::iterator MapVoxelIt;
    BOOST_TEST_MESSAGE( "==========   Using iterator   =============" );
    for(MapVoxelIt it = map.begin(); it != map.end(); ++it) {
      const IOFormat vecfmt(StreamPrecision, DontAlignCols, ", ", ", ", "", "", "[", "]");
      BOOST_TEST_MESSAGE( it->payload().probability().format(vecfmt) );
      BOOST_CHECK(it->payload().probability().isApprox(Vector3f(0.3f,0.35f,0.35f)));
    }
  }

  {
    typedef typename MapType::const_iterator MapVoxelIt;
    BOOST_TEST_MESSAGE( "======== Using const_iterator ============" );
    for(MapVoxelIt it = map.begin(); it != map.end(); ++it) {
      const IOFormat vecfmt(StreamPrecision, DontAlignCols, ", ", ", ", "", "", "[", "]");
      BOOST_TEST_MESSAGE( it->payload().probability().format(vecfmt) );
      BOOST_CHECK(it->payload().probability().isApprox(Vector3f(0.3f,0.35f,0.35f)));
    }
  }

  {
    BOOST_TEST_MESSAGE( "==== Using Range based For loop ==========" );
    for (typename MapType::VoxelNodeType& voxel : map) {
      const IOFormat vecfmt(StreamPrecision, DontAlignCols, ", ", ", ", "", "", "[", "]");
      BOOST_TEST_MESSAGE( voxel.payload().probability().format(vecfmt) );
      BOOST_CHECK(voxel.payload().probability().isApprox(Vector3f(0.3f,0.35f,0.35f)));
    }
  }
}


template<class MapType>
class KeyVoxelIterator: public boost::iterator_adaptor<
      KeyVoxelIterator<MapType>   // Derived
    , typename MapType::iterator  // Base
    , boost::use_default // Value
    , typename MapType::iterator::iterator_category // CategoryOrTraversal
    , std::pair<const typename MapType::KeyType&, typename MapType::VoxelNodeType&>
> {

 private:

   typedef boost::iterator_adaptor<
       KeyVoxelIterator<MapType>   // Derived
     , typename MapType::iterator  // Base
     , boost::use_default // Value
     , typename MapType::iterator::iterator_category // CategoryOrTraversal
     , std::pair<const typename MapType::KeyType&, typename MapType::VoxelNodeType&>
   > super_t;

 public:
   /// Constructor from Base Iterator
   explicit KeyVoxelIterator(typename MapType::iterator const& x)
       : super_t(x) {
   }


 private:
   friend class boost::iterator_core_access;

   typename super_t::reference
   dereference() const {
     return typename super_t::reference(this->base().key(), *this->base_reference());
   }
};

template<class MapType>
KeyVoxelIterator<MapType> makeKeyVoxelIterator(const typename MapType::iterator& map_it) {
  return KeyVoxelIterator<MapType>(map_it);
}


//BOOST_AUTO_TEST_CASE_TEMPLATE(key_voxel_iteratorss, MapType, MapTypes_RVLP2f) {
BOOST_AUTO_TEST_CASE(key_voxel_iterators) {
typedef VolumetricMap<3,RVLP2f,UnorderedMapDataStructure,OccupancyMappingInteface,MapTransform> MapType;
  BOOST_TEST_MESSAGE( "\nTesting iterators_basic with :" );
  BOOST_TEST_MESSAGE( " MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") );

  MapType map(0.45);
  BOOST_CHECK_EQUAL(map.resolution(), 0.45);

  typedef MapType::KeyType KeyType;
  typedef KeyType::DataType KeyDataType;

  const KeyType min_bbx_key(KeyDataType::Constant(0));
  const KeyType max_bbx_key(KeyDataType::Constant(3));
  for (const KeyType& key : DenseKeyRange(min_bbx_key, max_bbx_key)) {
    map.addNewVoxel(key);
  }

  {
    typedef KeyVoxelIterator<MapType> MapKeyVoxelIt;
    MapKeyVoxelIt it = makeKeyVoxelIterator<MapType>(map.begin());
    MapKeyVoxelIt end = makeKeyVoxelIterator<MapType>(map.end());
    for(; it != end; ++it) {
      BOOST_CHECK((it->first.value.array() >= min_bbx_key.value.array()).all());
      BOOST_CHECK((it->first.value.array() < max_bbx_key.value.array()).all());
      BOOST_CHECK(it->second.payload().probability().isApprox(Vector2f(0.5f,0.5f)));
    }
  }
}
