/**
 * @file DenseKeyIterator_Tests.cpp
 * @brief 
 * 
 * @author Abhijit Kundu
 */

#include "Naksha/Keys/DenseKeyIterator.h"
#include "Naksha/Keys/Key.h"

#include <boost/test/unit_test.hpp>

using namespace Naksha;

BOOST_AUTO_TEST_CASE(Constructor) {
  typedef Key<3> KeyType;
  KeyType begin(0, 0, 0);
  DenseKeysIterator<KeyType> it (begin);
  BOOST_CHECK_EQUAL(*it, KeyType(0, 0, 0));

  DenseKeysIterator<KeyType> it2 (KeyType(10, 0, 0));
  BOOST_CHECK_EQUAL(*it2, KeyType(10, 0, 0));
}

BOOST_AUTO_TEST_CASE(IncrementDecrement) {
  typedef Key<3> KeyType;
  KeyType begin(0, 0, 0);
  DenseKeysIterator<KeyType> it (begin);

  BOOST_CHECK_EQUAL(*it, KeyType(0, 0, 0));
  ++it;
  BOOST_CHECK_EQUAL(*it, KeyType(1, 0, 0));
  --it;
  BOOST_CHECK_EQUAL(*it, KeyType(0, 0, 0));
}

BOOST_AUTO_TEST_CASE(bbx) {
  typedef Key<3> KeyType;
  KeyType::DataType min_bbx(0, 0, 0);
  KeyType::DataType max_bbx(3, 3, 3);
  DenseKeysIterator<KeyType> it (min_bbx, max_bbx);

  BOOST_CHECK_EQUAL(*it, KeyType(0, 0, 0));
  ++it;
  BOOST_CHECK_EQUAL(*it, KeyType(1, 0, 0));
  ++it;
  BOOST_CHECK_EQUAL(*it, KeyType(2, 0, 0));
  ++it;
  BOOST_CHECK_EQUAL(*it, KeyType(0, 1, 0));
  ++it;
  BOOST_CHECK_EQUAL(*it, KeyType(1, 1, 0));
  ++it;
  BOOST_CHECK_EQUAL(*it, KeyType(2, 1, 0));
  ++it;
  BOOST_CHECK_EQUAL(*it, KeyType(0, 2, 0));
  ++it;
  BOOST_CHECK_EQUAL(*it, KeyType(1, 2, 0));

  --it;
  BOOST_CHECK_EQUAL(*it, KeyType(0, 2, 0));
  --it;
  BOOST_CHECK_EQUAL(*it, KeyType(2, 1, 0));
  --it;
  BOOST_CHECK_EQUAL(*it, KeyType(1, 1, 0));
  --it;
  BOOST_CHECK_EQUAL(*it, KeyType(0, 1, 0));
  --it;
  BOOST_CHECK_EQUAL(*it, KeyType(2, 0, 0));
  --it;
  BOOST_CHECK_EQUAL(*it, KeyType(1, 0, 0));
  --it;
  BOOST_CHECK_EQUAL(*it, KeyType(0, 0, 0));
  --it;
  BOOST_CHECK_EQUAL(*it, KeyType(2, 2, 2));
}

BOOST_AUTO_TEST_CASE(HalfClosedRangeCheck) {
  typedef Key<3> KeyType;
  KeyType min(0,0,0);
  KeyType max(100,100,100);

  std::size_t count = 0;
  for (const KeyType& key : DenseKeyRange(min, max)) {
    ++count;
    for(int i = 0; i < KeyType::Dimension; ++i )
      BOOST_REQUIRE(key[i] < max[i]);
  }

  BOOST_CHECK_EQUAL(count, 100*100*100);
}


