/**
 * @file KeyUtils_Tests.cpp
 * @brief KeyUtils_Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Keys/Key.h"
#include "Naksha/Keys/KeyUtils.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/test/unit_test.hpp>

using namespace Naksha;

BOOST_AUTO_TEST_CASE(computeChildIndex_2Dcheck) {
  typedef Key<2> KeyType;
  BOOST_TEST_MESSAGE("\nTesting computeChildIndex_2Dcheck with KeyType: "
      << PRETTY_TYPE_INFO_REMOVE_NS(KeyType, "Naksha"));

  unsigned int expected(0);

  for (unsigned int y = 0; y < 2; ++y)
    for (unsigned int x = 0; x < 2; ++x) {
      BOOST_CHECK_EQUAL(computeChildIndex(KeyType(x,y), 0), expected);
      BOOST_TEST_MESSAGE("ChildIdx for (" << x << "," << y << ") = "
                         << computeChildIndex(KeyType(x,y), 0));
      ++expected;
    }
}

BOOST_AUTO_TEST_CASE(computeChildIndex_3Dcheck) {
  typedef Key<3> KeyType;
  BOOST_TEST_MESSAGE( "\nTesting computeChildIndex_3Dcheck with KeyType: "
        << PRETTY_TYPE_INFO_REMOVE_NS(KeyType, "Naksha") );

  unsigned int expected(0);

  for (unsigned int z = 0; z < 2; ++z)
    for (unsigned int y = 0; y < 2; ++y)
      for (unsigned int x = 0; x < 2; ++x) {
        BOOST_CHECK_EQUAL(computeChildIndex(KeyType(x,y,z), 0), expected);
        BOOST_TEST_MESSAGE("ChildIdx for (" << x <<"," << y << "," << z << ") = "
                           << computeChildIndex(KeyType(x,y,z), 0) );
        ++expected;
      }
}
