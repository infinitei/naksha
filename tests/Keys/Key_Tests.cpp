/**
 * @file Key_Tests.h
 * @brief Key Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Keys/Key.h"
#include "Naksha/Keys/KeyHash.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

#include <boost/unordered_set.hpp>
#include <boost/cstdint.hpp>
#include <set>


typedef boost::mpl::list<
    unsigned short int,
    int,
    unsigned int,
    std::size_t
  > KeyScalars;

using namespace Naksha;
using namespace Eigen;

BOOST_AUTO_TEST_CASE_TEMPLATE(CompileTimeChecks, KeyScalar, KeyScalars) {
  typedef Key<3, KeyScalar> KeyType3D;
  typedef Key<2, KeyScalar> KeyType2D;
  static_assert(KeyType3D::Dimension == 3, "Should have been 3");
  static_assert(KeyType2D::Dimension == 2, "Should have been 2");
}


BOOST_AUTO_TEST_CASE(Constructors) {
  typedef Key<3> KeyType3D;
  typedef Key<2> KeyType2D;
  KeyType3D key1;
  BOOST_CHECK_EQUAL(key1.value, KeyType3D::DataType(0,0,0));

  KeyType3D key2(KeyType3D::DataType(1,100,200));
  BOOST_CHECK_EQUAL(key2.value, KeyType3D::DataType(1,100,200));

  KeyType2D key3(KeyType2D::DataType(100,200));
  BOOST_CHECK_EQUAL(key3.value, KeyType2D::DataType(100,200));

  KeyType2D key4(100,200);
  BOOST_CHECK_EQUAL(key4.value, KeyType2D::DataType(100,200));

  KeyType3D key5(100,200,300);
  BOOST_CHECK_EQUAL(key5.value, KeyType3D::DataType(100,200,300));

  // Following should fail to compile
//  Key<4> key;
//  Key<3,float> key;
}

BOOST_AUTO_TEST_CASE(SizeChecks) {
  typedef Key<3, unsigned short int> KeyType3D;
  typedef Key<2, unsigned short int> KeyType2D;

  KeyType3D key;
  BOOST_TEST_MESSAGE("sizeof(KeyType3D::Scalar) = " << sizeof(KeyType3D::Scalar));
  BOOST_TEST_MESSAGE("sizeof(KeyType3D) = " << sizeof(KeyType3D));
  BOOST_TEST_MESSAGE("sizeof(key) = " << sizeof(key));
  BOOST_TEST_MESSAGE("sizeof(KeyType2D) = " << sizeof(KeyType2D));

  BOOST_CHECK_EQUAL(sizeof(KeyType3D), 3 * sizeof(KeyType3D::Scalar));
}

BOOST_AUTO_TEST_CASE(EigenNeedsToAlign) {
  typedef Key<3, unsigned short int> KeyType3DSI;
  typedef Key<2, unsigned short int> KeyType2DSI;
  typedef Key<2, boost::uint64_t> KeyType2Duint64;

  BOOST_TEST_MESSAGE("sizeof(KeyType3DSI) = " << sizeof(KeyType3DSI));
  BOOST_TEST_MESSAGE("sizeof(KeyType2DSI) = " << sizeof(KeyType2DSI));
  BOOST_TEST_MESSAGE("sizeof(KeyType2Duint64) = " << sizeof(KeyType2Duint64));

  BOOST_CHECK_EQUAL(KeyType3DSI::EigenNeedsToAlign(), 0);
  BOOST_CHECK_EQUAL(KeyType2DSI::EigenNeedsToAlign::value, 0);
  BOOST_CHECK_EQUAL(KeyType2Duint64::EigenNeedsToAlign(), 1);
}

BOOST_AUTO_TEST_CASE(KeyUnorderdSet) {
  typedef Key<3> KeyType;
  typedef boost::unordered_set<KeyType, KeyHash> KeySet;

  KeySet keys;
  keys.insert( KeyType(1,100,200) );
  keys.insert( KeyType(1,100,200) );
  keys.insert( KeyType(2,100,200) );

  BOOST_CHECK_EQUAL(keys.size(), 2);
  BOOST_CHECK(keys.find(KeyType(1,100,200)) != keys.end());
  BOOST_CHECK(keys.find(KeyType(2,100,200)) != keys.end());
  BOOST_CHECK(keys.find(KeyType(7,7,7)) == keys.end());
}

BOOST_AUTO_TEST_CASE(KeyOrderdSet) {
  typedef Key<3> KeyType;
  typedef std::set<KeyType> KeySet;

  KeySet keys;
  keys.insert( KeyType(1,100,200) );
  keys.insert( KeyType(1,100,200) );
  keys.insert( KeyType(2,100,200) );

  BOOST_CHECK_EQUAL(keys.size(), 2);
  BOOST_CHECK(keys.find(KeyType(1,100,200)) != keys.end());
  BOOST_CHECK(keys.find(KeyType(2,100,200)) != keys.end());
  BOOST_CHECK(keys.find(KeyType(7,7,7)) == keys.end());
}


BOOST_AUTO_TEST_CASE_TEMPLATE(Hash3D, KeyScalar, KeyScalars)  {
  typedef Key<3, KeyScalar> KeyType;

  BOOST_CHECK_EQUAL(KeyHash()(KeyType(0, 0, 0)), 0);
  BOOST_CHECK_EQUAL(KeyHash()(KeyType(1, 0, 0)), 1);
  BOOST_CHECK_EQUAL(KeyHash()(KeyType(0, 1, 0)), 1337);
  BOOST_CHECK_EQUAL(KeyHash()(KeyType(0, 0, 1)), 345637);
  BOOST_CHECK_EQUAL(KeyHash()(KeyType(1, 1, 1)), 346975);

  typedef typename KeyType::DataType KeyDataType;
  std::size_t max = std::numeric_limits<KeyScalar>::max();
  std::size_t expected = max + max * 1337 + max * 345637;
  BOOST_CHECK_EQUAL(
      KeyHash()(KeyType(KeyDataType::Constant(std::numeric_limits<KeyScalar>::max()))),
      expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Hash2D, KeyScalar, KeyScalars) {
  typedef Key<2, KeyScalar> KeyType;
  BOOST_CHECK_EQUAL(KeyHash()(KeyType(0, 0)), 0);
  BOOST_CHECK_EQUAL(KeyHash()(KeyType(1, 0)), 1);
  BOOST_CHECK_EQUAL(KeyHash()(KeyType(0, 1)), 1337);
  BOOST_CHECK_EQUAL(KeyHash()(KeyType(1, 1)), 1338);

  typedef typename KeyType::DataType KeyDataType;
  std::size_t max = std::numeric_limits<KeyScalar>::max();
  std::size_t expected = max + max * 1337;
  BOOST_CHECK_EQUAL(
      KeyHash()(KeyType(KeyDataType::Constant(std::numeric_limits<KeyScalar>::max()))),
      expected);
}
