/**
 * @file KeySerialization_Tests.cpp
 * @brief Key Serialization Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Keys/Key.h"
#include "Naksha/Keys/KeyHash.h"

#include <boost/serialization/vector.hpp>
#include "Naksha/Common/unordered_map_serialization.h"
#include "Naksha/Common/unordered_set_serialization.h"
#include "Naksha/Common/SerializationHelper.h"
#include <boost/test/unit_test.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(KeySerialization) {
  typedef Key<3> KeyType3D;
  KeyType3D key(KeyType3D::DataType(1,100,200));
  BOOST_CHECK_EQUAL(key.value, KeyType3D::DataType(1,100,200));
  serializeBinary(key, "key.tmp");

  KeyType3D key_in;
  deSerializeBinary(key_in, "key.tmp");
  BOOST_CHECK_EQUAL(key, key_in);
}

BOOST_AUTO_TEST_CASE(KeyVectorSerialization) {
  typedef Key<3> KeyType3D;
  typedef vector<KeyType3D> KeyVector;

  KeyVector keys;
  keys.push_back(KeyType3D(KeyType3D::DataType(1,100,200)));
  keys.push_back(KeyType3D(KeyType3D::DataType(2,100,200)));
  serializeBinary(keys, "keys.tmp");

  KeyVector keys_in;
  deSerializeBinary(keys_in, "keys.tmp");
  BOOST_CHECK_EQUAL(keys.size(), keys_in.size());
  BOOST_CHECK_EQUAL_COLLECTIONS(keys.begin(), keys.end(), keys_in.begin(), keys_in.end());
}

BOOST_AUTO_TEST_CASE(KeyMapSerialization) {
  typedef Key<3> KeyType;
  typedef boost::unordered_map<KeyType, int, KeyHash> KeyIntMap;

  KeyIntMap map;
  map[KeyType(1,100,200)] = 10;
  map[KeyType(2,100,200)] = 15;
  serializeBinary(map, "key_map.tmp");

  KeyIntMap map_in;
  deSerializeBinary(map_in, "key_map.tmp");
  BOOST_CHECK_EQUAL(map.size(), map_in.size());

  BOOST_CHECK(map_in.find(KeyType(1,100,200)) != map_in.end());
  BOOST_CHECK_EQUAL(map_in.find(KeyType(1,100,200))->second, 10);

  BOOST_CHECK(map_in.find(KeyType(2,100,200)) != map_in.end());
  BOOST_CHECK_EQUAL(map_in.find(KeyType(2,100,200))->second, 15);

  BOOST_CHECK(map_in.find(KeyType(7,7,7)) == map_in.end());
}

BOOST_AUTO_TEST_CASE(KeySetSerialization) {
  typedef Key<3> KeyType;
  typedef boost::unordered_set<KeyType, KeyHash> KeySet;

  KeySet keys;
  keys.insert(KeyType(1,100,200));
  keys.insert(KeyType(2,100,200));
  serializeBinary(keys, "keys.tmp");

  KeySet keys_in;
  deSerializeBinary(keys_in, "keys.tmp");
  BOOST_CHECK_EQUAL(keys.size(), keys_in.size());

  BOOST_CHECK(keys_in.find(KeyType(1,100,200)) != keys_in.end());
  BOOST_CHECK_EQUAL(*keys_in.find(KeyType(1,100,200)), KeyType(1,100,200));

  BOOST_CHECK(keys_in.find(KeyType(2,100,200)) != keys_in.end());
  BOOST_CHECK_EQUAL(*keys_in.find(KeyType(2,100,200)), KeyType(2,100,200));

  BOOST_CHECK(keys_in.find(KeyType(7,7,7)) == keys_in.end());

  // The following fails becuse of some sorting issues
  //BOOST_CHECK_EQUAL_COLLECTIONS(keys.begin(), keys.end(), keys_in.begin(), keys_in.end());
}

