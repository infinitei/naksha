/**
 * @file ImportExport_Tests.cpp
 * @brief ImportExport Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Tools/ImportExport.h"
#include "Naksha/Algorithms/CompareMaps.h"

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Keys/DenseKeyIterator.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

using namespace Naksha;

template <int MapDimension, typename Payload>
void testIOwithSamePayload() {
  typedef VolumetricMap<MapDimension,Payload,TreeDataStructure,HybridMappingInterface,MapTransform> OctreeMap;

  OctreeMap om(0.4);

  typedef typename OctreeMap::KeyType KeyType;
  typedef typename KeyType::DataType KeyDataType;
  typedef Naksha::MeasurementModel<typename Payload::RVType> MeasurementModelType;

  for (const KeyType& key : DenseKeyRange(KeyType(KeyDataType::Constant(100)), KeyType(KeyDataType::Constant(130)))) {
    om.updateVoxel(key, MeasurementModelType::HIT); // integrate 'occupied' measurement
  }

  for (const KeyType& key : DenseKeyRange(KeyType(KeyDataType::Constant(120)), KeyType(KeyDataType::Constant(150)))) {
    om.updateVoxel(key, MeasurementModelType::MISS); // integrate 'free' measurement
  }

  const std::string file_name("map.naksha");
  BOOST_REQUIRE(saveMap(om, file_name));

  {
    typedef VolumetricMap<MapDimension, Payload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    boost::scoped_ptr<MapType> map_in(Naksha::loadMap<MapType>(file_name));
    BOOST_CHECK(compareMaps(om, *map_in));
  }
  {
    typedef VolumetricMap<MapDimension, Payload, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    boost::scoped_ptr<MapType> map_in(Naksha::loadMap<MapType>(file_name));
    BOOST_CHECK(compareMaps(om, *map_in));
  }
  {
    typedef VolumetricMap<MapDimension, Payload, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    boost::scoped_ptr<MapType> map_in(Naksha::loadMap<MapType>(file_name));
    BOOST_CHECK(compareMaps(om, *map_in));
  }

  std::remove(file_name.c_str());
}

typedef boost::mpl::list
    <
        DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable>
      , DiscreteRandomVariable<2, double, LogOddsTag, BinaryRandomVariable>
      , DiscreteRandomVariable<6, float, LogProbabilityTag>
      , DiscreteRandomVariable<8, float, LogProbabilityTag>
      , DiscreteRandomVariable<9, float, LogProbabilityTag>
      , DiscreteRandomVariable<11, float, LogProbabilityTag>
    > RVPayloads;

BOOST_AUTO_TEST_CASE_TEMPLATE(IO_test_with_same_payload, Payload, RVPayloads) {
  BOOST_TEST_MESSAGE( "\nRunning IO_test_with_same_payload with :" );
  BOOST_TEST_MESSAGE( " Payload: " << PRETTY_TYPE(Payload, "Naksha", 3) );

  testIOwithSamePayload<2, Payload>();

  testIOwithSamePayload<3, Payload>();
}


template <int MapDimension, typename SavePayload>
void testIOforColoredPayloads() {
  typedef VolumetricMap<MapDimension,SavePayload,HashedVectorDataStructure,HybridMappingInterface,MapTransform> SaveMap;

  SaveMap save_map(0.4);

  typedef typename SaveMap::KeyType KeyType;
  typedef typename KeyType::DataType KeyDataType;

  for (const KeyType& key : DenseKeyRange(KeyType(KeyDataType::Constant(100)), KeyType(KeyDataType::Constant(130)))) {
    typename SaveMap::VoxelNodeType* voxel = save_map.addNewVoxel(key);
    unsigned char color = key[0];
    voxel->payload().setColor(color, color, color);
  }

  const std::string file_name("map.naksha");
  BOOST_REQUIRE(saveMap(save_map, file_name));

  {
    typedef VolumetricMap<MapDimension, ColoredPayload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    boost::scoped_ptr<MapType> map_in(Naksha::loadMap<MapType>(file_name));
    BOOST_REQUIRE(map_in);
    for (const KeyType& key : DenseKeyRange(KeyType(KeyDataType::Constant(100)), KeyType(KeyDataType::Constant(130)))) {
      typename MapType::const_iterator it = map_in->find(key);
      BOOST_REQUIRE(it != map_in->end());
      unsigned char color = key[0];
      BOOST_CHECK_EQUAL(it->payload().getColor(), ColoredPayload::DataType::Constant(color));
    }
  }
  {
    typedef VolumetricMap<MapDimension, ColoredPayload, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    boost::scoped_ptr<MapType> map_in(Naksha::loadMap<MapType>(file_name));
    BOOST_REQUIRE(map_in);
    for (const KeyType& key : DenseKeyRange(KeyType(KeyDataType::Constant(100)), KeyType(KeyDataType::Constant(130)))) {
      typename MapType::const_iterator it = map_in->find(key);
      BOOST_REQUIRE(it != map_in->end());
      unsigned char color = key[0];
      BOOST_CHECK_EQUAL(it->payload().getColor(), ColoredPayload::DataType::Constant(color));
    }
  }
  {
    typedef VolumetricMap<MapDimension, ColoredPayload, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    boost::scoped_ptr<MapType> map_in(Naksha::loadMap<MapType>(file_name));
    BOOST_REQUIRE(map_in);
    for (const KeyType& key : DenseKeyRange(KeyType(KeyDataType::Constant(100)), KeyType(KeyDataType::Constant(130)))) {
      typename MapType::const_iterator it = map_in->find(key);
      BOOST_REQUIRE(it != map_in->end());
      unsigned char color = key[0];
      BOOST_CHECK_EQUAL(it->payload().getColor(), ColoredPayload::DataType::Constant(color));
    }
  }

  std::remove(file_name.c_str());
}

typedef boost::mpl::list
    <   ColoredPayload
      , ColoredRandomVariable<DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<2, double, LogOddsTag, BinaryRandomVariable> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<9, float, LogProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<19, float, LogProbabilityTag> >::type
    > ColoredPayloads;

BOOST_AUTO_TEST_CASE_TEMPLATE(IO_test_for_colored_payloads, SavePayload, ColoredPayloads) {
  BOOST_TEST_MESSAGE( "\nRunning IO_test_for_colored_payloads with :" );
  BOOST_TEST_MESSAGE( " SavePayload: " << PRETTY_TYPE(SavePayload, "Naksha", 3) );

  testIOforColoredPayloads<2, SavePayload>();

  testIOforColoredPayloads<3, SavePayload>();
}



template <int MapDimension, typename SavePayload>
void testIOforRVColoredPayloads() {
  typedef VolumetricMap<MapDimension,SavePayload,HashedVectorDataStructure,HybridMappingInterface,MapTransform> SaveMap;

  SaveMap save_map(0.4);

  typedef typename SaveMap::KeyType KeyType;
  typedef typename KeyType::DataType KeyDataType;


  for (const KeyType& key : DenseKeyRange(KeyType(KeyDataType::Constant(100)), KeyType(KeyDataType::Constant(130)))) {
    typename SaveMap::VoxelNodeType* voxel = save_map.addNewVoxel(key);

    unsigned char color = key[0];
    voxel->payload().setColor(color, color, color);

    typedef typename SaveMap::Payload::RVType::DataType RVDataType;
    voxel->payload().setProbability(RVDataType::Constant(1.0 / SaveMap::Payload::RVType::Dimension));
  }

  const std::string file_name("map.naksha");
  BOOST_REQUIRE(saveMap(save_map, file_name));

  typedef DiscreteRandomVariable<SaveMap::Payload::RVType::Dimension, double, LogProbabilityTag> RV;
  typedef typename ColoredRandomVariable<RV>::type ColoredRVPayload;

  {
    typedef VolumetricMap<MapDimension, ColoredRVPayload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    boost::scoped_ptr<MapType> map_in(Naksha::loadMap<MapType>(file_name));
    BOOST_REQUIRE(map_in);
    for (const KeyType& key : DenseKeyRange(KeyType(KeyDataType::Constant(100)), KeyType(KeyDataType::Constant(130)))) {
      typename MapType::const_iterator it = map_in->find(key);
      BOOST_REQUIRE(it != map_in->end());
      unsigned char color = key[0];
      BOOST_REQUIRE(it->payload().getColor() == ColoredPayload::DataType::Constant(color));
      BOOST_REQUIRE(it->payload().probability().isApprox(RV::DataType::Constant(1.0 / RV::Dimension), 1e-6));
    }
  }
  {
    typedef VolumetricMap<MapDimension, ColoredRVPayload, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    boost::scoped_ptr<MapType> map_in(Naksha::loadMap<MapType>(file_name));
    BOOST_REQUIRE(map_in);
    for (const KeyType& key : DenseKeyRange(KeyType(KeyDataType::Constant(100)), KeyType(KeyDataType::Constant(130)))) {
      typename MapType::const_iterator it = map_in->find(key);
      BOOST_REQUIRE(it != map_in->end());
      unsigned char color = key[0];
      BOOST_REQUIRE(it->payload().getColor() == ColoredPayload::DataType::Constant(color));
      BOOST_REQUIRE(it->payload().probability().isApprox(RV::DataType::Constant(1.0 / RV::Dimension), 1e-6));
    }
  }
  {
    typedef VolumetricMap<MapDimension, ColoredRVPayload, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    boost::scoped_ptr<MapType> map_in(Naksha::loadMap<MapType>(file_name));
    BOOST_REQUIRE(map_in);
    for (const KeyType& key : DenseKeyRange(KeyType(KeyDataType::Constant(100)), KeyType(KeyDataType::Constant(130)))) {
      typename MapType::const_iterator it = map_in->find(key);
      BOOST_REQUIRE(it != map_in->end());
      unsigned char color = key[0];
      BOOST_REQUIRE(it->payload().getColor() == ColoredPayload::DataType::Constant(color));
      BOOST_REQUIRE(it->payload().probability().isApprox(RV::DataType::Constant(1.0 / RV::Dimension), 1e-6));
    }
  }

  std::remove(file_name.c_str());
}

// TODO: Run test with BinaryRandomVariable
typedef boost::mpl::list
    <
//        ColoredRandomVariable<DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> >::type
//      , ColoredRandomVariable<DiscreteRandomVariable<2, double, LogOddsTag, BinaryRandomVariable> >::type
        ColoredRandomVariable<DiscreteRandomVariable<9, float, LogProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<19, float, LogProbabilityTag> >::type
    > ColoredRVPayloads;

BOOST_AUTO_TEST_CASE_TEMPLATE(IO_test_for_rv_colored_payloads, SavePayload, ColoredRVPayloads) {
  BOOST_TEST_MESSAGE( "\nRunning IO_test_for_rv_colored_payloads with :" );
  BOOST_TEST_MESSAGE( " SavePayload: " << PRETTY_TYPE(SavePayload, "Naksha", 3) );

  testIOforRVColoredPayloads<2, SavePayload>();

  testIOforRVColoredPayloads<3, SavePayload>();
}
