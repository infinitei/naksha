/**
 * @file OccupancyNodeInterface_Tests.cpp
 * @brief OccupancyNodeInterface Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Nodes/OccupancyNodeInterface.h"

// Payloads
#include "Naksha/Payload/DiscreteRandomVariable.h"

#include <boost/test/unit_test.hpp>



namespace Naksha {

// Create a host class for Policy class OccupancyNodeInterface
template<class _Payload, template<class> class _InterfacePolicy>
struct Node: public _InterfacePolicy<_Payload>  {

  typedef _InterfacePolicy<_Payload> Interface;
  typedef _Payload Payload;

  Node(const Payload& init_payload = Payload())
    : Interface(init_payload) {}
};

} // end name space


using namespace Naksha;
using namespace std;

BOOST_AUTO_TEST_CASE(Constructor_IntPayload) {
  typedef Node<int, OccupancyNodeInterface> OccupancyNodeI;
  OccupancyNodeI node(1);
  BOOST_CHECK_EQUAL(node.payload(), 1);
  node.setPayload(2);
  BOOST_CHECK_EQUAL(node.payload(), 2);
}

BOOST_AUTO_TEST_CASE(Constructor_VectorPayload) {
  typedef Node<Eigen::Vector3f, OccupancyNodeInterface> OccupancyNodeVector3f;
  OccupancyNodeVector3f node(Eigen::Vector3f::Ones());
  BOOST_CHECK_EQUAL(node.payload(), Eigen::Vector3f::Ones());
}

BOOST_AUTO_TEST_CASE(Constructor_RVPayload) {
  typedef DiscreteRandomVariable<2, float> RV;
  typedef Node<RV, OccupancyNodeInterface> OccupancyRV;
  OccupancyRV node(Eigen::Vector2f(0.5,0.5));
  BOOST_CHECK_EQUAL(node.payload().getData(), Eigen::Vector2f(0.5,0.5));

  BOOST_CHECK_EQUAL(OccupancyRV::Payload::Dimension, 2);
  static_assert(OccupancyRV::Payload::Dimension == RV::Dimension, "Dimension should be 2 by definition.");
}

BOOST_AUTO_TEST_CASE(RVPayload_insertMeasurement) {
  typedef DiscreteRandomVariable<2, double, ProbabilityTag> RV;
  typedef Node<RV, OccupancyNodeInterface> OccupancyRV;
  OccupancyRV node(RV::DataType(0.5,0.5));
  BOOST_CHECK_EQUAL(node.payload().getData(), RV::DataType(0.5,0.5));
  node.insertMeasurement(RV::DataType::Ones());
  BOOST_CHECK_EQUAL(node.payload().getData(), RV::DataType(0.5,0.5));
  node.insertMeasurement(RV::DataType(1.0, 0.0));
  BOOST_CHECK_EQUAL(node.payload().probability(), RV::DataType(1.0, 0.0));
  node.setPayload(RV::DataType(0.0, 1.0));
  BOOST_CHECK_EQUAL(node.payload().getData(), RV::DataType(0.0,1.0));
}

BOOST_AUTO_TEST_CASE(EigenNeedsToAlign) {
  typedef DiscreteRandomVariable<2, double, ProbabilityTag> RV2d;
  typedef DiscreteRandomVariable<3, float, ProbabilityTag> RV3f;
  typedef DiscreteRandomVariable<8, float> RV8f;
  typedef DiscreteRandomVariable<9, double> RV9d;

  typedef Node<RV2d, OccupancyNodeInterface> OccupancyRV2d;
  typedef Node<RV3f, OccupancyNodeInterface> OccupancyRV3f;
  typedef Node<RV8f, OccupancyNodeInterface> OccupancyRV8f;
  typedef Node<RV9d, OccupancyNodeInterface> OccupancyRV9d;

  BOOST_CHECK_EQUAL(RV2d::EigenNeedsToAlign(), 1);
  BOOST_CHECK_EQUAL(RV3f::EigenNeedsToAlign::value, 0);
  BOOST_CHECK_EQUAL(RV8f::EigenNeedsToAlign(), 1);
  BOOST_CHECK_EQUAL(RV9d::EigenNeedsToAlign(), 0);

  BOOST_CHECK_EQUAL(OccupancyRV2d::EigenNeedsToAlign::value, 1);
  BOOST_CHECK_EQUAL(OccupancyRV3f::EigenNeedsToAlign::value, 0);
  BOOST_CHECK_EQUAL(OccupancyRV8f::EigenNeedsToAlign(), 1);
  BOOST_CHECK_EQUAL(OccupancyRV9d::EigenNeedsToAlign(), 0);
}
