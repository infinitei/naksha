/**
 * @file NodeUtils_Tests.cpp
 * @brief NodeUtils_Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Nodes/NodeUtils.h"
#include "Naksha/Nodes/NodeTypedefs.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/LabelToMeasurementMapping.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

using namespace Naksha;
using namespace std;

template<class NodeType, class MeasurementModelType>
void testNodeOccupancy() {
  NodeType node;
  IsNodeOccupied<NodeType> occ_checker;

  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");

  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Occupancy = " << std::boolalpha << occ_checker(node));
  BOOST_CHECK_EQUAL(occ_checker(node), false);


  node.insertMeasurement(MeasurementModelType::HIT);
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Occupancy = " << std::boolalpha << occ_checker(node));
  BOOST_CHECK_EQUAL(occ_checker(node), true);

  node.insertMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Occupancy = " << std::boolalpha << occ_checker(node));
  BOOST_CHECK_EQUAL(occ_checker(node), true);

  node.insertMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Occupancy = " << std::boolalpha << occ_checker(node));
  BOOST_CHECK_EQUAL(occ_checker(node), true);

  node.insertMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Occupancy = " << std::boolalpha << occ_checker(node));
  BOOST_CHECK_EQUAL(occ_checker(node), false);

}

typedef boost::mpl::list<ProbabilityTag, LogProbabilityTag, LogOddsTag> BinaryProbTypes;


BOOST_AUTO_TEST_CASE_TEMPLATE(BinaryNodeOccupancyTests, ProbType, BinaryProbTypes) {
  BOOST_TEST_MESSAGE("\nRunning BinaryNodeOccupancyTests with " << PRETTY_TYPE_INFO(ProbType));
  typedef DiscreteRandomVariable<2, double, ProbabilityTag> RV;
  typedef ColoredRandomVariable<RV>::type Payload;
  typedef VoxelNode<Payload, RVOccupancyNodeInterface, OctreeNode> NodeType;
  typedef MeasurementModel<RV> MeasurementModelType;
  testNodeOccupancy<NodeType, MeasurementModelType>();
}

BOOST_AUTO_TEST_CASE_TEMPLATE(HybridNodeOccupancyTests, ProbType, BinaryProbTypes) {
  BOOST_TEST_MESSAGE("\nRunning BinaryNodeOccupancyTests with " << PRETTY_TYPE_INFO(ProbType));
  typedef DiscreteRandomVariable<9, double, ProbabilityTag> RV;
  typedef ColoredRandomVariable<RV>::type Payload;
  typedef VoxelNode<Payload, RVOccupancyNodeInterface, OctreeNode> NodeType;
  typedef MeasurementModel<RV> MeasurementModelType;
  testNodeOccupancy<NodeType, MeasurementModelType>();
}

typedef boost::mpl::list<ProbabilityTag, LogProbabilityTag> MultivariateProbTypes;

BOOST_AUTO_TEST_CASE_TEMPLATE(modeOfNodeTests, ProbType, MultivariateProbTypes) {
  BOOST_TEST_MESSAGE("\nRunning modeOfNodeTests with " << PRETTY_TYPE_INFO(ProbType));

  typedef DiscreteRandomVariable<9, double, ProbType> RV;
  typedef typename ColoredRandomVariable<RV>::type Payload;
  typedef MeasurementModel<RV> MeasurementModelType;
  typedef MeasurementMap<RV> RVMeasurementMap;
  typedef VoxelNode<Payload, RVOccupancyNodeInterface, OctreeNode> NodeType;

  NodeType node;

  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");

  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt) << " Mode = " << modeOfNode(node));
  BOOST_CHECK_EQUAL(modeOfNode(node), 0);

  node.insertMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt) << " Mode = " << modeOfNode(node));
  BOOST_CHECK_EQUAL(modeOfNode(node), 0);

  node.insertMeasurement(RVMeasurementMap::template at<ImageLabels::ROAD>());
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt) << " Mode = " << modeOfNode(node));
  BOOST_CHECK_EQUAL(modeOfNode(node), ImageLabels::ROAD);

  node.insertMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt) << " Mode = " << modeOfNode(node));
  BOOST_CHECK_EQUAL(modeOfNode(node), 0);

  node.insertMeasurement(RVMeasurementMap::template at<ImageLabels::CAR>());
  node.insertMeasurement(RVMeasurementMap::template at<ImageLabels::CAR>());
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt) << " Mode = " << modeOfNode(node));
  BOOST_CHECK_EQUAL(modeOfNode(node), ImageLabels::CAR);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(modeOfNodeOccChekedTests, ProbType, MultivariateProbTypes) {
  BOOST_TEST_MESSAGE("\nRunning modeOfNodeOccChekedTests with " << PRETTY_TYPE_INFO(ProbType));

  typedef DiscreteRandomVariable<9, double, ProbType> RV;
  typedef typename ColoredRandomVariable<RV>::type Payload;
  typedef MeasurementModel<RV> MeasurementModelType;
  typedef MeasurementMap<RV> RVMeasurementMap;
  typedef VoxelNode<Payload, RVOccupancyNodeInterface, OctreeNode> NodeType;

  NodeType node;
  ModeOfNodeOccChecked<NodeType> getModeOccCheked;

  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");

  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt) << " Mode = " << getModeOccCheked(node));
  BOOST_CHECK_EQUAL(getModeOccCheked(node), 0);

  node.insertMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt) << " Mode = " << getModeOccCheked(node));
  BOOST_CHECK_EQUAL(getModeOccCheked(node), 0);

  node.insertMeasurement(RVMeasurementMap::template at<ImageLabels::ROAD>());
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt) << " Mode = " << getModeOccCheked(node));
  BOOST_CHECK_EQUAL(getModeOccCheked(node), ImageLabels::ROAD);

  node.insertMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt) << " Mode = " << getModeOccCheked(node));
  BOOST_CHECK_EQUAL(getModeOccCheked(node), ImageLabels::ROAD);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ModeOfNodeTests, ProbType, MultivariateProbTypes) {
  BOOST_TEST_MESSAGE("\nRunning ModeOfNodeTests with " << PRETTY_TYPE_INFO(ProbType));

  typedef DiscreteRandomVariable<9, double, ProbType> RV;
  typedef typename ColoredRandomVariable<RV>::type Payload;
  typedef MeasurementModel<RV> MeasurementModelType;
  typedef MeasurementMap<RV> RVMeasurementMap;
  typedef VoxelNode<Payload, RVOccupancyNodeInterface, OctreeNode> NodeType;

  NodeType node;
  ModeOfNode<NodeType> getMode;
  ModeOfNode<NodeType, true> getModeOccCheked;

  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");

  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Mode = " << getMode(node) << " ModeOccCheked = " << getModeOccCheked(node) << "\n");
  BOOST_CHECK_EQUAL(getMode(node), 0);
  BOOST_CHECK_EQUAL(getModeOccCheked(node), 0);

  node.insertMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Mode = " << getMode(node) << " ModeOccCheked = " << getModeOccCheked(node) << "\n");
  BOOST_CHECK_EQUAL(getMode(node), 0);
  BOOST_CHECK_EQUAL(getModeOccCheked(node), 0);

  node.insertMeasurement(RVMeasurementMap::template at<ImageLabels::ROAD>());
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Mode = " << getMode(node) << " ModeOccCheked = " << getModeOccCheked(node) << "\n");
  BOOST_CHECK_EQUAL(getMode(node), ImageLabels::ROAD);
  BOOST_CHECK_EQUAL(getModeOccCheked(node), ImageLabels::ROAD);

  node.insertMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Mode = " << getMode(node) << " ModeOccCheked = " << getModeOccCheked(node) << "\n");
  BOOST_CHECK_EQUAL(getMode(node), 0);
  BOOST_CHECK_EQUAL(getModeOccCheked(node), ImageLabels::ROAD);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ModeOfNodeTests_VoxelLabels, ProbType, MultivariateProbTypes) {
  BOOST_TEST_MESSAGE("\nRunning ModeOfNodeTests with " << PRETTY_TYPE_INFO(ProbType));

  typedef DiscreteRandomVariable<9, double, ProbType> RV;
  typedef typename ColoredRandomVariable<RV>::type Payload;
  typedef MeasurementModel<RV> MeasurementModelType;
  typedef MeasurementMap<RV> RVMeasurementMap;
  typedef VoxelNode<Payload, RVOccupancyNodeInterface, OctreeNode> NodeType;

  NodeType node;
  ModeOfNode<NodeType, false, VoxelLabels::SemanticLabel> getMode;
  ModeOfNode<NodeType, true, VoxelLabels::SemanticLabel> getModeOccCheked;

  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");

  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Mode = " << getMode(node) << " ModeOccCheked = " << getModeOccCheked(node) << "\n");
  BOOST_CHECK_EQUAL(VoxelLabels::FREE, getMode(node));
  BOOST_CHECK_EQUAL(VoxelLabels::FREE, getModeOccCheked(node));

  node.insertMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Mode = " << getMode(node) << " ModeOccCheked = " << getModeOccCheked(node) << "\n");
  BOOST_CHECK_EQUAL(VoxelLabels::FREE, getMode(node));
  BOOST_CHECK_EQUAL(VoxelLabels::FREE, getModeOccCheked(node));

  node.insertMeasurement(RVMeasurementMap::template at<ImageLabels::ROAD>());
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Mode = " << getMode(node) << " ModeOccCheked = " << getModeOccCheked(node) << "\n");
  BOOST_CHECK_EQUAL(ImageLabels::ROAD, getMode(node));
  BOOST_CHECK_EQUAL(ImageLabels::ROAD, getModeOccCheked(node));

  node.insertMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << node.payload().probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Mode = " << getMode(node) << " ModeOccCheked = " << getModeOccCheked(node) << "\n");
  BOOST_CHECK_EQUAL(VoxelLabels::FREE, getMode(node));
  BOOST_CHECK_EQUAL(ImageLabels::ROAD, getModeOccCheked(node));
}
