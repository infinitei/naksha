/**
 * @file TreeNode_Tests.h
 * @brief Tree Node Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Nodes/TreeNode.h"

#include <boost/test/unit_test.hpp>

namespace Naksha {
// Test DerivedOctreeNode
template <typename _Payload>
class DerivedOctreeNode: public OctreeNode<DerivedOctreeNode<_Payload> > {
public:

  typedef _Payload Payload;
  typedef TreeNodeBase<DerivedOctreeNode> Base;
  typedef boost::scoped_ptr<DerivedOctreeNode<Payload> > Ptr;

  template <typename T>
  DerivedOctreeNode(const T& payload)
      : payload_(payload) {
  }

  DerivedOctreeNode() {}

  const Payload& payload() const { return payload_;}

private:
  /// Payload
  Payload payload_;
};

} // end name space


using namespace Naksha;
using namespace std;


BOOST_AUTO_TEST_CASE(Constructors) {

  typedef DerivedOctreeNode<int> Node;
  Node node1(1);
  BOOST_CHECK_EQUAL(node1.payload(), 1);
  BOOST_CHECK_EQUAL(node1.hasAnyChildren(), false);
  BOOST_CHECK_EQUAL(node1.childExistsAt(2), false);

  Node::Ptr node2(new Node(3));
  BOOST_CHECK_EQUAL(node2->payload(), 3);
  BOOST_CHECK_EQUAL(node2->hasAnyChildren(), false);
  BOOST_CHECK_EQUAL(node2->childExistsAt(2), false);
}


BOOST_AUTO_TEST_CASE(childFunctinality1) {

  typedef DerivedOctreeNode<int> Node;
  Node node1(1);
  BOOST_CHECK_EQUAL(node1.payload(), 1);
  BOOST_CHECK_EQUAL(node1.hasAnyChildren(), false);
  BOOST_CHECK_EQUAL(node1.childExistsAt(2), false);

  BOOST_CHECK_EQUAL(node1.createChild(2,2), true);
  BOOST_CHECK_EQUAL(node1.payload(), 1);
  BOOST_CHECK_EQUAL(node1.hasAnyChildren(), true);
  BOOST_CHECK_EQUAL(node1.childExistsAt(2), true);
  BOOST_CHECK_EQUAL(node1.childExistsAt(0), false);
  BOOST_CHECK_EQUAL(node1.createChild(2), false);

  BOOST_CHECK_EQUAL(node1.createChild(3), true);  ;
  BOOST_CHECK_EQUAL(node1.hasAnyChildren(), true);
  BOOST_CHECK_EQUAL(node1.childExistsAt(3), true);
  BOOST_CHECK_EQUAL(node1.childExistsAt(0), false);
  BOOST_CHECK_EQUAL(node1.createChild(3), false);

  node1.deleteChild(3);
  BOOST_CHECK_EQUAL(node1.childExistsAt(3), false);
}

BOOST_AUTO_TEST_CASE(childFunctinality2) {
  typedef DerivedOctreeNode<int> Node;
  Node node0(0);
  BOOST_CHECK_EQUAL(node0.payload(), 0);

  BOOST_CHECK_EQUAL(node0.createChild(0, 0), true);
  BOOST_CHECK_EQUAL(node0.childExistsAt(0), true);

  BOOST_CHECK_EQUAL(node0.createChild(1, 1), true);
  BOOST_CHECK_EQUAL(node0.childExistsAt(1), true);

  BOOST_CHECK_EQUAL(node0.createChild(2, 2), true);
  BOOST_CHECK_EQUAL(node0.childExistsAt(2), true);

  const Node* node0_0 = node0.childPtr(0);
  const Node& node0_1 = node0.child(1);
  const Node* node0_2 = node0.childPtr(2);

  BOOST_CHECK_EQUAL(node0_0->payload(), 0);
  BOOST_CHECK_EQUAL(node0_1.payload(), 1);
  BOOST_CHECK_EQUAL(node0_2->payload(), 2);

  node0.deleteChild(0);
  BOOST_CHECK_EQUAL(node0.childExistsAt(0), false);


//  BOOST_CHECK_EQUAL(node0_1->createChild(4,4), true);

}

