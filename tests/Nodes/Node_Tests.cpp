/**
 * @file Node_Tests.cpp
 * @brief Unit Tests for Node
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Nodes/VoxelNode.h"
#include "Naksha/Nodes/NodeTypedefs.h"

#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/MeasurementModel.h"

#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/test/unit_test.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(Constructor_int_Occ_Tree) {
  typedef VoxelNode<int, OccupancyNodeInterface, OctreeNode> IntOccupancyOctreeNode;
  IntOccupancyOctreeNode node(5);
  BOOST_CHECK_EQUAL(node.payload(), 5);
  BOOST_CHECK_EQUAL(node.createChild(1, 3), true);
  BOOST_CHECK_EQUAL(node.childExistsAt(1), true);
  BOOST_CHECK_EQUAL(node.childExistsAt(0), false);
  BOOST_CHECK_EQUAL(node.createChild(5), true);
  BOOST_CHECK_EQUAL(node.createChild(9), false);
}

template <typename Derived>
class OneChildNode : public TreeNodeBase<Derived, 1> {
};

BOOST_AUTO_TEST_CASE(Constructor_int_Occ_OneChild) {
  typedef VoxelNode<int, OccupancyNodeInterface, OneChildNode> IntOccupancyOneChildNode;
  IntOccupancyOneChildNode node(1);

  BOOST_CHECK_EQUAL(node.payload(), 1);
  BOOST_CHECK_EQUAL(node.childExistsAt(0), false);
  BOOST_CHECK_EQUAL(node.createChild(0,3), true);
  BOOST_CHECK_EQUAL(node.createChild(1,3), false);
  BOOST_CHECK_EQUAL(node.childExistsAt(0), true);
  BOOST_CHECK_EQUAL(node.createChild(100), false);
}

template <typename Derived>
class NonTreeNode {
};

BOOST_AUTO_TEST_CASE(Constructor_int_Occ_NonTree) {
  typedef VoxelNode<int, OccupancyNodeInterface, NonTreeNode> IntOccupancyNonTreeNode;
  IntOccupancyNonTreeNode node(1);
  BOOST_CHECK_EQUAL(node.payload(), 1);

  // The following expression should not compile
//  node.createChild(0);
//  node.childExistsAt(0);
}



BOOST_AUTO_TEST_CASE(Constructor_OccupancyNodeInterface) {
  typedef DiscreteRandomVariable<2, float> RV;
  typedef VoxelNode<RV, OccupancyNodeInterface, OctreeNode> OccupancyOctreeNode;
  OccupancyOctreeNode node(Eigen::Vector2f(0.5,0.5));
  BOOST_CHECK_EQUAL(node.payload().getData(), Eigen::Vector2f(0.5,0.5));


  BOOST_CHECK_EQUAL(OccupancyOctreeNode::Payload::Dimension, 2);
  static_assert(OccupancyOctreeNode::Payload::Dimension == RV::Dimension, "Dimension should be 2 by definition.");
}

BOOST_AUTO_TEST_CASE(Constructor_RVOccupancyNodeInterface) {
  typedef DiscreteRandomVariable<2, float> RV;
  typedef VoxelNode<RV, RVOccupancyNodeInterface, OctreeNode> RVOccupancyOctreeNode;
  RVOccupancyOctreeNode node(Eigen::Vector2f(0.5,0.5));
  BOOST_CHECK_EQUAL(node.payload().getData(), Eigen::Vector2f(0.5,0.5));


  BOOST_CHECK_EQUAL(RVOccupancyOctreeNode::Payload::Dimension, 2);
  static_assert(RVOccupancyOctreeNode::Payload::Dimension == RV::Dimension, "Dimension should be 2 by definition.");
}

BOOST_AUTO_TEST_CASE(RVOccupancyNodeInterface_numOfObservations) {
  typedef DiscreteRandomVariable<2, float> RV;
  typedef VoxelNode<RV, RVOccupancyNodeInterface, OctreeNode> RVOccupancyOctreeNode;
  RVOccupancyOctreeNode node(Eigen::Vector2f(0.5,0.5));
  BOOST_CHECK_EQUAL(node.payload().getData(), Eigen::Vector2f(0.5,0.5));


  BOOST_CHECK_EQUAL(RVOccupancyOctreeNode::Payload::Dimension, 2);
  static_assert(RVOccupancyOctreeNode::Payload::Dimension == RV::Dimension, "Dimension should be 2 by definition.");

  for (size_t i = 0; i< 30; ++i) {
    node.insertMeasurement(Eigen::Vector2f(0.7,0.3));
  }
  BOOST_CHECK_EQUAL(node.numOfObservations(), 30);
}


BOOST_AUTO_TEST_CASE(NodePtrVectorConstructors) {
  typedef DiscreteRandomVariable<8, float, LogProbabilityTag> RV;
  typedef VoxelNode<RV, RVOccupancyNodeInterface, OctreeNode> NodeType;

  typedef boost::ptr_vector<NodeType> NodePtrVector;

  RV::DataType check(RV::DataType::Ones() * (0.5f / (RV::Dimension - 1)));
  check[0] = 0.5f;

  const size_t num_of_nodes = 100000000;

  NodePtrVector nodes_in_heap(num_of_nodes);
  for (NodePtrVector::const_iterator it = nodes_in_heap.begin(); it != nodes_in_heap.end(); ++it)
    BOOST_CHECK_EQUAL(it->payload().probability().isApprox(check), true);

  // Verify that these are pointers to separate objects
  for (size_t i = 0; i < nodes_in_heap.size(); ++i) {
    for(size_t j = 0; j < i; ++j )
      nodes_in_heap[i].insertMeasurement(RV::DataType::Ones());
  }
  for (size_t i = 0; i < nodes_in_heap.size(); ++i) {
    BOOST_CHECK_EQUAL(nodes_in_heap[i].numOfObservations(), i);
  }

}


