/*
 * NodeSerialization_Tests.cpp
 *
 *  Created on: Apr 4, 2014
 *      Author: abhijit
 */

/**
 * @file Serialization_Tests.h
 * @brief Serialization Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Common/SerializationHelper.h"
#include "Naksha/Payload/DiscreteRandomVariable.h"
#include "Naksha/Nodes/NodeTypedefs.h"

#include <boost/test/unit_test.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(OccupancyOctreeNodeInt) {
  typedef VoxelNode<int, OccupancyNodeInterface, OctreeNode> Node;
  Node node(1);
  BOOST_CHECK_EQUAL(node.createChild(2,2), true);
  serializeBinary(node, "onode.tmp");

  Node node_in;
  deSerializeBinary(node_in, "onode.tmp");
  BOOST_CHECK_EQUAL(node.payload(), node_in.payload());
  BOOST_CHECK_EQUAL(node_in.childExistsAt(2), true);
  const Node& node_in_2 = node_in.child(2);
  BOOST_CHECK_EQUAL(node_in_2.payload(), 2);
}

BOOST_AUTO_TEST_CASE(RVOccupancyOctreeNode) {
  typedef DiscreteRandomVariable<2, float, ProbabilityTag> RV;
  typedef OccupancyTreeNode<RV, 3>::type Node;
  Node node(Vector2f(0.5f,0.5f));
  BOOST_CHECK_EQUAL(node.createChild(2,Vector2f(0.1f,0.1f)), true);
  node.insertMeasurement(Vector2f::Ones());
  BOOST_CHECK_EQUAL(node.payload().probability(), Vector2f(0.5f,0.5f));
  BOOST_CHECK_EQUAL(node.numOfObservations(), 1);
  serializeBinary(node, "onode.tmp");

  Node node_in;
  deSerializeBinary(node_in, "onode.tmp");
  BOOST_CHECK_EQUAL(node.payload().probability(), node_in.payload().probability());
  BOOST_CHECK_EQUAL(node_in.childExistsAt(2), true);
  const Node& node_in_2 = node_in.child(2);
  BOOST_CHECK_EQUAL(node_in_2.payload().probability(), Vector2f(0.1f,0.1f));
  BOOST_CHECK_EQUAL(node.numOfObservations(), node_in.numOfObservations());
}

BOOST_AUTO_TEST_CASE(VoxelNode_with_NoChild) {
  typedef DiscreteRandomVariable<2, float, ProbabilityTag> RV;
  typedef VoxelNode<RV, RVOccupancyNodeInterface, NoChildPolicy> Node;
  Node node(Vector2f(0.5f,0.5f));
  node.insertMeasurement(Vector2f::Ones());
  BOOST_CHECK_EQUAL(node.payload().probability(), Vector2f(0.5f,0.5f));
  BOOST_CHECK_EQUAL(node.numOfObservations(), 1);
  serializeBinary(node, "onode.tmp");

  Node node_in;
  deSerializeBinary(node_in, "onode.tmp");
  BOOST_CHECK_EQUAL(node.payload().probability(), node_in.payload().probability());
  BOOST_CHECK_EQUAL(node.numOfObservations(), node_in.numOfObservations());
}
