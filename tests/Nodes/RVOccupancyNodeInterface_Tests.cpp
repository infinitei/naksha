/**
 * @file RVOccupancyNodeInterface_Tests.cpp
 * @brief Occupancy Node Interface for RandomVariables Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Nodes/RVOccupancyNodeInterface.h"
#include "Naksha/Payload/DiscreteRandomVariable.h"

#include "Naksha/Nodes/VoxelNode.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;

template <typename Derived>
class NonTreeNode {
};

BOOST_AUTO_TEST_CASE(ConstructorWithRVPayload) {
  typedef DiscreteRandomVariable<3, float> RV;
  typedef VoxelNode<RV, RVOccupancyNodeInterface, NonTreeNode> NodeType;
  NodeType rv_occ_node;
  BOOST_CHECK_EQUAL(rv_occ_node.payload().probability(), Vector3f(0.5f,0.25f,0.25f));
  BOOST_CHECK_EQUAL(rv_occ_node.numOfObservations(), 0);
}

BOOST_AUTO_TEST_CASE(ConstructorWithRVPayload2) {
  typedef DiscreteRandomVariable<4, double> RV;
  typedef VoxelNode<RV, RVOccupancyNodeInterface, NonTreeNode> NodeType;
  NodeType rv_occ_node(Vector4d::Ones());
  BOOST_CHECK_EQUAL(rv_occ_node.payload().getData(), Vector4d::Ones());
  BOOST_CHECK_EQUAL(rv_occ_node.numOfObservations(), 0);
}

BOOST_AUTO_TEST_CASE(ConstructorWithIntPayload) {
  typedef VoxelNode<int, RVOccupancyNodeInterface, NonTreeNode> NodeType;
  NodeType node(1);
  BOOST_CHECK_EQUAL(node.payload(), 1);
  BOOST_CHECK_EQUAL(node.numOfObservations(), 0);
}

BOOST_AUTO_TEST_CASE(insertMeasurement) {

  typedef DiscreteRandomVariable<3, float, ProbabilityTag> RV;
  typedef VoxelNode<RV, RVOccupancyNodeInterface, NonTreeNode> RVOccupancyNode;
  RVOccupancyNode rv_occ_node;
  BOOST_CHECK_EQUAL(rv_occ_node.payload().probability(), Vector3f(0.5f, 0.25f, 0.25f));
  BOOST_CHECK_EQUAL(rv_occ_node.payload().probability().sum(), 1);

  RV rv(Vector3f(0.5f, 0.25f, 0.25f));

  for (int i = 0; i < 3; ++i) {
    rv_occ_node.insertMeasurement(RV::DataType(0.6f, 0.2f, 0.2f));
    rv.integrateMeasurement(RV::DataType(0.6f, 0.2f, 0.2f));

    BOOST_CHECK(rv_occ_node.payload().probability().isApprox(rv.probability()));
  }
}


typedef boost::mpl::list<ProbabilityTag, LogProbabilityTag, LogOddsTag> ProbTypes;

BOOST_AUTO_TEST_CASE_TEMPLATE(OctoMapMeasModel, ProbType, ProbTypes) {

  typedef DiscreteRandomVariable<2, float, ProbType> RV;
  typedef VoxelNode<RV, RVOccupancyNodeInterface, NonTreeNode> RVOccupancyNode;

  typedef typename RV::DataType RVDataType;
  typedef typename RV::ProbTypeTag RVProbTypeTag;

  RVOccupancyNode rv_occ_node;

  RVDataType hit (0.3f,0.7f);
  RVDataType miss (0.6f,0.4f);

  if(!std::is_same<RVProbTypeTag, LogOddsTag>::value) {
    hit = hit.cwiseQuotient(RVDataType(0.5f,0.5f));
    miss = miss.cwiseQuotient(RVDataType(0.5f,0.5f));
  }

  hit = convertRandomVariable(hit, RVProbTypeTag(), ProbabilityTag());
  miss = convertRandomVariable(miss, RVProbTypeTag(), ProbabilityTag());

  const float precision = 1e-4f;

  BOOST_CHECK_CLOSE(rv_occ_node.payload().probability()[1], 0.5f, precision);

  rv_occ_node.insertMeasurement(hit);
  BOOST_CHECK_CLOSE(rv_occ_node.payload().probability()[1], 0.7f, precision);

  rv_occ_node.insertMeasurement(hit);
  BOOST_CHECK_CLOSE(rv_occ_node.payload().probability()[1], 0.844828f, precision);

  rv_occ_node.insertMeasurement(miss);
  BOOST_CHECK_CLOSE(rv_occ_node.payload().probability()[1], 0.784f, precision);

  rv_occ_node.insertMeasurement(miss);
  BOOST_CHECK_CLOSE(rv_occ_node.payload().probability()[1], 0.707581f, precision);

  rv_occ_node.insertMeasurement(miss);
  BOOST_CHECK_CLOSE(rv_occ_node.payload().probability()[1], 0.617323f, precision);

  rv_occ_node.insertMeasurement(miss);
  BOOST_CHECK_CLOSE(rv_occ_node.payload().probability()[1], 0.518176f, precision);
}
