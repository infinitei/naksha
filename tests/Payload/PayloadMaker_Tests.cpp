/**
 * @filePayloadMaker_Tests.cpp
 * @brief Payload Maker Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Common/MakeAlignedContainer.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>


using namespace Naksha;
using namespace Eigen;
using namespace std;

const IOFormat vecfmt(StreamPrecision, DontAlignCols, ", ", ", ", "", "", "[", "]");
typedef boost::mpl::list<ProbabilityTag, LogProbabilityTag, LogOddsTag> ProbTypes;

BOOST_AUTO_TEST_CASE_TEMPLATE(RVwithColorPayloadTests, ProbType, ProbTypes) {
  typedef DiscreteRandomVariable<2, double, ProbType> RV;
  typedef typename ColoredRandomVariable<RV>::type Payload;

  static_assert((std::is_same<typename Payload::RVType, RV>::value), "RVType not set");

  typedef typename Payload::RVType::DataType RVDataType;
  typedef typename Payload::ColoredPayloadType::DataType ColorDataType;

  cout << "Running RVwithColorPayloadTests with RV = " << PRETTY_TYPE_INFO_REMOVE_NS(RV, "Naksha") << '\n';

  BOOST_CHECK_EQUAL(RV::Dimension, 2);
  static_assert(RV::Dimension == 2, "Dimension should be 2 by definition.");

  Payload payload_1(RVDataType(0.5,0.5));
  BOOST_CHECK_EQUAL(payload_1.getData(), RVDataType(0.5,0.5));
  BOOST_CHECK_EQUAL(payload_1.getColor(), ColorDataType(0,0,0));
  payload_1.setData(RVDataType(4,6));
  BOOST_CHECK_EQUAL(payload_1.getData(), RVDataType(4,6));
  payload_1.setColor(ColorDataType(128,0,128));
  BOOST_CHECK_EQUAL(payload_1.getColor(), ColorDataType(128,0,128));

  Payload payload_2;
  BOOST_CHECK_EQUAL(payload_2.probability(), RVDataType(0.5,0.5));
  BOOST_CHECK_EQUAL(payload_2.getColor(), ColorDataType(0,0,0));

  Payload payload_3(ColorDataType(128,0,128));
  BOOST_CHECK_EQUAL(payload_3.probability(), RVDataType(0.5,0.5));
  BOOST_CHECK_EQUAL(payload_3.getColor(), ColorDataType(128,0,128));

  Payload payload_4(RVDataType(4,6), ColorDataType(128,0,128));
  BOOST_CHECK_EQUAL(payload_4.getData(), RVDataType(4,6));
  BOOST_CHECK_EQUAL(payload_4.getColor(), ColorDataType(128,0,128));

  BOOST_CHECK(payload_3 != payload_4);
  payload_3.setData(RVDataType(4,6));
  BOOST_CHECK(payload_3 == payload_4);
  payload_3.setColor(ColorDataType(128,128,128));
  BOOST_CHECK(payload_3 != payload_4);
}

BOOST_AUTO_TEST_CASE(SizeChecks) {

  typedef DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> BRV2f;
  typedef DiscreteRandomVariable<2, double, ProbabilityTag> RV2d;
  typedef DiscreteRandomVariable<3, float, ProbabilityTag> RV3f;
  typedef DiscreteRandomVariable<8, double> RV8d;
  typedef DiscreteRandomVariable<9, float> RV9f;
  typedef DiscreteRandomVariable<7, float> RV7f;

  typedef ColoredRandomVariable<BRV2f>::type BRV2fPayload;
  typedef ColoredRandomVariable<RV2d>::type RV2dPayload;
  typedef ColoredRandomVariable<RV3f>::type RV3fPayload;
  typedef ColoredRandomVariable<RV8d>::type RV8dPayload;
  typedef ColoredRandomVariable<RV9f>::type RV9fPayload;
  typedef ColoredRandomVariable<RV7f>::type RV7fPayload;

  BOOST_TEST_MESSAGE("sizeof(BRV2fPayload) = " << sizeof(BRV2fPayload));
  BOOST_TEST_MESSAGE("sizeof(RV2dPayload) = " << sizeof(RV2dPayload));
  BOOST_TEST_MESSAGE("sizeof(RV3fPayload) = " << sizeof(RV3fPayload));
  BOOST_TEST_MESSAGE("sizeof(RV8dPayload) = " << sizeof(RV8dPayload));
  BOOST_TEST_MESSAGE("sizeof(RV9fPayload) = " << sizeof(RV9fPayload));
  BOOST_TEST_MESSAGE("sizeof(RV7fPayload) = " << sizeof(RV7fPayload));

  // All the following should not hold due to alignment
//  BOOST_CHECK_EQUAL(sizeof(BRV2fPayload), 1 * sizeof(BRV2f::Scalar) + sizeof(ColoredPayload));
//  BOOST_CHECK_EQUAL(sizeof(RV2dPayload), 2 * sizeof(RV2dPayload::Scalar) + sizeof(ColoredPayload));
//  BOOST_CHECK_EQUAL(sizeof(RV3fPayload), 3 * sizeof(RV3fPayload::Scalar) + sizeof(ColoredPayload));
//  BOOST_CHECK_EQUAL(sizeof(RV8dPayload), 8 * sizeof(RV8dPayload::Scalar) + sizeof(ColoredPayload));
//  BOOST_CHECK_EQUAL(sizeof(RV9fPayload), 9 * sizeof(RV9fPayload::Scalar) + sizeof(ColoredPayload));
//  BOOST_CHECK_EQUAL(sizeof(RV7fPayload), 7 * sizeof(RV7fPayload::Scalar) + sizeof(ColoredPayload));
}

BOOST_AUTO_TEST_CASE(CheckAlignment) {
  typedef DiscreteRandomVariable<4, double, ProbabilityTag> RV4d;
  typedef DiscreteRandomVariable<3, float, LogProbabilityTag> RV3f;
  typedef DiscreteRandomVariable<2, float, ProbabilityTag> RV2f;
  typedef DiscreteRandomVariable<4, float, LogProbabilityTag> RV4f;

  typedef ColoredRandomVariable<RV4d>::type ColoredRV4d;
  typedef ColoredRandomVariable<RV3f>::type ColoredRV3f;

  BOOST_CHECK((IsEigenNewAllignRequired<RV4d>::value == true));
  BOOST_CHECK((IsEigenNewAllignRequired<RV3f>::value == false));
  BOOST_CHECK((IsEigenNewAllignRequired<RV2f>::value == false));

  BOOST_CHECK((IsEigenNewAllignRequired<ColoredRV3f>::value == false));
  BOOST_CHECK((IsEigenNewAllignRequired<ColoredRV4d>::value == true));


  BOOST_CHECK((std::is_same<
              MakeStdVector<int>::type::allocator_type,
              std::allocator<int> >::value == true));

  BOOST_CHECK((std::is_same<
              MakeStdVector<RV3f>::type::allocator_type,
              std::allocator<RV3f> >::value == true));

  BOOST_CHECK((std::is_same<
              MakeStdVector<RV4d>::type::allocator_type,
              std::allocator<RV4d> >::value == false));

  BOOST_CHECK((std::is_same<
              MakeStdVector<RV4f>::type::allocator_type,
              std::allocator<RV4f> >::value == false));

  BOOST_CHECK((std::is_same<
              MakeStdVector<ColoredRV3f>::type::allocator_type,
              std::allocator<ColoredRV3f> >::value == true));

  BOOST_CHECK((std::is_same<
              MakeStdVector<ColoredRV4d>::type::allocator_type,
              std::allocator<ColoredRV4d> >::value == false));
}







