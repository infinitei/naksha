/**
 * @file PayloadUtils_Tests.cpp
 * @brief PayloadUtils_Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Payload/PayloadUtils.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/LabelToMeasurementMapping.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

using namespace Naksha;
using namespace std;

template<class Payload, class MeasurementModelType>
void testPayLoadOccupancy() {
  Payload payload;
  IsPayloadOccupied<Payload> payload_occ_checker;

  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");

  BOOST_TEST_MESSAGE("Payload Prob = " << payload.probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Payload Occupancy = " << std::boolalpha << payload_occ_checker(payload));
  BOOST_CHECK_EQUAL(payload_occ_checker(payload), false);

  payload.integrateMeasurement(MeasurementModelType::HIT);
  BOOST_TEST_MESSAGE("Payload Prob = " << payload.probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Payload Occupancy = " << std::boolalpha << payload_occ_checker(payload));
  BOOST_CHECK_EQUAL(payload_occ_checker(payload), true);

  payload.integrateMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Payload Prob = " << payload.probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Payload Occupancy = " << std::boolalpha << payload_occ_checker(payload));
  BOOST_CHECK_EQUAL(payload_occ_checker(payload), true);

  payload.integrateMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Payload Prob = " << payload.probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Payload Occupancy = " << std::boolalpha << payload_occ_checker(payload));
  BOOST_CHECK_EQUAL(payload_occ_checker(payload), true);

  payload.integrateMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Payload Prob = " << payload.probability().format(vecfmt));
  BOOST_TEST_MESSAGE("Payload Occupancy = " << std::boolalpha << payload_occ_checker(payload));
  BOOST_CHECK_EQUAL(payload_occ_checker(payload), false);

}

typedef boost::mpl::list<ProbabilityTag, LogProbabilityTag, LogOddsTag> BinaryProbTypes;

BOOST_AUTO_TEST_CASE_TEMPLATE(BinaryPayLoadOccupancyTests, ProbType, BinaryProbTypes) {
  BOOST_TEST_MESSAGE("\nRunning BinaryPayLoadOccupancyTests with " << PRETTY_TYPE_INFO(ProbType));
  typedef DiscreteRandomVariable<2, double, ProbType> RV;
  typedef typename ColoredRandomVariable<RV>::type Payload;
  typedef MeasurementModel<RV> MeasurementModelType;
  testPayLoadOccupancy<Payload, MeasurementModelType>();
}

BOOST_AUTO_TEST_CASE_TEMPLATE(HybridPayLoadOccupancyTests, ProbType, BinaryProbTypes) {
  BOOST_TEST_MESSAGE("\nRunning HybridPayLoadOccupancyTests with " << PRETTY_TYPE_INFO(ProbType));
  typedef DiscreteRandomVariable<9, double, ProbType> RV;
  typedef typename ColoredRandomVariable<RV>::type Payload;
  typedef MeasurementModel<RV> MeasurementModelType;
  testPayLoadOccupancy<Payload, MeasurementModelType>();
}

typedef boost::mpl::list<ProbabilityTag, LogProbabilityTag> MultivariateProbTypes;

BOOST_AUTO_TEST_CASE_TEMPLATE(modeOfPayloadTests, ProbType, MultivariateProbTypes) {
  BOOST_TEST_MESSAGE("\nRunning modeOfPayloadTests with " << PRETTY_TYPE_INFO(ProbType));

  typedef DiscreteRandomVariable<9, double, ProbType> RV;
  typedef typename ColoredRandomVariable<RV>::type Payload;
  typedef MeasurementModel<RV> MeasurementModelType;
  typedef MeasurementMap<RV> RVMeasurementMap;

  Payload payload;

  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");

  BOOST_TEST_MESSAGE("Prob = " << payload.probability().format(vecfmt) << " Mode = " << modeOfPayload(payload));
  BOOST_CHECK_EQUAL(modeOfPayload(payload), 0);

  payload.integrateMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << payload.probability().format(vecfmt) << " Mode = " << modeOfPayload(payload));
  BOOST_CHECK_EQUAL(modeOfPayload(payload), 0);

  payload.integrateMeasurement(RVMeasurementMap::template at<ImageLabels::ROAD>());
  BOOST_TEST_MESSAGE("Prob = " << payload.probability().format(vecfmt) << " Mode = " << modeOfPayload(payload));
  BOOST_CHECK_EQUAL(modeOfPayload(payload), ImageLabels::ROAD);

  payload.integrateMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << payload.probability().format(vecfmt) << " Mode = " << modeOfPayload(payload));
  BOOST_CHECK_EQUAL(modeOfPayload(payload), 0);

  payload.integrateMeasurement(RVMeasurementMap::template at<ImageLabels::CAR>());
  payload.integrateMeasurement(RVMeasurementMap::template at<ImageLabels::CAR>());
  BOOST_TEST_MESSAGE("Prob = " << payload.probability().format(vecfmt) << " Mode = " << modeOfPayload(payload));
  BOOST_CHECK_EQUAL(modeOfPayload(payload), ImageLabels::CAR);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(modeOfPayloadOccChekedTests, ProbType, MultivariateProbTypes) {
  BOOST_TEST_MESSAGE("\nRunning modeOfPayloadOccChekedTests with " << PRETTY_TYPE_INFO(ProbType));

  typedef DiscreteRandomVariable<9, double, ProbType> RV;
  typedef typename ColoredRandomVariable<RV>::type Payload;
  typedef MeasurementModel<RV> MeasurementModelType;
  typedef MeasurementMap<RV> RVMeasurementMap;

  Payload payload;
  ModeOfPayloadOccChecked<Payload> getModeOccCheked;

  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");

  BOOST_TEST_MESSAGE("Prob = " << payload.probability().format(vecfmt) << " Mode = " << getModeOccCheked(payload));
  BOOST_CHECK_EQUAL(getModeOccCheked(payload), 0);

  payload.integrateMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << payload.probability().format(vecfmt) << " Mode = " << getModeOccCheked(payload));
  BOOST_CHECK_EQUAL(getModeOccCheked(payload), 0);

  payload.integrateMeasurement(RVMeasurementMap::template at<ImageLabels::ROAD>());
  BOOST_TEST_MESSAGE("Prob = " << payload.probability().format(vecfmt) << " Mode = " << getModeOccCheked(payload));
  BOOST_CHECK_EQUAL(getModeOccCheked(payload), ImageLabels::ROAD);

  payload.integrateMeasurement(MeasurementModelType::MISS);
  BOOST_TEST_MESSAGE("Prob = " << payload.probability().format(vecfmt) << " Mode = " << getModeOccCheked(payload));
  BOOST_CHECK_EQUAL(getModeOccCheked(payload), ImageLabels::ROAD);
}
