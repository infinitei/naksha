/**
 * @file PayloadSerialization_Tests.cpp
 * @brief PayloadSerialization_Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Common/SerializationHelper.h"
#include "Naksha/Common/MakeAlignedContainer.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/scoped_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/make_shared.hpp>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;

typedef DiscreteRandomVariable<2, float> LPRV2f;
typedef DiscreteRandomVariable<2, float, ProbabilityTag> PRV2f;

BOOST_SERIALIZATION_SHARED_PTR(Vector4f)

BOOST_AUTO_TEST_CASE(LogProbabilityRandomVariableTests) {
  typedef PRV2f RV;
  typedef boost::scoped_ptr<RV> RVPtr;

  RVPtr ptr_rv( new RV(Vector2f(0.5,0.5)));
  serializeBinary(ptr_rv, "payload.tmp");
  RVPtr ptr_rv_in;
  deSerializeBinary(ptr_rv_in, "payload.tmp");
  BOOST_CHECK_EQUAL(ptr_rv_in->getData(), Vector2f(0.5,0.5));

  RV rv(Vector2f(0.3f,0.7f));
  serializeBinary(rv, "payload.tmp");
  RV rv_in;
  deSerializeBinary(rv_in, "payload.tmp");
  BOOST_CHECK_EQUAL(rv_in.getData(), Vector2f(0.3f,0.7f));
  std::remove("payload.tmp");
}

BOOST_AUTO_TEST_CASE(ProbabilityRandomVariableTests) {
  typedef LPRV2f RV;
  RV node(Vector2f(0.5f,0.5f));
  serializeBinary(&node, "prob_rv.tmp");
  RV * node_in;
  deSerializeBinary(node_in, "prob_rv.tmp");
  BOOST_CHECK_EQUAL(node_in->getData(), Vector2f(0.5,0.5));
  std::remove("prob_rv.tmp");
}

typedef DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> BLORVf;
typedef DiscreteRandomVariable<8, float, LogProbabilityTag> LPRV8f;
typedef DiscreteRandomVariable<9, float, LogProbabilityTag> LPRV9f;

typedef boost::mpl::list<LPRV2f, PRV2f, BLORVf, LPRV8f, LPRV9f> RVTypes;
typedef boost::mpl::list<LPRV8f, LPRV9f> MultiRVTypes;

BOOST_AUTO_TEST_CASE_TEMPLATE(RandomVariableSerializationTests, RVType, MultiRVTypes) {
  BOOST_TEST_MESSAGE("Running RandomVariableSerializationTests with RVTYpe = " << PRETTY_TYPE_INFO_REMOVE_NS(RVType, "Naksha") );
  typedef typename RVType::Scalar Scalar;
  typedef typename RVType::DataType RVDatatype;

  RVType payload;
  payload.setProbability(RVDatatype::Constant(Scalar(1) / RVType::Dimension));

  serializeBinary(payload, "payload.tmp");

  RVType payload_in;
  BOOST_CHECK(payload != payload_in);

  deSerializeBinary(payload_in, "payload.tmp");

  BOOST_CHECK(payload == payload_in);
  std::remove("payload.tmp");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ColoredRandomVariableSerializationTests, RV, RVTypes) {
  typedef typename ColoredRandomVariable<RV>::type Payload;
  typedef typename Payload::ColoredPayloadType::DataType ColorDataType;

  static_assert((std::is_same<typename Payload::RVType, RV>::value), "RVType not set");

  BOOST_TEST_MESSAGE("Running ColoredRandomVariableSerializationTests with Payload = " << PRETTY_TYPE_INFO_REMOVE_NS(Payload, "Naksha"));

  Payload payload (ColorDataType(100, 0, 0));
  serializeBinary(payload, "payload.tmp");

  Payload payload_in;

  BOOST_CHECK(payload != payload_in);

  deSerializeBinary(payload_in, "payload.tmp");

  BOOST_CHECK(payload == payload_in);
  std::remove("payload.tmp");
}


typedef boost::mpl::list
    <
        DiscreteRandomVariable<4, double, ProbabilityTag>
      , DiscreteRandomVariable<9, float, LogProbabilityTag>
      , DiscreteRandomVariable<8, float, LogProbabilityTag>
      , DiscreteRandomVariable<16, double, ProbabilityTag>
      , DiscreteRandomVariable<3, float, LogProbabilityTag>
    > AlignedRVs;

typedef boost::make_variant_over<
      boost::mpl::transform<AlignedRVs, MakeStdVector<boost::mpl::_> >::type
    >::type VariantPayloadStdVector;

BOOST_AUTO_TEST_CASE_TEMPLATE(StdVectorRandomVariableSerializationTests, RVType, AlignedRVs) {
  BOOST_TEST_MESSAGE("Testing StdVector of RV serialization with RVTYpe = " << PRETTY_TYPE_INFO_REMOVE_NS(RVType, "Naksha") );

  typedef typename RVType::Scalar Scalar;
  typedef typename RVType::DataType RVDatatype;

  typedef typename MakeStdVector<RVType>::type PayloadStdVector;

  PayloadStdVector payloads(100);
  for(RVType& payload : payloads) {
    payload.setProbability(RVDatatype::Constant(Scalar(1) / RVType::Dimension));
  }

  serializeBinary(payloads, "payload-vector.tmp");

  PayloadStdVector payloads_in;
  BOOST_REQUIRE(deSerializeBinary(payloads_in, "payload-vector.tmp"));
  BOOST_REQUIRE(payloads.size() == payloads_in.size());
  for (typename PayloadStdVector::size_type i = 0; i < payloads.size(); ++i) {
    BOOST_CHECK(payloads[i] == payloads_in[i]);
  }

  VariantPayloadStdVector v = payloads;
  serializeBinary(v, "payload-vector.tmp");

  VariantPayloadStdVector v_in;
  BOOST_REQUIRE(deSerializeBinary(v_in, "payload-vector.tmp"));

  PayloadStdVector v_p_in = boost::get<PayloadStdVector>(v_in);
  BOOST_REQUIRE(payloads.size() == v_p_in.size());
  for (typename PayloadStdVector::size_type i = 0; i < payloads.size(); ++i) {
    BOOST_CHECK(payloads[i] == v_p_in[i]);
  }

  std::remove("payload-vector.tmp");
}
