/**
 * @file PayloadTraits_Tests.cpp
 * @brief PayloadTraits_Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Payload/PayloadTraits.h"
#include "Naksha/Payload/ConvertPayload.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include <Naksha/Common/EigenTraits.h>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

using namespace Naksha;

BOOST_AUTO_TEST_CASE(HasColorInfo_Tests) {
  static_assert((std::is_same<HasColorInfo<ColoredPayload>::type, std::true_type>::value), "Type mismatch");
  BOOST_CHECK_EQUAL(HasColorInfo<ColoredPayload>::value, true);

  static_assert((std::is_same<HasColorInfo<float>::type, std::false_type>::value), "Type mismatch");
  BOOST_CHECK_EQUAL(HasColorInfo<float>::value, false);

  typedef DiscreteRandomVariable<9, float, ProbabilityTag> RV;
  BOOST_CHECK_EQUAL(HasColorInfo<RV>::value, false);
  BOOST_CHECK_EQUAL(HasColorInfo<ColoredRandomVariable<RV>::type>::value, true);
}

BOOST_AUTO_TEST_CASE(HasRandomVariableInfo_Tests) {
  bool result = HasRandomVariableInfo<float>::value;
  BOOST_CHECK_EQUAL(result, false);
  result = HasRandomVariableInfo<ColoredPayload>::value;
  BOOST_CHECK_EQUAL(result, false);

  typedef DiscreteRandomVariable<9, float, ProbabilityTag> RV;
  result = HasRandomVariableInfo<RV>::value;
  BOOST_CHECK_EQUAL(result, true);

  typedef ColoredRandomVariable<RV>::type ColoredRV;
  result = HasRandomVariableInfo<ColoredRV>::value;
  BOOST_CHECK_EQUAL(result, true);
}

BOOST_AUTO_TEST_CASE(IsDiscreteRandomVariable_Tests) {
  BOOST_CHECK_EQUAL(IsDiscreteRandomVariable<float>::value, false);

  static_assert((std::is_same<IsDiscreteRandomVariable<ColoredPayload>::type, std::false_type>::value), "Type mismatch");
  BOOST_CHECK_EQUAL(IsDiscreteRandomVariable<ColoredPayload>::value, false);

  typedef DiscreteRandomVariable<9, float, ProbabilityTag> RV;
  static_assert((std::is_same<IsDiscreteRandomVariable<RV>::type, std::true_type>::value), "Type mismatch");
  BOOST_CHECK_EQUAL(IsDiscreteRandomVariable<RV>::value, true);

  typedef ColoredRandomVariable<RV>::type ColoredRV;
  BOOST_CHECK_EQUAL(IsDiscreteRandomVariable<ColoredRV>::value, false);
}

BOOST_AUTO_TEST_CASE(HasCompatibleColorInfo_Tests) {
  static_assert(
      (std::is_same<HasCompatibleColorInfo<ColoredPayload, ColoredPayload>::type, std::true_type>::value),
      "Type mismatch");
  BOOST_CHECK_EQUAL((HasCompatibleColorInfo<ColoredPayload, ColoredPayload>::value), true);
  BOOST_CHECK_EQUAL((HasCompatibleColorInfo<float, ColoredPayload>::value), false);
  typedef ColoredRandomVariable<DiscreteRandomVariable<9, float, LogProbabilityTag> >::type ColoredPayloadA;
  typedef ColoredRandomVariable<DiscreteRandomVariable<9, float, ProbabilityTag> >::type ColoredPayloadB;
  BOOST_CHECK_EQUAL((HasCompatibleColorInfo<ColoredPayloadA, ColoredPayloadB>::value), true);
  BOOST_CHECK_EQUAL((HasCompatibleColorInfo<ColoredPayloadA, int>::value), false);
}

BOOST_AUTO_TEST_CASE(HasCompatibleRandomVariableInfo_Tests) {
  typedef DiscreteRandomVariable<9, float, ProbabilityTag> RVP9f;
  typedef DiscreteRandomVariable<9, double, LogProbabilityTag> RVLP9d;
  typedef DiscreteRandomVariable<6, float, ProbabilityTag> RVP6f;

  typedef ColoredRandomVariable<RVP9f>::type ColoredRVP9f;


  static_assert(
      (std::is_same<HasCompatibleRandomVariableInfo<RVP9f, ColoredRVP9f>::type, std::true_type>::value),
      "Type mismatch");
  static_assert(
      (std::is_same<HasCompatibleRandomVariableInfo<RVP9f, RVP6f>::type, std::false_type>::value),
      "Type mismatch");

  BOOST_CHECK_EQUAL((HasCompatibleRandomVariableInfo<RVP9f, RVP6f>::value) , false);
  BOOST_CHECK_EQUAL((HasCompatibleRandomVariableInfo<RVP9f, RVLP9d>::value) , true);
  BOOST_CHECK_EQUAL((HasCompatibleRandomVariableInfo<RVP9f, RVP9f>::value) , true);
  BOOST_CHECK_EQUAL((HasCompatibleRandomVariableInfo<RVP9f, ColoredRVP9f>::value) , true);
  BOOST_CHECK_EQUAL((HasCompatibleRandomVariableInfo<RVP9f, int>::value), false);
  BOOST_CHECK_EQUAL((HasCompatibleRandomVariableInfo<float, int>::value), false);
}
