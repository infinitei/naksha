/**
 * @file MeasurementModel_Tests.cpp
 * @brief MeasurementModel_Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Payload/LabelToMeasurementMapping.h"


#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Common/BoostUnitTestHelper.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>


using namespace Naksha;
using namespace std;

typedef boost::mpl::list<ProbabilityTag, LogProbabilityTag, LogOddsTag> ProbTypes;
typedef boost::mpl::list<float, double> FloatingPointTypes;

BOOST_AUTO_TEST_CASE_TEMPLATE(BinaryMeasurementModelParams, ScalarType, FloatingPointTypes) {
  typedef DiscreteRandomVariable<2, ScalarType> RV;
  typedef BinaryMeasurementModel<RV> RVMeasModel;

  BOOST_CHECK(RVMeasModel::free_hit_prob < 1);
  BOOST_CHECK(RVMeasModel::free_miss_prob < 1);
  BOOST_CHECK(RVMeasModel::free_hit_prob < RVMeasModel::free_miss_prob);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(HybridMeasurementModelParams, ScalarType, FloatingPointTypes) {
  typedef DiscreteRandomVariable<2, ScalarType> RV;
  typedef HybridMeasurementModel<RV> RVMeasModel;

  BOOST_CHECK(RVMeasModel::semantic_hit_prob < 1);
  BOOST_CHECK((RVMeasModel::free_hit_prob + RVMeasModel::semantic_hit_prob) < 1);
}


BOOST_AUTO_TEST_CASE(BinaryMeasurementModelBasics) {
  typedef DiscreteRandomVariable<2, double, ProbabilityTag> PRV;
  typedef BinaryMeasurementModel<PRV> PRVMeasModel;
  BOOST_CHECK_EQUAL(PRVMeasModel::HIT, PRV::DataType(0.6, 1.4));

  typedef DiscreteRandomVariable<3, double, ProbabilityTag> PRV3d;
  typedef BinaryMeasurementModel<PRV3d> PRV3dMeasModel;
  BOOST_CHECK_EQUAL(PRV3dMeasModel::HIT, PRV3d::DataType(0.6, 1.4, 1.4));

  typedef DiscreteRandomVariable<2, double, LogOddsTag, BinaryRandomVariable> BLORV;
  typedef BinaryMeasurementModel<BLORV> BLORVMeasModel;
  BLORV::DataType expected = convertRandomVariable(BLORV::DataType(0.3), LogOddsTag(), ProbabilityTag());
  BOOST_CHECK_EQUAL(BLORVMeasModel::HIT, expected);

//  static_assert(BLORVMeasModel::HIT == 0.3, "Should be available Compile time");
//  static_assert(PRVMeasModel::DataType::Ones() == PRV::DataType::Ones(), "Should be available Compile time");
}

BOOST_AUTO_TEST_CASE(HybridMeasurementModelBasics) {
  typedef DiscreteRandomVariable<3, double, ProbabilityTag> PRV3d;
  typedef HybridMeasurementModel<PRV3d> PRV3dMeasModel;

  BOOST_CHECK_EQUAL(PRV3dMeasModel::HIT, PRV3d::DataType(0.3/0.5, 0.35/0.25, 0.35/0.25));
  BOOST_CHECK_EQUAL(PRV3dMeasModel::MISS, PRV3d::DataType(1.2, 0.8, 0.8));
  BOOST_CHECK(PRV3dMeasModel::SEMANTIC_HIT[0].isApprox(PRV3d::DataType(0.3/0.5, 0.52/0.25, 0.18/0.25)));
  BOOST_CHECK(PRV3dMeasModel::SEMANTIC_HIT[1].isApprox(PRV3d::DataType(0.3/0.5, 0.18/0.25, 0.52/0.25)));
}

BOOST_AUTO_TEST_CASE(GenricMeasurementModel) {
  typedef DiscreteRandomVariable<3, double, ProbabilityTag> PRV3d;
  typedef MeasurementModel<PRV3d> PRV3dMeasModel;

  BOOST_CHECK_EQUAL(PRV3dMeasModel::HIT, PRV3d::DataType(0.3/0.5, 0.35/0.25, 0.35/0.25));
  BOOST_CHECK_EQUAL(PRV3dMeasModel::MISS, PRV3d::DataType(1.2, 0.8, 0.8));
  BOOST_CHECK(PRV3dMeasModel::SEMANTIC_HIT[0].isApprox(PRV3d::DataType(0.3/0.5, 0.52/0.25, 0.18/0.25)));
  BOOST_CHECK(PRV3dMeasModel::SEMANTIC_HIT[1].isApprox(PRV3d::DataType(0.3/0.5, 0.18/0.25, 0.52/0.25)));


  typedef DiscreteRandomVariable<2, double, LogOddsTag, BinaryRandomVariable> BLORV;

  BLORV::DataType expected = convertRandomVariable(BLORV::DataType(0.3), LogOddsTag(), ProbabilityTag());

  //The following will fail to compile
//  typedef HybridMeasurementModel<BLORV> BLORVHybridMeasModel;
//  BOOST_CHECK_EQUAL(BLORVHybridMeasModel::HIT, expected);


  typedef MeasurementModel<BLORV> BLORVMeasModel;
  BOOST_CHECK_EQUAL(BLORVMeasModel::HIT, expected);

}


BOOST_AUTO_TEST_CASE(MeasurementUsage) {
  typedef DiscreteRandomVariable<3, double, ProbabilityTag> PRV3d;
  typedef HybridMeasurementModel<PRV3d> PRV3dMeasModel;

  typedef MeasurementMap<PRV3d> PRV3dMeasurementMap;

  BOOST_CHECK_EQUAL(PRV3dMeasModel::MISS, PRV3dMeasurementMap::at<ImageLabels::SKY>());
  BOOST_CHECK_EQUAL(PRV3dMeasModel::HIT, PRV3dMeasurementMap::at<ImageLabels::UNKNOWN>());
  BOOST_CHECK(PRV3dMeasurementMap::at<ImageLabels::ROAD>().isApprox(PRV3d::DataType(0.3/0.5, 0.52/0.25, 0.18/0.25)));
  BOOST_CHECK(PRV3dMeasurementMap::at<ImageLabels::SIDEWALK>().isApprox(PRV3d::DataType(0.3/0.5, 0.18/0.25, 0.52/0.25)));

  // Should fail to compile
//  BOOST_CHECK(PRV3dMeasurementMap::at<ImageLabels::CAR>().isApprox(PRV3d::DataType(0.3/0.5, 0.52/0.25, 0.18/0.25)));


  typedef DiscreteRandomVariable<2, double, LogOddsTag, BinaryRandomVariable> BLORV;
  typedef MeasurementModel<BLORV> BLORVMeasModel;

  typedef MeasurementMap<BLORV> BLORVMeasurementMap;
  BOOST_CHECK_EQUAL(BLORVMeasModel::MISS, BLORVMeasurementMap::at<ImageLabels::SKY>());
  BOOST_CHECK_EQUAL(BLORVMeasModel::HIT, BLORVMeasurementMap::at<ImageLabels::UNKNOWN>());

  // Should fail to compile
//  BLORVMeasurementMap::MeasurementModelDataType val =  BLORVMeasurementMap::at<ImageLabels::ROAD>();

}


BOOST_AUTO_TEST_CASE_TEMPLATE(OctoMapMeasModel_with_BinaryMeasurementModelStatic, ProbType, ProbTypes) {

  BOOST_TEST_MESSAGE("Verifying OctoMapMeasModel_with_BinaryMeasurementModelStatic with " << PRETTY_TYPE_INFO(ProbType) );

  typedef DiscreteRandomVariable<2, float, ProbType> RV;

  typedef typename RV::DataType RVDataType;
  typedef typename RV::ProbTypeTag RVProbTypeTag;

  typedef BinaryMeasurementModel<RV> MeasModel;

  const float precision = 1e-4f;


  RVDataType hit (0.3f,0.7f);
  RVDataType miss (0.6f,0.4f);

  if(!std::is_same<RVProbTypeTag, LogOddsTag>::value) {
    hit = hit.cwiseQuotient(RVDataType(0.5f,0.5f));
    miss = miss.cwiseQuotient(RVDataType(0.5f,0.5f));
  }

  hit = convertRandomVariable(hit, RVProbTypeTag(), ProbabilityTag());
  miss = convertRandomVariable(miss, RVProbTypeTag(), ProbabilityTag());

  CHECK_CLOSE_EIGEN_MATRIX(MeasModel::HIT, hit, precision);
  CHECK_CLOSE_EIGEN_MATRIX(MeasModel::MISS, miss, precision);



  RV rv;

  BOOST_CHECK_CLOSE(rv.probability()[1], 0.5f, precision);

  rv.integrateMeasurement(MeasModel::HIT);
  BOOST_CHECK_CLOSE(rv.probability()[1], 0.7f, precision);

  rv.integrateMeasurement(MeasModel::HIT);
  BOOST_CHECK_CLOSE(rv.probability()[1], 0.844828f, precision);

  rv.integrateMeasurement(MeasModel::MISS);
  BOOST_CHECK_CLOSE(rv.probability()[1], 0.784f, precision);

  rv.integrateMeasurement(MeasModel::MISS);
  BOOST_CHECK_CLOSE(rv.probability()[1], 0.707581f, precision);

  rv.integrateMeasurement(MeasModel::MISS);
  BOOST_CHECK_CLOSE(rv.probability()[1], 0.617323f, precision);

  rv.integrateMeasurement(MeasModel::MISS);
  BOOST_CHECK_CLOSE(rv.probability()[1], 0.518176f, precision);
}

BOOST_AUTO_TEST_CASE(BinaryOctoMapMeasModel) {

  typedef DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> BLORV;
  typedef DiscreteRandomVariable<2, float, LogOddsTag> LORV;


  BLORV blorv;
  LORV lorv;


  typedef BinaryMeasurementModel<BLORV> BLORVMeasModel;
  typedef BinaryMeasurementModel<LORV> LORVMeasModel;

  const float precision = 1e-4f;

  BOOST_CHECK_CLOSE(blorv.probability(), 0.5f, precision);
  BOOST_CHECK_CLOSE(lorv.probability()[1], 0.5f, precision);

  blorv.integrateMeasurement(BLORVMeasModel::HIT);
  BOOST_CHECK_CLOSE(1. - blorv.probability(), 0.7f, precision);
  lorv.integrateMeasurement(LORVMeasModel::HIT);
  BOOST_CHECK_CLOSE(lorv.probability()[1], 0.7f, precision);

  blorv.integrateMeasurement(BLORVMeasModel::HIT);
  BOOST_CHECK_CLOSE(1. - blorv.probability(), 0.844828f, precision);

  blorv.integrateMeasurement(BLORVMeasModel::MISS);
  BOOST_CHECK_CLOSE(1. - blorv.probability(), 0.784f, precision);

  blorv.integrateMeasurement(BLORVMeasModel::MISS);
  BOOST_CHECK_CLOSE(1. - blorv.probability(), 0.707581f, precision);

  blorv.integrateMeasurement(BLORVMeasModel::MISS);
  BOOST_CHECK_CLOSE(1. - blorv.probability(), 0.617323f, precision);

  blorv.integrateMeasurement(BLORVMeasModel::MISS);
  BOOST_CHECK_CLOSE(1. - blorv.probability(), 0.518176f, precision);
}
