/**
 * @file ConvertPayload_Tests.cpp
 * @brief Payload Conversion Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Payload/ConvertPayload.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include <boost/algorithm/string/replace.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>
#include <map>


struct PrettyTypeShortener {
  PrettyTypeShortener() {
    string_map["float"] = "f";
    string_map["double"] = "d";
    string_map["int"] = "i";
    string_map["ProbabilityTag"] = "P";
    string_map["LogProbabilityTag"] = "LP";
    string_map["LogOddsTag"] = "LO";
    string_map["CategoricalRandomVariable"] = "CRV";
    string_map["BinaryRandomVariable"] = "BRV";
    string_map["DiscreteRandomVariable"] = "DRV";
    string_map["CombinePayloads2"] = "Combine";
    string_map["ColoredPayload"] = "Color";
  }
  std::string operator()(const std::string& input) const {
    std::string shortened = input;
    typedef std::map<std::string, std::string>::const_iterator it_type;
    for(it_type iterator = string_map.begin(); iterator != string_map.end(); iterator++) {
      boost::replace_all(shortened, iterator->first, iterator->second);
    }
    return shortened;
  }
  std::map<std::string, std::string> string_map;
};


template<class ToPayloadType>
struct ColoredPayloadConversionTest {
  template<typename FromPayloadType>
  void operator()(FromPayloadType) {
    BOOST_TEST_MESSAGE(shrortner(PRETTY_TYPE(ToPayloadType, "Naksha", 2))
                       << " <----- " << shrortner(PRETTY_TYPE(FromPayloadType, "Naksha", 2)));

    FromPayloadType from_payload;
    from_payload.setColor(100,101,102);
    ToPayloadType to_payload = Naksha::convertPayload<ToPayloadType>(from_payload);
    BOOST_CHECK_EQUAL(to_payload.getColor(), typename Naksha::ColoredPayload::DataType(100,101,102));

  }

  PrettyTypeShortener shrortner;
};

template<class ToPayloadType>
struct ColoredRVPayloadConversionTest {
  template<typename FromPayloadType>
  void operator()(FromPayloadType) {
    BOOST_TEST_MESSAGE(shrortner(PRETTY_TYPE(ToPayloadType, "Naksha", 2))
                       << " <----- " << shrortner(PRETTY_TYPE(FromPayloadType, "Naksha", 2)));

    typedef typename FromPayloadType::RVType::DataType FromRVDataType;
    typedef typename ToPayloadType::RVType::DataType ToRVDataType;

    FromPayloadType from_payload;
    from_payload.setProbability(FromRVDataType(0.6,0.2,0.2));
    from_payload.setColor(100,101,102);
    ToPayloadType to_payload = Naksha::convertPayload<ToPayloadType>(from_payload);
    BOOST_CHECK_EQUAL(to_payload.getColor(), typename Naksha::ColoredPayload::DataType(100,101,102));
    BOOST_CHECK(to_payload.probability().isApprox(ToRVDataType(0.6,0.2,0.2), 1e-6));

  }

  PrettyTypeShortener shrortner;
};

template<typename ToProbType>
struct RandomVariablePayloadTest {
  template<typename FromProbType>
  void operator()(FromProbType) {
    BOOST_TEST_MESSAGE(PRETTY_TYPE_INFO(ToProbType) << " <----- " << PRETTY_TYPE_INFO(FromProbType));

    typedef Naksha::DiscreteRandomVariable<3, double, ToProbType> ToPayloadType;

    typedef Naksha::DiscreteRandomVariable<3, double, FromProbType> FromPayloadTypeD;
    FromPayloadTypeD from_payloadD;
    BOOST_CHECK_EQUAL(from_payloadD.probability(), Eigen::Vector3d(0.5,0.25,0.25));
    ToPayloadType to_payload = Naksha::convertPayload<ToPayloadType>(from_payloadD);
    BOOST_CHECK_EQUAL(to_payload.probability(), Eigen::Vector3d(0.5,0.25,0.25));

    typedef Naksha::DiscreteRandomVariable<3, float, FromProbType> FromPayloadTypeF;
    FromPayloadTypeF from_payloadF;
    BOOST_CHECK(from_payloadF.probability().isApprox(Eigen::Vector3f(0.5f,0.25f,0.25f)));
    to_payload = Naksha::convertPayload<ToPayloadType>(from_payloadF);
    BOOST_CHECK( to_payload.probability().isApprox(Eigen::Vector3d(0.5,0.25,0.25), 1e-6));

  }
};

using namespace Naksha;

BOOST_AUTO_TEST_CASE(same_payload_type_conversion_tests) {
  BOOST_TEST_MESSAGE("\nRunning same_payload_conversion_tests");
  BOOST_CHECK_EQUAL(Naksha::convertPayload<double>(0.01), 0.01);
  BOOST_CHECK_EQUAL(Naksha::convertPayload<Eigen::Vector3d>(Eigen::Vector3d(0.5,0.25,0.25)), Eigen::Vector3d(0.5,0.25,0.25));
}


typedef boost::mpl::list<ProbabilityTag, LogProbabilityTag, LogOddsTag> ProbTypes;

BOOST_AUTO_TEST_CASE_TEMPLATE(RandomVariablePayLoadConversion_Tests, ProbType, ProbTypes) {
  BOOST_TEST_MESSAGE("\nRunning RandomVariablePayLoadConversion_Tests");
  boost::mpl::for_each<ProbTypes>(RandomVariablePayloadTest<ProbType>());
}

typedef boost::mpl::list
    <   ColoredPayload
//      , ColoredRandomVariable<DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> >::type
//      , ColoredRandomVariable<DiscreteRandomVariable<2, double, LogOddsTag, BinaryRandomVariable> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<6, float, ProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<8, float, LogProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<9, float, LogProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<9, float, ProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<11, float, LogProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<12, float, LogProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<13, float, LogProbabilityTag> >::type
    > ColoredPayloads;

BOOST_AUTO_TEST_CASE_TEMPLATE(ColoredPayloadConversion_Tests, ToPayload, ColoredPayloads) {
  BOOST_TEST_MESSAGE("\nRunning ColoredPayloadConversion_Tests");
  boost::mpl::for_each<ColoredPayloads>(ColoredPayloadConversionTest<ToPayload>());
}

typedef boost::mpl::list
    <   ColoredRandomVariable<DiscreteRandomVariable<3, float, ProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<3, double, ProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<3, float, LogProbabilityTag> >::type
      , ColoredRandomVariable<DiscreteRandomVariable<3, double, LogProbabilityTag> >::type
    > ColoredRVPayloads;

BOOST_AUTO_TEST_CASE_TEMPLATE(ColoredRVPayloadConversion_Tests, ToPayload, ColoredRVPayloads) {
  BOOST_TEST_MESSAGE("\nRunning ColoredRVPayloadConversion_Tests");
  boost::mpl::for_each<ColoredRVPayloads>(ColoredRVPayloadConversionTest<ToPayload>());
}
