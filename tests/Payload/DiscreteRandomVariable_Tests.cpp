/**
 * @file DiscreteRandomVariable_Tests.cpp
 * @brief Discrete RandomVariable Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Payload/DiscreteRandomVariable.h"

#include "Naksha/Common/BoostUnitTestHelper.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>


using namespace Naksha;
using namespace Eigen;
using namespace std;

const IOFormat vecfmt(StreamPrecision, DontAlignCols, ", ", ", ", "", "", "[", "]");
typedef boost::mpl::list<ProbabilityTag, LogProbabilityTag, LogOddsTag> ProbTypes;

BOOST_AUTO_TEST_CASE_TEMPLATE(CategoricalRandomVariableTests, ProbType, ProbTypes) {
  typedef DiscreteRandomVariable<2, double, ProbType> RV;
  typedef typename RV::DataType RVDataType;

  BOOST_CHECK_EQUAL(RV::Dimension, 2);
  static_assert(RV::Dimension == 2, "Dimension should be 2 by definition.");

  RV rv_1(RVDataType(0.5,0.5));
  BOOST_CHECK_EQUAL(rv_1.getData(), RVDataType(0.5,0.5));
  rv_1.setData(RVDataType(4,6));
  BOOST_CHECK_EQUAL(rv_1.getData(), RVDataType(4,6));

  RV rv_2;
  BOOST_CHECK_EQUAL(rv_2.probability(), RVDataType(0.5,0.5));

  rv_2.setData(RVDataType(4,6));
  BOOST_CHECK(rv_2 == rv_1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(setData_checks, ProbType, ProbTypes) {
  typedef DiscreteRandomVariable<2, double, ProbType> RV;
  typedef typename RV::DataType RVDataType;

  BOOST_CHECK_EQUAL(RV::Dimension, 2);
  static_assert(RV::Dimension == 2, "Dimension should be 2 by definition.");

  RV rv_1(RVDataType(0.5,0.5));
  BOOST_CHECK_EQUAL(rv_1.getData(), RVDataType(0.5,0.5));

  rv_1.setData(RVDataType(4,6));
  BOOST_CHECK_EQUAL(rv_1.getData(), RVDataType(4,6));

  rv_1.setData(RVDataType(0.5,0.5), ProbabilityTag());
  BOOST_CHECK_EQUAL(rv_1.getData(), convertRandomVariable(RVDataType(0.5,0.5), ProbType(), ProbabilityTag()));
  BOOST_CHECK_EQUAL(rv_1.probability(), RVDataType(0.5,0.5));
}

BOOST_AUTO_TEST_CASE(BinaryRandomVariableTests) {
  typedef DiscreteRandomVariable<2, double, LogOddsTag, BinaryRandomVariable> RV;
  typedef RV::DataType RVDataType;

  BOOST_CHECK_EQUAL(RV::Dimension, 2);
  static_assert(RV::Dimension == 2, "Dimension should be 2 by definition.");
  static_assert((std::is_same<RVDataType, double>::value), "DataType should be double");

  RV rv1(RVDataType(0.5));
  BOOST_CHECK_EQUAL(rv1.getData(), 0.5);
  rv1.setData(4);
  BOOST_CHECK_EQUAL(rv1.getData(), 4);

  RV rv_2;
  BOOST_CHECK_EQUAL(rv_2.probability(), 0.5);

  // This should not compile
//  DiscreteRandomVariable<1, double, LogOddsTag, BinaryRandomVariable> rv;
//  DiscreteRandomVariable<3, double, LogOddsTag, BinaryRandomVariable> rv;
//  DiscreteRandomVariable<2, double, LogProbabilityTag, BinaryRandomVariable> rv;
//  DiscreteRandomVariable<2, double, ProbabilityTag, BinaryRandomVariable> rv;
}

BOOST_AUTO_TEST_CASE(EigenNeedsToAlign) {
  typedef DiscreteRandomVariable<2, double, ProbabilityTag> RV2d;
  typedef DiscreteRandomVariable<3, float, ProbabilityTag> RV3f;
  typedef DiscreteRandomVariable<8, float> RV8f;
  typedef DiscreteRandomVariable<8, double> RV8d;
  typedef DiscreteRandomVariable<9, float> RV9f;
  typedef DiscreteRandomVariable<9, double> RV9d;

  BOOST_CHECK_EQUAL(RV2d::EigenNeedsToAlign(), 1);
  BOOST_CHECK_EQUAL(RV3f::EigenNeedsToAlign::value, 0);
  BOOST_CHECK_EQUAL(RV8f::EigenNeedsToAlign(), 1);
  BOOST_CHECK_EQUAL(RV8d::EigenNeedsToAlign(), 1);
  BOOST_CHECK_EQUAL(RV9f::EigenNeedsToAlign(), 0);
  BOOST_CHECK_EQUAL(RV9d::EigenNeedsToAlign::value, 0);
}

BOOST_AUTO_TEST_CASE(SizeChecks) {

  typedef DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> BRV2f;

  typedef DiscreteRandomVariable<2, double, ProbabilityTag> RV2d;
  typedef DiscreteRandomVariable<3, float, ProbabilityTag> RV3f;
  typedef DiscreteRandomVariable<8, double> RV8d;
  typedef DiscreteRandomVariable<9, float> RV9f;
  typedef DiscreteRandomVariable<7, float> RV7f;

  BOOST_CHECK_EQUAL(sizeof(BRV2f), 1 * sizeof(BRV2f::Scalar));
  BOOST_CHECK_EQUAL(sizeof(RV2d), 2 * sizeof(RV2d::Scalar));
  BOOST_CHECK_EQUAL(sizeof(RV3f), 3 * sizeof(RV3f::Scalar));
  BOOST_CHECK_EQUAL(sizeof(RV8d), 8 * sizeof(RV8d::Scalar));
  BOOST_CHECK_EQUAL(sizeof(RV9f), 9 * sizeof(RV9f::Scalar));
  BOOST_CHECK_EQUAL(sizeof(RV7f), 7 * sizeof(RV7f::Scalar));
}

BOOST_AUTO_TEST_CASE(DynamicAllocation) {
  typedef DiscreteRandomVariable<8, float> RV8f;
  BOOST_CHECK_EQUAL(RV8f::EigenNeedsToAlign::value, 1);

  RV8f* rv;
  rv = new RV8f;
  rv->setData(RV8f::DataType::Ones());
  BOOST_CHECK_EQUAL(rv->getData(), RV8f::DataType::Ones());
  delete rv;
}

BOOST_AUTO_TEST_CASE(ProbabilityRandomVariableFunctionality) {
  DiscreteRandomVariable<2, float, ProbabilityTag> prob_node;
  BOOST_CHECK_EQUAL(prob_node.getData(), Vector2f(0.5, 0.5));

  DiscreteRandomVariable<2, float, LogOddsTag> logodds_node;
  BOOST_CHECK_EQUAL(logodds_node.getData(), Vector2f(0, 0));

  BOOST_CHECK_EQUAL(prob_node.probability(), logodds_node.probability());
  BOOST_CHECK_EQUAL(prob_node.logProbability(), logodds_node.logProbability());
  BOOST_CHECK_EQUAL(prob_node.logOddsRatio(), logodds_node.logOddsRatio());
}

BOOST_AUTO_TEST_CASE(ProbabilityRandomVariableNormalization) {
  DiscreteRandomVariable<3, float, ProbabilityTag> node(Vector3f(2,6,2));
  BOOST_CHECK_EQUAL(node.probability().sum(), 10);
  node.normalize();
  BOOST_CHECK_EQUAL(node.probability().sum(), 1);
}

BOOST_AUTO_TEST_CASE(LogProbabilityRandomVariableFunctionality) {
  DiscreteRandomVariable<3, float, LogProbabilityTag> node;
  BOOST_CHECK_EQUAL(node.probability(), Vector3f(0.5,0.25,0.25));
  BOOST_CHECK_EQUAL(node.probability().sum(), 1);

  Vector3f tempProb (2,1,1);
  node.setData(tempProb.array().log());
  BOOST_CHECK_EQUAL(node.probability(), Vector3f(2,1,1));
  node.normalize();
  BOOST_CHECK_EQUAL(node.probability(), Vector3f(0.5,0.25,0.25));
}

BOOST_AUTO_TEST_CASE(LogProbabilityRandomVariableNormalization) {
  typedef DiscreteRandomVariable<4, float, LogProbabilityTag> LogProbRV4f;
  LogProbRV4f node;
  LogProbRV4f::DataType tempProb(1,997,1,1);
  node.setData(tempProb.array().log());
  node.normalize();
  const float tolerancePercent = 0.0001f;
  const LogProbRV4f::DataType& prob = node.probability();
  BOOST_CHECK_CLOSE(prob.x(), 0.001, tolerancePercent);
  BOOST_CHECK_CLOSE(prob.y(), 0.997, tolerancePercent);
  BOOST_CHECK_CLOSE(prob.z(), 0.001, tolerancePercent);
  BOOST_CHECK_CLOSE(prob.w(), 0.001, tolerancePercent);

}

BOOST_AUTO_TEST_CASE(setProbability) {
  DiscreteRandomVariable<3, float, ProbabilityTag> prob_node;
  DiscreteRandomVariable<3, float, LogProbabilityTag> logProb_node;
  DiscreteRandomVariable<3, float, LogOddsTag> logOdds_node;

  Vector3f gt(0.5f, 0.3f, 0.2f);

  prob_node.setProbability(gt);
  logProb_node.setProbability(gt);
  logOdds_node.setProbability(gt);

  const float tolerancePercent = 0.0001f;
  CHECK_CLOSE_EIGEN_MATRIX(logProb_node.probability(), gt, tolerancePercent);
  CHECK_CLOSE_EIGEN_MATRIX(logProb_node.probability(), gt, tolerancePercent);
  CHECK_CLOSE_EIGEN_MATRIX(logOdds_node.probability(), gt, tolerancePercent);
}

BOOST_AUTO_TEST_CASE(integrate2) {
  cout << "\nProb\n";
  DiscreteRandomVariable<2, float, ProbabilityTag> prob_node;


  cout << prob_node.probability().format(vecfmt) << endl;
  prob_node.integrateMeasurement(Vector2f(0.7,0.3));
  cout << prob_node.probability().format(vecfmt) << endl;
  prob_node.integrateMeasurement(Vector2f(0.7,0.3));
  cout << prob_node.probability().format(vecfmt) << endl;
  prob_node.integrateMeasurement(Vector2f(0.3,0.7));
  cout << prob_node.probability().format(vecfmt) << endl;
  prob_node.integrateMeasurement(Vector2f(0.3,0.7));
  cout << prob_node.probability().format(vecfmt) << endl;
  BOOST_CHECK_EQUAL(prob_node.probability(), Vector2f(0.5,0.5));

  cout << "\nLogProb\n";
  DiscreteRandomVariable<2, float> logProb_node;

  cout << logProb_node.probability().format(vecfmt) << endl;
  logProb_node.integrateMeasurement(Vector2f(log(0.7), log(0.3)));
  cout << logProb_node.probability().format(vecfmt) << endl;
  logProb_node.integrateMeasurement(Vector2f(log(0.7), log(0.3)));
  cout << logProb_node.probability().format(vecfmt) << endl;
  logProb_node.integrateMeasurement(Vector2f(log(0.3), log(0.7)));
  cout << logProb_node.probability().format(vecfmt) << endl;
  logProb_node.integrateMeasurement(Vector2f(log(0.3), log(0.7)));
  cout << logProb_node.probability().format(vecfmt) << endl;
  BOOST_CHECK_EQUAL(logProb_node.probability(), Vector2f(0.5, 0.5));

  cout << "\nLogOdds\n";
  DiscreteRandomVariable<2, float, LogOddsTag> logOdds_node;

  cout << logOdds_node.probability().format(vecfmt) << endl;
  logOdds_node.integrateMeasurement(Vector2f(logOddsFromProb(0.7), logOddsFromProb(0.3)));
  cout << logOdds_node.probability().format(vecfmt) << endl;
  logOdds_node.integrateMeasurement(Vector2f(logOddsFromProb(0.7), logOddsFromProb(0.3)));
  cout << logOdds_node.probability().format(vecfmt) << endl;
  logOdds_node.integrateMeasurement(Vector2f(logOddsFromProb(0.3), logOddsFromProb(0.7)));
  cout << logOdds_node.probability().format(vecfmt) << endl;
  logOdds_node.integrateMeasurement(Vector2f(logOddsFromProb(0.3), logOddsFromProb(0.7)));
  cout << logOdds_node.probability().format(vecfmt) << endl;
  BOOST_CHECK_EQUAL(logOdds_node.probability(), Vector2f(0.5, 0.5));

}

BOOST_AUTO_TEST_CASE(integrate3) {
  Vector3f hit_sensor_model (0.3f,0.35f,0.35f);
  Vector3f miss_sensor_model (0.7f,0.15f,0.15f);

  hit_sensor_model = hit_sensor_model.cwiseQuotient(Vector3f(0.5f,0.25f,0.25f));
  miss_sensor_model = miss_sensor_model.cwiseQuotient(Vector3f(0.5f,0.25f,0.25f));


  cout << "\nProb\n";
  DiscreteRandomVariable<3, float, ProbabilityTag> prob_node;
//  prob_node.setProbability(Vector3f(0.5f,0.3f,0.2f));

  cout << prob_node.probability().format(vecfmt) << "   Sum = " << prob_node.probability().sum() << endl;
  prob_node.integrateMeasurement(hit_sensor_model);
  cout << prob_node.probability().format(vecfmt) << "   Sum = " << prob_node.probability().sum() << endl;
  prob_node.integrateMeasurement(hit_sensor_model);
  cout << prob_node.probability().format(vecfmt) << "   Sum = " << prob_node.probability().sum() << endl;
  prob_node.integrateMeasurement(miss_sensor_model);
  cout << prob_node.probability().format(vecfmt) << "   Sum = " << prob_node.probability().sum() << endl;
  prob_node.integrateMeasurement(miss_sensor_model);
  cout << prob_node.probability().format(vecfmt) << "   Sum = " << prob_node.probability().sum() << endl;
  BOOST_CHECK_EQUAL(prob_node.probability(), Vector3f(0.5,0.25,0.25));

  cout << "\nLogProb\n";
  DiscreteRandomVariable<3, float, LogProbabilityTag> logProb_node;

  hit_sensor_model = hit_sensor_model.array().log();
  miss_sensor_model = miss_sensor_model.array().log();

  cout << logProb_node.probability().format(vecfmt) << "   Sum = " << logProb_node.probability().sum() << endl;
  logProb_node.integrateMeasurement(hit_sensor_model);
  cout << logProb_node.probability().format(vecfmt) << "   Sum = " <<  logProb_node.probability().sum() << endl;
  logProb_node.integrateMeasurement(hit_sensor_model);
  cout << logProb_node.probability().format(vecfmt) << "   Sum = " << logProb_node.probability().sum() << endl;
  logProb_node.integrateMeasurement(miss_sensor_model);
  cout << logProb_node.probability().format(vecfmt) << "   Sum = " << logProb_node.probability().sum() << endl;
  logProb_node.integrateMeasurement(miss_sensor_model);
  cout << logProb_node.probability().format(vecfmt) << "   Sum = " << logProb_node.probability().sum() << endl;

  BOOST_CHECK_EQUAL(logProb_node.probability(), Vector3f(0.5,0.25,0.25));
}

BOOST_AUTO_TEST_CASE(lazy_integrate_measurement) {
  Vector3f hit_sensor_model(0.3f, 0.35f, 0.35f);
  Vector3f miss_sensor_model(0.7f, 0.15f, 0.15f);

  hit_sensor_model = hit_sensor_model.cwiseQuotient(Vector3f(0.5f, 0.25f, 0.25f));
  miss_sensor_model = miss_sensor_model.cwiseQuotient(Vector3f(0.5f, 0.25f, 0.25f));

  cout << "\nlazy_integrate_measurement Prob\n";
  DiscreteRandomVariable<3, float, ProbabilityTag> prob_node;
  prob_node.integrateMeasurementLazy(hit_sensor_model);
  prob_node.integrateMeasurementLazy(hit_sensor_model);
  prob_node.integrateMeasurementLazy(miss_sensor_model);

  prob_node.normalize();

  BOOST_CHECK(prob_node.probability().isApprox(Vector3f(0.3, 0.35, 0.35)));


  cout << "\nlazy_integrate_measurement LogProb\n";
  DiscreteRandomVariable<3, float, LogProbabilityTag> logProb_node;

  hit_sensor_model = hit_sensor_model.array().log();
  miss_sensor_model = miss_sensor_model.array().log();

  logProb_node.integrateMeasurementLazy(hit_sensor_model);
  logProb_node.integrateMeasurementLazy(hit_sensor_model);
  logProb_node.integrateMeasurementLazy(miss_sensor_model);

  logProb_node.normalize();

  BOOST_CHECK(logProb_node.probability().isApprox(Vector3f(0.3, 0.35, 0.35)));
}


BOOST_AUTO_TEST_CASE(integrate4) {
  const float tolerancePercent = 0.01f;
  Vector3f hit_sensor_model (0.3f,0.35f,0.35f);
  Vector3f miss_sensor_model (0.7f,0.15f,0.15f);

  hit_sensor_model = hit_sensor_model.cwiseQuotient(Vector3f(0.5,0.25,0.25));
  miss_sensor_model = miss_sensor_model.cwiseQuotient(Vector3f(0.5,0.25,0.25));

  cout << "\nProb\n";
  DiscreteRandomVariable<3, float, ProbabilityTag> prob_node;
  cout << prob_node.probability().format(vecfmt) << "   Sum = " << prob_node.probability().sum() << endl;
  CHECK_CLOSE_EIGEN_MATRIX(prob_node.probability(), Vector3f(0.5,0.25,0.25), tolerancePercent);

  prob_node.integrateMeasurement(hit_sensor_model);
  cout << prob_node.probability().format(vecfmt) << "   Sum = " << prob_node.probability().sum() << endl;

  prob_node.integrateMeasurement(hit_sensor_model);
  cout << prob_node.probability().format(vecfmt) << "   Sum = " << prob_node.probability().sum() << endl;

  prob_node.integrateMeasurement(hit_sensor_model);
  cout << prob_node.probability().format(vecfmt) << "   Sum = " << prob_node.probability().sum() << endl;

  prob_node.integrateMeasurement(hit_sensor_model);
  cout << prob_node.probability().format(vecfmt) << "   Sum = " << prob_node.probability().sum() << endl;

  cout << "\nLogProb\n";
  DiscreteRandomVariable<3, float, LogProbabilityTag> logProb_node;

  hit_sensor_model = hit_sensor_model.array().log();
  miss_sensor_model = miss_sensor_model.array().log();

  cout << logProb_node.probability().format(vecfmt) << "   Sum = " << logProb_node.probability().sum() << endl;
  logProb_node.integrateMeasurement(hit_sensor_model);
  cout << logProb_node.probability().format(vecfmt) << "   Sum = " <<  logProb_node.probability().sum() << endl;
  logProb_node.integrateMeasurement(hit_sensor_model);
  cout << logProb_node.probability().format(vecfmt) << "   Sum = " << logProb_node.probability().sum() << endl;
  logProb_node.integrateMeasurement(miss_sensor_model);
  cout << logProb_node.probability().format(vecfmt) << "   Sum = " << logProb_node.probability().sum() << endl;
  logProb_node.integrateMeasurement(miss_sensor_model);
  cout << logProb_node.probability().format(vecfmt) << "   Sum = " << logProb_node.probability().sum() << endl;

  BOOST_CHECK_EQUAL(logProb_node.probability(), Vector3f(0.5,0.25,0.25));

}

BOOST_AUTO_TEST_CASE(integrateSemantic) {

  typedef DiscreteRandomVariable<11, double, LogProbabilityTag> RV;
  typedef RV::DataType DataType;

  RV node;


  DataType prior(DataType::Ones() * 0.5/(DataType::RowsAtCompileTime -1));
  prior[0] = 0.5;

  double free_hit_prob = 0.3;
  double semantic_hit_prob = 0.55;

  DataType road_hit_model(DataType::Ones() * (1.0 - free_hit_prob - semantic_hit_prob ) / (DataType::RowsAtCompileTime -2));
  road_hit_model[0] = free_hit_prob;  //0.3
  road_hit_model[2] = semantic_hit_prob;

  road_hit_model = road_hit_model.cwiseQuotient(prior);
  road_hit_model = road_hit_model.array().log();

  cout << "\nProb\n";
  cout << node.probability().format(vecfmt) << "   Sum = " << node.probability().sum() << endl;

  node.integrateMeasurement(road_hit_model);
  cout << node.probability().format(vecfmt) << "   Sum = " << node.probability().sum() << endl;

  node.integrateMeasurement(road_hit_model);
  cout << node.probability().format(vecfmt) << "   Sum = " << node.probability().sum() << endl;

  node.integrateMeasurement(road_hit_model);
  cout << node.probability().format(vecfmt) << "   Sum = " << node.probability().sum() << endl;

  node.integrateMeasurement(road_hit_model);
  cout << node.probability().format(vecfmt) << "   Sum = " << node.probability().sum() << endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(OctoMapMeasModel, ProbType, ProbTypes) {

  cout << "Verifying OctoMapMeasModel with " << PRETTY_TYPE_INFO(ProbType) << '\n';

  typedef DiscreteRandomVariable<2, float, ProbType, CategoricalRandomVariable> RV;

  typedef typename RV::DataType RVDataType;
  typedef typename RV::ProbTypeTag RVProbTypeTag;

  RV rv;

  RVDataType hit (0.3f,0.7f);
  RVDataType miss (0.6f,0.4f);

  if(!std::is_same<RVProbTypeTag, LogOddsTag>::value) {
    hit = hit.cwiseQuotient(RVDataType(0.5f,0.5f));
    miss = miss.cwiseQuotient(RVDataType(0.5f,0.5f));
  }

  hit = convertRandomVariable(hit, RVProbTypeTag(), ProbabilityTag());
  miss = convertRandomVariable(miss, RVProbTypeTag(), ProbabilityTag());

  const float precision = 1e-4f;

  BOOST_CHECK_CLOSE(rv.probability()[1], 0.5f, precision);

  rv.integrateMeasurement(hit);
  BOOST_CHECK_CLOSE(rv.probability()[1], 0.7f, precision);

  rv.integrateMeasurement(hit);
  BOOST_CHECK_CLOSE(rv.probability()[1], 0.844828f, precision);

  rv.integrateMeasurement(miss);
  BOOST_CHECK_CLOSE(rv.probability()[1], 0.784f, precision);

  rv.integrateMeasurement(miss);
  BOOST_CHECK_CLOSE(rv.probability()[1], 0.707581f, precision);

  rv.integrateMeasurement(miss);
  BOOST_CHECK_CLOSE(rv.probability()[1], 0.617323f, precision);

  rv.integrateMeasurement(miss);
  BOOST_CHECK_CLOSE(rv.probability()[1], 0.518176f, precision);
}

BOOST_AUTO_TEST_CASE(VerifyBinaryRVwithOthers) {

  typedef DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> BLORV;
  typedef DiscreteRandomVariable<2, float, LogOddsTag> LORV;
  typedef DiscreteRandomVariable<2, float, ProbabilityTag> PRV;

  BLORV blorv;
  LORV lorv;
  PRV prv;
  BOOST_CHECK_EQUAL(blorv.probability(), 0.5f);
  BOOST_CHECK_EQUAL(lorv.probability(), Vector2f(0.5f, 0.5f));
  BOOST_CHECK_EQUAL(prv.probability(), Vector2f(0.5f, 0.5f));

  float meas_blorv(0.7f);
  Vector2f meas_lorv(0.7,0.3);
  Vector2f meas_prv(0.7,0.3);

  convertRandomVariableInPlace(meas_blorv, LogOddsTag(), ProbabilityTag());
  convertRandomVariableInPlace(meas_lorv, LogOddsTag(), ProbabilityTag());
  convertRandomVariableInPlace(meas_prv, ProbabilityTag(), ProbabilityTag());


  for (int i = 0; i < 5; ++i) {
    lorv.integrateMeasurement(meas_lorv);
    blorv.integrateMeasurement(meas_blorv);
    prv.integrateMeasurement(meas_prv);

    const float precision = 1e-5f;
    BOOST_CHECK_CLOSE(lorv.getData()[0], blorv.getData(), precision);
    BOOST_CHECK_CLOSE(lorv.probability()[0], blorv.probability(), precision);
    BOOST_CHECK_CLOSE(prv.probability()[0], blorv.probability(), precision);
    cout << "BLORV: " << blorv.probability() << "   ";
    cout << "LORV: " << lorv.probability().format(vecfmt) << "   ";
    cout << "PRV: " << prv.probability().format(vecfmt) << endl;
  }
}

BOOST_AUTO_TEST_CASE(BinaryOctoMapMeasModel) {

  typedef DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> BLORV;
  typedef DiscreteRandomVariable<2, float, LogOddsTag> LORV;


  BLORV blorv;
  LORV lorv;

  float hit_blorv (0.3f);
  float miss_blorv (0.6f);
//  hit = hit * 0.5f;
//  miss = miss * 0.5f;

  hit_blorv = convertRandomVariable(hit_blorv, LogOddsTag(), ProbabilityTag());
  miss_blorv = convertRandomVariable(miss_blorv, LogOddsTag(), ProbabilityTag());

  Vector2f hit_lorv (0.3f,0.7f);
  Vector2f miss_lorv (0.6f,0.4f);

//  hit_lorv = hit_lorv.cwiseQuotient(Vector2f(0.5f,0.5f));
//  miss_lorv = miss_lorv.cwiseQuotient(Vector2f(0.5f,0.5f));

  hit_lorv = convertRandomVariable(hit_lorv, LogOddsTag(), ProbabilityTag());
  miss_lorv = convertRandomVariable(miss_lorv, LogOddsTag(), ProbabilityTag());

  const float precision = 1e-4f;

  BOOST_CHECK_CLOSE(blorv.probability(), 0.5f, precision);
  BOOST_CHECK_CLOSE(lorv.probability()[1], 0.5f, precision);

  blorv.integrateMeasurement(hit_blorv);
  BOOST_CHECK_CLOSE(1. - blorv.probability(), 0.7f, precision);
  lorv.integrateMeasurement(hit_lorv);
  BOOST_CHECK_CLOSE(lorv.probability()[1], 0.7f, precision);

  blorv.integrateMeasurement(hit_blorv);
  BOOST_CHECK_CLOSE(1. - blorv.probability(), 0.844828f, precision);

  blorv.integrateMeasurement(miss_blorv);
  BOOST_CHECK_CLOSE(1. - blorv.probability(), 0.784f, precision);

  blorv.integrateMeasurement(miss_blorv);
  BOOST_CHECK_CLOSE(1. - blorv.probability(), 0.707581f, precision);

  blorv.integrateMeasurement(miss_blorv);
  BOOST_CHECK_CLOSE(1. - blorv.probability(), 0.617323f, precision);

  blorv.integrateMeasurement(miss_blorv);
  BOOST_CHECK_CLOSE(1. - blorv.probability(), 0.518176f, precision);
}
