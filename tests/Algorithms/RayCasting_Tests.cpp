/**
 * @file RayCasting_Tests.cpp
 * @brief RayCasting Unit Tests
 *
 * @author Abhijit Kundu
 */
#include "Naksha/Algorithms/RayCasting.h"
#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Maps/MapSerialization.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;

//TODO Test for 2D Maps

typedef DiscreteRandomVariable<9, double, LogProbabilityTag> RVLP9d;
typedef ColoredRandomVariable<RVLP9d>::type CRVLP3d;
typedef VolumetricMap<3, CRVLP3d, UnorderedMapDataStructure, HybridMappingInterface, MapTransform> UnorderedMapCRVLP3d;
typedef VolumetricMap<3, CRVLP3d, TreeDataStructure, HybridMappingInterface, MapTransform> OctreeMapCRVLP3d;
typedef VolumetricMap<3, CRVLP3d, HashedVectorDataStructure, HybridMappingInterface, MapTransform> HashedVectorMapCRVLP3d;

typedef boost::mpl::list<
    UnorderedMapCRVLP3d
  , OctreeMapCRVLP3d
  , HashedVectorMapCRVLP3d
> MapTypes3D;


BOOST_AUTO_TEST_CASE_TEMPLATE(computeRayCasting_simple, MapType, MapTypes3D) {
  BOOST_TEST_MESSAGE("\nStarting Test computeRayCasting_simple with :");

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;
  typedef Eigen::Matrix<unsigned char, 3, 1> EigenColor;
  typedef RayCasting<MapType, true> RayCasterType;


  BOOST_TEST_MESSAGE(" MapType : " << PRETTY_TYPE(MapType, "Naksha", 1));
  BOOST_TEST_MESSAGE(" RayCasterType : " << PRETTY_TYPE(RayCasterType, "Naksha", 1));
  BOOST_TEST_MESSAGE(" RayCasterType::VoxelNodePtrType : " << PRETTY_TYPE(typename RayCasterType::VoxelNodePtrType, "Naksha", 1));

  MapType map(0.1);

  Eigen::Vector3d start(0.05, 0.05, 0.05);
  Eigen::Vector3d end(0.05, 0.05, 100.05);
  KeyType end_key = map.keyFromCoord(end);

  VoxelNodeType* end_node = map.updateVoxel(end_key, MeasurementModelType::HIT);
  end_node->payload().setColor(255, 123, 120);

  RayCasterType ray_caster(&map);

  bool hit; KeyType hit_key;  VoxelNodeType* hit_node;

  // Do ray casting and check if the correct voxel is returned
  std::tie(hit, hit_key, hit_node) = ray_caster(start, end - start);
  BOOST_CHECK_EQUAL(hit, true);
  BOOST_CHECK_EQUAL(hit_key, end_key);
  BOOST_CHECK_EQUAL(hit_node, end_node);
  BOOST_CHECK_EQUAL(hit_node->payload().getColor(), EigenColor(255, 123, 120));

  // Now modify the voxel returned by the ray caster
  std::get<2>(ray_caster(start, end - start))->payload().setColor(100, 101, 102);
  BOOST_CHECK_EQUAL(end_node->payload().getColor(), EigenColor(100, 101, 102));

  std::tie(hit, hit_key, hit_node) = ray_caster(start, Eigen::Vector3d(1, 20, 1));
  BOOST_CHECK_EQUAL(hit, false);
  BOOST_CHECK_EQUAL(map.insideMaximumBbx(hit_key), false);

  std::tie(hit, hit_key, hit_node) = ray_caster(start, Eigen::Vector3d(1, 20, 1), 20.0);
  BOOST_CHECK_EQUAL(hit, false);
  double ray_length = (map.template voxelCenter<Eigen::Vector3d>(hit_key) - start).norm();
  BOOST_CHECK(ray_length < (20.0 + map.resolution()));
}

template<class MapType>
void doRayCastingTest(MapType& map, const Eigen::Vector3d& start, const Eigen::Vector3d& end) {
  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef Eigen::Matrix<unsigned char, 3, 1> EigenColor;

  typedef RayCasting<MapType, true> RayCasterType;

  BOOST_TEST_MESSAGE("\n RayCasterType : " << PRETTY_TYPE(RayCasterType, "Naksha", 1));
  BOOST_TEST_MESSAGE(" RayCasterType::VoxelNodePtrType : " << PRETTY_TYPE(typename RayCasterType::VoxelNodePtrType, "Naksha", 0));

  const RayCasterType ray_caster(&map);

  bool hit;
  KeyType hit_key;
  typename RayCasterType::VoxelNodePtrType hit_node;

  std::tie(hit, hit_key, hit_node) = ray_caster(start, end - start);
  BOOST_CHECK_EQUAL(hit, true);

  KeyType end_key = map.keyFromCoord(end);
  BOOST_CHECK_EQUAL(hit_key, end_key);

  const VoxelNodeType* end_node = map.search(end_key);
  BOOST_CHECK_EQUAL(hit_node, end_node);
  BOOST_CHECK_EQUAL(hit_node->payload().getColor(), EigenColor(255, 123, 120));

  // Th following modifier should now compile for const MapType
//  boost::get<2>(ray_caster(start, end - start))->payload().setColor(100, 101, 102);
//  BOOST_CHECK_EQUAL(end_node->payload().getColor(), EigenColor(100, 101, 102));

  std::tie(hit, hit_key, hit_node) = ray_caster(start, Eigen::Vector3d(1, 20, 1));
  BOOST_CHECK_EQUAL(hit, false);
  BOOST_CHECK_EQUAL(map.insideMaximumBbx(hit_key), false);

  std::tie(hit, hit_key, hit_node) = ray_caster(start, Eigen::Vector3d(1, 20, 1), 20.0);
  BOOST_CHECK_EQUAL(hit, false);
  double ray_length = (map.template voxelCenter<Eigen::Vector3d>(hit_key) - start).norm();
  BOOST_CHECK(ray_length < (20.0 + map.resolution()));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(computeRayCasting_constness, MapType, MapTypes3D) {
  BOOST_TEST_MESSAGE("\nStarting Test computeRayCasting_constness with :");

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;

  BOOST_TEST_MESSAGE(" MapType : " << PRETTY_TYPE(MapType, "Naksha", 1));

  MapType map(0.1);

  Eigen::Vector3d start(0.05, 0.05, 0.05);
  Eigen::Vector3d end(0.05, 0.05, 100.05);
  KeyType end_key = map.keyFromCoord(end);

  VoxelNodeType* end_node = map.updateVoxel(end_key, MeasurementModelType::HIT);
  end_node->payload().setColor(255, 123, 120);

  doRayCastingTest<const MapType>(map, start, end);
  doRayCastingTest<MapType>(map, start, end);
}
