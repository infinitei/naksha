/**
 * @file RayTracing_Tests.cpp
 * @brief RayTracing Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Algorithms/RayTracing.h"
#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Maps/MapSerialization.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;


//TODO Test for 2D Maps

typedef DiscreteRandomVariable<9, double, LogProbabilityTag> RVLP9d;
typedef ColoredRandomVariable<RVLP9d>::type CRVLP3d;
typedef VolumetricMap<3, CRVLP3d, UnorderedMapDataStructure, HybridMappingInterface, MapTransform> UnorderedMapCRVLP3d;
typedef VolumetricMap<3, CRVLP3d, TreeDataStructure, HybridMappingInterface, MapTransform> OctreeMapCRVLP3d;
typedef VolumetricMap<3, CRVLP3d, HashedVectorDataStructure, HybridMappingInterface, MapTransform> HashedVectorMapCRVLP3d;

typedef boost::mpl::list<
    UnorderedMapCRVLP3d
  , OctreeMapCRVLP3d
  , HashedVectorMapCRVLP3d
> MapTypes3D;

BOOST_AUTO_TEST_CASE_TEMPLATE(computeKeysOnRay_PointTypes, MapType, MapTypes3D) {
  BOOST_TEST_MESSAGE("\nStarting Test computeKeysOnRay_PointTypes with :");
  BOOST_TEST_MESSAGE(" MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha"));

  typedef typename MapType::KeyType KeyType;
  typedef typename std::vector<KeyType> KeyTypeContainer;

  MapType map(0.1);

  KeyTypeContainer ray_keys;
  BOOST_CHECK_EQUAL(ray_keys.size(), 0);
  bool success = computeKeysOnRay(map, Eigen::Vector3d(0,0,0), Eigen::Vector3d(2, 2, 2), ray_keys);
  BOOST_CHECK_EQUAL(success, true);
  BOOST_CHECK(ray_keys.size() != 0);

  ray_keys.clear();
  BOOST_CHECK_EQUAL(ray_keys.size(), 0);
  success = computeKeysOnRay(map, Eigen::Vector3f(0, 0, 0), Eigen::Vector3f(2, 2, 2), ray_keys);
  BOOST_CHECK_EQUAL(success, true);
  BOOST_CHECK(ray_keys.size() != 0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(computeKeysOnRay_key_check, MapType, MapTypes3D) {
  BOOST_TEST_MESSAGE("\nStarting Test computeKeysOnRay_key_check with :");
  BOOST_TEST_MESSAGE(" MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha"));

  typedef typename MapType::KeyType KeyType;
  typedef typename std::vector<KeyType> KeyTypeContainer;

  MapType map(0.1);

  Eigen::Vector3d start(0.05,0.05,0.05);
  Eigen::Vector3d end(0.05,0.05,100.05);
  KeyType start_key = map.keyFromCoord(start);
  KeyType end_key = map.keyFromCoord(end);

  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
  BOOST_TEST_MESSAGE("Shooting Ray from " << start.format(vecfmt) << " --> " << end.format(vecfmt));
  BOOST_TEST_MESSAGE("Shooting Ray from " << start_key << " --> " << end_key);

  KeyTypeContainer ray_keys;
  bool success = computeKeysOnRay(map, start, end, ray_keys);
  BOOST_CHECK_EQUAL(success, true);
  BOOST_CHECK_EQUAL(ray_keys.size(), end_key[2] - start_key[2]);

  size_t index = 0;
  for (typename KeyType::Scalar i = start_key[2]; i < end_key[2]; ++i) {
    BOOST_CHECK_EQUAL(ray_keys[index], KeyType(start_key[0], start_key[1], i));
    ++index;
  }
}

BOOST_AUTO_TEST_CASE_TEMPLATE(computeKeysOnRay_SizeCheck, MapType, MapTypes3D) {
  BOOST_TEST_MESSAGE("\nStarting Test computeKeysOnRay_SizeCheck with :");
  BOOST_TEST_MESSAGE(" MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha"));

  typedef typename MapType::KeyType KeyType;
  typedef typename std::vector<KeyType> KeyTypeContainer;

  MapType map(0.1);

  {
    KeyTypeContainer ray_keys;
    BOOST_CHECK_EQUAL(ray_keys.size(), 0);
    bool success = computeKeysOnRay(map, Eigen::Vector3d(0,0,0), Eigen::Vector3d(2, 2, 2), ray_keys);
    BOOST_CHECK_EQUAL(success, true);

    BOOST_TEST_MESSAGE("ray_keys.size() = " << ray_keys.size());
    BOOST_CHECK_EQUAL(ray_keys.size() , 58);
  }

  {
    KeyTypeContainer ray_keys;
    BOOST_CHECK_EQUAL(ray_keys.size(), 0);
    bool success = computeKeysOnRay(map, Eigen::Vector3d(0,0,0), Eigen::Vector3d(2, 2, -2), ray_keys);
    BOOST_CHECK_EQUAL(success, true);

    BOOST_TEST_MESSAGE("ray_keys.size() = " << ray_keys.size());
    BOOST_CHECK(std::abs((int)ray_keys.size() - 58) < 2);
  }

  {
    KeyTypeContainer ray_keys;
    BOOST_CHECK_EQUAL(ray_keys.size(), 0);
    bool success = computeKeysOnRay(map, Eigen::Vector3d(0,0,0), Eigen::Vector3d(2, -2, 2), ray_keys);
    BOOST_CHECK_EQUAL(success, true);

    BOOST_TEST_MESSAGE("ray_keys.size() = " << ray_keys.size());
    BOOST_CHECK(std::abs((int)ray_keys.size() - 58) < 3);
  }

  {
    KeyTypeContainer ray_keys;
    BOOST_CHECK_EQUAL(ray_keys.size(), 0);
    bool success = computeKeysOnRay(map, Eigen::Vector3d(0,0,0), Eigen::Vector3d(-2, -2, -2), ray_keys);
    BOOST_CHECK_EQUAL(success, true);

    BOOST_TEST_MESSAGE("ray_keys.size() = " << ray_keys.size());
    BOOST_CHECK(std::abs((int)ray_keys.size() - 58) < 3);
  }

  {
    KeyTypeContainer ray_keys;
    BOOST_CHECK_EQUAL(ray_keys.size(), 0);
    bool success = computeKeysOnRay(map, Eigen::Vector3d(0,0,0), Eigen::Vector3d(-2, 2, -2), ray_keys);
    BOOST_CHECK_EQUAL(success, true);

    BOOST_TEST_MESSAGE("ray_keys.size() = " << ray_keys.size());
    BOOST_CHECK(std::abs((int)ray_keys.size() - 58) < 3);
  }
}





