/**
 * @file EraseIf_Tests.cpp
 * @brief EraseIf Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Algorithms/BoundingBoxOperations.h"

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>
#include <boost/mpl/joint_view.hpp>

using namespace Naksha;

typedef DiscreteRandomVariable<9, float, LogProbabilityTag> RVLP9f;
typedef ColoredRandomVariable<RVLP9f>::type CRVLP9f;

typedef boost::mpl::list<
    VolumetricMap<3, CRVLP9f, UnorderedMapDataStructure, HybridMappingInterface, MapTransform, RVOccupancyNodeInterface>
  , VolumetricMap<3, CRVLP9f, TreeDataStructure, HybridMappingInterface, MapTransform, RVOccupancyNodeInterface>
  , VolumetricMap<3, CRVLP9f, HashedVectorDataStructure, HybridMappingInterface, MapTransform, RVOccupancyNodeInterface>
> MapTypes3D;

typedef boost::mpl::list<
    VolumetricMap<2, CRVLP9f, HashedVectorDataStructure, HybridMappingInterface, MapTransform, RVOccupancyNodeInterface>
  , VolumetricMap<2, CRVLP9f, TreeDataStructure, HybridMappingInterface, MapTransform, RVOccupancyNodeInterface>
  , VolumetricMap<2, CRVLP9f, UnorderedMapDataStructure, HybridMappingInterface, MapTransform, RVOccupancyNodeInterface>
> MapTypes2D;

typedef boost::mpl::joint_view<MapTypes3D, MapTypes2D> MapTypes;

BOOST_AUTO_TEST_CASE_TEMPLATE(cullMap_tests, MapType, MapTypes) {
  BOOST_TEST_MESSAGE("\nStarting Test cullMap_tests with :");
  BOOST_TEST_MESSAGE(" MapType: " << PRETTY_TYPE(MapType, "Naksha", 1));

  MapType map(0.1);

  for(int i = 0; i<1000; ++i) {
    typedef typename MapType::KeyType KeyType;
    typedef typename KeyType::DataType KeyDataType;
    map.addNewVoxel(KeyType(KeyDataType::Constant(i)));
  }
  BOOST_CHECK_EQUAL(map.size(), 1000);

  typedef Eigen::Matrix<double , MapType::Dimension, 1> VectorType;
  eraseVoxelsOutsideBbx(map, VectorType::Constant(200), VectorType::Constant(300));

  BOOST_CHECK_EQUAL(map.size(), 0);
}

template <typename KeyType>
struct KeyFunctor {
  typedef typename KeyType::Scalar KeyScalar;
  typedef Eigen::AlignedBox<KeyScalar, KeyType::Dimension> KeyBoundingBox;

  KeyFunctor(const KeyType& min_key, const KeyType& max_key)
   : key_bbx(min_key.value, max_key.value) {
  }

  bool operator() (const KeyType& key) const {
    return key_bbx.contains(key.value);
  }

  KeyBoundingBox key_bbx;
};

BOOST_AUTO_TEST_CASE_TEMPLATE(cullMap3d_tests, MapType, MapTypes3D) {
  BOOST_TEST_MESSAGE("\nStarting Test cullMap3d_tests with :");
  BOOST_TEST_MESSAGE(" MapType: " << PRETTY_TYPE(MapType, "Naksha", 1));

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::const_iterator MapConstIterator;
  typedef Naksha::MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;

  MapType map(0.1);

  const double range = 20.0;
  const double step = 0.5;

  for (double x  = -range; x <= range; x += step)
    for (double y  = -range; y <= range; y += step)
      for (double z = -range; z <= range; z += step)
        map.updateVoxel(Eigen::Vector3d(x,y,z), MeasurementModelType::HIT);

  std::size_t numbet_of_voxels = std::pow((range*2)/step + 1, 3);
  BOOST_CHECK_EQUAL(map.size(), numbet_of_voxels);

  for(MapConstIterator it = map.begin(); it!= map.end(); ++it) {
    // Now the voxel found through find API should also be consistent
    MapConstIterator voxel = map.find(it.key());
    BOOST_REQUIRE(voxel == it);
  }


  BOOST_TEST_MESSAGE(" # of Initial Voxels: " << map.size());

  const double cull_range = 10.0;
  eraseVoxelsOutsideBbx(map, Eigen::Vector3d::Constant(-cull_range), Eigen::Vector3d::Constant(cull_range));

  std::size_t number_of_voxels_remaining = std::pow((cull_range*2)/step + 1, 3);
  BOOST_CHECK_EQUAL(map.size(), number_of_voxels_remaining);

  BOOST_TEST_MESSAGE(" # of Voxels culled: " << numbet_of_voxels - number_of_voxels_remaining);
  BOOST_TEST_MESSAGE(" # of Voxels after culling: " << map.size());

  KeyFunctor<KeyType> checkIfKeyInsideCullRange(
        map.keyFromCoord(Eigen::Vector3d::Constant(-cull_range)),
        map.keyFromCoord(Eigen::Vector3d::Constant(cull_range)));



  std::size_t num_of_voxels_inside_cull_range = 0;
  for(MapConstIterator it = map.begin(); it!= map.end(); ++it) {
    const KeyType& key = it.key();
    if(checkIfKeyInsideCullRange(it.key()))
      ++num_of_voxels_inside_cull_range;

    // Now the voxel found through find API should also be consistent
    MapConstIterator voxel = map.find(key);
    BOOST_REQUIRE(voxel == it);
  }

  BOOST_CHECK_EQUAL(num_of_voxels_inside_cull_range, number_of_voxels_remaining);
}



