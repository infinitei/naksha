/**
 * @file RayAABBIntersection_Tests.cpp
 * @brief RayAABBIntersection unit tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Algorithms/RayIntersections.h"
#include <Eigen/Geometry>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(test_2d) {
  typedef AlignedBox2d BoundingBoxType;
  typedef BoundingBoxType::VectorType VectorType;

  BoundingBoxType bbx(VectorType::Constant(-10), VectorType::Constant(10));
  VectorType origin = VectorType(0, 0);

  RayAABBIntersection<double, 2> intersection(bbx, origin);

  BOOST_CHECK_EQUAL(intersection(VectorType(-0, 2).normalized()), VectorType(0, 10));

  BOOST_CHECK_EQUAL(intersection(VectorType(1, 1).normalized()),  VectorType(10, 10));

  BOOST_CHECK_EQUAL(intersection(VectorType(2, 1).normalized()),  VectorType(10, 5));

  BOOST_CHECK_EQUAL(intersection(VectorType(2, -1).normalized()), VectorType(10, -5));
}
