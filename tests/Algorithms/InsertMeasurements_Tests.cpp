/**
 * @file InsertMeasurements_Tests.cpp
 * @brief InsertMeasurements Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Algorithms/InsertMeasurements.h"
#include "Naksha/Algorithms/CompareMaps.h"
#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Maps/MapSerialization.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/LabelToMeasurementMapping.h"
#include "Naksha/Nodes/NodeUtils.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Common/EigenTypedefs.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

using namespace Naksha;
using namespace Eigen;

//TODO Test for 2D Maps

typedef DiscreteRandomVariable<3, double, LogProbabilityTag> RVLP3d;
typedef DiscreteRandomVariable<4, double, LogProbabilityTag> RVLP4d;
typedef ColoredRandomVariable<RVLP3d>::type CRVLP3d;
typedef ColoredRandomVariable<RVLP4d>::type CRVLP4d;

typedef VolumetricMap<3, CRVLP3d, UnorderedMapDataStructure, HybridMappingInterface, MapTransform, RVOccupancyNodeInterface> UnorderedMapCRVLP3d;
typedef VolumetricMap<3, CRVLP3d, TreeDataStructure, HybridMappingInterface, MapTransform, RVOccupancyNodeInterface> OctreeMapCRVLP3d;
typedef VolumetricMap<3, CRVLP3d, HashedVectorDataStructure, HybridMappingInterface, MapTransform, RVOccupancyNodeInterface> HashedVectorMapCRVLP3d;

typedef VolumetricMap<3, CRVLP3d, TreeDataStructure, OccupancyMappingInteface, MapTransform> OccOctreeMapCRVLP3d;
typedef VolumetricMap<3, CRVLP3d, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> OccHashedVectorMapCRVLP3d;

typedef VolumetricMap<3, CRVLP4d, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> UnorderedMapCRVLP4d;
typedef VolumetricMap<3, CRVLP4d, TreeDataStructure, OccupancyMappingInteface, MapTransform> OctreeMapCRVLP4d;
typedef VolumetricMap<3, CRVLP4d, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> HashedVectorMapCRVLP4d;


typedef boost::mpl::list<
    UnorderedMapCRVLP3d
  , OctreeMapCRVLP3d
  , HashedVectorMapCRVLP3d
  , OccOctreeMapCRVLP3d
  , OccHashedVectorMapCRVLP3d
  , UnorderedMapCRVLP4d
  , OctreeMapCRVLP4d
  , HashedVectorMapCRVLP4d
> MapTypes3D;

typedef boost::mpl::list<
    UnorderedMapCRVLP3d
  , OctreeMapCRVLP3d
  , HashedVectorMapCRVLP3d
> HybridMapTypes3D;

BOOST_AUTO_TEST_CASE_TEMPLATE(insertDepthScan_tests, MapType, MapTypes3D) {
  BOOST_TEST_MESSAGE("\nStarting Test insertDepthScan_tests with :");
  BOOST_TEST_MESSAGE(" MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha"));

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;


  MapType map(0.1);

  Eigen::Vector3d start(0.05, 0.05, 0.05);
  Eigen::Vector3d end(0.05, 0.05, 100.05);
  KeyType start_key = map.keyFromCoord(start);
  KeyType end_key = map.keyFromCoord(end);

  typedef EigenAligned<Eigen::Vector3d>::std_vector PointCloudType;
  PointCloudType point_cloud;

  point_cloud.push_back(end);

  insertDepthScan(map, start, point_cloud, MeasurementModelType::HIT);

  BOOST_CHECK_EQUAL(map.size(), end_key[2] - start_key[2] + 1);

  IsNodeOccupied<VoxelNodeType> occ_check;
  std::size_t solid_voxels = std::count_if(map.begin(), map.end(), occ_check);
  std::size_t free_voxels = map.size() - solid_voxels;

  BOOST_CHECK_EQUAL(solid_voxels, 1);
  BOOST_CHECK_EQUAL(free_voxels, end_key[2] - start_key[2]);

  map.clear();
  BOOST_CHECK_EQUAL(map.size(), 0);

  map.setMaximumBbx(Eigen::Vector3d::Constant(-1),  Eigen::Vector3d::Constant(50));
  insertDepthScan(map, start, point_cloud, MeasurementModelType::HIT);

  BOOST_CHECK_EQUAL(map.size(), 50 / map.resolution());
  solid_voxels = std::count_if(map.begin(), map.end(), occ_check);
  BOOST_CHECK_EQUAL(solid_voxels, 0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(insertDepthScan_overloads, MapType, MapTypes3D) {
  BOOST_TEST_MESSAGE("\nStarting Test insertDepthScan_tests with :");
  BOOST_TEST_MESSAGE(" MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha"));

  Eigen::Vector3d start(0.05, 0.05, 0.05);
  Eigen::Vector3d end(0.05, 0.05, 100.05);

  typedef EigenAligned<Eigen::Vector3d>::std_vector PointCloudType;
  PointCloudType point_cloud;

  point_cloud.push_back(end);

  MapType mapA(0.1);
  insertDepthScan(mapA, start, point_cloud);

  MapType mapB(0.1);
  typedef MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;
  insertDepthScan(mapB, start, point_cloud, MeasurementModelType::HIT);

  BOOST_CHECK(compareMaps(mapA, mapB));

}

BOOST_AUTO_TEST_CASE_TEMPLATE(insertMeasurementsAlongRays_tests, MapType, HybridMapTypes3D) {
  BOOST_TEST_MESSAGE("\nStarting Test insertDepthScan_tests with :");
  BOOST_TEST_MESSAGE(" MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha"));

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;

  MapType map(0.1);

  Eigen::Vector3d start(0.05, 0.05, 0.05);
  Eigen::Vector3d end(0.05, 0.05, 100.05);
  KeyType start_key = map.keyFromCoord(start);
  KeyType end_key = map.keyFromCoord(end);

  typedef EigenAligned<Eigen::Vector3d>::std_vector PointCloudType;
  PointCloudType point_cloud;

  point_cloud.push_back(end);

  insertMeasurementsAlongRays(map, start, point_cloud, MeasurementModelType::HIT);

  BOOST_CHECK_EQUAL(map.size(), end_key[2] - start_key[2] + 1);

  IsNodeOccupied<VoxelNodeType> occ_check;
  std::size_t solid_voxels = std::count_if(map.begin(), map.end(), occ_check);
  std::size_t free_voxels = map.size() - solid_voxels;

  BOOST_CHECK_EQUAL(solid_voxels, map.size());
  BOOST_CHECK_EQUAL(free_voxels, 0);

  map.clear();
  BOOST_CHECK_EQUAL(map.size(), 0);

  map.setMaximumBbx(Eigen::Vector3d::Constant(-1),  Eigen::Vector3d::Constant(50));
  insertMeasurementsAlongRays(map, start, point_cloud, MeasurementModelType::HIT);

  BOOST_CHECK_EQUAL(map.size(), 50 / map.resolution());
  solid_voxels = std::count_if(map.begin(), map.end(), occ_check);
  BOOST_CHECK_EQUAL(solid_voxels, map.size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(updateOnceAndShallowAngleTest, MapType, HybridMapTypes3D) {
  BOOST_TEST_MESSAGE("\nStarting update Once And Shallow Angle Test with :");
  BOOST_TEST_MESSAGE(" MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha"));

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  typedef MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;

  MapType map(0.1);

  typedef EigenAligned<Eigen::Vector3d>::std_vector PointCloudType;
  PointCloudType point_cloud;

  Eigen::Vector3d start(0.05, 0.05, 0.05);
  KeyType start_key = map.keyFromCoord(start);

  Eigen::Vector3d end1(0.05, 0.05, 100.05);
  point_cloud.push_back(end1);
  KeyType end1_key = map.keyFromCoord(end1);

  Eigen::Vector3d end2(0.05, 0.05, 101.05);
  point_cloud.push_back(end2);
  KeyType end2_key = map.keyFromCoord(end2);


  insertDepthScan(map, start, point_cloud, MeasurementModelType::HIT);

  map.finishLazyEvaluation();

  typedef typename MapType::const_iterator VoxelConstIterator;
  for(VoxelConstIterator voxel_it = map.begin(), end =  map.end(); voxel_it != end; ++voxel_it ) {
    BOOST_REQUIRE_EQUAL(voxel_it->numOfObservations(), 1);
  }

  BOOST_CHECK_EQUAL(map.size(), end2_key[2] - start_key[2] + 1);

  IsNodeOccupied<VoxelNodeType> occ_check;
  std::size_t solid_voxels = std::count_if(map.begin(), map.end(), occ_check);
  BOOST_CHECK_EQUAL(solid_voxels, 2);


  map.clear();
  BOOST_CHECK_EQUAL(map.size(), 0);

  insertMeasurementsAlongRays(map, start, point_cloud, MeasurementModelType::HIT);
  map.finishLazyEvaluation();

  BOOST_CHECK_EQUAL(map.size(), end2_key[2] - start_key[2] + 1);

  for(typename KeyType::Scalar z= start_key[2]; z < end1_key[2]; ++z ) {
    KeyType key(start_key[0], start_key[1], z);
    VoxelConstIterator voxel_it = map.find(key);
    BOOST_CHECK(voxel_it != map.end());
    BOOST_CHECK_EQUAL(voxel_it->numOfObservations(), 2);
  }
}


BOOST_AUTO_TEST_CASE_TEMPLATE(insertDepthScan_With_MesurementMap_tests, MapType, HybridMapTypes3D) {
  BOOST_TEST_MESSAGE("\nStarting insertDepthScan_With_MesurementMap_tests with :");
  BOOST_TEST_MESSAGE(" MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha"));

  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;

  typedef typename MapType::Payload::RVType RV;
  typedef MeasurementModel<RV>  MeasurementModelType;
  typedef MeasurementMap<RV> StaticMeasurementMap;



  MapType map(0.1);

  typedef EigenAligned<Eigen::Vector3d>::std_vector PointCloudType;
  PointCloudType point_cloud;

  typedef std::map<ImageLabels::SemanticLabel, typename MeasurementModelType::DataType> RuntimeMeasurementMap;
  RuntimeMeasurementMap measurement_map;

  measurement_map[ImageLabels::SKY] = MeasurementModelType::MISS;
  measurement_map[ImageLabels::ROAD] = StaticMeasurementMap::template at<ImageLabels::ROAD>();
  measurement_map[ImageLabels::SIDEWALK] = StaticMeasurementMap::template at<ImageLabels::SIDEWALK>();


  std::vector<ImageLabels::SemanticLabel> measurement_ids;

  Eigen::Vector3d start(0.05, 0.05, 0.05);
  KeyType start_key = map.keyFromCoord(start);

  Eigen::Vector3d end1(0.05, 0.05, 100.05);
  point_cloud.push_back(end1);
  measurement_ids.push_back(ImageLabels::ROAD);
  KeyType end1_key = map.keyFromCoord(end1);

  Eigen::Vector3d end2(0.05, 0.05, 101.05);
  point_cloud.push_back(end2);
  measurement_ids.push_back(ImageLabels::SIDEWALK);
  KeyType end2_key = map.keyFromCoord(end2);

  insertDepthScan(map, start, point_cloud, measurement_ids, measurement_map);

  map.finishLazyEvaluation();

  typedef typename MapType::const_iterator VoxelConstIterator;
  for(VoxelConstIterator voxel_it = map.begin(), end =  map.end(); voxel_it != end; ++voxel_it ) {
    BOOST_CHECK_EQUAL(voxel_it->numOfObservations(), 1);
  }

  BOOST_CHECK_EQUAL(map.size(), end2_key[2] - start_key[2] + 1);

  IsNodeOccupied<VoxelNodeType> occ_check;
  std::size_t solid_voxels = std::count_if(map.begin(), map.end(), occ_check);
  BOOST_CHECK_EQUAL(solid_voxels, 2);

  ModeOfNode<VoxelNodeType, false, VoxelLabels::SemanticLabel> get_mode;

  VoxelConstIterator voxel_end1 = map.find(end1_key);
  BOOST_CHECK(voxel_end1 != map.end());
  BOOST_CHECK_EQUAL(get_mode(*voxel_end1), VoxelLabels::ROAD);

  VoxelConstIterator voxel_end2 = map.find(end2_key);
  BOOST_CHECK(voxel_end2 != map.end());
  BOOST_CHECK_EQUAL(get_mode(*voxel_end2), VoxelLabels::SIDEWALK);
}
