/**
 * @file EigenSerialization_Tests.cpp
 * @brief Eigen Serialization Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Common/EigenBoostSerialization.h"
#include "Naksha/Common/SerializationHelper.h"
#include "Naksha/Common/EigenTypedefs.h"

#include <boost/make_shared.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>
#include <cstdio>

using namespace Naksha;
using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(EigenMatrix) {
  serializeBinary(Vector2f(0.5f,0.5f), "a.tmp");
  Vector2f a2f;
  deSerializeBinary(a2f, "a.tmp");
  BOOST_CHECK_EQUAL(a2f, Vector2f(0.5f,0.5f));
  VectorXf axf;
  deSerializeBinary(axf, "a.tmp");
  BOOST_CHECK_EQUAL(axf, Vector2f(0.5f,0.5f));

  MatrixXd m(100,100);
  m.setRandom();
  serializeText(m, "m.txt");

  MatrixXd m_in;
  deSerializeText(m_in, "m.txt");
  BOOST_CHECK_EQUAL(m, m_in);
}


BOOST_AUTO_TEST_CASE(EigenTranformTypes) {
  SE3d se3(AngleAxisd(0.25*M_PI, Vector3d::UnitX()));
  serializeText(se3.matrix(), "se3.txt");
  SE3d se3_in;
  deSerializeText(se3_in.matrix(), "se3.txt");
  BOOST_CHECK(se3.matrix() == se3_in.matrix());
  BOOST_CHECK(se3.rotation() == se3_in.rotation());
  BOOST_CHECK(se3.translation() == se3_in.translation());
  std::remove("se3.txt");
}

typedef boost::mpl::list<
        Vector4d
      , Matrix3f
      , Matrix<float, 30, 30>
      , Matrix<float, 6, 1>
      , Matrix<float, 8, 1>
      , Matrix<double, 9, 1>
      , Matrix<double, 10, 10, RowMajor>
> EigenFixedSizeTypes;

BOOST_AUTO_TEST_CASE_TEMPLATE( SimpleSaveLoadVerification, T , EigenFixedSizeTypes) {
  T data = T::Random();
  const std::string file_name = "EigenType.tmp";
  serializeBinary(data, file_name);
  T data_in;
  deSerializeBinary(data_in, file_name);
  BOOST_CHECK_EQUAL(data, data_in);
  std::remove(file_name.c_str());
}



BOOST_AUTO_TEST_CASE_TEMPLATE( PtrToEigenTypes, T , EigenFixedSizeTypes) {
  boost::shared_ptr<T> ptr = boost::make_shared<T>(T::Random());
  const std::string file_name = "EigenType_ptr.tmp";
  serializeBinary(ptr, file_name);
  boost::shared_ptr<T> ptr_in;
  deSerializeBinary(ptr_in, file_name);
  BOOST_CHECK_EQUAL(*ptr, *ptr_in);
  std::remove(file_name.c_str());
}

BOOST_AUTO_TEST_CASE_TEMPLATE( VectorOfEigenTypes, T , EigenFixedSizeTypes) {
  typename EigenAligned<T>::std_vector vector, vector_in;
  for (size_t i = 0; i< 100; ++i) {
    vector.push_back(T::Random());
  }

  const string file_name = "EigenType_vector.bin";
  serializeBinary(vector, file_name);
  deSerializeBinary(vector_in, file_name);

  for (size_t i =0; i < vector.size(); ++i)
    BOOST_CHECK_EQUAL(vector[i], vector_in[i]);

  std::remove(file_name.c_str());
}

BOOST_AUTO_TEST_CASE(SubMatrix) {
  Matrix4f m(Matrix4f::Random());
  serializeText(m.topRows<2>(), "m.txt");
  MatrixXf m_in;
  deSerializeText(m_in, "m.txt");
  BOOST_CHECK_EQUAL(m.topRows<2>(), m_in);

  serializeText(m.block<2,2>(1,1), "m.txt");
  deSerializeText(m_in, "m.txt");
  BOOST_CHECK_EQUAL((m.block<2,2>(1,1)), m_in);

  serializeText(m.topRows(2), "m.txt");
  deSerializeText(m_in, "m.txt");
  BOOST_CHECK_EQUAL(m.topRows(2), m_in);
}

BOOST_AUTO_TEST_CASE(SubVectors) {
  VectorXf vec(100);
  vec.setRandom();

  serializeBinary(vec.tail<5>(), "vec.txt");
  MatrixXf vec_in;
  deSerializeBinary(vec_in, "vec.txt");
  BOOST_CHECK_EQUAL(vec.tail<5>(), vec_in);

  serializeBinary(vec.segment<5>(10), "vec.txt");
  deSerializeBinary(vec_in, "vec.txt");
  BOOST_CHECK_EQUAL(vec.segment<5>(10), vec_in);
}

BOOST_AUTO_TEST_CASE(ProjectiveTransformTypes) {
  Eigen::Projective3d proj;
  proj.matrix() = Eigen::Matrix4d::Random();
  serializeBinary(proj, "proj.bin");
  Eigen::Projective3d proj_in;
  deSerializeBinary(proj_in, "proj.bin");
  BOOST_CHECK(proj.isApprox(proj_in));
}

BOOST_AUTO_TEST_CASE(AffineCompactTransformTypes) {
  Eigen::AffineCompact3f tfm;
  tfm.matrix().setRandom();
  serializeBinary(tfm, "tfm.bin");
  Eigen::AffineCompact3f tfm_in;
  deSerializeBinary(tfm_in, "tfm.bin");
  BOOST_CHECK(tfm.isApprox(tfm_in));
}

BOOST_AUTO_TEST_CASE(AlignedBoxTypes) {
  Eigen::AlignedBox3d bbx(Eigen::Vector3d(-2, -1, 0), Eigen::Vector3d(100, 566, 45));
  serializeBinary(bbx, "bbx.bin");
  Eigen::AlignedBox3d bbx_in;
  deSerializeBinary(bbx_in, "bbx.bin");
  BOOST_CHECK(bbx.isApprox(bbx_in));
}



