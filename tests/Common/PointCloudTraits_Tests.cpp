/**
 * @file PointCloudTraits_Tests.cpp
 * @brief PointCloudTraits Unit Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Common/PointCloudTraits.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <type_traits>
#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

using namespace Naksha;

typedef boost::mpl::list<
      Eigen::Matrix<float, 3, Eigen::Dynamic, Eigen::RowMajor>
    , Eigen::Matrix<float, 3, Eigen::Dynamic, Eigen::ColMajor>
    , Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
    , Eigen::Vector2d
    , Eigen::Vector3d
    , Eigen::Vector4f
    , Eigen::MatrixXd
    , Eigen::MatrixXf
> EigenPointCloudTypes;

typedef boost::mpl::list<
      std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> >
    , std::vector<Eigen::Vector4f, Eigen::aligned_allocator<Eigen::Vector4f> >
    , std::vector<Eigen::Vector3f >
    , std::vector<Eigen::Vector2f >
> StdVectorPointCloudTypes;


typedef std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> > StdVectorofEigenVector3d;
typedef std::vector<Eigen::Vector3f, Eigen::aligned_allocator<Eigen::Vector3f> > StdVectorofEigenVector3f;

BOOST_AUTO_TEST_CASE_TEMPLATE(test_PointCloudTraits_EigenOnly, PointCloudType, EigenPointCloudTypes){

  typedef typename PointCloudTraits<PointCloudType>::type Tag;
  BOOST_TEST_MESSAGE("Tag of " << PRETTY_TYPE(PointCloudType, "Naksha", 2) <<
                     " : " << PRETTY_TYPE(Tag, "Naksha", 2) );

  BOOST_CHECK((std::is_same<Tag, EigenPointCloudTag>::value));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(test_PointCloudTraits_StdVector, PointCloudType, StdVectorPointCloudTypes){

  typedef typename PointCloudTraits<PointCloudType>::type Tag;
  BOOST_TEST_MESSAGE("Tag of " << PRETTY_TYPE(PointCloudType, "Naksha", 2) <<
                     " : " << PRETTY_TYPE(Tag, "Naksha", 2) );

  BOOST_CHECK((std::is_same<Tag, StdVectorOfEigenPointsTag>::value));
}

int tagDispatch(EigenPointCloudTag) {
  return 0;
}

int tagDispatch(SingleEigenFixedSizePointTag) {
  return 1;
}

BOOST_AUTO_TEST_CASE(test_EigenPointCloudTraits) {

  {
    typedef Eigen::Vector3d PointCloudType;
    typedef EigenPointCloudTraits<3, PointCloudType>::type Tag;
    BOOST_TEST_MESSAGE("Tag of " << PRETTY_TYPE(PointCloudType, "Naksha", 2) <<
                       " : " << PRETTY_TYPE(Tag, "Naksha", 2) );
    BOOST_CHECK((std::is_same<Tag, SingleEigenFixedSizePointTag>::value));
    BOOST_CHECK_EQUAL(tagDispatch(Tag()), 1);
  }
  {
    typedef Eigen::Vector4f PointCloudType;
    typedef EigenPointCloudTraits<4, PointCloudType>::type Tag;
    BOOST_TEST_MESSAGE("Tag of " << PRETTY_TYPE(PointCloudType, "Naksha", 2) <<
                       " : " << PRETTY_TYPE(Tag, "Naksha", 2) );
    BOOST_CHECK((std::is_same<Tag, SingleEigenFixedSizePointTag>::value));
    BOOST_CHECK_EQUAL(tagDispatch(Tag()), 1);
  }
  {
    typedef Eigen::Matrix<float, 3, Eigen::Dynamic, Eigen::RowMajor> PointCloudType;
    typedef EigenPointCloudTraits<3, PointCloudType>::type Tag;
    BOOST_TEST_MESSAGE("Tag of " << PRETTY_TYPE(PointCloudType, "Naksha", 2) <<
                       " : " << PRETTY_TYPE(Tag, "Naksha", 2) );
    BOOST_CHECK((std::is_same<Tag, MultiEigenFixedSizePointsTag>::value));
    BOOST_CHECK_EQUAL(tagDispatch(Tag()), 0);
  }
  {
    typedef Eigen::MatrixXd PointCloudType;
    typedef EigenPointCloudTraits<3, PointCloudType>::type Tag;
    BOOST_TEST_MESSAGE("Tag of " << PRETTY_TYPE(PointCloudType, "Naksha", 2) <<
                       " : " << PRETTY_TYPE(Tag, "Naksha", 2) );
    BOOST_CHECK((std::is_same<Tag, EigenPointCloudTag>::value));
    BOOST_CHECK_EQUAL(tagDispatch(Tag()), 0);
  }
  {
    typedef Eigen::MatrixXf PointCloudType;
    typedef EigenPointCloudTraits<3, PointCloudType>::type Tag;
    BOOST_TEST_MESSAGE("Tag of " << PRETTY_TYPE(PointCloudType, "Naksha", 2) <<
                       " : " << PRETTY_TYPE(Tag, "Naksha", 2) );
    BOOST_CHECK((std::is_same<Tag, EigenPointCloudTag>::value));
    BOOST_CHECK_EQUAL(tagDispatch(Tag()), 0);
  }

}
