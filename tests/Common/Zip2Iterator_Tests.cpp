/**
 * @file Zip2Iterator_Tests.cpp
 * @brief Zip2Iterator_Tests
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Common/Zip2Iterator.h"
#include <boost/test/unit_test.hpp>

#include <boost/iterator/zip_iterator.hpp>

using namespace Naksha;

BOOST_AUTO_TEST_CASE(Simple) {
  typedef std::vector<int> KeyContainer;
  typedef std::vector<double> ValueContainer;

  typedef Zip2Iterator<KeyContainer::iterator, ValueContainer::iterator> MyZipIterator;

  KeyContainer key_container;
  ValueContainer value_container;

  for (int i = 0; i< 100; ++i) {
    key_container.push_back(i);
    value_container.push_back(i / 2.0);
  }

  const MyZipIterator zip_iterator_begin(key_container.begin(), value_container.begin());
  const MyZipIterator zip_iterator_end(key_container.end(), value_container.end());

  KeyContainer::iterator key_it = key_container.begin();
  ValueContainer::iterator value_it = value_container.begin();

  for (MyZipIterator zip_it = zip_iterator_begin; zip_it != zip_iterator_end ; ++zip_it ) {
    BOOST_TEST_MESSAGE("Key = " << zip_it.key() << " Value = " << *zip_it );
    BOOST_CHECK_EQUAL(*value_it, *zip_it );
    BOOST_CHECK_EQUAL(*key_it, zip_it.key() );
    BOOST_CHECK(value_it == zip_it.valueIterator() );
    BOOST_CHECK(key_it == zip_it.keyIterator() );
    ++key_it;
    ++value_it;
  }

  MyZipIterator zip_it = zip_iterator_begin + 5;
  BOOST_CHECK(zip_it.valueIterator() == value_container.begin() + 5 );
  BOOST_CHECK(zip_it.keyIterator() == key_container.begin() + 5 );
  BOOST_CHECK_EQUAL(*zip_it , *(value_container.begin() + 5) );
}

BOOST_AUTO_TEST_CASE(Constructor_Interoperability) {
  typedef std::vector<int> KeyContainer;
  typedef std::vector<double> ValueContainer;

  typedef Zip2Iterator<KeyContainer::iterator, ValueContainer::iterator> MyZipIterator;
  typedef Zip2Iterator<KeyContainer::const_iterator, ValueContainer::const_iterator> MyZipConstIterator;

  KeyContainer key_container;
  ValueContainer value_container;

  {
    MyZipIterator zip_iterator_begin(key_container.begin(), value_container.begin());
    MyZipConstIterator zip_const_iterator(zip_iterator_begin);
  }

//  {
//    MyZipConstIterator zip_const_iterator(key_container.cbegin(), value_container.cbegin());
//    MyZipIterator zip_iterator(zip_const_iterator);
//  }

  {
    MyZipIterator zip_iterator_begin(key_container.begin(), value_container.begin());
    MyZipConstIterator zip_const_iterator(key_container.cbegin(), value_container.cbegin());
  }
  {
    MyZipConstIterator zip_const_iterator(key_container.cend(), value_container.cend());
  }
  {
    MyZipConstIterator zip_const_iterator(key_container.begin(), value_container.begin());
  }

  {
    MyZipConstIterator zip_const_iterator(key_container.end(), value_container.end());
  }
}

BOOST_AUTO_TEST_CASE(Comparison_Interoperability) {
  typedef std::vector<int> KeyContainer;
  typedef std::vector<double> ValueContainer;

  typedef Zip2Iterator<KeyContainer::iterator, ValueContainer::iterator> MyZipIterator;
  typedef Zip2Iterator<KeyContainer::const_iterator, ValueContainer::const_iterator> MyZipConstIterator;

  KeyContainer key_container;
  ValueContainer value_container;

  {
    MyZipIterator zip_iterator_begin(key_container.begin(), value_container.begin());
    MyZipConstIterator zip_const_iterator(zip_iterator_begin);
    BOOST_CHECK(zip_iterator_begin == zip_const_iterator );
    BOOST_CHECK(zip_const_iterator ==  zip_iterator_begin);
  }
}
