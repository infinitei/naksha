/**
 * @file ProbabilityUtils_Tests.cpp
 * @brief ProbabilityUtils Unit Tests
 *
 * @author Abhijit Kundu
 */


#include "Naksha/Common/ProbabilityUtils.h"
#include "Naksha/Common/BoostUnitTestHelper.h"
#include <boost/test/unit_test.hpp>
#include <iostream>

using namespace Naksha;
using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(ProbLogOddsConversion) {
  const float tolerancePercent = 0.00001f;
  BOOST_CHECK_CLOSE(logOddsFromProb(0.5), 0, tolerancePercent);
  BOOST_CHECK_CLOSE(probFromLogOdds(0.0), 0.5, tolerancePercent);
  BOOST_CHECK_CLOSE(logOddsFromProb(0.5f), 0.0f, tolerancePercent);
  BOOST_CHECK_CLOSE(probFromLogOdds(0.0f), 0.5f, tolerancePercent);
}

BOOST_AUTO_TEST_CASE(logSumExpTest) {
  Matrix<double, 30, 1> data;
  data.setRandom();
  double actual = logSumExp(data);
  double expected = std::log(data.array().exp().sum());
  BOOST_CHECK_CLOSE( actual, expected, 0.0001 );
}

BOOST_AUTO_TEST_CASE(convertTest1_EigenTypes) {
  Vector2f prob2f(0.5,0.5);
  BOOST_CHECK_EQUAL(convertRandomVariable(prob2f, ProbabilityTag(), ProbabilityTag()), prob2f);
  BOOST_CHECK_EQUAL(convertRandomVariable(prob2f, LogProbabilityTag(), LogProbabilityTag()), prob2f);
  BOOST_CHECK_EQUAL(convertRandomVariable(prob2f, LogOddsTag(), LogOddsTag()), prob2f);
}

BOOST_AUTO_TEST_CASE(convertTest1_ScalarTypes) {
  float prob = 0.5;
  BOOST_CHECK_EQUAL(convertRandomVariable(prob, ProbabilityTag(), ProbabilityTag()), prob);
  BOOST_CHECK_EQUAL(convertRandomVariable(prob, LogProbabilityTag(), LogProbabilityTag()), prob);
  BOOST_CHECK_EQUAL(convertRandomVariable(prob, LogOddsTag(), LogOddsTag()), prob);
}

BOOST_AUTO_TEST_CASE(convertTest2_EigenTypes) {
  typedef Vector2f DataType;
  const DataType prob(0.5,0.5);

  DataType log_prob = convertRandomVariable(prob, LogProbabilityTag(), ProbabilityTag());
  DataType log_odds_prob = convertRandomVariable(prob, LogOddsTag(), ProbabilityTag());

  const float tolerancePercent = 0.001f;
  BOOST_CHECK_EQUAL(log_odds_prob, Vector2f (0.0,0.0));
  CHECK_CLOSE_EIGEN_MATRIX(log_prob, Vector2f (-0.693147,-0.693147), tolerancePercent);
}

BOOST_AUTO_TEST_CASE(convertTest2_ScalarTypes) {
  typedef double DataType;
  const DataType prob(0.5);

  DataType log_prob = convertRandomVariable(prob, LogProbabilityTag(), ProbabilityTag());
  DataType log_odds_prob = convertRandomVariable(prob, LogOddsTag(), ProbabilityTag());

  const float tolerancePercent = 0.001f;
  BOOST_CHECK_EQUAL(log_odds_prob, 0.0);
  BOOST_CHECK_CLOSE(log_prob, -0.693147, tolerancePercent);
}

BOOST_AUTO_TEST_CASE(convertTest3) {
  typedef Vector2f DataType;
  const DataType log_odds_prob(0.0,0.0);

  DataType  prob = convertRandomVariable(log_odds_prob, ProbabilityTag(), LogOddsTag());
  DataType log_prob = convertRandomVariable(log_odds_prob, LogProbabilityTag(), LogOddsTag());

  const float tolerancePercent = 0.001f;
  BOOST_CHECK_EQUAL(prob, Vector2f (0.5f,0.5f));
  CHECK_CLOSE_EIGEN_MATRIX(log_prob, Vector2f (-0.693147f,-0.693147f), tolerancePercent);
}

BOOST_AUTO_TEST_CASE(convertTest4) {
  typedef Vector3f DataType;
  const DataType prob(0.5f, 0.3f, 0.2f);

  DataType log_prob = convertRandomVariable(prob, LogProbabilityTag(), ProbabilityTag());
  DataType log_odds_prob = convertRandomVariable(prob, LogOddsTag(), ProbabilityTag());

  const float tolerancePercent = 0.001f;
  CHECK_CLOSE_EIGEN_MATRIX(log_prob, Vector3f(-0.69314718056f,-1.20397280433f, -1.60943791243f), tolerancePercent);
  CHECK_CLOSE_EIGEN_MATRIX(log_odds_prob, Vector3f(logOddsFromProb(0.5f),logOddsFromProb(0.3f), logOddsFromProb(0.2f)), tolerancePercent);

}

BOOST_AUTO_TEST_CASE(modeTest_ScalarTypes) {
  {
    float prob = 0.3f;
    BOOST_CHECK_EQUAL(modeOfRandomVariable(prob, ProbabilityTag()), 0);
    prob = 0.7f;
    BOOST_CHECK_EQUAL(modeOfRandomVariable(prob, ProbabilityTag()), 1);
  }

  {
    float log_prob = std::log(0.3f);
    BOOST_CHECK_EQUAL(modeOfRandomVariable(log_prob, LogProbabilityTag()), 0);
    log_prob = std::log(0.7f);
    BOOST_CHECK_EQUAL(modeOfRandomVariable(log_prob, LogProbabilityTag()), 1);
  }

  {
    float log_odds = logOddsFromProb(0.3f);
    BOOST_CHECK_EQUAL(modeOfRandomVariable(log_odds, LogOddsTag()), 0);
    log_odds = logOddsFromProb(0.7f);
    BOOST_CHECK_EQUAL(modeOfRandomVariable(log_odds, LogOddsTag()), 1);
  }
}

BOOST_AUTO_TEST_CASE(modeTest_EigenTypes) {
  typedef Vector3d DataType;
  DataType data(0.5, 0.25, 0.25);
  BOOST_CHECK_EQUAL(modeOfRandomVariable(data, ProbabilityTag()), 0);

  data = DataType(0.25, 0.5, 0.25);
  BOOST_CHECK_EQUAL(modeOfRandomVariable(data, ProbabilityTag()), 1);

  data = DataType(std::log(0.25), std::log(0.25), std::log(0.5));
  BOOST_CHECK_EQUAL(modeOfRandomVariable(data, LogProbabilityTag()), 2);
}
