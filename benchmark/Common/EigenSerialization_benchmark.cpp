/**
 * @file EigenSerialization_benchmark.cpp
 * @brief Eigen Serialization benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Common/EigenBoostSerialization.h"
#include "Naksha/Common/EigenTypedefs.h"
#include "Naksha/Common/SerializationHelper.h"
#include <boost/serialization/vector.hpp>

#include <boost/progress.hpp>

using namespace Eigen;
using namespace std;
using namespace Naksha;


int main(int argc, char **argv) {

  const size_t num_cols = 20000000;
  cout << "num_cols: " << num_cols << endl;
  const std::string filename = "matrix.tmp";

  {
    EigenAligned<Vector3d>::std_vector m(num_cols, Vector3d::Random());
    cout << "EigenAligned<Vector3d>::std_vector:" << endl;
    boost::progress_timer t;
    serializeBinary(m, filename);
  }

  {
    MatrixXd m(3, num_cols);
    m.setRandom();
    cout << "m(3, num_cols):" << endl;
    boost::progress_timer t;
    serializeBinary(m, filename);
  }

  {
    MatrixXd m(3, 2*num_cols);
    m.setRandom();
    cout << "m(3, 2*num_cols): m.leftCols<num_cols>():" << endl;
    boost::progress_timer t;
    serializeBinary(m.leftCols<num_cols>(), filename);
  }

  return std::remove(filename.c_str());
}
