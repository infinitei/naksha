/**
 * @file ProbabilityUtils_benchmark.cpp
 * @brief ProbabilityUtils_benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Common/ProbabilityUtils.h"

#include <boost/progress.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;

int main(int argc, char **argv) {

  typedef Matrix <double, 9, 1> Vector9d;
  const size_t num_iterations= 1000000000;
  const Vector9d value_original(Vector9d::Random());
  const Vector9d prob_original(Vector9d::Random());

  {
    Vector9d value = value_original;
    cout << "normalizeRandomVariable:" << endl;
    boost::progress_timer t;
    for (size_t i= 0; i != num_iterations; ++i) {
      normalizeRandomVariable(value.head<6>(), ProbabilityTag());
    }
    cout << "value:= " << value.transpose() << endl;
  }


  {
    Vector9d value = value_original;
    Vector9d prob = prob_original;
    cout << "multiplyRandomVariable:" << endl;
    boost::progress_timer t;
    for (size_t i= 0; i != num_iterations; ++i) {
      multiplyRandomVariable(value, prob, ProbabilityTag());
    }
    cout << "value:= " << value.transpose() << endl;
  }

  {
    Vector9d value = value_original;
    cout << "convertRandomVariable:" << endl;
    boost::progress_timer t;
    for (size_t i= 0; i != num_iterations; ++i) {
      value = convertRandomVariable(value, ProbabilityTag(), LogOddsTag());
    }
    cout << "value:= " << value.transpose() << endl;
  }

  {
    Vector9d value = value_original;
    cout << "convertRandomVariable:" << endl;
    boost::progress_timer t;
    for (size_t i= 0; i != num_iterations; ++i) {
      value.head<3>() = convertRandomVariable(value.head<3>(), ProbabilityTag(), LogProbabilityTag());
    }
    cout << "value:= " << value.transpose() << endl;
  }

  {
    Vector9d value = value_original;
    cout << "convertRandomVariable:" << endl;
    boost::progress_timer t;
    for (size_t i= 0; i != num_iterations; ++i) {
      convertRandomVariableInPlace(value.head<3>(), ProbabilityTag(), LogProbabilityTag());
    }
    cout << "value:= " << value.transpose() << endl;
  }



}
