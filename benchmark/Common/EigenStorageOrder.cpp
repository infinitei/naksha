/**
 * @file EigenStorageOrder.cpp
 * @brief Benchmarks different storage order for Eigen matrices
 * 
 * @author Abhijit Kundu
 */

#include "Naksha/Common/EigenTypedefs.h"
#include "Naksha/Common/BuildConfig.h"

#include <boost/timer/timer.hpp>
#include <boost/format.hpp>


int main(int argc, char **argv) {
  using namespace Eigen;
  using std::cout;
  using std::flush;
  using boost::format;

  cout << "\n# Eigen Storage Order benchmarks\n\n";
  Naksha::Build::printBuildConfig();

  const int num_points = 1000000;

  typedef boost::timer::auto_cpu_timer TimerType;
  typedef double RawDataType;
  typedef EigenPointCloudT<RawDataType>::type EigenPointCloud;
  typedef EigenSE3<RawDataType>::type Pose3;
  EigenPointCloud point_cloud (3,num_points);
  point_cloud.setRandom();

  const Pose3 transform(AngleAxis<double>(-0.3*M_PI, Vector3d::UnitX()));

  const int num_of_trials = 100;

  cout << "\n";
  cout << "Affine Transformation  | Time                                         \n";
  cout << "---------------------- | ---------------------------------------------\n";

  // Method 1
  {
    EigenPointCloud pc1 = point_cloud;
    cout << format("%-23s|") % "Method 1 (RM)" << flush;
    TimerType t;
    for (int nt = 0; nt < num_of_trials; ++nt) {
      pc1 = transform.linear() * pc1;
      pc1.colwise() += transform.translation();
    }
  }

  // Method 2
  {
    Eigen::Matrix<double, 3, Dynamic, ColMajor> pc2;
    pc2 = point_cloud;
    cout << format("%-23s|") % "Method 2 (CM)" << flush;
    TimerType t;
    for (int nt = 0; nt < num_of_trials; ++nt) {
      pc2.noalias() = transform * pc2;
    }
  }

  // Method 3
  {
    EigenPointCloud pc3 = point_cloud;
    cout << format("%-23s|") % "Method 3 (RM)" << flush;
    TimerType t;
    for (int nt = 0; nt < num_of_trials; ++nt) {
      pc3.noalias() = transform * pc3;
    }
  }

  // Method 4
  {
    Eigen::Matrix<double, 3, Dynamic, ColMajor> pc4 = point_cloud;
    cout << format("%-23s|") % "Method 4 (CM)" << flush;
    TimerType t;
    for (int nt = 0; nt < num_of_trials; ++nt) {
      pc4 = transform * pc4;
    }
  }

  // Method 5
  {
    EigenPointCloud pc5 = point_cloud;
    cout << format("%-23s|") % "Method 5 (RM)" << flush;
    TimerType t;
    for (int nt = 0; nt < num_of_trials; ++nt) {
      pc5 = transform * pc5;
    }
  }


  // Method 4
  {
    Eigen::Matrix<double, 3, Dynamic, ColMajor> pc4 = point_cloud;
    cout << format("%-23s|") % "Method 6 (CM)" << flush;
    TimerType t;
    for (int nt = 0; nt < num_of_trials; ++nt) {
      pc4 = transform * pc4;
    }
  }

  cout << "\n";
  cout << "Colwise Normalization  | Time                                         \n";
  cout << "---------------------- | ---------------------------------------------\n";


  {
    EigenPointCloud pc = point_cloud;
    cout << format("%-23s|") % "Normalize (RM)" << flush;
    TimerType t;
    for (int nt = 0; nt < num_of_trials; ++nt) {
      pc.colwise().normalize();
    }
  }

  {
    Eigen::Matrix<double, 3, Dynamic, ColMajor> pc = point_cloud;
    cout << format("%-23s|") % "Normalize (CM)" << flush;
    TimerType t;
    for (int nt = 0; nt < num_of_trials; ++nt) {
      pc.colwise().normalize();
    }
  }
  return EXIT_SUCCESS;
}



