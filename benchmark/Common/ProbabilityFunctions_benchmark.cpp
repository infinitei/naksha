/**
 * @file ProbabilityFunctions_benchmark.cpp
 * @brief ProbabilityFunctions_benchmark
 *
 * @author Abhijit Kundu
 */

#define EIGEN_USE_THREADS

#include "Naksha/Common/ProbabilityFunctions.h"
#include "Naksha/Common/MakeAlignedContainer.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Common/BuildConfig.h"

#include <vector>
#include <iostream>

#include <boost/timer/timer.hpp>
#include <boost/format.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/range/numeric.hpp> // accumulate
#include <boost/range/adaptor/transformed.hpp> // transformed

typedef boost::timer::auto_cpu_timer TimerType;

double benchmarkLogitFunction(const std::vector<double>& probs) {
  std::cout << boost::format("%-32s|") % "Naksha::logit(Scalar): " << std::flush;
  double sum = 0;
  TimerType t;
  for(const double p : probs) {
    sum += Naksha::logit(p);
  }
  return sum;
}

template <class Functor>
double benchmarkFunctorOnContainer(const std::vector<double>& probs, Functor f) {
  std::cout << boost::format("%-32s|") % PRETTY_TYPE_INFO(Functor) << std::flush;
  TimerType t;
  double sum = boost::accumulate(probs | boost::adaptors::transformed(f), 0.0);
  return sum;
}

template <class Container, class Functor>
double benchmarkFunctorOnEigenContainer(const Container& probs, Functor f) {
  std::cout << boost::format("%-32s|") % PRETTY_TYPE_INFO(Functor) << std::flush;
  double sum = 0;
  TimerType t;
  for(const typename Container::value_type& p : probs) {
    sum += f(p).sum();
  }
  return sum;
}

template <class Container, class Functor>
double benchmarkFunctorOnEigenType(const Container& probs, Functor f) {
  std::cout << boost::format("%-32s|") % PRETTY_TYPE_INFO(Functor) << std::flush;
  TimerType t;
  double sum = f(probs).sum();
  return sum;
}


struct LogitA {
  template<typename Scalar>
  Scalar operator()(const Scalar& p) const {
    return std::log(p / (1. -p));
  }
};

struct LogitB {
  template<typename Scalar>
  Scalar operator()(const Scalar& p) const {
    return - std::log((1. / p ) - 1.);
  }
};


struct EigenLogitA {
  template<class Derived>
  typename Derived::PlainObject operator()(const Eigen::MatrixBase<Derived>& p) const {
    return p.unaryExpr(LogitA());
  }
};

struct EigenLogitB {
  template<class Derived>
  typename Derived::PlainObject operator()(const Eigen::MatrixBase<Derived>& p) const {
    return -(p.array().inverse() - 1.).log();
  }
};

struct EigenLogitC {
  template<class Derived>
  typename Derived::PlainObject operator()(const Eigen::MatrixBase<Derived>& p) const {
    return (p.array() / (1. - p.array())).log();
  }
};



struct SigmoidA {
  template<typename Scalar>
  Scalar operator()(const Scalar& x) const {
    return 1. / (1. + std::exp(-x));
  }
};

struct SigmoidB {
  template<typename Scalar>
  Scalar operator()(const Scalar& x) const {
    return std::exp(x) / (1. + std::exp(x));
  }
};

struct SigmoidC {
  template<typename Scalar>
  Scalar operator()(const Scalar& x) const {
    const Scalar exp = std::exp(x);
    return exp / (1. + exp);
  }
};

struct EigenSigmoidA {
  template<class Derived>
  typename Derived::PlainObject operator()(const Eigen::MatrixBase<Derived>& x) const {
    return (1. + (-x).array().exp()).inverse();
  }
};

struct EigenSigmoidAA {
  template<class Derived>
  typename Derived::PlainObject operator()(const Eigen::ArrayBase<Derived>& x) const {
    using namespace std;
    return (1. + exp(-x)).inverse();
  }
};

struct EigenSigmoidB {
  template<class Derived>
  typename Derived::PlainObject operator()(const Eigen::MatrixBase<Derived>& x) const {
    typename Derived::PlainObject exp = x.array().exp();
    return exp.array() / (1. + exp.array());
  }
};


template <typename T>
struct CheckFloatingPoint {
  CheckFloatingPoint(const T expected_acc)
      : expected_(expected_acc) {
  }
  void operator()(const T x, const T precision = 1e-8) const {
    if (! (std::abs(x - expected_) < precision)) {
      std::cout << "Error! Output is not sane. "
          << boost::format("%1$.16f") % x << " != " << boost::format("%1$.16f") % expected_ << " (expected)" << "\n";
    }
  }
  const T expected_;
};

int main(int argc, char **argv) {
  using namespace Naksha;
  using std::cout;
  using std::endl;
  using std::flush;

  cout << "\n# Benchmark of Probability Functions Implementation\n\n";

  Build::printBuildConfig();

  cout << "\n";

//  {
//    std::cout << "Logit(Scalar)                   | Time                                  \n";
//    std::cout << "------------------------------- | --------------------------------------\n";
//
//    std::vector<double> probs;
//    {
//      boost::random::mt19937 eng = boost::random::mt19937();
//      boost::random::uniform_real_distribution<double> urd(0, 1);
//      for (std::size_t i = 0; i < 100000000; ++i)
//        probs.push_back(urd(eng));
//    }
//    CheckFloatingPoint<double> check_logit_sum(-4557.8468213192427356);
//
//    check_logit_sum(benchmarkLogitFunction(probs));
//    check_logit_sum(benchmarkFunctorOnContainer(probs, Naksha::Logit()));
//    check_logit_sum(benchmarkFunctorOnContainer(probs, LogitA()));
//    check_logit_sum(benchmarkFunctorOnContainer(probs, LogitB()));
//  }
//
//  cout << "\n";
//
//  {
//    std::cout << "Logit(Vector4d)                 | Time                                  \n";
//    std::cout << "------------------------------- | --------------------------------------\n";
//
//    typedef MakeStdVector<Eigen::Vector4d>::type EigenStdVector;
//    EigenStdVector probs;
//    {
//      boost::random::mt19937 eng = boost::random::mt19937();
//      boost::random::uniform_real_distribution<double> urd(0, 1);
//      for (std::size_t i = 0; i < 25000000; ++i)
//        probs.push_back(Eigen::Vector4d(urd(eng), urd(eng), urd(eng), urd(eng)));
//    }
//    CheckFloatingPoint<double> check_logit_sum(-4557.8468213192427356);
//
//    check_logit_sum(benchmarkFunctorOnEigenContainer(probs, EigenLogitA()));
//    check_logit_sum(benchmarkFunctorOnEigenContainer(probs, EigenLogitB()));
//    check_logit_sum(benchmarkFunctorOnEigenContainer(probs, EigenLogitC()));
//  }
//
//  cout << "\n";
//
//  {
//    std::cout << "Logit(Eigen::MatrixXd)          | Time                                  \n";
//    std::cout << "------------------------------- | --------------------------------------\n";
//
//    Eigen::MatrixXd probs(4, 25000000);
//    {
//      boost::random::mt19937 eng = boost::random::mt19937();
//      boost::random::uniform_real_distribution<double> urd(0, 1);
//      for (std::size_t i = 0; i < 25000000; ++i)
//        probs.col(i) = Eigen::Vector4d(urd(eng), urd(eng), urd(eng), urd(eng));
//    }
//    CheckFloatingPoint<double> check_logit_sum(-4557.8468213192427356);
//
//    check_logit_sum(benchmarkFunctorOnEigenType(probs, EigenLogitA()));
//    check_logit_sum(benchmarkFunctorOnEigenType(probs, EigenLogitB()));
//    check_logit_sum(benchmarkFunctorOnEigenType(probs, EigenLogitC()));
//  }

  cout << "\n";

  {
    std::cout << "Sigmoid(Scalar)                 | Time                                  \n";
    std::cout << "------------------------------- | --------------------------------------\n";

    std::vector<double> logodds;
    {
      boost::random::mt19937 eng = boost::random::mt19937();
      boost::random::uniform_real_distribution<double> urd(0, 1);
      for (std::size_t i = 0; i < 100000000; ++i)
        logodds.push_back(Naksha::logit(urd(eng)));
    }
    CheckFloatingPoint<double> check_logit_sum(49999807.9772);

    check_logit_sum(benchmarkFunctorOnContainer(logodds, Naksha::Sigmoid()), 1e-4);
    check_logit_sum(benchmarkFunctorOnContainer(logodds, SigmoidA()), 1e-3);
    check_logit_sum(benchmarkFunctorOnContainer(logodds, SigmoidB()), 1e-3);
    check_logit_sum(benchmarkFunctorOnContainer(logodds, SigmoidC()), 1e-3);
  }

  cout << "\n";

  {
    std::cout << "Sigmoid(Vector4d)               | Time                                  \n";
    std::cout << "------------------------------- | --------------------------------------\n";

    typedef MakeStdVector<Eigen::Vector4d>::type EigenStdVector;
    EigenStdVector logodds;
    {
      boost::random::mt19937 eng = boost::random::mt19937();
      boost::random::uniform_real_distribution<double> urd(0, 1);
      for (std::size_t i = 0; i < 25000000; ++i)
        logodds.push_back(Eigen::Vector4d(Naksha::logit(urd(eng)), Naksha::logit(urd(eng)), Naksha::logit(urd(eng)), Naksha::logit(urd(eng))));
    }
    CheckFloatingPoint<double> check_logit_sum(49999807.9772);

    check_logit_sum(benchmarkFunctorOnEigenContainer(logodds, EigenSigmoidA()), 1e-3);
    check_logit_sum(benchmarkFunctorOnEigenContainer(logodds, EigenSigmoidB()), 1e-3);
//    check_logit_sum(benchmarkFunctorOnEigenContainer(logodds, SigmoidC()), 1e-3);
  }

  cout << "\n";

  {
    std::cout << "Sigmoid(Array4d)                | Time                                  \n";
    std::cout << "------------------------------- | --------------------------------------\n";

    typedef MakeStdVector<Eigen::Array4d>::type EigenStdVector;
    EigenStdVector logodds;
    {
      boost::random::mt19937 eng = boost::random::mt19937();
      boost::random::uniform_real_distribution<double> urd(0, 1);
      for (std::size_t i = 0; i < 25000000; ++i)
        logodds.push_back(Eigen::Array4d(Naksha::logit(urd(eng)), Naksha::logit(urd(eng)), Naksha::logit(urd(eng)), Naksha::logit(urd(eng))));
    }
    CheckFloatingPoint<double> check_logit_sum(49999807.9772);

    check_logit_sum(benchmarkFunctorOnEigenContainer(logodds, EigenSigmoidAA()), 1e-3);
  }

  cout << "\n";

  {
    std::cout << "Sigmoid(ArrayXd)                | Time                                  \n";
    std::cout << "------------------------------- | --------------------------------------\n";

    Eigen::ArrayXd log_array(100000000);
    {
      boost::random::mt19937 eng = boost::random::mt19937();
      boost::random::uniform_real_distribution<double> urd(0, 1);
      for (Eigen::DenseIndex i = 0; i < 100000000; ++i)
        log_array[i] = Naksha::logit(urd(eng));
    }
    CheckFloatingPoint<double> check_logit_sum(49999807.9772);

    check_logit_sum(benchmarkFunctorOnEigenType(log_array, EigenSigmoidAA()), 1e-3);

    Eigen::MatrixXd log_mat = log_array.matrix();
    check_logit_sum(benchmarkFunctorOnEigenType(log_mat, EigenSigmoidA()), 1e-3);

    {
      std::cout << boost::format("%-32s|") % "sigmoid(log_array)" << std::flush;
      TimerType t;
      check_logit_sum(sigmoid(log_array).sum(), 1e-3);
    }

    {
      std::cout << boost::format("%-32s|") % "sigmoid(log_mat)" << std::flush;
      TimerType t;
      check_logit_sum(sigmoid(log_mat).sum(), 1e-3);
    }
  }

  return EXIT_SUCCESS;
}
