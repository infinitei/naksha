/**
 * @file TreeNode_benchmark.cpp
 * @brief TreeNode_benchmark
 *
 * @author Abhijit Kundu
 */


#include "Naksha/Nodes/NodeTypedefs.h"
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/progress.hpp>

using namespace std;
using namespace Eigen;
using namespace Naksha;

typedef DiscreteRandomVariable<10, float, LogProbabilityTag> RV;
typedef VoxelNode<RV, OccupancyNodeInterface, OctreeNode> NodeType;
typedef boost::ptr_vector<NodeType> NodePtrVector;
typedef std::vector<NodeType> NodeStdVector;

 size_t num_of_calls;

void makeBalancedTree(NodeType *node, const unsigned int level) {
  for (unsigned int i = 0; i < NodeType::MaxNumOfChilds; ++i) {
    node->createChild(i);
  }
  if (level > 0) {
    for (unsigned int i = 0; i < NodeType::MaxNumOfChilds; ++i) {
      makeBalancedTree(node->childPtr(i), level - 1);
    }
  }

  num_of_calls +=8;
}

int main(int argc, char **argv) {


  RV::DataType check(RV::DataType::Ones() * (0.5f / (RV::Dimension - 1)));
  check[0] = 0.5f;

  cout << "Size of each NodeType = " << sizeof(NodeType) << " bytes." << endl;
  cout << "Size of each NodeType::Payload = " << sizeof(NodeType::Payload) << " bytes." << endl;

  const unsigned int depth = 9;
  cout << "Created tree with depth " << depth << endl;
  num_of_calls = 0;
  {
    boost::progress_timer t;
    NodeType *root = new NodeType();
    makeBalancedTree(root, depth - 1);
  }
  const size_t num_of_nodes = num_of_calls + 1;
  cout << "Done. Created " << num_of_nodes << " nodes." << endl;

  size_t size = sizeof(NodeType) * num_of_nodes;
  cout << "Size of Data = " << size << " bytes (" << size / (1024. * 1024.) << " MB)." << endl;


  return EXIT_SUCCESS;
}
