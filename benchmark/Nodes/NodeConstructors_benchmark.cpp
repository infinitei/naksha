/**
 * @file NodeConstructors_benchmark.cpp
 * @brief This profiles Node Constructors
 *
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Nodes/NodeTypedefs.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include <boost/ptr_container/ptr_vector.hpp>


//#include <boost/timer/timer.hpp>
//typedef boost::timer::auto_cpu_timer TimerType;

#include <boost/progress.hpp>
typedef boost::progress_timer TimerType;


template<typename NodeType>
void printNodeInformation(const std::size_t num_of_nodes) {
  std::cout << "NodeType: " << PRETTY_TYPE_INFO_REMOVE_NS(NodeType, "Naksha") << "\n";
  std::cout << "Size of each NodeType = " << sizeof(NodeType) << " bytes.\n";
  std::cout << "Size of each NodeType::Payload = " << sizeof(typename NodeType::Payload) << " bytes.\n";
  std::cout << "Size of each NodeType::RVDatatype = " << sizeof(typename NodeType::Payload::RVType::DataType) << " bytes.\n";
  std::cout << "Number of Nodes = " << num_of_nodes << "\n";
  std::size_t size_in_bytes = sizeof(NodeType) * num_of_nodes;
  std::cout << "Size of whole vector = " << size_in_bytes << " bytes (" << size_in_bytes / (1024. * 1024.) << " MB).\n";
}

template<typename NodeContainer, typename RVDataType>
void checkNodes(const NodeContainer& nodes, const RVDataType& check) {
  for (typename NodeContainer::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
    const RVDataType& probability = it->payload().probability();
    if (!probability.isApprox(check))
      std::cout << "Unexpected: " << probability.transpose() << "\n";
  }
}

template<typename NodeType>
void doBenchMarking(const size_t num_of_nodes, const bool do_checking = true) {
  std::cout << "========================================================================\n";
  std::cout << "========================================================================\n";



  typedef typename NodeType::Payload::RVType RV;
  typedef typename RV::DataType RVDataType;
  typedef std::vector<NodeType> NodeStdVector;
  typedef boost::ptr_vector<NodeType> NodePtrVector;


  RVDataType check(RVDataType::Ones() * (0.5f / (RV::Dimension - 1)));
  check[0] = 0.5f;

  RVDataType init_data(RVDataType::Ones() * (0.5f / (RV::Dimension - 1)));
  init_data[0] = 0.5f;
  init_data = convertRandomVariable(init_data, typename RV::ProbTypeTag(), Naksha::ProbabilityTag());

  printNodeInformation<NodeType>(num_of_nodes);

  std::cout << "=====================Using std::vector container=====================\n";

  {
    NodeStdVector nodes;
    nodes.reserve(num_of_nodes);
    std::cout << "Creating Nodes with Default Constructor (std::vector push_back):" << std::endl;
    {
      TimerType t;
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(NodeType());
      }
    }

    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    std::cout << "Creating Nodes with Default Constructor (std::vector constructor) :" << std::endl;
    {
      TimerType t;
      NodeStdVector nodes(num_of_nodes);
    }

    if(do_checking) {
      NodeStdVector nodes(num_of_nodes);
      checkNodes(nodes, check);
    }
  }

  {
    NodeStdVector nodes;
    nodes.reserve(num_of_nodes);
    std::cout << "Creating Nodes with Init Constructor (std::vector push_back) :" << std::endl;
    {
      TimerType t;
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(NodeType(init_data));
      }
    }

    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    NodeStdVector nodes;
    nodes.reserve(num_of_nodes);
    std::cout << "Creating Nodes from single local instance (std::vector push_back) :" << std::endl;
    {
      TimerType t;
      const NodeType node(init_data);
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(node);
      }
    }

    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    NodeStdVector nodes;
    nodes.reserve(num_of_nodes);
    std::cout << "Creating Nodes from a local static (std::vector push_back) :" << std::endl;
    {
      TimerType t;
      static const NodeType node(init_data);
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(node);
      }
    }

    if(do_checking)
      checkNodes(nodes, check);
  }

  std::cout << "====================Using boost::ptr_vector container====================\n";

  {
    NodePtrVector nodes;
    nodes.reserve(num_of_nodes);
    std::cout << "Creating Nodes with Default Constructor (boost::ptr_vector push_back):" << std::endl;
    {
      TimerType t;
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(new NodeType());
      }
    }

    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    NodePtrVector nodes;
    nodes.reserve(num_of_nodes);
    std::cout << "Creating Nodes with Init Constructor (boost::ptr_vector push_back):" << std::endl;
    {
      TimerType t;
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(new NodeType(init_data));
      }
    }

    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    NodePtrVector nodes;
    nodes.reserve(num_of_nodes);
    std::cout << "Creating Nodes with Copy Constructor (instance) (boost::ptr_vector push_back):" << std::endl;
    {
      TimerType t;
      const NodeType node(init_data);
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(new NodeType(node));
      }
    }

    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    NodePtrVector nodes;
    nodes.reserve(num_of_nodes);
    std::cout << "Creating Nodes with Copy Constructor (static) (boost::ptr_vector push_back):" << std::endl;
    {
      TimerType t;
      static const NodeType node(init_data);
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(new NodeType(node));
      }
    }

    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    std::cout << "Creating Nodes with boost::ptr_vector Resize :" << std::endl;
    NodePtrVector nodes;
    {
      TimerType t;
      nodes.resize(num_of_nodes);
    }

    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    std::cout << "Creating Nodes with boost::ptr_vector constructor :"
        << std::endl;
    {
      TimerType t;
      NodePtrVector nodes(num_of_nodes);
    }

    if(do_checking){
      NodePtrVector nodes(num_of_nodes);
      checkNodes(nodes, check);
    }
  }

}


using namespace std;
using namespace Eigen;
using namespace Naksha;

int main(int argc, char **argv) {

  const size_t num_of_nodes = 100000000;

  {
    typedef DiscreteRandomVariable<9, double, LogProbabilityTag> RV;
    typedef VoxelNode<RV, OccupancyNodeInterface, NoChildPolicy> NodeType;
    doBenchMarking<NodeType>(num_of_nodes,  false);
  }

  {
    typedef DiscreteRandomVariable<9, double, ProbabilityTag> RV;
    typedef VoxelNode<RV, RVOccupancyNodeInterface, NoChildPolicy> NodeType;
    doBenchMarking<NodeType>(num_of_nodes, false);
  }

  {
    typedef DiscreteRandomVariable<9, double, LogProbabilityTag> RV;
    typedef Naksha::ColoredRandomVariable<RV>::type Payload;
    typedef VoxelNode<Payload, RVOccupancyNodeInterface, NoChildPolicy> NodeType;
    doBenchMarking<NodeType>(num_of_nodes, false);
  }

//  {
//    typedef DiscreteRandomVariable<9, double, ProbabilityTag> RV;
//    typedef Naksha::ColoredRandomVariable<RV>::type Payload;
//    typedef VoxelNode<Payload, RVOccupancyNodeInterface, OctreeNode> NodeType;
//    doBenchMarking<NodeType>(num_of_nodes);
//  }


  return EXIT_SUCCESS;
}

