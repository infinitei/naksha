/**
 * @file NodeUtils_benchmark.cpp
 * @brief NodeUtils_benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Nodes/NodeTypedefs.h"
#include "Naksha/Nodes/NodeUtils.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/LabelToMeasurementMapping.h"

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/progress.hpp>

#include <boost/range/numeric.hpp> // accumulate
#include <boost/range/adaptor/transformed.hpp> // transformed

#include <vector>
#include <algorithm>

using namespace Naksha;
using namespace Eigen;
using namespace std;


int main(int argc, char **argv) {

  typedef DiscreteRandomVariable<9, double, LogProbabilityTag> RV;
  typedef ColoredRandomVariable<RV>::type Payload;
  typedef MeasurementModel<RV> MeasurementModelType;
  typedef MeasurementMap<RV> RVMeasurementMap;
  typedef VoxelNode<Payload, RVOccupancyNodeInterface, OctreeNode> NodeType;


  cout << "Creating Nodes... " << flush;

  typedef std::vector<NodeType> NodeContainer;
  NodeContainer nodes(80000000);

  boost::random::mt19937 gen;
  boost::random::uniform_int_distribution<> dist(1, 6);
  for (size_t i = 0; i < nodes.size(); ++i) {
    switch(dist(gen)) {
    case 0:
      nodes[i].insertMeasurement(MeasurementModelType::HIT);
      break;
    case 1:
      nodes[i].insertMeasurement(RVMeasurementMap::at<ImageLabels::ROAD>());
      break;
    case 2:
      nodes[i].insertMeasurement(RVMeasurementMap::at<ImageLabels::SIDEWALK>());
      break;
    case 3:
      nodes[i].insertMeasurement(RVMeasurementMap::at<ImageLabels::BUILDING>());
      break;
    case 4:
      nodes[i].insertMeasurement(RVMeasurementMap::at<ImageLabels::TREE>());
      break;
    case 5:
      nodes[i].insertMeasurement(RVMeasurementMap::at<ImageLabels::FENCE>());
      break;
    default:
      nodes[i].insertMeasurement(MeasurementModelType::MISS);
      break;
    }
  }
  cout << " Done." << endl;

  cout << "Starting Benchmarks ..." << endl;


  {
    size_t count = 0;
    cout << "IsNodeOccupied::Standard Loop: " << flush;
    {
      boost::progress_timer t;
      IsNodeOccupied<NodeType> occ_checker;

      for (NodeContainer::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
        if (occ_checker(*it))
          ++count;
      }
    }
    cout << "Count = " << count << "\n";
  }
  cout << "------------------------------------\n";


  {
    size_t count = 0;
    cout << "IsNodeOccupied::STL Loop: " << flush;
    {
      boost::progress_timer t;
      count = std::count_if(nodes.begin(), nodes.end(), IsNodeOccupied<NodeType>());
    }
    cout << "Count = " << count << "\n";
  }
  cout << "------------------------------------\n";
  cout << "------------------------------------\n";


  {
    size_t count = 0;
    cout << "modeOfNode::Standard Loop: " << flush;
    {
      boost::progress_timer t;
      for (NodeContainer::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
        count += modeOfNode(*it);
      }
    }
    cout << "Count = " << count << "\n";
  }
  cout << "------------------------------------\n";

  {
    size_t count = 0;
    cout << "ModeOfNode::Standard Loop with Functor: " << flush;
    {
      boost::progress_timer t;
      ModeOfNode<NodeType> node_mode;
      for (NodeContainer::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
        count += node_mode(*it);
      }
    }
    cout << "Count = " << count << "\n";
  }
  cout << "------------------------------------\n";

  {
    size_t count = 0;
    cout << "ModeOfNode::STL Loop with Functor: " << flush;
    {
      boost::progress_timer t;
      ModeOfNode<NodeType> node_mode;
      count = boost::accumulate( nodes | boost::adaptors::transformed(node_mode), 0);
    }
    cout << "Count = " << count << "\n";
  }
  cout << "------------------------------------\n";
  cout << "------------------------------------\n";

  {
    size_t count = 0;
    cout << "modeOfNodeOccChecked::Standard Loop with Specialized Functor: " << flush;
    {
      boost::progress_timer t;
      ModeOfNodeOccChecked<NodeType> node_mode_occ_cheked;
      for (NodeContainer::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
        count += node_mode_occ_cheked(*it);
      }
    }
    cout << "Count = " << count << "\n";
  }
  cout << "------------------------------------\n";

  {
    size_t count = 0;
    cout << "modeOfNodeOccChecked::Standard Loop with Generic Functor: " << flush;
    {
      boost::progress_timer t;
      for (NodeContainer::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
        ModeOfNode<NodeType, true> node_mode_occ_cheked;
        count += node_mode_occ_cheked(*it);
      }
    }
    cout << "Count = " << count << "\n";
  }
  cout << "------------------------------------\n";

  {
    size_t count = 0;
    cout << "modeOfNodeOccChecked::STL Loop with Generic Functor: " << flush;
    {
      boost::progress_timer t;
      ModeOfNode<NodeType, true> node_mode_occ_cheked;
      count = boost::accumulate( nodes | boost::adaptors::transformed(node_mode_occ_cheked), 0);
    }
    cout << "Count = " << count << "\n";
  }
  cout << "------------------------------------\n";





}
