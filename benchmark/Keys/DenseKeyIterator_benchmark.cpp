/**
 * @file DenseKeyIterator_benchmark.cpp
 * @brief DenseKey Iterator Benchmark
 * 
 * @author Abhijit Kundu
 */

#include "Naksha/Keys/DenseKeyIterator.h"
#include "Naksha/Maps/MapMaker.h"

#include <boost/progress.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;

int main(int argc, char **argv) {

  Vector3f hit_sensor_model(0.3f, 0.35f, 0.35f);
  Vector3f miss_sensor_model(0.7f, 0.15f, 0.15f);

  hit_sensor_model = hit_sensor_model.cwiseQuotient(Vector3f(0.5, 0.25, 0.25));
  miss_sensor_model = miss_sensor_model.cwiseQuotient(Vector3f(0.5, 0.25, 0.25));
  hit_sensor_model = hit_sensor_model.array().log();
  miss_sensor_model = miss_sensor_model.array().log();

  typedef DiscreteRandomVariable<3, float, LogProbabilityTag> Payload;
  typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
  typedef MapType::KeyType KeyType;
  typedef MapType::VoxelNodeType VoxelNodeType;

  MapType map(0.1);

  KeyType begin(0, 0, 0);
  KeyType end(100, 100, 200);

  {
    cout << "updateVoxel(HIT) 1 (First Time, so involves node creation) :" << endl;
    boost::progress_timer t;
    for(const KeyType& key : DenseKeyRange(begin, end)) {
      map.updateVoxel(key, hit_sensor_model);
    }
  }

  {
    cout << "updateVoxel(HIT) 2:" << endl;
    boost::progress_timer t;
    for(const KeyType& key : DenseKeyRange(begin, end)) {
      map.updateVoxel(key, hit_sensor_model);
    }
  }

  {
    cout << "updateVoxel(HIT) 3:" << endl;
    boost::progress_timer t;
    for(const KeyType& key : DenseKeyRange(begin, end)) {
      map.updateVoxel(key, hit_sensor_model);
    }
  }

  {
    cout << "updateVoxel(HIT) 4:(Raw Loops)" << endl;
    boost::progress_timer t;
    for (KeyType::Scalar z = begin.value.z(); z <= end.value.z(); ++z)
      for (KeyType::Scalar y = begin.value.y(); y <= end.value.y(); ++y)
        for (KeyType::Scalar x = begin.value.x(); x <= end.value.x(); ++x) {
          map.updateVoxel(KeyType(x, y, z), hit_sensor_model);
        }
  }

  {
    cout << "updateVoxel(MISS) 4:" << endl;
    boost::progress_timer t;
    for(const KeyType& key : DenseKeyRange(begin, end)) {
      map.updateVoxel(key, miss_sensor_model);
    }
  }

  {
    cout << "updateVoxel(MISS) 3:" << endl;
    boost::progress_timer t;
    for(const KeyType& key : DenseKeyRange(begin, end)) {
      map.updateVoxel(key, miss_sensor_model);
    }
  }

  {
    cout << "updateVoxel(MISS) 2:" << endl;
    boost::progress_timer t;
    for(const KeyType& key : DenseKeyRange(begin, end)) {
      map.updateVoxel(key, miss_sensor_model);
    }
  }

  {
    cout << "updateVoxel(MISS) 1:" << endl;
    boost::progress_timer t;
    for(const KeyType& key : DenseKeyRange(begin, end)) {
      map.updateVoxel(key, miss_sensor_model);
    }
  }

  {
    cout << "Iterate and sanity check:" << endl;
    boost::progress_timer t;
    for(const KeyType& key : DenseKeyRange(begin, end)) {
      VoxelNodeType* voxel = map.search(key);
      if (voxel) {
        if (voxel->payload().probability() != Vector3f(0.5f, 0.25f, 0.25f)) {
          cout << "Unexpected payload for " << key << endl;
        }
      }
      else
        cout << "Something is terribly wrong" << endl;
    }
  }

  ///////////////////////////////////////////////////////////////////////////////////////
  end = KeyType(1000, 1000, 200);
  {
    cout << "Simple DenseKeyIterator Loop:" << endl;
    boost::progress_timer t;
    for(const KeyType& key : DenseKeyRange(begin, end)) {
      Eigen::Vector3d center = map.voxelCenter<Eigen::Vector3d>(key);
      center += key.value.cast<double>();
      double sum = center.sum();++sum;
      if (sum == 0.999)
        cout << "Magic";
    }
  }

  {
    cout << "Simple Raw Loops:" << endl;
    boost::progress_timer t;
    for (KeyType::Scalar z = begin.value.z(); z <= end.value.z(); ++z)
      for (KeyType::Scalar y = begin.value.y(); y <= end.value.y(); ++y)
        for (KeyType::Scalar x = begin.value.x(); x <= end.value.x(); ++x) {
          const KeyType key(x, y, z);
          Eigen::Vector3d center = map.voxelCenter<Eigen::Vector3d>(key);
          center += key.value.cast<double>();
          double sum = center.sum();++sum;
          if (sum == 0.999)
            cout << "Magic";
        }
  }

  return EXIT_SUCCESS;
}
