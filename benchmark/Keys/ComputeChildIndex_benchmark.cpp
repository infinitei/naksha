/**
 * @file ComputeChildIndex_benchmark.cpp
 * @brief Benchmark for different implementations of Naksha::computeChildIndex
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Keys/Key.h"
#include "Naksha/Keys/KeyUtils.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Common/BuildConfig.h"

#include <boost/timer/timer.hpp>
#include <bitset>

template<class KeyType>
unsigned long int benchmarkWithFunction(const KeyType& range_key) {
  std::cout << "Function: computeChildIndex \t|" << std::flush;
  typedef typename KeyType::Scalar KeyScalar;
  typedef boost::timer::auto_cpu_timer TimerType;
  TimerType t;
  unsigned long int acc = 0;

  for (KeyScalar z = 0; z < range_key.value.z(); ++z)
    for (KeyScalar y = 0; y < range_key.value.y(); ++y)
      for (KeyScalar x = 0; x < range_key.value.x(); ++x) {
        acc += Naksha::computeChildIndex(KeyType(x, y, z), 0);
      }

  for (KeyScalar z = 0; z < range_key.value.z(); ++z)
    for (KeyScalar y = 0; y < range_key.value.y(); ++y)
      for (KeyScalar x = 0; x < range_key.value.x(); ++x) {
        acc += Naksha::computeChildIndex(KeyType(x, y, z), 3);
      }

  for (KeyScalar z = 0; z < range_key.value.z(); ++z)
    for (KeyScalar y = 0; y < range_key.value.y(); ++y)
      for (KeyScalar x = 0; x < range_key.value.x(); ++x) {
        acc += Naksha::computeChildIndex(KeyType(x, y, z), 8);
      }
  return acc;
}

template<class Functor, class KeyType>
unsigned long int benchmarkWithFunctor(Functor f, const KeyType& range_key) {
  std::cout << "Functor: " << PRETTY_TYPE_INFO(Functor) << "\t|" << std::flush;
  typedef typename KeyType::Scalar KeyScalar;
  typedef boost::timer::auto_cpu_timer TimerType;
  TimerType t;
  unsigned long int acc = 0;

  for (KeyScalar z = 0; z < range_key.value.z(); ++z)
    for (KeyScalar y = 0; y < range_key.value.y(); ++y)
      for (KeyScalar x = 0; x < range_key.value.x(); ++x) {
        acc += f(KeyType(x, y, z), 0);
      }

  for (KeyScalar z = 0; z < range_key.value.z(); ++z)
    for (KeyScalar y = 0; y < range_key.value.y(); ++y)
      for (KeyScalar x = 0; x < range_key.value.x(); ++x) {
        acc += f(KeyType(x, y, z), 3);
      }

  for (KeyScalar z = 0; z < range_key.value.z(); ++z)
    for (KeyScalar y = 0; y < range_key.value.y(); ++y)
      for (KeyScalar x = 0; x < range_key.value.x(); ++x) {
        acc += f(KeyType(x, y, z), 8);
      }
  return acc;
}


struct ComputeChildIndex {
  template<class KeyType>
  unsigned int operator()(const KeyType& key, const int depth_diff) {
    return Naksha::computeChildIndex(key, depth_diff);
  }
};


struct ComputeChildIndex_Exp1 {
  template<class KeyType>
  unsigned int operator()(const KeyType& key, const int depth_diff) {
    unsigned int pos = 0;
    const typename KeyType::Scalar pow_2_depth_diff = 1 << depth_diff;
    if (key.value[0] & pow_2_depth_diff) pos += 1;
    if (key.value[1] & pow_2_depth_diff) pos += 2;
    if (KeyType::Dimension == 3) {
      if (key.value[2] & pow_2_depth_diff) pos += 4;
    }
    return pos;
  }
};



struct ComputeChildIndex_Exp2 {
  template<class KeyType>
  unsigned int operator()(const KeyType& key, const int depth_diff) {
    const typename KeyType::Scalar pow_2_depth_diff = 1 << depth_diff;
    unsigned int pos =
          1 * bool(key.value[0] & pow_2_depth_diff)
        + 2 * bool(key.value[1] & pow_2_depth_diff)
        + 4 * bool(key.value[2] & pow_2_depth_diff);
    return pos;
  }
};

struct ComputeChildIndex_Exp3 {
  template<class KeyType>
  unsigned int operator()(const KeyType& key, const int depth_diff) {
    const typename KeyType::Scalar pow_2_depth_diff = 1 << depth_diff;
    unsigned int pos =
          ((key.value[0] & pow_2_depth_diff) ? 1u : 0u)
        + ((key.value[1] & pow_2_depth_diff) ? 2u : 0u)
        + ((key.value[2] & pow_2_depth_diff) ? 4u : 0u);
    return pos;
  }
};

struct ComputeChildIndex_Exp4 {
  template<class KeyType>
  unsigned int operator()(const KeyType& key, const int depth_diff) {
    unsigned int pos =
          1 * bool(key.value[0] & (1 << depth_diff))
        + 2 * bool(key.value[1] & (1 << depth_diff))
        + 4 * bool(key.value[2] & (1 << depth_diff));
    return pos;
  }
};

template<typename Scalar>
struct CwiseAndOp {
  CwiseAndOp(const int depth_diff)
      : _pow_2_depth_diff(1 << depth_diff) {
  }
  const Scalar operator()(const Scalar& x) const {
    return bool(x & _pow_2_depth_diff);
  }
  const Scalar _pow_2_depth_diff;
};

struct ComputeChildIndex_Exp5 {
  template<class KeyType>
  unsigned int operator()(const KeyType& key, const int depth_diff) {
    typedef typename KeyType::DataType KeyDataType;
    typedef typename KeyType::Scalar KeyScalar;
    unsigned int pos = KeyDataType(1, 2, 4).dot(key.value.unaryExpr(CwiseAndOp<KeyScalar>(depth_diff)));
    return pos;
  }
};

struct ComputeChildIndex_Exp6 {
  template<class KeyType>
  unsigned int operator()(const KeyType& key, const int depth_diff) {
    typedef typename KeyType::Scalar KeyScalar;
    typedef std::bitset<sizeof(KeyScalar)*8> BitSetType;

    typedef Eigen::Matrix<BitSetType, 3, 1> Vector3BitSet;
    Vector3BitSet a = key.value.template cast<BitSetType>();
    unsigned int pos =
          1 * a[0][depth_diff]
        + 2 * a[1][depth_diff]
        + 4 * a[2][depth_diff];
    return pos;
  }
};

struct ComputeChildIndex_Exp7 {
  template<class KeyType>
  unsigned int operator()(const KeyType& key, const int depth_diff) {
    const typename KeyType::Scalar child_branch_bit = 1 << depth_diff;
        unsigned int pos =
              ((key.value[0] & child_branch_bit) >> depth_diff)
            + (((key.value[1] & child_branch_bit) >> depth_diff) << 1)
            + (((key.value[2] & child_branch_bit) >> depth_diff) << 2);
        return pos;
  }
};


template<typename Scalar>
struct CwiseAndOp2 {
  CwiseAndOp2(const int depth_diff)
      : _depth_diff(depth_diff) {
  }
  const Scalar operator()(const Scalar& x) const {
    return ((x & (1 << _depth_diff)) >> _depth_diff);
  }
  const int _depth_diff;
};

struct ComputeChildIndex_Exp8 {
  template<class KeyType>
  unsigned int operator()(const KeyType& key, const int depth_diff) {
    typedef typename KeyType::DataType KeyDataType;
    typedef typename KeyType::Scalar KeyScalar;
    unsigned int pos = KeyDataType(1, 2, 4).dot(key.value.unaryExpr(CwiseAndOp2<KeyScalar>(depth_diff)));
    return pos;
  }
};

struct CheckResult {
  CheckResult(const unsigned long int expected_acc)
      : _expected_acc(expected_acc) {
  }
  void operator()(const unsigned long int x) const {
    if (x != _expected_acc) {
      std::cout << "Error! Output is not sane. "
          << x << " != " << _expected_acc << " (expected)" << "\n";
    }
  }
  const unsigned long int _expected_acc;
};


int main(int argc, char **argv) {

  std::cout << "\n# Benchmark for different implementations of Naksha::computeChildIndex\n\n";

  Naksha::Build::printBuildConfig();

  typedef Naksha::Key<3,  unsigned int> KeyType;
  const KeyType range_key(2000, 1000, 1000);
  CheckResult check_result(20784000000ul);

  std::cout << "\nNumber of Keys = " << range_key.value.prod() << "\n\n";

  std::cout << "Method                          |          Time                                         \n";
  std::cout << "------------------------------- | ------------------------------------------------------\n";

  {
    unsigned long int acc = benchmarkWithFunction(range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildIndex(), range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildIndex_Exp1(), range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildIndex_Exp2(), range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildIndex_Exp3(), range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildIndex_Exp4(), range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildIndex_Exp5(), range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildIndex_Exp6(), range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildIndex_Exp7(), range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildIndex_Exp8(), range_key);
    check_result(acc);
  }

}




