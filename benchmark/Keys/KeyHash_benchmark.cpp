/**
 * @file KeyHash_benchmark.cpp
 * @brief KeyHash_benchmark.cpp
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Keys/Key.h"
#include "Naksha/Keys/KeyHash.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Common/BuildConfig.h"

#include <boost/unordered_map.hpp>
#include <boost/timer/timer.hpp>

template<class Functor, class KeyType>
unsigned long int benchmarkRangeKeys(Functor f, const KeyType& range_key) {
  std::cout << " HashFunctor: " << PRETTY_TYPE_INFO(Functor) << "\t|" << std::flush;
  typedef typename KeyType::Scalar KeyScalar;
  typedef boost::timer::auto_cpu_timer TimerType;
  TimerType t;
  unsigned long int acc = 0;

  for (KeyScalar z = 0; z < range_key.value.z(); ++z)
    for (KeyScalar y = 0; y < range_key.value.y(); ++y)
      for (KeyScalar x = 0; x < range_key.value.x(); ++x) {
        acc += f(KeyType(x, y, z));
      }
  return acc;
}

template<class KeyType, class Functor>
unsigned long int benchmarkRandomKeys(Functor f) {
  std::cout << " HashFunctor: " << PRETTY_TYPE_INFO(Functor) << "\t|" << std::flush;
  typedef boost::timer::auto_cpu_timer TimerType;
  TimerType t;
  unsigned long int acc = 0;
  for (std::size_t i = 0; i < 100000000; ++i) {
    typedef typename KeyType::DataType KeyDataType;
    acc += f(KeyType(KeyDataType::Random()));
  }
  return acc;
}

template<class HashFunctor, class KeyType>
std::size_t benchmarkHashMap(HashFunctor f, const KeyType& range_key) {
  std::cout << " HashFunctor: " << PRETTY_TYPE_INFO(HashFunctor) << "\t|" << std::flush;
  typedef typename KeyType::Scalar KeyScalar;
  typedef boost::timer::auto_cpu_timer TimerType;
  typedef boost::unordered_map<KeyType, std::size_t, HashFunctor> KeyHashMap;


  KeyHashMap hash_map;
  hash_map.clear();
  hash_map.reserve(range_key.value.template cast<std::size_t>().prod());

  TimerType t;

  std::size_t i = 0;
  for (KeyScalar z = 0; z < range_key.value.z(); ++z)
    for (KeyScalar y = 0; y < range_key.value.y(); ++y)
      for (KeyScalar x = 0; x < range_key.value.x(); ++x) {
        hash_map[KeyType(x, y, z)] = i;
        ++i;
      }
  return hash_map.size();
}

struct KeyHashSimple {
  /// simple prime number based hash function
  template<typename Key>
  inline std::size_t operator()(const Key& key) const {
    return std::size_t(key[0]) + 1337u * std::size_t(key[1]) + 345637u * std::size_t(key[2]);
  }
};

struct KeyHashEigenDot {
  /// simple prime number based hash function
  template<typename Key>
  inline std::size_t operator()(const Key& key) const {
    typedef Eigen::Matrix<std::size_t, 3, 1> VectorType;
    return VectorType(1u, 1337u, 345637u).dot(key.value.template cast<std::size_t>());
  }
};


template <typename Scalar = const unsigned long int>
struct CheckResult {
  CheckResult(Scalar expected_acc)
      : _expected_acc(expected_acc) {
  }
  void operator()(Scalar x) const {
    if (x != _expected_acc) {
      std::cout << "Error! Output is not sane. "
          << x << " != " << _expected_acc << " (expected)" << "\n";
    }
  }
  Scalar _expected_acc;
};

int main(int argc, char **argv) {

  std::cout << "\n# Benchmark for different implementations of Key Hashing\n\n";

  Naksha::Build::printBuildConfig();

  typedef Naksha::Key<3> KeyType;

  std::cout << "\n\n### benchmarkRangeKeys\n\n";

  {
    const KeyType range_key(2000, 2000, 2000);

    CheckResult<> check_result(2774412100000000000ul);

    std::cout << " Method                         |          Time                             \n";
    std::cout << "------------------------------- | ------------------------------------------\n";

    {
      unsigned long int acc = benchmarkRangeKeys(Naksha::KeyHash(), range_key);
      check_result(acc);
    }

    {
      unsigned long int acc = benchmarkRangeKeys(KeyHashSimple(), range_key);
      check_result(acc);
    }

    {
      unsigned long int acc = benchmarkRangeKeys(KeyHashEigenDot(), range_key);
      check_result(acc);
    }

  }

  std::cout << "\n\n### benchmarkRandomKeys\n\n";

  {
    std::cout << " Method                         |          Time                            \n";
    std::cout << "------------------------------- | -----------------------------------------\n";

    CheckResult<> check_result(113ul);

    {
      unsigned long int acc = benchmarkRandomKeys<KeyType>(Naksha::KeyHash());
      check_result(acc / 10000000000000000);
    }

    {
      unsigned long int acc = benchmarkRandomKeys<KeyType>(KeyHashSimple());
      check_result(acc / 10000000000000000);
    }

    {
      unsigned long int acc = benchmarkRandomKeys<KeyType>(KeyHashEigenDot());
      check_result(acc / 10000000000000000);
    }
  }

  std::cout << "\n\n### benchmarkHashMap\n\n";

  {
    std::cout << " Method                         |          Time                             \n";
    std::cout << "------------------------------- | ------------------------------------------\n";


    const KeyType range_key(1000, 100, 100);
    CheckResult<std::size_t> check_result(range_key.value.cast<std::size_t>().prod());

    {
      std::size_t res = benchmarkHashMap(Naksha::KeyHash(), range_key);
      check_result(res);
    }

    {
      std::size_t res = benchmarkHashMap(KeyHashSimple(), range_key);
      check_result(res);
    }

    {
      std::size_t res = benchmarkHashMap(KeyHashEigenDot(), range_key);
      check_result(res);
    }
  }

}


