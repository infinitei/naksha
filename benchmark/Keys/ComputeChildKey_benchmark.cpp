/**
 * @file ComputeChildKey_benchmark.cpp
 * @brief ComputeChildKey_benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Keys/Key.h"
#include "Naksha/Keys/KeyUtils.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Common/BuildConfig.h"

#include <boost/timer/timer.hpp>

template<class KeyType>
unsigned long int benchmarkWithFunction(const KeyType& range_key) {
  std::cout << "Function: computeChildKey \t|" << std::flush;
  typedef typename KeyType::Scalar KeyScalar;
  typedef boost::timer::auto_cpu_timer TimerType;
  TimerType t;
  unsigned long int acc = 0;
  unsigned short int depth = 1;
  unsigned int child_index = 0;

  for (KeyScalar z = 0; z < range_key.value.z(); ++z)
    for (KeyScalar y = 0; y < range_key.value.y(); ++y)
      for (KeyScalar x = 0; x < range_key.value.x(); ++x) {
        unsigned short int center_offset_key = 32768 >> depth;
        acc += Naksha::computeChildKey(child_index, center_offset_key, KeyType(x, y, z)).value.sum();
        ++depth;
        if (depth > 16)
          depth = 0;
        ++child_index;
        if (child_index > 7)
          child_index = 0;
      }
  return acc;
}


template<class Functor, class KeyType>
unsigned long int benchmarkWithFunctor(Functor f, const KeyType& range_key) {
  std::cout << "Functor: " << PRETTY_TYPE_INFO(Functor) << "\t|" << std::flush;
  typedef typename KeyType::Scalar KeyScalar;
  typedef boost::timer::auto_cpu_timer TimerType;
  TimerType t;
  unsigned long int acc = 0;
  unsigned short int depth = 1;
  unsigned int child_index = 0;

  for (KeyScalar z = 0; z < range_key.value.z(); ++z)
    for (KeyScalar y = 0; y < range_key.value.y(); ++y)
      for (KeyScalar x = 0; x < range_key.value.x(); ++x) {
        unsigned short int center_offset_key = 32768 >> depth;
        KeyType key = f(child_index, center_offset_key, KeyType(x, y, z));
        acc += key.value.sum();
        ++depth;
        if (depth > 16)
          depth = 0;
        ++child_index;
        if(child_index > 7)
          child_index= 0;
      }
  return acc;
}


struct ComputeChildKey {
  template<class KeyType>
  KeyType operator()(const unsigned int child_index,
                          const typename KeyType::Scalar center_offset_key,
                          const KeyType& parent_key) const {
    return Naksha::computeChildKey(child_index, center_offset_key, parent_key);
  }
};


struct ComputeChildKey_Exp1 {
  template<class KeyType>
  KeyType operator()(const unsigned int child_index,
                     const typename KeyType::Scalar center_offset_key,
                     const KeyType& parent_key) const {
    KeyType child_key;

    // x-axis
    if (child_index & 1)
      child_key.value[0] = parent_key.value[0] + center_offset_key;
    else
      child_key.value[0] = parent_key.value[0] - center_offset_key - (center_offset_key ? 0 : 1);
    // y-axis
    if (child_index & 2)
      child_key.value[1] = parent_key.value[1] + center_offset_key;
    else
      child_key.value[1] = parent_key.value[1] - center_offset_key - (center_offset_key ? 0 : 1);

    if (KeyType::Dimension == 3) {
      // z-axis
      if (child_index & 4)
        child_key.value[2] = parent_key.value[2] + center_offset_key;
      else
        child_key.value[2] = parent_key.value[2] - center_offset_key - (center_offset_key ? 0 : 1);
    }

    return child_key;
  }
};

struct ComputeChildKey_Exp2 {
  template<class KeyType>
  KeyType operator()(const unsigned int child_index,
                     const typename KeyType::Scalar center_offset_key,
                     const KeyType& parent_key) const {
    KeyType child_key (parent_key.value);

    // x-axis
    if (child_index & 1)
      child_key.value[0] += center_offset_key;
    else
      child_key.value[0] -=  (center_offset_key + (center_offset_key ? 0 : 1));
    // y-axis
    if (child_index & 2)
      child_key.value[1] += center_offset_key;
    else
      child_key.value[1] -= (center_offset_key + (center_offset_key ? 0 : 1));

    if (KeyType::Dimension == 3) {
      // z-axis
      if (child_index & 4)
        child_key.value[2] += center_offset_key;
      else
        child_key.value[2] -= (center_offset_key + (center_offset_key ? 0 : 1));
    }

    return child_key;
  }
};


struct ComputeChildKey_Exp3 {
  template<class KeyType>
  KeyType operator()(const unsigned int child_index,
                     const typename KeyType::Scalar center_offset_key,
                     const KeyType& parent_key) const {
    KeyType child_key(parent_key.value);
    if (center_offset_key != 0) {
      child_key.value[0] += (child_index & 1) ? center_offset_key : -center_offset_key;
      child_key.value[1] += (child_index & 2) ? center_offset_key : -center_offset_key;
      child_key.value[2] += (child_index & 4) ? center_offset_key : -center_offset_key;
    }
    else {
      child_key.value[0] -= (child_index & 1) ? 0 : 1;
      child_key.value[1] -= (child_index & 2) ? 0 : 1;
      child_key.value[2] -= (child_index & 4) ? 0 : 1;
    }
    return child_key;
  }
};

struct ComputeChildKey_Exp4 {
  template<class KeyType>
  KeyType operator()(const unsigned int child_index,
                     const typename KeyType::Scalar center_offset_key,
                     const KeyType& parent_key) const {
    KeyType child_key(parent_key.value);
    child_key.value[0] += (child_index & 1) ? center_offset_key : (-center_offset_key - (center_offset_key ? 0 : 1));
    child_key.value[1] += (child_index & 2) ? center_offset_key : (-center_offset_key - (center_offset_key ? 0 : 1));
    child_key.value[2] += (child_index & 4) ? center_offset_key : (-center_offset_key - (center_offset_key ? 0 : 1));
    return child_key;
  }
};

struct ComputeChildKey_Exp5 {
  template<class KeyType>
  KeyType operator()(const unsigned int child_index,
                     const typename KeyType::Scalar center_offset_key,
                     const KeyType& parent_key) const {
    KeyType child_key(parent_key.value);
    const typename KeyType::Scalar other_offset = -center_offset_key - (center_offset_key ? 0 : 1);
    child_key.value[0] += (child_index & 1) ? center_offset_key : other_offset;
    child_key.value[1] += (child_index & 2) ? center_offset_key : other_offset;
    child_key.value[2] += (child_index & 4) ? center_offset_key : other_offset;
    return child_key;
  }
};





struct CheckResult {
  CheckResult(const unsigned long int expected_acc)
      : _expected_acc(expected_acc) {
  }
  void operator()(const unsigned long int x) const {
    if (x != _expected_acc) {
      std::cout << "Error! Output is not sane. "
          << x << " != " << _expected_acc << " (expected)" << "\n";
    }
  }
  const unsigned long int _expected_acc;
};

int main(int argc, char **argv) {

  std::cout << "\n# Benchmark for different implementations of Naksha::ComputeChildKey\n\n";

  Naksha::Build::printBuildConfig();

  typedef Naksha::Key<3, unsigned int> KeyType;
  const KeyType range_key(2000, 1000, 1000);

  CheckResult check_result(1272134105437031663ul);

  std::cout << "\nNumber of Keys = " << range_key.value.prod() << "\n\n";

  std::cout << "Method                          |          Time                                \n";
  std::cout << "------------------------------- | ---------------------------------------------\n";

  {
    unsigned long int acc = benchmarkWithFunction(range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildKey(), range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildKey_Exp1(), range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildKey_Exp2(), range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildKey_Exp3(), range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildKey_Exp4(), range_key);
    check_result(acc);
  }

  {
    unsigned long int acc = benchmarkWithFunctor(ComputeChildKey_Exp5(), range_key);
    check_result(acc);
  }

}


