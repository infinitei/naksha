/**
 * @file UnorderedMapDataStructureIterator_benchmark.cpp
 * @brief UnorderedMap voxel Iterator Benchmark
 * 
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Maps/MapSerialization.h"
#include "Naksha/Keys/DenseKeyIterator.h"
#include "Naksha/Nodes/NodeUtils.h"
#include "Naksha/Payload/MeasurementModel.h"

#include <boost/progress.hpp>

#include <algorithm>

using namespace Naksha;
using namespace Eigen;
using namespace std;

int main(int argc, char **argv) {

  typedef DiscreteRandomVariable<3, float, LogProbabilityTag> Payload;
  typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
  typedef MapType::KeyType KeyType;
  typedef MapType::VoxelNodeType VoxelNodeType;
  typedef MeasurementModel<MapType::Payload::RVType> MeasurementModelType;

  MapType map(0.1);

  {
    cout << "updateVoxel(HIT) 1 (First Time, so involves node creation) ..." << endl;
    boost::progress_timer t;
    for (const KeyType& key : DenseKeyRange(KeyType(0, 0, 0), KeyType (200, 200, 200))) {
      map.updateVoxel(key, MeasurementModelType::HIT);
    }
    for (const KeyType& key : DenseKeyRange(KeyType(200, 200, 200), KeyType (400, 500, 600))) {
      map.updateVoxel(key, MeasurementModelType::MISS);
    }

    cout << "Map Size = " << map.size() << endl;
  }

  {
    typedef MapType::iterator MapVoxelIt;
    cout << "Using MapType::iterator:" << endl;
    boost::progress_timer t;
    for (MapVoxelIt it = map.begin(); it != map.end(); ++it) {
      if (it->payload().probability().sum() > 2.0)
        cout << "Magic";
    }
  }

  {
    typedef MapType::const_iterator MapVoxelIt;
    cout << "Using MapType::const_iterator:" << endl;
    boost::progress_timer t;
    for (MapVoxelIt it = map.begin(); it != map.end(); ++it) {
      if (it->payload().probability().sum() > 2.0)
        cout << "Magic";
    }
  }


  {
    typedef MapType::Datastructure::DataType::iterator MapInnertIt;
    cout << "Using MapType::inner_iterator:" << endl;
    boost::progress_timer t;
    for (MapInnertIt it = map.data().begin(); it != map.data().end(); ++it) {
      if (it->second.payload().probability().sum() > 2.0)
        cout << "Magic";
    }
  }





  ///////////////////////////////////////////////////////////////////////////////////////

  {
    cout << "(Node Occupancy Check) Using std::count_if + IsNodeOccupied:" << endl;
    boost::progress_timer t;

    size_t count = std::count_if(map.begin(), map.end(), IsNodeOccupied<VoxelNodeType>());
    cout << "Count = " << count << "\n";
  }

  {
    typedef MapType::iterator MapVoxelIt;
    cout << "(Node Occupancy Check) Using Manual IsNodeOccupied:" << endl;
    boost::progress_timer t;

    IsNodeOccupied<VoxelNodeType> occ_checker;
    size_t count = 0;
    for (MapVoxelIt it = map.begin(); it != map.end(); ++it) {
      if (occ_checker(*it))
        ++count;
    }
    cout << "Count = " << count << "\n";
  }

  {
    typedef MapType::iterator MapVoxelIt;
    cout << "(Node Occupancy Check) Using Manual IsPayloadOccupied:" << endl;
    boost::progress_timer t;

    IsPayloadOccupied<Payload> occ_checker;
    size_t count = 0;
    for (MapVoxelIt it = map.begin(); it != map.end(); ++it) {
      if (occ_checker(it->payload()))
        ++count;
    }
    cout << "Count = " << count << "\n";
  }

  {
    typedef MapType::Datastructure::DataType::iterator MapInnertIt;
    cout << "(Node Occupancy Check) Using Manual IsNodeOccupied with InnerIterator:" << endl;
    boost::progress_timer t;

    IsNodeOccupied<VoxelNodeType> occ_checker;
    size_t count = 0;
    for (MapInnertIt it = map.data().begin(); it != map.data().end(); ++it) {
      if (occ_checker(it->second))
        ++count;
    }
    cout << "Count = " << count << "\n";
  }





  return EXIT_SUCCESS;
}
