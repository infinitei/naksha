/**
 * @file TreeDataStructureIterator_benchmark.cpp
 * @brief Tree Datastructure Iterator Benchmark
 * 
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Maps/MapSerialization.h"
#include "Naksha/Keys/DenseKeyIterator.h"
#include "Naksha/Nodes/NodeUtils.h"
#include "Naksha/Payload/MeasurementModel.h"

#include <boost/progress.hpp>
#include <boost/chrono.hpp>

#include <algorithm>

class Timer {
public:
    Timer() : beg_(clock_::now()) {}
    void reset() { beg_ = clock_::now(); }
    double elapsed() const {
        return boost::chrono::duration_cast<second_>
            (clock_::now() - beg_).count(); }

private:
    typedef boost::chrono::high_resolution_clock clock_;
    typedef boost::chrono::duration<double, boost::ratio<1> > second_;
    boost::chrono::time_point<clock_> beg_;
};

using namespace Naksha;
using namespace Eigen;
using namespace std;

int main(int argc, char **argv) {

  typedef DiscreteRandomVariable<3, float, LogProbabilityTag> Payload;
  typedef VolumetricMap<3, Payload, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
  typedef MapType::KeyType KeyType;
  typedef MeasurementModel<MapType::Payload::RVType> MeasurementModelType;

  MapType map(0.1);

  {
    cout << "updateVoxel(HIT) 1 (First Time, so involves node creation) ..." << endl;

    Timer tmr;

    for (const KeyType& key : DenseKeyRange(KeyType(0, 0, 0), KeyType (200, 200, 200))) {
      map.updateVoxel(key, MeasurementModelType::HIT);
    }
    for (const KeyType& key : DenseKeyRange(KeyType(200, 200, 200), KeyType (400, 500, 600))) {
      map.updateVoxel(key, MeasurementModelType::MISS);
    }

    double elapsed_seconds = tmr.elapsed();
    std::cout << "Time = " << elapsed_seconds << " s" << std::endl;

    cout << "Number of Voxels = " << map.numberOfVoxels() << endl;
    cout << "Number of Nodes = " << map.numberOfNodes() << endl;
  }

  {
    typedef MapType::iterator VoxelIt;
    cout << "\nUsing MapType::iterator:" << endl;

    Timer tmr;

    size_t num_of_nodes_travelled = 0;
    VoxelIt end = map.end();
    for (VoxelIt it = map.begin(); it != end; ++it) {
      ++num_of_nodes_travelled;
      if (it->payload().probability().sum() > 2.0)
        cout << "Magic";
      if (!it.isLeafNode())
        cout << "NotLeaf!! ";
    }

    double elapsed_seconds = tmr.elapsed();
    std::cout << "Time = " << elapsed_seconds << " s" << std::endl;

    cout << "Number of Nodes Traversed: " << num_of_nodes_travelled << "\n";
  }

  {
    typedef MapType::const_iterator VoxelIt;
    cout << "\nUsing MapType::const_iterator:" << endl;

    Timer tmr;

    size_t num_of_nodes_travelled = 0;
    VoxelIt end = map.end();
    for (VoxelIt it = map.begin(); it != end; ++it) {
      ++num_of_nodes_travelled;
      if (it->payload().probability().sum() > 2.0)
        cout << "Magic";
      if (!it.isLeafNode())
        cout << "NotLeaf!! ";
    }

    double elapsed_seconds = tmr.elapsed();
    std::cout << "Time = " << elapsed_seconds << " s" << std::endl;

    cout << "Number of Nodes Traversed: " << num_of_nodes_travelled << "\n";
  }

  {
    typedef MapType::node_iterator NodeIt;
    cout << "\nUsing MapType::node_iterator:" << endl;
    Timer tmr;

    size_t num_of_nodes_travelled = 0;
    NodeIt end = map.end_node();
    for (NodeIt it = map.begin_node(); it != end; ++it) {
      ++num_of_nodes_travelled;
      if (it->payload().probability().sum() > 2.0)
        cout << "Magic";
    }
    double elapsed_seconds = tmr.elapsed();
    std::cout << "Time = " << elapsed_seconds << " s" << std::endl;

    cout << "Number of Nodes Traversed: " << num_of_nodes_travelled << "\n";
  }

  {
    typedef MapType::node_const_iterator NodeIt;
    cout << "\nUsing MapType::node_const_iterator:" << endl;
    Timer tmr;

    size_t num_of_nodes_travelled = 0;
    NodeIt end = map.end_node();
    for (NodeIt it = map.begin_node(); it != end; ++it) {
      ++num_of_nodes_travelled;
      if (it->payload().probability().sum() > 2.0)
        cout << "Magic";
    }
    double elapsed_seconds = tmr.elapsed();
    std::cout << "Time = " << elapsed_seconds << " s" << std::endl;

    cout << "Number of Nodes Traversed: " << num_of_nodes_travelled << "\n";
  }

  return EXIT_SUCCESS;
}
