/**
 * @file InsertMeasurements_benchmark.cpp
 * @brief InsertMeasurements benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Algorithms/InsertMeasurements.h"
#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Maps/MapSerialization.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Payload/LabelToMeasurementMapping.h"
#include "Naksha/Common/EigenTypedefs.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/timer/timer.hpp>

template<class MapType, class PointCloudType, class MeasurementType>
void doInsertDepthScanBenchmark(MapType& map, const PointCloudType& point_cloud, const MeasurementType& measurement) {
  typedef boost::timer::auto_cpu_timer TimerType;
  std::cout << "\n===================================================================\n";
  std::cout << "MapType: " << PRETTY_TYPE(MapType, "Naksha", 2) << "\n";
  {
    std::cout << "insertDepthScan: " << std::flush;
    TimerType t;
    insertDepthScan(map, Eigen::Vector3d::Zero(), point_cloud, measurement);
  }

  std::cout << "Number of voxel = " << map.size() << "\n";
}

using namespace Naksha;

int main(int argc, char **argv) {

  std::cout << "===================================================================\n";
  std::cout << "================InsertMeasurements_benchmark==================\n";

  typedef DiscreteRandomVariable<9, float> RV;
  typedef ColoredRandomVariable<RV>::type Payload;
  typedef MeasurementMap<RV> Measurement;

  // Create point cloud
  typedef EigenAligned<Eigen::Vector3d>::std_vector PointCloudType;
  const int width = 20;
  const int xoffset = 20;
  PointCloudType point_cloud;
  for (double x = xoffset - width ; x < (xoffset + width); x += 0.1)
    for (double y = -width; y < width; y += 0.1) {
      point_cloud.push_back(Eigen::Vector3d(x, y, -10));
    }
  std::cout << "Created PointCloud of Size = " << point_cloud.size() << "\n";

  {
    typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(0.1);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>());
  }

  {
    typedef VolumetricMap<3, Payload, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(0.1);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>());
  }

  {
    typedef VolumetricMap<3, Payload, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(0.1);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>());
  }

  std::cout << "\n==================== With Change Detection =======================\n";

  {
    typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, HybridMappingInterface, MapTransform> MapType;
    MapType map(0.1);
    map.enableChangeDetection();
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>());
  }

  {
    typedef VolumetricMap<3, Payload, TreeDataStructure, HybridMappingInterface, MapTransform> MapType;
    MapType map(0.1);
    map.enableChangeDetection();
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>());
  }

  {
    typedef VolumetricMap<3, Payload, HashedVectorDataStructure, HybridMappingInterface, MapTransform> MapType;
    MapType map(0.1);
    map.enableChangeDetection();
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>());
  }

  std::cout << "\n==================== With Reserve call =======================\n";

  {
    typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(0.1);
    map.reserve(5426398);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>());
  }

  {
    typedef VolumetricMap<3, Payload, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(0.1);
    map.reserve(5426398);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>());
  }

  {
    typedef VolumetricMap<3, Payload, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(0.1);
    map.reserve(5426398);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>());
  }

  return EXIT_SUCCESS;
}

