/**
 * @file ComputeKeysFromDepthScan_benchmark.cpp
 * @brief ComputeKeysFromDepthScan_benchmark
 *
 * @author Abhijit Kundu
 */

#include <Naksha/Algorithms/RayTracing.h>
#include <Naksha/Maps/MapMaker.h>
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Common/EigenTypedefs.h"

#include <boost/unordered_set.hpp>
#include <boost/container/flat_set.hpp>
#include <boost/move/move.hpp>
#include <boost/timer/timer.hpp>
#include <set>

template<
  class HitKeysContainer,
  class MissKeysContainer,
  class MapType,
  class PointCloudType>
void benchmarkT1T2(const MapType& map, const PointCloudType& point_cloud) {

  std::cout << "\n========================= benchmarkT1T2  ==========================\n";
  std::cout << "HitKeysContainer: " << PRETTY_TYPE_INFO_REMOVE_NS(HitKeysContainer, "Naksha") << "\n";
  std::cout << "MissKeysContainer: " << PRETTY_TYPE_INFO_REMOVE_NS(MissKeysContainer, "Naksha") << "\n";

  std::size_t total_num_keys = 0;
  std::cout << "computeKeysFromDepthScan: " << std::flush;
  {
    typedef boost::timer::auto_cpu_timer TimerType;
    TimerType t;

    HitKeysContainer hit_keys;
    MissKeysContainer miss_keys;
    std::tie(hit_keys, miss_keys) = Naksha::computeKeysFromDepthScan<HitKeysContainer,
        MissKeysContainer>(map, Eigen::Vector3d::Zero(), point_cloud);
    total_num_keys = hit_keys.size() + miss_keys.size();
  }
  std::cout << "total_num_keys = " << total_num_keys << std::endl;
}

template<class KeySetType, class MapType, class PointCloudType>
void benchmarkKeyStdVectorWithT(const MapType& map, const PointCloudType& point_cloud) {

  typedef typename MapType::KeyType KeyType;
  typedef std::vector<KeyType> KeyStdVector;

  std::cout << "\n==================== benchmarkKeyStdVectorWithT =======================\n";
  std::cout << "KeySetType: " << PRETTY_TYPE_INFO_REMOVE_NS(KeySetType, "Naksha") << "\n";

  std::size_t total_num_keys = 0;
  std::cout << "computeKeysFromDepthScan: " << std::flush;
  {
    typedef boost::timer::auto_cpu_timer TimerType;
    TimerType t;

    KeyStdVector hit_keys;
    KeyStdVector miss_keys;
    std::tie(hit_keys, miss_keys) = Naksha::computeKeysFromDepthScan<KeyStdVector,
        KeyStdVector>(map, Eigen::Vector3d::Zero(), point_cloud);

    KeySetType unique_miss_keys(miss_keys.begin(), miss_keys.end());
//    KeySetType unique_miss_keys(boost::make_move_iterator(miss_keys.begin()),
//                                boost::make_move_iterator(miss_keys.end()));

    total_num_keys = hit_keys.size() + unique_miss_keys.size();
  }
  std::cout << "total_num_keys = " << total_num_keys << std::endl;
}

template<class MapType, class PointCloudType>
void benchmarkKeyStdVectorWithSort(const MapType& map, const PointCloudType& point_cloud) {

  typedef typename MapType::KeyType KeyType;
  typedef std::vector<KeyType> KeyStdVector;

  std::cout << "\n=================== benchmarkKeyStdVectorWithSort =======================\n";

  std::size_t total_num_keys = 0;
  std::cout << "computeKeysFromDepthScan: " << std::flush;
  {
    typedef boost::timer::auto_cpu_timer TimerType;
    TimerType t;

    KeyStdVector hit_keys;
    KeyStdVector miss_keys;
    std::tie(hit_keys, miss_keys) = Naksha::computeKeysFromDepthScan<KeyStdVector,
        KeyStdVector>(map, Eigen::Vector3d::Zero(), point_cloud);

    std::sort(miss_keys.begin(), miss_keys.end());

    total_num_keys = hit_keys.size() + miss_keys.size();
  }
  std::cout << "total_num_keys = " << total_num_keys << std::endl;
}

template<class KeySetType, class MapType, class PointCloudType>
void benchmarkKeyStdVectorWithKeySetLoop(const MapType& map, const PointCloudType& point_cloud) {

  typedef typename MapType::KeyType KeyType;
  typedef std::vector<KeyType> KeyStdVector;

  std::cout << "\n================= benchmarkKeyStdVectorWithKeySetLoop ====================\n";

  std::size_t total_num_keys = 0;
  std::cout << "computeKeysFromDepthScan: " << std::flush;
  {
    typedef boost::timer::auto_cpu_timer TimerType;
    TimerType t;

    KeyStdVector hit_keys;
    KeyStdVector miss_keys;
    std::tie(hit_keys, miss_keys) = Naksha::computeKeysFromDepthScan<KeyStdVector,
        KeyStdVector>(map, Eigen::Vector3d::Zero(), point_cloud);

    total_num_keys = hit_keys.size();

    KeySetType unique_miss_keys;
    unique_miss_keys.reserve(miss_keys.size());
    for (const KeyType& key : miss_keys) {
      typedef std::pair<typename KeySetType::iterator, bool> KeySetInsertReturnType;
      KeySetInsertReturnType ret = unique_miss_keys.insert(key);
      if(ret.second)
        ++total_num_keys;
    }
  }
  std::cout << "total_num_keys = " << total_num_keys << std::endl;
}

using namespace Naksha;

int main(int argc, char **argv) {

  std::cout << "\n============ ComputeKeysFromDepthScan Benchmark ==============\n";

  typedef DiscreteRandomVariable<9, float> RV;
  typedef ColoredRandomVariable<RV>::type Payload;
  typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, HybridMappingInterface, MapTransform> MapType;

  typedef MapType::KeyType KeyType;

  typedef KeyHash KeyHashType;

  // Create point cloud
  typedef EigenAligned<Eigen::Vector3d>::std_vector PointCloudType;
  const int width = 20;
  const int xoffset = 20;
  PointCloudType point_cloud;
  for (double x = xoffset - width; x < (xoffset + width); x += 0.1)
    for (double y = -width; y < width; y += 0.1) {
      point_cloud.push_back(Eigen::Vector3d(x, y, -10));
    }

  std::cout << "MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") << "\n";
  std::cout << "PointCloud Size = : " << point_cloud.size() << "\n";

  MapType map(0.1);

  {
    typedef std::vector<KeyType> HitKeysContainer;
    typedef std::vector<KeyType> MissKeysContainer;
    benchmarkT1T2<HitKeysContainer, MissKeysContainer>(map, point_cloud);
  }

//  {
//    typedef std::vector<KeyType> HitKeysContainer;
//    typedef boost::unordered_set<KeyType, KeyHashType> MissKeysContainer;
//    benchmarkT1T2<HitKeysContainer, MissKeysContainer>(map, point_cloud);
//  }
//
//  {
//    typedef std::vector<KeyType> HitKeysContainer;
//    typedef std::set<KeyType> MissKeysContainer;
//    benchmarkT1T2<HitKeysContainer, MissKeysContainer>(map, point_cloud);
//  }

//   //This takes forever
//  {
//    typedef std::vector<KeyType> HitKeysContainer;
//    typedef boost::container::flat_set<KeyType> MissKeysContainer;
//    benchmark<HitKeysContainer, MissKeysContainer>(map, point_cloud);
//  }

  {
    benchmarkKeyStdVectorWithSort(map, point_cloud);
  }

  {
    typedef std::set<KeyType> KeySet;
    benchmarkKeyStdVectorWithT<KeySet>(map, point_cloud);
  }

  {
    typedef boost::unordered_set<KeyType, KeyHashType> KeySet;
    benchmarkKeyStdVectorWithT<KeySet>(map, point_cloud);
  }

//   //This takes forever
//  {
//    typedef boost::container::flat_set<KeyType> KeySet;
//    benchmarkKeyStdVectorWithT<KeySet>(map, point_cloud);
//  }

//  {
//    typedef std::set<KeyType> KeySet;
//    benchmarkKeyStdVectorWithKeySetLoop<KeySet>(map, point_cloud);
//  }

  {
    typedef boost::unordered_set<KeyType, KeyHashType> KeySet;
    benchmarkKeyStdVectorWithKeySetLoop<KeySet>(map, point_cloud);
  }

  return EXIT_SUCCESS;
}



