/**
 * @file RayBBXIntersection_benchmark.cpp
 * @brief RayBBXIntersection_benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Algorithms/RayIntersections.h"

#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Common/BuildConfig.h"

#include <Eigen/Geometry>

#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/timer/timer.hpp>
#include <boost/format.hpp>

template <template<typename,int> class Functor, class V, class A>
double benchmarkSum(const std::vector<V,A>& rays) {
  typedef typename std::vector<V,A>::value_type VectorType;
  typedef Functor<typename VectorType::Scalar, VectorType::SizeAtCompileTime> FunctorType;
  typedef Eigen::AlignedBox<typename VectorType::Scalar, VectorType::SizeAtCompileTime> BoundingBoxType;
  typedef boost::timer::auto_cpu_timer TimerType;
  std::cout << boost::format("%-32s|") % PRETTY_TYPE(FunctorType, "Naksha", 1) << std::flush;
  TimerType t;

  BoundingBoxType bbx(VectorType::Constant(-20), VectorType::Constant(20));
  FunctorType func(bbx, VectorType::Zero());
  double sum = 0;
  for(const VectorType& ray : rays) {
    sum += func(ray).sum();
  }
  return sum;
}

template <template<typename,int> class Functor, typename EigenPointCloudType>
double benchmarkSum(const EigenPointCloudType& eigen_rays) {
  typedef boost::timer::auto_cpu_timer TimerType;
  typedef Eigen::Matrix<typename EigenPointCloudType::Scalar, 3, 1> VectorType;
  typedef Eigen::AlignedBox<typename EigenPointCloudType::Scalar, 3> BoundingBoxType;
  typedef Functor<typename EigenPointCloudType::Scalar, VectorType::SizeAtCompileTime> FunctorType;

  std::cout << boost::format("%-32s|") % PRETTY_TYPE(FunctorType, "Naksha", 1) << std::flush;
  TimerType t;

  BoundingBoxType bbx(VectorType::Constant(-20), VectorType::Constant(20));
  FunctorType func(bbx, VectorType::Zero());
  return func(eigen_rays).sum();
}

template <template<typename,int> class Functor, class V, class A>
void transform(std::vector<V,A>& rays) {
  typedef typename std::vector<V,A>::value_type VectorType;
  typedef Functor<typename VectorType::Scalar, VectorType::SizeAtCompileTime> FunctorType;
  typedef Eigen::AlignedBox<typename VectorType::Scalar, VectorType::SizeAtCompileTime> BoundingBoxType;
  typedef boost::timer::auto_cpu_timer TimerType;
  std::cout << boost::format("%-32s|") % PRETTY_TYPE(FunctorType, "Naksha", 1) << std::flush;
  TimerType t;

  BoundingBoxType bbx(VectorType::Constant(-20), VectorType::Constant(20));
  FunctorType func(bbx, VectorType::Zero());
  std::transform(rays.begin(), rays.end(), rays.begin(), func);
}

template <template<typename,int> class Functor, typename EigenPointCloudType>
void transform(EigenPointCloudType& eigen_rays) {
  typedef boost::timer::auto_cpu_timer TimerType;
  typedef Eigen::Matrix<typename EigenPointCloudType::Scalar, 3, 1> VectorType;
  typedef Eigen::AlignedBox<typename EigenPointCloudType::Scalar, 3> BoundingBoxType;
  typedef Functor<typename EigenPointCloudType::Scalar, VectorType::SizeAtCompileTime> FunctorType;

  std::cout << boost::format("%-32s|") % PRETTY_TYPE(FunctorType, "Naksha", 1) << std::flush;
  TimerType t;

  BoundingBoxType bbx(VectorType::Constant(-20), VectorType::Constant(20));
  FunctorType func(bbx, VectorType::Zero());
  eigen_rays = func(eigen_rays);
}

template <template<typename,int> class Functor, typename EigenPointCloudType>
void transformInPlace(EigenPointCloudType& eigen_rays) {
  typedef boost::timer::auto_cpu_timer TimerType;
  typedef Eigen::Matrix<typename EigenPointCloudType::Scalar, 3, 1> VectorType;
  typedef Eigen::AlignedBox<typename EigenPointCloudType::Scalar, 3> BoundingBoxType;
  typedef Functor<typename EigenPointCloudType::Scalar, VectorType::SizeAtCompileTime> FunctorType;

  std::cout << boost::format("%-32s|") % PRETTY_TYPE(FunctorType, "Naksha", 1) << std::flush;
  TimerType t;

  BoundingBoxType bbx(VectorType::Constant(-20), VectorType::Constant(20));
  FunctorType func(bbx, VectorType::Zero());
  func(eigen_rays);
}

template <typename Scalar, int Dimension>
struct MethodB {
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType;
  typedef Eigen::AlignedBox<Scalar, Dimension> BoundingBoxType;

  MethodB(BoundingBoxType bbx, VectorType start)
    :m_bbx(bbx), m_start(start) {
  }
  VectorType operator()(const VectorType& ray) const {
    VectorType tMax = ( m_bbx.min() - m_start ).cwiseQuotient(ray).cwiseMax( ( m_bbx.max() - m_start ).cwiseQuotient(ray) );
    return m_start + ray * tMax.minCoeff();
  }

private:
  BoundingBoxType m_bbx;
  VectorType m_start;
};

template <typename Scalar, int Dimension>
struct MethodC {
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType;
  typedef Eigen::AlignedBox<Scalar, Dimension> BoundingBoxType;

  MethodC(BoundingBoxType bbx, VectorType start)
    :bbx(bbx), ray_origin(start) {
  }
  VectorType operator()(const VectorType& ray_direction) const {
    VectorType tMax = (ray_direction.array() >= 0).select(
        (bbx.min() - ray_origin).cwiseMax(bbx.max() - ray_origin),
        (bbx.min() - ray_origin).cwiseMin(bbx.max() - ray_origin));
    return ray_origin + ray_direction * tMax.cwiseQuotient(ray_direction).minCoeff();
  }

private:
  BoundingBoxType bbx;
  VectorType ray_origin;
};

template <typename Scalar, int Dimension>
struct MethodD {
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType;
  typedef Eigen::AlignedBox<Scalar, Dimension> BoundingBoxType;

  MethodD(BoundingBoxType bbx, VectorType ray_origin)
    :min_start(bbx.min() - ray_origin),
     max_start(bbx.max() - ray_origin),
     ray_origin(ray_origin) {
  }
  VectorType operator()(const VectorType& ray) const {
    const VectorType ray_inv = ray.cwiseInverse();
    const VectorType tmin = min_start.cwiseProduct(ray_inv);
    const VectorType tmax = max_start.cwiseProduct(ray_inv);
    return ray_origin + ray * tmin.cwiseMax(tmax).minCoeff();
  }

private:
  const VectorType min_start;
  const VectorType max_start;
  const VectorType ray_origin;
};

template <typename Scalar, int Dimension>
struct MethodE {
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType;
  typedef Eigen::AlignedBox<Scalar, Dimension> BoundingBoxType;

  MethodE(BoundingBoxType bbx, VectorType ray_origin)
    :max((bbx.min() - ray_origin).cwiseMax(bbx.max() - ray_origin)),
     min((bbx.min() - ray_origin).cwiseMin(bbx.max() - ray_origin)),
     ray_origin(ray_origin) {
  }
  VectorType operator()(const VectorType& ray) const {
    return ray_origin + ray * (ray.array() >= 0).select(max,min).cwiseQuotient(ray).minCoeff();
  }

private:
  const VectorType max;
  const VectorType min;
  const VectorType ray_origin;
};

template <typename Scalar, int Dimension>
struct MethodF {
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType;
  typedef Eigen::Matrix<Scalar, Dimension, 2> MaxMinType;
  typedef Eigen::AlignedBox<Scalar, Dimension> BoundingBoxType;

  MethodF(BoundingBoxType bbx, VectorType ray_origin)
    :max_min((MaxMinType() << bbx.min() - ray_origin, bbx.max() - ray_origin).finished()),
     ray_origin(ray_origin) {
  }
  template<class Derived>
  typename Derived::PlainObject operator()(const Eigen::MatrixBase<Derived>& ray) const {
    return ray_origin + ray * (ray.cwiseInverse().asDiagonal() * max_min).rowwise().maxCoeff().minCoeff();
  }

private:
  const MaxMinType max_min;
  const VectorType ray_origin;
};

template <typename Scalar, int Dimension>
struct BatchMethodB {
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType;
  typedef Eigen::AlignedBox<Scalar, Dimension> BoundingBoxType;

  BatchMethodB(BoundingBoxType bbx, VectorType ray_origin)
    :min_start(bbx.min() - ray_origin),
     max_start(bbx.max() - ray_origin),
     ray_origin(ray_origin) {
  }
  template <typename PointCloudType>
  PointCloudType operator()(const PointCloudType& rays) const {
    PointCloudType ray_inv = rays.cwiseInverse();
    PointCloudType tmin = min_start.asDiagonal() * ray_inv;
    PointCloudType tmax = max_start.asDiagonal() * ray_inv;
    return (rays * tmin.cwiseMax(tmax).colwise().minCoeff().asDiagonal()).colwise() + ray_origin;
  }

private:
  const VectorType min_start;
  const VectorType max_start;
  const VectorType ray_origin;
};

template <typename Scalar, int Dimension>
struct BatchMethodC {
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType;
  typedef Eigen::AlignedBox<Scalar, Dimension> BoundingBoxType;

  BatchMethodC(BoundingBoxType bbx, VectorType ray_origin)
    :min_start(bbx.min() - ray_origin),
     max_start(bbx.max() - ray_origin),
     ray_origin(ray_origin) {
  }
  template<typename PointCloudType>
  PointCloudType operator()(const PointCloudType& rays) const {
    PointCloudType ray_inv = rays.cwiseInverse();
    return (rays * (min_start.asDiagonal() * ray_inv).cwiseMax(max_start.asDiagonal() * ray_inv)
            .colwise().minCoeff().asDiagonal()
           ).colwise() + ray_origin;
  }

private:
  const VectorType min_start;
  const VectorType max_start;
  const VectorType ray_origin;
};

template <typename Scalar, int Dimension>
struct BatchMethodD {
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType;
  typedef Eigen::AlignedBox<Scalar, Dimension> BoundingBoxType;

  BatchMethodD(BoundingBoxType bbx, VectorType ray_origin)
    :min_start(bbx.min() - ray_origin),
     max_start(bbx.max() - ray_origin),
     ray_origin(ray_origin) {
  }
  template <typename PointCloudType>
  PointCloudType operator()(const PointCloudType& rays) const {
    PointCloudType intersections(rays.rows(), rays.cols());
    for(typename PointCloudType::Index i = 0; i < rays.cols(); ++i) {
      const VectorType& ray = rays.col(i);
      const VectorType ray_inv = ray.cwiseInverse();
      const VectorType tmin = min_start.cwiseProduct(ray_inv);
      const VectorType tmax = max_start.cwiseProduct(ray_inv);
      intersections.col(i) = ray_origin + ray * tmin.cwiseMax(tmax).minCoeff();
    }
    return intersections;
  }

private:
  const VectorType min_start;
  const VectorType max_start;
  const VectorType ray_origin;
};

template <typename Scalar, int Dimension>
struct BatchMethodE {
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType;
  typedef Eigen::AlignedBox<Scalar, Dimension> BoundingBoxType;

  BatchMethodE(BoundingBoxType bbx, VectorType ray_origin)
      : max((bbx.min() - ray_origin).cwiseMax(bbx.max() - ray_origin)),
        min((bbx.min() - ray_origin).cwiseMin(bbx.max() - ray_origin)),
        ray_origin(ray_origin) {
  }

  template <typename PointCloudType>
  PointCloudType operator()(const PointCloudType& rays) const {
    PointCloudType intersections = ray_origin.replicate(1, rays.cols());
    for(typename PointCloudType::Index i = 0; i < rays.cols(); ++i) {
      const VectorType& ray = rays.col(i);
      VectorType tMax = (ray.array() >= 0).select(max,min);
      intersections.col(i) += ray * tMax.cwiseQuotient(ray).minCoeff();
    }
    return intersections;
  }

private:
  const VectorType max;
  const VectorType min;
  const VectorType ray_origin;
};

template <typename Scalar, int Dimension>
struct BatchMethodF {
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType;
  typedef Eigen::AlignedBox<Scalar, Dimension> BoundingBoxType;

  BatchMethodF(BoundingBoxType bbx, VectorType ray_origin)
      : max((bbx.min() - ray_origin).cwiseMax(bbx.max() - ray_origin)),
        min((bbx.min() - ray_origin).cwiseMin(bbx.max() - ray_origin)),
        ray_origin(ray_origin) {
  }

  template <typename PointCloudType>
  PointCloudType operator()(const PointCloudType& rays) const {
//    PointCloudType tMax = (rays.array() >= 0).select(max.replicate(1, rays.cols()),
//                                                     min.replicate(1, rays.cols()));
//    PointCloudType intersections = rays * tMax.cwiseQuotient(rays).colwise().minCoeff().asDiagonal();
//    intersections.colwise() += ray_origin;
//    return intersections;
//
//    PointCloudType tMax = (rays.array() >= 0).select(max.replicate(1, rays.cols()),
//                                                     min.replicate(1, rays.cols()));
    return (rays
        * (rays.array() >= 0).select(max.replicate(1, rays.cols()), min.replicate(1, rays.cols()))
            .cwiseQuotient(rays).colwise().minCoeff().asDiagonal()).colwise() + ray_origin;
  }

private:
  const VectorType max;
  const VectorType min;
  const VectorType ray_origin;
};



template <typename Scalar, int Dimension>
struct InplaceBatchMethodA {
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType;
  typedef Eigen::AlignedBox<Scalar, Dimension> BoundingBoxType;

  InplaceBatchMethodA(BoundingBoxType bbx, VectorType ray_origin)
    :min_start(bbx.min() - ray_origin),
     max_start(bbx.max() - ray_origin),
     ray_origin(ray_origin) {
  }
  template <typename PointCloudType>
  void operator()(PointCloudType& rays) const {
    for(typename PointCloudType::Index i = 0; i < rays.cols(); ++i) {
      const VectorType ray_inv = rays.col(i).cwiseInverse();
      const VectorType tmin = min_start.cwiseProduct(ray_inv);
      const VectorType tmax = max_start.cwiseProduct(ray_inv);
      rays.col(i) = ray_origin + rays.col(i) * tmin.cwiseMax(tmax).minCoeff();
    }
  }

private:
  const VectorType min_start;
  const VectorType max_start;
  const VectorType ray_origin;
};

template <typename Scalar, int Dimension>
struct BatchMethodG {
  typedef Eigen::Matrix<Scalar, Dimension, 1> VectorType;
  typedef Eigen::Matrix<Scalar, Dimension, 2> MaxMinType;
  typedef Eigen::AlignedBox<Scalar, Dimension> BoundingBoxType;

  BatchMethodG(BoundingBoxType bbx, VectorType ray_origin)
    :max_min((MaxMinType() << bbx.min() - ray_origin, bbx.max() - ray_origin).finished()),
     ray_origin(ray_origin) {
  }
  template <typename PointCloudType>
  PointCloudType operator()(const PointCloudType& rays) {
    Eigen::Matrix<Scalar, 1, Eigen::Dynamic> temp(1, rays.cols());
    for(typename PointCloudType::Index i = 0; i< rays.cols(); ++i) {
      temp(i) = (rays.col(i).cwiseInverse().asDiagonal() * max_min).rowwise().maxCoeff().minCoeff();
    }
    return (rays * temp.asDiagonal()).colwise() + ray_origin;
  }

private:
  MaxMinType max_min;
  VectorType ray_origin;
};

template <typename Scalar, int Dimension>
struct BatchMethodH {
  typedef MethodD<Scalar, Dimension> ColFunctor;

  template<class BoundingBoxType, class VectorType>
  BatchMethodH(BoundingBoxType bbx, VectorType ray_origin)
    :col_functor(bbx, ray_origin) {
  }
  template <typename PointCloudType>
  PointCloudType operator()(const PointCloudType& rays) {
    PointCloudType intersections(rays.rows(), rays.cols());
    for(typename PointCloudType::Index i = 0; i< rays.cols(); ++i) {
      intersections.col(i) = col_functor(rays.col(i));
    }
    return intersections;
  }

private:
  ColFunctor col_functor;
};


template<typename T>
struct CheckScalarResult {
  CheckScalarResult(T expected)
      : expected_result(expected) {
  }
  void operator()(T result) const {
    if(result != expected_result)
      std::cout << "Error: " << result << " != " << expected_result << "\n";
  }
  T expected_result;
};

struct CheckRays {
  CheckRays(int sum)
      : expected_sum(sum) {
  }

  template <class V, class A>
  void operator()(const std::vector<V,A>& rays) const {
    double sum = 0;
    for(const V& ray : rays) {
      sum += ray.sum();
    }
    if(expected_sum != static_cast<int>(sum))
      std::cout << "Error: " << sum << " != " << expected_sum << "\n";
  }

  template <class EigenPointCloud>
  void operator()(const EigenPointCloud rays) const {
    double sum = rays.sum();
    if(expected_sum != static_cast<int>(sum))
      std::cout << "Error: " << sum << " != " << expected_sum << "\n";
  }

  int expected_sum;
};

int main(int argc, char **argv) {
  using namespace Naksha;

  std::cout << "\n# Benchmark of Naksha::computeAABBIntersectionWithInnerRay\n\n";

  Build::printBuildConfig();

  std::cout << "\n";

  typedef Eigen::Matrix< float, 3, Eigen::Dynamic, Eigen::RowMajor> RowMajorPointCloudF;
  typedef Eigen::Matrix< double, 3, Eigen::Dynamic, Eigen::RowMajor> RowMajorPointCloudD;

  typedef Eigen::Matrix< float, 3, Eigen::Dynamic, Eigen::ColMajor> ColMajorPointCloudF;
  typedef Eigen::Matrix< double, 3, Eigen::Dynamic, Eigen::ColMajor> ColMajorPointCloudD;

  typedef std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> > StdVectorofEigenVector3d;
  typedef std::vector<Eigen::Vector3f, Eigen::aligned_allocator<Eigen::Vector3f> > StdVectorofEigenVector3f;

  boost::mt19937 rng;
  boost::normal_distribution<> nd(0.0, 1.0);
  boost::variate_generator<boost::mt19937&, boost::normal_distribution<> > var_nor(rng, nd);

  const size_t num_of_rays = 10000000;

  StdVectorofEigenVector3d rays_std_vector_d;
  rays_std_vector_d.reserve(num_of_rays);
  for (size_t i = 0; i < num_of_rays; ++i) {
    rays_std_vector_d.push_back(Eigen::Vector3d(var_nor(), var_nor(), var_nor()).normalized());
  }

  StdVectorofEigenVector3f rays_std_vector_f;
  rays_std_vector_f.reserve(num_of_rays);
  for(StdVectorofEigenVector3d::const_iterator it = rays_std_vector_d.begin(); it != rays_std_vector_d.end(); ++it) {
    rays_std_vector_f.push_back(it->cast<float>());
  }

  RowMajorPointCloudD eigen_rays_rmd(3, rays_std_vector_d.size());
  for(RowMajorPointCloudD::Index i =0 ; i < eigen_rays_rmd.cols(); ++i) {
    eigen_rays_rmd.col(i) = rays_std_vector_d[i];
  }
  RowMajorPointCloudF eigen_rays_rmf = eigen_rays_rmd.cast<float>();

  ColMajorPointCloudD eigen_rays_cmd = eigen_rays_rmd;
  ColMajorPointCloudF eigen_rays_cmf = eigen_rays_cmd.cast<float>();

  CheckScalarResult<int> check_sum(37706);
  CheckRays check_intersections(37706);

  std::cout << "Sum Benchmark (double)          |          Time                                         \n";
  std::cout << "------------------------------- | ------------------------------------------------------\n";

  {
    check_sum(benchmarkSum<RayAABBIntersection>(rays_std_vector_d));
  }
  {
    check_sum(benchmarkSum<MethodB>(rays_std_vector_d));
  }
  {
    check_sum(benchmarkSum<MethodC>(rays_std_vector_d));
  }
  {
    check_sum(benchmarkSum<MethodD>(rays_std_vector_d));
  }
  {
    check_sum(benchmarkSum<MethodE>(rays_std_vector_d));
  }
  {
    check_sum(benchmarkSum<MethodF>(rays_std_vector_d));
  }

  std::cout << "\n";
  std::cout << "Sum Benchmark (float )          |          Time                                         \n";
  std::cout << "------------------------------- | ------------------------------------------------------\n";

  {
    check_sum(benchmarkSum<RayAABBIntersection>(rays_std_vector_f));
  }
  {
    check_sum(benchmarkSum<MethodB>(rays_std_vector_f));
  }
  {
    check_sum(benchmarkSum<MethodC>(rays_std_vector_f));
  }
  {
    check_sum(benchmarkSum<MethodD>(rays_std_vector_f));
  }
  {
    check_sum(benchmarkSum<MethodE>(rays_std_vector_f));
  }
  {
    check_sum(benchmarkSum<MethodF>(rays_std_vector_f));
  }


  std::cout << "\n\n";

  std::cout << "SumBenchmark (batch)            |          Time                                         \n";
  std::cout << "------------------------------- | ------------------------------------------------------\n";


  {
    check_sum(benchmarkSum<RayAABBIntersection>(eigen_rays_rmd));
  }
  {
    check_sum(benchmarkSum<RayAABBIntersection>(eigen_rays_cmd));
  }
  {
    check_sum(benchmarkSum<BatchMethodB>(eigen_rays_rmd));
  }
  {
    check_sum(benchmarkSum<BatchMethodB>(eigen_rays_cmd));
  }
//  {
//    check_sum(benchmarkSum<BatchMethodB>(eigen_rays_rmf));
//  }
//  {
//    check_sum(benchmarkSum<BatchMethodB>(eigen_rays_cmf));
//  }


  std::cout << "\n\n";
  std::cout << "Transform Benchmark (double)    |          Time                                         \n";
  std::cout << "------------------------------- | ------------------------------------------------------\n";

  {
    StdVectorofEigenVector3d rays = rays_std_vector_d;
    transform<RayAABBIntersection>(rays);
    check_intersections(rays);
  }
  {
    StdVectorofEigenVector3d rays = rays_std_vector_d;
    transform<MethodE>(rays);
    check_intersections(rays);
  }
  {
    StdVectorofEigenVector3d rays = rays_std_vector_d;
    transform<MethodF>(rays);
    check_intersections(rays);
  }

  std::cout << "\n";
  std::cout << "Transform Benchmark (float )    |          Time                                         \n";
  std::cout << "------------------------------- | ------------------------------------------------------\n";

  {
    StdVectorofEigenVector3f rays = rays_std_vector_f;
    transform<RayAABBIntersection>(rays);
    check_intersections(rays);
  }
  {
    StdVectorofEigenVector3f rays = rays_std_vector_f;
    transform<MethodE>(rays);
    check_intersections(rays);
  }
  {
    StdVectorofEigenVector3f rays = rays_std_vector_f;
    transform<MethodF>(rays);
    check_intersections(rays);
  }

  std::cout << "\n\n";
  std::cout << "Transform Benchmark (Batch)     |          Time                                         \n";
  std::cout << "------------------------------- | ------------------------------------------------------\n";
  {
    RowMajorPointCloudD rays = eigen_rays_rmd;
    transform<RayAABBIntersection>(rays);
    check_intersections(rays);
  }
//  {
//    ColMajorPointCloudD rays = eigen_rays_cmd;
//    transform<RayAABBIntersection>(rays);
//    check_intersections(rays);
//  }
  {
    RowMajorPointCloudD rays = eigen_rays_rmd;
    transform<BatchMethodB>(rays);
    check_intersections(rays);
  }
//  {
//    ColMajorPointCloudD rays = eigen_rays_cmd;
//    transform<BatchMethodB>(rays);
//    check_intersections(rays);
//  }

  {
    RowMajorPointCloudD rays = eigen_rays_rmd;
    transform<BatchMethodC>(rays);
    check_intersections(rays);
  }
//  {
//    ColMajorPointCloudD rays = eigen_rays_cmd;
//    transform<BatchMethodC>(rays);
//    check_intersections(rays);
//  }

  {
    RowMajorPointCloudD rays = eigen_rays_rmd;
    transform<BatchMethodD>(rays);
    check_intersections(rays);
  }
//  {
//    ColMajorPointCloudD rays = eigen_rays_cmd;
//    transform<BatchMethodD>(rays);
//    check_intersections(rays);
//  }

  {
    RowMajorPointCloudD rays = eigen_rays_rmd;
    transform<BatchMethodE>(rays);
    check_intersections(rays);
  }
//  {
//    ColMajorPointCloudD rays = eigen_rays_cmd;
//    transform<BatchMethodE>(rays);
//    check_intersections(rays);
//  }

  {
    RowMajorPointCloudD rays = eigen_rays_rmd;
    transform<BatchMethodF>(rays);
    check_intersections(rays);
  }
//  {
//    ColMajorPointCloudD rays = eigen_rays_cmd;
//    transform<BatchMethodF>(rays);
//    check_intersections(rays);
//  }

  {
    RowMajorPointCloudD rays = eigen_rays_rmd;
    transform<BatchMethodG>(rays);
    check_intersections(rays);
  }

  {
    RowMajorPointCloudD rays = eigen_rays_rmd;
    transform<BatchMethodH>(rays);
    check_intersections(rays);
  }

  {
    RowMajorPointCloudD rays = eigen_rays_rmd;
    transformInPlace<InplaceBatchMethodA>(rays);
    check_intersections(rays);
  }
  {
    RowMajorPointCloudD rays = eigen_rays_rmd;
    transformInPlace<RayAABBIntersectionInPlace>(rays);
    check_intersections(rays);
  }

  return EXIT_SUCCESS;
}
