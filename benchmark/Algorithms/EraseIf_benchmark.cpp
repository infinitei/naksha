/**
 * @file EraseIf_benchmark.cpp
 * @brief EraseIf_benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Algorithms/EraseIf.h"
#include "Naksha/Algorithms/BoundingBoxOperations.h"

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/MeasurementModel.h"

#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Common/BuildConfig.h"

#include <boost/timer/timer.hpp>
#include <boost/format.hpp>

template <class MapType>
void populateMap(MapType& map) {
  typedef Naksha::MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;

  const double range = 40.0;
  const double step = 0.5;

  for (double x = -range; x <= range; x += step)
    for (double y = -range; y <= range; y += step)
      for (double z = -range; z <= range; z += step)
        map.updateVoxel(Eigen::Vector3d(x, y, z), MeasurementModelType::HIT);
}

template <class MapType>
void doEraseVoxelsOutsideBbxBenchmark(MapType& map) {
  std::cout << boost::format("%-32s|") % PRETTY_TYPE(typename MapType::Datastructure, "Naksha", 0) << std::flush;

  typedef boost::timer::auto_cpu_timer TimerType;
  TimerType t;

  const double cull_range = 10.0;
  eraseVoxelsOutsideBbx(map, Eigen::Vector3d::Constant(-cull_range), Eigen::Vector3d::Constant(cull_range));
}

template<typename T>
struct CheckResult {
  CheckResult(T expected)
      : expected_result(expected) {
  }
  void operator()(T result) const {
    if(result != expected_result)
      std::cout << "Error: " << result << " != " << expected_result << "\n";
  }
  T expected_result;
};

int main(int argc, char **argv) {
  using namespace Naksha;

  std::cout << "\n# Benchmark of Naksha::EraseIf\n\n";

  Build::printBuildConfig();

  std::cout << "\n";

  std::cout << "MapType                         |          Time                                         \n";
  std::cout << "------------------------------- | ------------------------------------------------------\n";


  typedef DiscreteRandomVariable<9, float, LogProbabilityTag> RVLP9f;
  typedef ColoredRandomVariable<RVLP9f>::type CRVLP9f;
  CheckResult<std::size_t> check_map_size(68921);

  {
    typedef VolumetricMap<3, CRVLP9f, UnorderedMapDataStructure, HybridMappingInterface, MapTransform> MapType;
    MapType map(0.1);
    populateMap(map);
    doEraseVoxelsOutsideBbxBenchmark(map);
    check_map_size(map.size());
  }

  {
    typedef VolumetricMap<3, CRVLP9f, TreeDataStructure, HybridMappingInterface, MapTransform> MapType;
    MapType map(0.1);
    populateMap(map);
    doEraseVoxelsOutsideBbxBenchmark(map);
    check_map_size(map.size());
  }

  {
    typedef VolumetricMap<3, CRVLP9f, HashedVectorDataStructure, HybridMappingInterface, MapTransform> MapType;
    MapType map(0.1);
    populateMap(map);
    doEraseVoxelsOutsideBbxBenchmark(map);
    check_map_size(map.size());
  }

  return EXIT_SUCCESS;
}
