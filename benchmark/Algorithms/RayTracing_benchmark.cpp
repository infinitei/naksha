/**
 * @file RayTracing_benchmark.cpp
 * @brief RayTracing_benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Algorithms/RayTracing.h"
#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Maps/MapSerialization.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/timer/timer.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;

int main(int argc, char **argv) {

  typedef DiscreteRandomVariable<9, float> RV;
  typedef ColoredRandomVariable<RV>::type Payload;
  typedef VolumetricMap<3, Payload, TreeDataStructure, HybridMappingInterface, MapTransform> MapType;

  typedef MapType::KeyType KeyType;
  typedef std::vector<KeyType> KeyTypeContainer;

  MapType map(0.1);
  const Vector3d start_coord(0, 0, 0);
  const Vector3d end_coord(2000, 2000, 2000);
  const MapType::VectorType direction = (end_coord - start_coord).normalized();
  const KeyType start_key = map.keyFromCoord(start_coord);
  bool inside_map = map.maximumBbx().contains(end_coord);

  const RayMapBoundaryIntersection<MapType> find_key_at_map_boundary(map, start_coord);
  KeyType end_key = inside_map ? map.keyFromCoord(end_coord) : find_key_at_map_boundary(direction);

  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");

  cout << "MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") << "\n";
  cout << "Map Resolution: " << map.resolution() << "\n";
  cout << "Map Bounding Box: "
      << map.maximumBbx().min().format(vecfmt) << " <----> "
      << map.maximumBbx().max().format(vecfmt) << "\n";
  cout << "Shooting Ray from: "
      << start_coord.format(vecfmt) << " <----> "
      << end_coord.format(vecfmt) << "\n";
  cout << "EndPoint Inside Map Bbx= " << std::boolalpha << inside_map << "\n";


  const double ray_length = (end_coord - start_coord).norm();
  const size_t reserve_hint = static_cast<size_t>(2 * ray_length / map.resolution()); // 59998

  typedef boost::timer::auto_cpu_timer TimerType;

  const size_t num_of_trials = 20000;
  cout << "======== computeKeysOnRay Benchmarks with " << num_of_trials
      << " Iterations ========\n";

  {
    cout << "computeKeysOnRay: " << flush;
    TimerType t;

    for (size_t i = 0; i < num_of_trials; ++i) {
      KeyTypeContainer ray_keys;
      ray_keys.reserve(reserve_hint);
      computeKeysOnRay(map, start_coord, end_coord, ray_keys);
    }
  }

  cout << "===================================================================\n";

  {
    cout << "computeKeysOnRay: " << flush;
    TimerType t;

    for (size_t i = 0; i < num_of_trials; ++i) {
      KeyTypeContainer ray_keys;
      ray_keys.reserve(reserve_hint);
      computeKeysOnRay(map, start_coord, end_coord, ray_keys);
    }
  }
  cout << "===================================================================\n";
  cout << "===== Benchmarking computeKeysOnRay with precomputations ==========\n";
  cout << "===================================================================\n";

  {
    cout << "computeKeysOnRay: (with precomputations)" << flush;
    TimerType t;

    for (size_t i = 0; i < num_of_trials; ++i) {
      KeyTypeContainer ray_keys;
      ray_keys.reserve(reserve_hint);
      computeKeysOnRay(map, start_key, end_key, start_coord, direction, ray_keys);
    }
  }
  cout << "===================================================================\n";

  {
    cout << "computeKeysOnRay: (with precomputations)" << flush;
    TimerType t;

    for (size_t i = 0; i < num_of_trials; ++i) {
      KeyTypeContainer ray_keys;
      ray_keys.reserve(reserve_hint);
      computeKeysOnRay(map, start_key, end_key, start_coord, direction, ray_keys);
    }
  }
  cout << "===================================================================\n";

  return EXIT_SUCCESS;
}

