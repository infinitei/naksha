/**
 * @file insertPointCloud_benchmark.cpp
 * @brief insertPointCloud_benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Algorithms/InsertMeasurements.h"
#include "Naksha/Algorithms/InsertMeasurementsParallel.h"
#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Common/EigenTypedefs.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/timer/timer.hpp>

struct SerialInsertion {
  template<class MapType, class PointCloudType>
  void operator()(MapType& map, const PointCloudType& point_cloud) {
    insertPointCloud(map, point_cloud);
  }
};

struct ParallelInsertion {
  template<class MapType, class PointCloudType>
  void operator()(MapType& map, const PointCloudType& point_cloud) {
    insertPointCloudParallel(map, point_cloud);
  }
};


template<class MapType, typename F, class PointCloudType>
void doInsertPointCloudBenchmark(const PointCloudType& point_cloud, F func) {
  typedef boost::timer::auto_cpu_timer TimerType;
  std::cout << "\n================== " << PRETTY_TYPE_INFO(F) << " =======================\n";
  std::cout << "MapType: " << PRETTY_TYPE(MapType, "Naksha", 2) << "\n";
  MapType map(0.05);
  map.setMaximumBbx(Eigen::Vector3d(5, -5, -20), Eigen::Vector3d(15, 5, 20));
  {
    std::cout << "insertPointCloud: " << std::flush;
    TimerType t;
    func(map, point_cloud);
  }
  std::cout << "Number of voxel = " << map.size() << "\n";
}

int main(int argc, char **argv) {
  using namespace Naksha;

  // Create point cloud
  typedef EigenAligned<Eigen::Vector3d>::std_vector PointCloudType;
  const int width = 20;
  const int xoffset = 20;
  PointCloudType point_cloud;
  for (double x = xoffset - width; x < (xoffset + width); x += 0.08)
    for (double y = -width; y < width; y += 0.07)
      for (double z = -10; z < 10; z += 0.05) {
        point_cloud.push_back(Eigen::Vector3d(x, y, z));
      }
  std::cout << "Created PointCloud of Size = " << point_cloud.size() << "\n";


  typedef DiscreteRandomVariable<2, double, LogOddsTag, BinaryRandomVariable> OccupancyRV;

  {
    typedef VolumetricMap<3, OccupancyRV, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doInsertPointCloudBenchmark<MapType>(point_cloud, SerialInsertion());
  }

  {
    typedef VolumetricMap<3, OccupancyRV, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doInsertPointCloudBenchmark<MapType>(point_cloud, SerialInsertion());
  }

  {
    typedef VolumetricMap<3, OccupancyRV, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doInsertPointCloudBenchmark<MapType>(point_cloud, SerialInsertion());
  }

  {
    typedef VolumetricMap<3, OccupancyRV, ConcurrentUnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doInsertPointCloudBenchmark<MapType>(point_cloud, SerialInsertion());
  }

  {
    typedef VolumetricMap<3, OccupancyRV, ConcurrentUnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doInsertPointCloudBenchmark<MapType>(point_cloud, ParallelInsertion());
  }

}
