/**
 * @file InsertDepthScanReal_benchmark.cpp
 * @brief InsertDepthScan benchmark with real world data
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Algorithms/InsertMeasurements.h"
#include "Naksha/Algorithms/InsertMeasurementsParallel.h"
#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Maps/MapSerialization.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Payload/LabelToMeasurementMapping.h"
#include "Naksha/Common/EigenTypedefs.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/timer/timer.hpp>

template <class PointType>
std::vector<PointType> readVelodyne(const std::string& file_name) {
  std::vector<PointType> point_cloud;

  // allocate 4 MB buffer (only ~130*4*4 KB are needed)
  int32_t num = 1000000;
  float *data = (float*)malloc(num*sizeof(float));

  // pointers
  float *px = data+0;
  float *py = data+1;
  float *pz = data+2;
  float *pr = data+3;

  // load point cloud
  FILE *stream;
  stream = fopen (file_name.c_str(),"rb");
  num = fread(data,sizeof(float),num,stream)/4;

  point_cloud.reserve(num);
  for (int32_t i=0; i<num; i++) {
    point_cloud.push_back(PointType(*px,*py,*pz));
    px+=4; py+=4; pz+=4; pr+=4;
  }
  fclose(stream);

  return point_cloud;
}

struct SerialInsertDepthScan {
  template<class MapType, class PointType, class PointCloudType, class MeasurementType>
  void operator()(MapType& map, const PointType& origin, const PointCloudType& point_cloud, const MeasurementType& measurement) {
    insertDepthScan(map, origin, point_cloud, measurement);
  }
};

struct ParallelInsertDepthScan {
  template<class MapType, class PointType, class PointCloudType, class MeasurementType>
  void operator()(MapType& map, const PointType& origin, const PointCloudType& point_cloud, const MeasurementType& measurement) {
    insertDepthScanParallel(map, origin, point_cloud, measurement);
  }
};

template<class MapType, class PointCloudType, class MeasurementType, typename Func>
void doInsertDepthScanBenchmark(MapType& map, const PointCloudType& point_cloud, const MeasurementType& measurement, Func fn) {
  typedef boost::timer::auto_cpu_timer TimerType;
  std::cout << "\n===================================================================\n";
  std::cout << "MapType: " << PRETTY_TYPE(MapType, "Naksha", 2) << "\n";
  {
    std::cout << PRETTY_TYPE_INFO(Func) << " : " << std::flush;
    TimerType t;
    fn(map, Eigen::Vector3d::Zero(), point_cloud, measurement);
  }

  std::cout << "Number of voxel = " << map.size() << "\n";
}

using namespace Naksha;

int main(int argc, char **argv) {

  if (argc != 2) {
    std::cout << "ERROR: pointcloud (bin) file not provided" << "\n";
    std::cout << "Usage: " << argv[0] << " /path/to/velo/file";
    std::cout << "\n";
    return EXIT_FAILURE;
  }


  std::cout << "===================================================================\n";
  std::cout << "================InsertMeasurements_benchmark==================\n";

  typedef DiscreteRandomVariable<9, float> RV;
  typedef ColoredRandomVariable<RV>::type Payload;
  typedef MeasurementMap<RV> Measurement;
  typedef EigenAligned<Eigen::Vector3d>::std_vector PointCloudType;

  // Create point cloud
  PointCloudType point_cloud;
  {
    std::vector<Eigen::Vector3d> velo_pc = readVelodyne<Eigen::Vector3d>(argv[1]);
    for (const Eigen::Vector3d& point : velo_pc) {
      if (point.x() < 4)
        continue;

      // remove velodyne points which are way below
      if (point.z() < -2.0)
        continue;

      point_cloud.push_back(point);
    }
  }
  std::cout << "Created PointCloud of Size = " << point_cloud.size() << "\n";

  const double resolution = 0.04;
  const std::size_t voxel_reserve_size = 8000000;

  {
    typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(resolution);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>(), SerialInsertDepthScan());
  }

  {
    typedef VolumetricMap<3, Payload, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(resolution);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>(), SerialInsertDepthScan());
  }

  {
    typedef VolumetricMap<3, Payload, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(resolution);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>(), SerialInsertDepthScan());
  }

  {
    typedef VolumetricMap<3, Payload, ConcurrentUnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(resolution);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>(), ParallelInsertDepthScan());
  }

  std::cout << "\n==================== With Change Detection =======================\n";

  {
    typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, HybridMappingInterface, MapTransform> MapType;
    MapType map(resolution);
    map.enableChangeDetection();
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>(), SerialInsertDepthScan());
  }

  {
    typedef VolumetricMap<3, Payload, TreeDataStructure, HybridMappingInterface, MapTransform> MapType;
    MapType map(resolution);
    map.enableChangeDetection();
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>(), SerialInsertDepthScan());
  }

  {
    typedef VolumetricMap<3, Payload, HashedVectorDataStructure, HybridMappingInterface, MapTransform> MapType;
    MapType map(resolution);
    map.enableChangeDetection();
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>(), SerialInsertDepthScan());
  }

  {
    typedef VolumetricMap<3, Payload, ConcurrentUnorderedMapDataStructure, HybridMappingInterface, MapTransform> MapType;
    MapType map(resolution);
    map.enableChangeDetection();
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>(), SerialInsertDepthScan());
  }

  std::cout << "\n==================== With Reserve call =======================\n";

  {
    typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(resolution);
    map.reserve(voxel_reserve_size);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>(), SerialInsertDepthScan());
  }

  {
    typedef VolumetricMap<3, Payload, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(resolution);
    map.reserve(voxel_reserve_size);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>(), SerialInsertDepthScan());
  }

  {
    typedef VolumetricMap<3, Payload, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(resolution);
    map.reserve(voxel_reserve_size);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>(), SerialInsertDepthScan());
  }

  {
    typedef VolumetricMap<3, Payload, ConcurrentUnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    MapType map(resolution);
    map.reserve(voxel_reserve_size);
    doInsertDepthScanBenchmark(map, point_cloud, Measurement::at<ImageLabels::ROAD>(), ParallelInsertDepthScan());
  }

  return EXIT_SUCCESS;
}

