/**
 * @file RayCasting_benchmark.cpp
 * @brief RayCasting_benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Algorithms/RayCasting.h"
#include "Naksha/Algorithms/RayTracing.h"
#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Maps/MapSerialization.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/timer/timer.hpp>

template<class MapType>
void doRayCastingBenchmark() {
  std::cout << "\n## MapType: " << PRETTY_TYPE(MapType, "Naksha", 2) << "\n";

  typedef typename MapType::KeyType KeyType;
  typedef std::vector<KeyType> KeyTypeContainer;
  typedef Naksha::MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;
  typedef boost::timer::auto_cpu_timer TimerType;

  MapType map(0.1);
  const Eigen::Vector3d start_coord(0, 0, 0);
  const Eigen::Vector3d end_coord(2000, 2000, 2000);

  KeyTypeContainer ray_keys;
  ray_keys.reserve(100000);
  Naksha::computeKeysOnRay(map, start_coord, end_coord, ray_keys);

  if (ray_keys.size() < 2) {
    std::cout << "Too small ray. Not going to do RayCasting Benchmarks \n";
    return;
  }

  for (size_t i = 0; i < ray_keys.size() - 1; ++i)
    map.updateVoxel(ray_keys[i], MeasurementModelType::MISS);
  map.updateVoxel(ray_keys.back(), MeasurementModelType::HIT);

  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
  const size_t num_of_trials = 1000;
  std::cout << "### num_of_trials = " << num_of_trials
            << " start = " <<  start_coord.format(vecfmt)
            << " end = " <<  end_coord.format(vecfmt)
            << " ray_keys.size() = " <<  ray_keys.size()
            << " map.size() = " <<  map.size() << "\n";

  std::cout << "  Method                                        |   Time                                                \n";
  std::cout << "----------------------------------------------- | ------------------------------------------------------\n";

  {
    std::cout << "ignore_unknown_voxels = true; maxranged = none" << "\t|" << std::flush;
    TimerType t;

    Naksha::RayCasting<MapType, true> ray_caster(&map);

    for (size_t i = 0; i < num_of_trials; ++i) {
      ray_caster(start_coord, end_coord - start_coord);
    }
  }

  {
    std::cout << "ignore_unknown_voxels = false; maxranged = none" << "\t|" << std::flush;
    TimerType t;

    Naksha::RayCasting<MapType, false> ray_caster(&map);

    for (size_t i = 0; i < num_of_trials; ++i) {
      ray_caster(start_coord, end_coord - start_coord);
    }
  }

  const double max_range = (end_coord - start_coord).norm();

  {
    std::cout << "ignore_unknown_voxels = true; maxranged = true" << "\t|" << std::flush;
    TimerType t;

    Naksha::RayCasting<MapType, true> ray_caster(&map);

    for (size_t i = 0; i < num_of_trials; ++i) {
      ray_caster(start_coord, end_coord - start_coord, max_range);
    }
  }

  {
    std::cout << "ignore_unknown_voxels = false; maxranged = true" << "\t|" << std::flush;
    TimerType t;

    Naksha::RayCasting<MapType, false> ray_caster(&map);

    for (size_t i = 0; i < num_of_trials; ++i) {
      ray_caster(start_coord, end_coord - start_coord, max_range);
    }
  }
}

using namespace Naksha;
using namespace Eigen;
using namespace std;

int main(int argc, char **argv) {

  std::cout << "\n# RayCasting Benchmark for different MapTypes\n\n";

  typedef DiscreteRandomVariable<9, float> RV;
  typedef ColoredRandomVariable<RV>::type Payload;
  typedef VolumetricMap<3, Payload, TreeDataStructure, HybridMappingInterface, MapTransform> OctreeMapType;
  typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, HybridMappingInterface, MapTransform> UnorderedMapType;
  typedef VolumetricMap<3, Payload, HashedVectorDataStructure, HybridMappingInterface, MapTransform> HashedVectorMapType;

  doRayCastingBenchmark<OctreeMapType>();
  doRayCastingBenchmark<UnorderedMapType>();
  doRayCastingBenchmark<HashedVectorMapType>();


  return EXIT_SUCCESS;
}
