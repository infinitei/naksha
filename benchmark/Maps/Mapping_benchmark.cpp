/**
 * @file Mapping_benchmark.cpp
 * @brief Mapping_benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Maps/MapSerialization.h"
#include "Naksha/Nodes/NodeUtils.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Keys/DenseKeyIterator.h"
#include "Naksha/Payload/MeasurementModel.h"

#include <boost/timer/timer.hpp>

template<class MapType>
void doNakshaMapping(MapType& map) {
  std::cout << " doNakshaMapping: " << std::flush;
  typedef boost::timer::auto_cpu_timer TimerType;
  TimerType t;

  using namespace Naksha;
  const int occ_span = 150;
  const int free_span = 100;

  typedef typename MapType::Payload RV;
  typedef BinaryMeasurementModel<RV> MeasModel;


  for (int x = -occ_span; x < occ_span; ++x) {
    for (int y = -occ_span; y < occ_span; ++y) {
      for (int z = -occ_span; z < occ_span; ++z) {
        const Eigen::Vector3f endpoint((float) x * 0.05f, (float) y * 0.05f,
            (float) z * 0.05f);
        map.updateVoxel(endpoint, MeasModel::HIT); // integrate 'occupied' measurement
      }
    }
  }

  // insert some measurements of free cells

  for (int x = -free_span; x < free_span; ++x) {
    for (int y = -free_span; y < free_span; ++y) {
      for (int z = -free_span; z < free_span; ++z) {
        const Eigen::Vector3f endpoint((float) x * 0.02f - 1.0f, (float) y * 0.02f - 1.0f,
            (float) z * 0.02f - 1.0f);
        map.updateVoxel(endpoint, MeasModel::MISS); // integrate 'free' measurement
      }
    }
  }
}


template<class MapType>
void doDumbMapping(MapType& map) {
  std::cout << " doDumbMapping: " << std::flush;
  typedef boost::timer::auto_cpu_timer TimerType;
  TimerType t;

  typedef typename MapType::KeyType KeyType;
  typedef Naksha::MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;

  for (const KeyType& key : DenseKeyRange(KeyType(0, 0, 0), KeyType (200, 200, 200))) {
    map.updateVoxel(key, MeasurementModelType::HIT);
  }
  for (const KeyType& key : DenseKeyRange(KeyType(200, 200, 200), KeyType (400, 400, 300))) {
    map.updateVoxel(key, MeasurementModelType::MISS);
  }

}

struct CheckResult {
  CheckResult(const std::size_t expected_value)
      : expected(expected_value) {
  }
  void operator()(const std::size_t x) const {
    if (x != expected) {
      std::cout << "Error! Output is not sane. "
          << x << " != " << expected << " (expected)" << "\n";
    }
  }
  const std::size_t expected;
};

template<class MapType>
std::size_t numOfSolidVoxels(const MapType& map) {
  std::cout << " numOfSolidVoxels: " << std::flush;
  typedef boost::timer::auto_cpu_timer TimerType;
  TimerType t;
  typedef typename MapType::VoxelNodeType VoxelNodeType;
  return std::count_if(map.begin(), map.end(), Naksha::IsNodeOccupied<VoxelNodeType>());
}

template<class MapType>
void doMappingBenchmak() {
  using std::cout;
  using std::flush;

  cout << "\n===================================================================\n";
  cout << "MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") << "\n";

  CheckResult check_naksha_mapping_result(3375000);
  CheckResult check_dumb_mapping_result(12000000);
  CheckResult check_solid_voxel_count(8000000);

  {
    MapType map(0.1);
    doNakshaMapping(map);
    check_naksha_mapping_result(map.numberOfVoxels());
  }

  {
    MapType map(0.1);
    doDumbMapping(map);
    check_dumb_mapping_result(map.numberOfVoxels());

    std::size_t solid_voxel_count = numOfSolidVoxels(map);
    check_solid_voxel_count(solid_voxel_count);
  }

  {
    MapType map(0.1);
    map.reserve(check_naksha_mapping_result.expected);
    doNakshaMapping(map);
    check_naksha_mapping_result(map.numberOfVoxels());
  }

  {
    MapType map(0.1);
    map.reserve(check_dumb_mapping_result.expected);
    doDumbMapping(map);
    check_dumb_mapping_result(map.numberOfVoxels());
  }

}



using namespace Eigen;
using namespace std;
using namespace Naksha;

int main(int argc, char **argv) {

  {
    typedef DiscreteRandomVariable<2, float> RV;
    typedef VolumetricMap<3, RV, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doMappingBenchmak<MapType>();
  }

  {
    typedef DiscreteRandomVariable<2, float> RV;
    typedef VolumetricMap<3, RV, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doMappingBenchmak<MapType>();
  }

  {
    typedef DiscreteRandomVariable<2, float> RV;
    typedef VolumetricMap<3, RV, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doMappingBenchmak<MapType>();
  }

  {
    typedef DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> RV;
    typedef VolumetricMap<3, RV, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doMappingBenchmak<MapType>();
  }

  {
    typedef DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> RV;
    typedef VolumetricMap<3, RV, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doMappingBenchmak<MapType>();
  }

  {
    typedef DiscreteRandomVariable<2, float, LogOddsTag, BinaryRandomVariable> RV;
    typedef VolumetricMap<3, RV, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doMappingBenchmak<MapType>();
  }
}

