/**
 * @file VoxelChangeDetector_benchmark.cpp
 * @brief VoxelChangeDetector benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapMaker.h"

#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Keys/DenseKeyIterator.h"

#include <boost/timer/timer.hpp>

struct EmptyPayload {
};

template<class MapType>
void doChangeDetection() {
  typedef typename MapType::KeyType KeyType;
  typedef typename MapType::VoxelNodeType VoxelNodeType;

  std::cout << "MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") << "\n";
  std::cout << "NodeType: " << PRETTY_TYPE_INFO_REMOVE_NS(VoxelNodeType, "Naksha") << "\n";
  std::cout << "Sizeof(NodeType) =  " << sizeof(VoxelNodeType) << "\n";
  std::cout << "Sizeof(EmptyPayload) =  " << sizeof(EmptyPayload) << "\n";

  MapType map(0.1);
  typedef boost::timer::auto_cpu_timer TimerType;
  {
    TimerType t;
    map.enableChangeDetection();

    for (const KeyType& key : DenseKeyRange(KeyType(0,0,0), KeyType(300,300,300))) {
      map.insertNewVoxel(key);
    }
    if(map.changedKeys().size() != (std::pow(301, 3) -1) )
      std::cout << "Changed Keys Size =  " << map.changedKeys().size() << "\n";

    map.resetChangeDetection();

    for (const KeyType& key : DenseKeyRange(KeyType(0,0,0), KeyType(100,100,100))) {
      map.insertNewVoxel(key);
    }
    if(map.changedKeys().size() != 0)
        std::cout << "Changed Keys Size =  " << map.changedKeys().size() << "\n";


    for (const KeyType& key : DenseKeyRange(KeyType(400,400,400), KeyType(410,410,410))) {
      map.insertNewVoxel(key);
    }
    if (map.changedKeys().size() != (std::pow(11, 3) -1))
      std::cout << "Changed Keys Size =  " << map.changedKeys().size() << "\n";
  }
  std::cout << "Map Size = " << map.size() << "\n";
  std::cout << "-----------------------------------------------------------------\n";
}

using namespace Naksha;

int main(int argc, char **argv) {
  typedef VolumetricMap<3, EmptyPayload, TreeDataStructure, HybridMappingInterface,
      MapTransform, OccupancyNodeInterface> OctreeVoxelChangeDetector;
  typedef VolumetricMap<3, EmptyPayload, UnorderedMapDataStructure, HybridMappingInterface,
      MapTransform, OccupancyNodeInterface> UnorderedMapVoxelChangeDetector;

  doChangeDetection<UnorderedMapVoxelChangeDetector>();
  doChangeDetection<OctreeVoxelChangeDetector>();

  return EXIT_SUCCESS;
}
