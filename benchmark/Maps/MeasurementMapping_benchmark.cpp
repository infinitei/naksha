/**
 * @file MeasurementMapping_benchmark.cpp
 * @brief Measurement Mapping benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Payload/LabelToMeasurementMapping.h"
#include "Naksha/Maps/VolumetricMap.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Maps/UnorderedMapDataStructure.h"
#include "Naksha/Maps/HybridMappingInterface.h"
#include "Naksha/Maps/MapTransform.h"
#include "Naksha/Common/SerializationHelper.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/progress.hpp>


template<class MapType>
void doNakshaMapping(MapType& map) {
  using namespace Naksha;
  const int occ_span = 80;
  const int free_span = 100;

  typedef typename MapType::Payload::RVType RV;

  typedef HybridMeasurementModel<RV> MeasModel;

  for (int x = -occ_span; x < occ_span; ++x) {
    for (int y = -occ_span; y < occ_span; ++y) {
      for (int z = -occ_span; z < occ_span; ++z) {
        const Eigen::Vector3f endpoint((float) x * 0.05f, (float) y * 0.05f,
            (float) z * 0.05f);
        map.updateVoxel(endpoint, MeasModel::HIT); // integrate 'occupied' measurement
      }
    }
  }

  // insert some measurements of free cells

  for (int x = -free_span; x < free_span; ++x) {
    for (int y = -free_span; y < free_span; ++y) {
      for (int z = -free_span; z < free_span; ++z) {
        const Eigen::Vector3f endpoint((float) x * 0.02f - 1.0f, (float) y * 0.02f - 1.0f,
            (float) z * 0.02f - 1.0f);
        map.updateVoxel(endpoint, MeasModel::MISS); // integrate 'free' measurement
        map.updateVoxel(endpoint, MeasModel::SEMANTIC_HIT[0]); // integrate 'free' measurement
      }
    }
  }
}


template<class MapType>
void doNakshaMapping2(MapType& map) {
  using namespace Naksha;
  const int occ_span = 80;
  const int free_span = 100;

  typedef typename MapType::Payload::RVType RV;

  typedef MeasurementMap<RV> Measurement;

  for (int x = -occ_span; x < occ_span; ++x) {
    for (int y = -occ_span; y < occ_span; ++y) {
      for (int z = -occ_span; z < occ_span; ++z) {
        const Eigen::Vector3f endpoint((float) x * 0.05f, (float) y * 0.05f,
            (float) z * 0.05f);
        map.updateVoxel(endpoint, Measurement::template at<ImageLabels::ROAD>() ); // integrate 'occupied' measurement
      }
    }
  }

  // insert some measurements of free cells

  for (int x = -free_span; x < free_span; ++x) {
    for (int y = -free_span; y < free_span; ++y) {
      for (int z = -free_span; z < free_span; ++z) {
        const Eigen::Vector3f endpoint((float) x * 0.02f - 1.0f, (float) y * 0.02f - 1.0f,
            (float) z * 0.02f - 1.0f);
        map.updateVoxel(endpoint, Measurement::template at<ImageLabels::SKY>()); // integrate 'free' measurement
        map.updateVoxel(endpoint, Measurement::template at<ImageLabels::ROAD>()); // integrate 'free' measurement
      }
    }
  }
}

using namespace Eigen;
using namespace std;
using namespace Naksha;


int main(int argc, char **argv) {
  typedef DiscreteRandomVariable<9, float> RV;
  typedef VolumetricMap<3, RV, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;

  {
    cout << "===================================================================" << endl;
    cout << "MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") << endl;
    cout << "VoxelNodeType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType::VoxelNodeType, "Naksha") << endl;

    MapType map(0.1);
    {
      cout << "updateVoxels:" << endl;
      boost::progress_timer t;
      doNakshaMapping2(map);
    }

  }

  {
    cout << "===================================================================" << endl;
    cout << "MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") << endl;
    cout << "VoxelNodeType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType::VoxelNodeType, "Naksha") << endl;

    MapType map(0.1);
    {
      cout << "updateVoxels:" << endl;
      boost::progress_timer t;
      doNakshaMapping(map);
    }
  }

}

