/**
 * @file VoxelCenter_becnchmark.cpp
 * @brief Benchmark for computing VoxelCenter i.e. key --> coord conversions
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Keys/DenseKeyIterator.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include <boost/timer/timer.hpp>
#include <boost/iterator/transform_iterator.hpp>
#include <numeric>

typedef boost::timer::auto_cpu_timer TimerType;

template<typename KeyType, typename Scalar>
class VoxelCenterFunctor {
public:
  typedef typename KeyType::Scalar KeyScalar;
  typedef Eigen::Matrix<Scalar, 3, 1> VectorType;

  typedef VectorType result_type;

  VoxelCenterFunctor(const Scalar resolution)
      : resolution_(resolution),
        max_key_radius_(std::numeric_limits<KeyScalar>::max() / 2 + 1) {
  }

  VectorType operator()(KeyType key) const {
    typedef typename VectorType::Scalar TScalar;
    return ((key.value.template cast<int>().array() - max_key_radius_).template cast<TScalar>().array() + 0.5) * resolution_;
  }

 private:
  const Scalar resolution_;
  const int max_key_radius_;
};

template <typename Scalar, class MapType>
Scalar doVoxelCenterTask(const MapType& map, const std::vector<typename MapType::KeyType>& keys) {

  typedef typename MapType::Transform TransformPolicy;
  typedef typename MapType::KeyType KeyType;
  typedef Eigen::Matrix<Scalar, 3, 1> VectorType;

  std::cout << "===================================================================\n";
  std::cout << "doVoxelCenterTask: " << "\n";
  std::cout << " TransformPolicy: " << PRETTY_TYPE_INFO_REMOVE_NS(TransformPolicy, "Naksha") << "\n";
  std::cout << " CoordScalar: " << PRETTY_TYPE_INFO(Scalar) << "\n";

  TimerType t;
  VectorType acc (VectorType::Zero());
  for (const KeyType& key : keys) {
    VectorType center = map.template voxelCenter<VectorType>(key);
    acc += center;
  }
  return acc.sum();
}

template<typename KeyType, typename VectorType>
struct UnaryFunctor {
  typedef VectorType result_type;
  VectorType operator()(KeyType key) const {
    return VectorType::Zero();
  }
};

template <
  typename Scalar,
  template <typename, typename> class Functor,
  class MapType
  >
Scalar doVoxelCenterTaskWithFunctor(const MapType& map, const std::vector<typename MapType::KeyType>& keys) {

  typedef typename MapType::KeyType KeyType;
  typedef Eigen::Matrix<Scalar, 3, 1> VectorType;
  typedef Functor<KeyType, Scalar> FunctorType;

  std::cout << "===================================================================\n";
  std::cout << "doVoxelCenterTaskWithFunctor: " << "\n";
  std::cout << " FunctorType: " << PRETTY_TYPE_INFO_REMOVE_NS(FunctorType, "Naksha") << "\n";
  std::cout << " CoordScalar: " << PRETTY_TYPE_INFO(Scalar) << "\n";

  TimerType t;

  FunctorType voxel_center_functor(map.resolution());

  VectorType acc = std::accumulate(
      boost::make_transform_iterator(keys.begin(), voxel_center_functor),
      boost::make_transform_iterator(keys.end(), voxel_center_functor),
      VectorType::Zero().eval());

  return acc.sum();
}


template <typename Scalar, class MapType>
std::size_t doVoxelCenterTransformTask(const MapType& map, const std::vector<typename MapType::KeyType>& keys) {

  typedef typename MapType::Transform TransformPolicy;
  typedef typename MapType::KeyType KeyType;
  typedef Eigen::Matrix<Scalar, 3, 1> VectorType;

  std::cout << "===================================================================\n";
  std::cout << "doVoxelCenterTransformTask: " << "\n";
  std::cout << " TransformPolicy: " << PRETTY_TYPE_INFO_REMOVE_NS(TransformPolicy, "Naksha") << "\n";
  std::cout << " CoordScalar: " << PRETTY_TYPE_INFO(Scalar) << "\n";


  std::vector<VectorType> centers;
  centers.reserve(keys.size());

  TimerType t;

  for (const KeyType& key : keys) {
    centers.push_back(map.template voxelCenter<VectorType>(key));
  }

  return centers.size();
}

template <class MapType>
std::size_t doVoxelCenterTransformTaskWithMapTransFormFunctor(const MapType& map, const std::vector<typename MapType::KeyType>& keys) {

  typedef typename MapType::Transform TransformPolicy;
  typedef typename TransformPolicy::KeyCoordTransformerType FunctorType;
  typedef typename FunctorType::VectorType VectorType;

  std::cout << "===================================================================\n";
  std::cout << "doVoxelCenterTransformTaskWithMapTransFormFunctor: " << "\n";
  std::cout << " TransformPolicy: " << PRETTY_TYPE_INFO_REMOVE_NS(TransformPolicy, "Naksha") << "\n";
  std::cout << " FunctorType: " << PRETTY_TYPE_INFO_REMOVE_NS(FunctorType, "Naksha") << "\n";
  std::cout << " VectorType: " << PRETTY_TYPE_INFO(VectorType) << "\n";


  std::vector<VectorType> centers;
  centers.reserve(keys.size());

  TimerType t;

  const FunctorType map_transform_functor = map.keyCoordTransformer();

  std::transform(keys.begin(), keys.end(), std::back_inserter(centers), map_transform_functor);

  return centers.size();
}

template <
  typename Scalar,
  template <typename, typename> class Functor,
  class MapType
  >
std::size_t doVoxelCenterTransformTaskWithFunctor(const MapType& map, const std::vector<typename MapType::KeyType>& keys) {

  typedef typename MapType::KeyType KeyType;
  typedef Eigen::Matrix<Scalar, 3, 1> VectorType;
  typedef Functor<KeyType, Scalar> FunctorType;

  std::cout << "===================================================================\n";
  std::cout << "doVoxelCenterTransformTaskWithFunctor: " << "\n";
  std::cout << " FunctorType: " << PRETTY_TYPE_INFO_REMOVE_NS(FunctorType, "Naksha") << "\n";
  std::cout << " CoordScalar: " << PRETTY_TYPE_INFO(Scalar) << "\n";


  std::vector<VectorType> centers;
  centers.reserve(keys.size());

  TimerType t;

  FunctorType voxel_center_functor(map.resolution());

  std::transform(keys.begin(), keys.end(), std::back_inserter(centers), voxel_center_functor);

  return centers.size();
}




int main(int argc, char **argv) {
  using namespace Naksha;
  using std::cout;
  using std::endl;
  using std::flush;

  typedef DiscreteRandomVariable<3, double, ProbabilityTag> RV;
  typedef VolumetricMap<3, RV, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
  typedef MapType::KeyType KeyType;

  MapType map(0.1);
  std::vector<KeyType> keys;

  {
    cout << "Creating Keys: " << flush;
    KeyType begin(0, 0, 0);
    KeyType end(1000, 1000, 1000);
    for (const KeyType& key : DenseKeyRange(begin, end)) {
      keys.push_back(key);
    }
    cout << "Done. Created " << keys.size() << " keys." << endl;
  }

  cout << "\n-------------------- VoxelCenter Tasks ----------------------\n";

  {
    typedef float Scalar;
    Scalar acc = doVoxelCenterTask<Scalar>(map, keys);
    cout << "acc = " << acc << endl;
  }

  {
    typedef double Scalar;
    Scalar acc = doVoxelCenterTask<Scalar>(map, keys);
    cout << "acc = " << acc << endl;
  }

  {
    typedef float Scalar;
    Scalar acc = doVoxelCenterTaskWithFunctor<Scalar, VoxelCenterFunctor>(map, keys);
    cout << "acc = " << acc << endl;
  }

  {
    typedef double Scalar;
    Scalar acc = doVoxelCenterTaskWithFunctor<Scalar, VoxelCenterFunctor>(map, keys);
    cout << "acc = " << acc << endl;
  }

#ifdef BOOST_RESULT_OF_USE_DECLTYPE
  {
    typedef float Scalar;
    Scalar acc = doVoxelCenterTaskWithFunctor<Scalar, KeyCoordTransformer>(map, keys);
    cout << "acc = " << acc << endl;
  }

  {
    typedef double Scalar;
    Scalar acc = doVoxelCenterTaskWithFunctor<Scalar, KeyCoordTransformer>(map, keys);
    cout << "acc = " << acc << endl;
  }
#endif
  cout << "\n--------------------- Transform Tasks (float) ----------------------\n";


  {
    std::size_t res = doVoxelCenterTransformTask<float>(map, keys);
    cout << "res = " << res << endl;
  }

  {
    std::size_t res = doVoxelCenterTransformTaskWithFunctor<float, VoxelCenterFunctor>(map, keys);
    cout << "res = " << res << endl;
  }

  {
    std::size_t res = doVoxelCenterTransformTaskWithFunctor<float, KeyCoordTransformer>(map, keys);
    cout << "res = " << res << endl;
  }

  cout << "\n-------------------- Transform Tasks (double) ----------------------\n";
  keys.resize(300000000);

  {
    std::size_t res = doVoxelCenterTransformTask<double>(map, keys);
    cout << "res = " << res << endl;
  }

  {
    std::size_t res = doVoxelCenterTransformTaskWithFunctor<double, VoxelCenterFunctor>(map, keys);
    cout << "res = " << res << endl;
  }

  {
    std::size_t res = doVoxelCenterTransformTaskWithMapTransFormFunctor(map, keys);
    cout << "res = " << res << endl;
  }

}
