/**
 * @file AddNewVoxel_bechmark.cpp
 * @brief Benchmark of addNewVoxel methods of the datastructure
 *
 * This benchmark should indirectly benchmark Node Constructors (at-least in c++98)
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Keys/DenseKeyIterator.h"
#include "Naksha/Common/BuildConfig.h"

#include <boost/timer/timer.hpp>


template<class MapType>
void addNewVoxels(MapType& map) {
  typedef typename MapType::KeyType KeyType;

  for (const KeyType& key : DenseKeyRange(KeyType(0, 0, 0), KeyType (300, 200, 200))) {
    map.addNewVoxel(key);
  }
}

template<class MapType>
void doBenchmak() {
  std::cout << "===================================================================\n";
  std::cout << "MapType: " << PRETTY_TYPE(MapType, "Naksha", 2) << "\n";
  std::cout << "Payload: " << PRETTY_TYPE(typename MapType::Payload, "Naksha", 3) << "\n";

  typedef boost::timer::auto_cpu_timer TimerType;
  MapType map(0.1);
  {
    std::cout << "addNewVoxels:" << std::flush;
    TimerType t;
    addNewVoxels(map);
  }
}


using namespace Naksha;

int main(int argc, char **argv) {

  std::cout << "\n# Benchmark of addNewVoxel of Naksha Maps\n\n";

  Naksha::Build::printBuildConfig();

  {
    typedef DiscreteRandomVariable<9, double, LogProbabilityTag> RV;
    typedef Naksha::ColoredRandomVariable<RV>::type Payload;
    typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doBenchmak<MapType>();
  }

  {
    typedef DiscreteRandomVariable<9, double, ProbabilityTag> RV;
    typedef Naksha::ColoredRandomVariable<RV>::type Payload;
    typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doBenchmak<MapType>();
  }

  {
    typedef DiscreteRandomVariable<9, double, LogProbabilityTag> RV;
    typedef Naksha::ColoredRandomVariable<RV>::type Payload;
    typedef VolumetricMap<3, Payload, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doBenchmak<MapType>();
  }

  {
    typedef DiscreteRandomVariable<9, double, LogProbabilityTag> RV;
    typedef Naksha::ColoredRandomVariable<RV>::type Payload;
    typedef VolumetricMap<3, Payload, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doBenchmak<MapType>();
  }

  {
    typedef DiscreteRandomVariable<9, double, LogProbabilityTag> RV;
    typedef Naksha::ColoredRandomVariable<RV>::type Payload;
    typedef VolumetricMap<3, Payload, ConcurrentUnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doBenchmak<MapType>();
  }

}
