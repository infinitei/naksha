/**
 * @file KeyFromCoord_benchmark.cpp
 * @brief Key From Coord conversions benchmark
 *
 * @author Abhijit Kundu
 */

#include <Naksha/Maps/MapMaker.h>
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Common/BuildConfig.h"

#include <boost/timer/timer.hpp>
#include <boost/format.hpp>
#include <Eigen/StdVector>

typedef boost::timer::auto_cpu_timer TimerType;

template <class Container>
std::size_t computeContainerSum(const Container& keys) {
  std::size_t acc = 0;
  for( std::size_t i = 0; i < keys.size(); ++i ) {
    acc += keys[i].value.sum();
  }
  return acc;
}

template <class MapType, typename Scalar>
std::vector<typename MapType::KeyType>
doKeyFromCoordTask(const MapType& map, const Eigen::Matrix<Scalar, 3, 1>& range, const Scalar step) {

  typedef typename MapType::KeyType KeyType;
  typedef Eigen::Matrix<Scalar, 3, 1> VectorType;
  typedef std::vector<KeyType> KeyStdVector;
  typedef typename KeyStdVector::size_type size_type;

  std::cout << boost::format("%-28s| %-9s|")
                % "doKeyFromCoordTask"
                % PRETTY_TYPE_INFO(Scalar)
            << std::flush;

  size_type reserve_size = ((range.array() / step).template cast<size_type>().array() + 1u).prod();

  KeyStdVector keys;
  keys.reserve(reserve_size);

  {
    TimerType t;

    for (Scalar x = 0; x <= range.x(); x += step)
      for (Scalar y = 0; y <= range.y(); y += step)
        for (Scalar z = 0; z <= range.z(); z += step) {
          keys.push_back(map.keyFromCoord(VectorType(x, y, z)));
        }
  }

  if(reserve_size != keys.size())
    std::cout << "Wrong Reserve size!! Reserved "
      << reserve_size << " instead of " << keys.size() << "\n";

  return keys;
}

template <class MapType, typename Scalar>
std::vector<typename MapType::KeyType>
doKeyFromCoordTaskWithMapTransformsFunctor(const MapType& map, const Eigen::Matrix<Scalar, 3, 1>& range, const Scalar step) {

  typedef typename MapType::Transform TransformPolicy;
  typedef typename MapType::KeyType KeyType;
  typedef Eigen::Matrix<Scalar, 3, 1> VectorType;
  typedef std::vector<KeyType> KeyStdVector;
  typedef typename KeyStdVector::size_type size_type;

  std::cout << boost::format("%-28s| %-9s|")
                % "MapTransformsFunctor"
                % PRETTY_TYPE_INFO(Scalar)
            << std::flush;

  size_type reserve_size = ((range.array() / step).template cast<size_type>().array() + 1u).prod();

  KeyStdVector keys;
  keys.reserve(reserve_size);

  {
    TimerType t;

    typedef typename TransformPolicy::KeyCoordTransformerType FunctorType;
    const FunctorType keyFromCord_functor = map.keyCoordTransformer();

    for (Scalar x = 0; x <= range.x(); x += step)
      for (Scalar y = 0; y <= range.y(); y += step)
        for (Scalar z = 0; z <= range.z(); z += step) {
          keys.push_back(keyFromCord_functor(VectorType(x, y, z)));
        }

  }

  if(reserve_size != keys.size())
    std::cout << "Wrong Reserve size!! Reserved "
      << reserve_size << " instead of " << keys.size() << "\n";

  return keys;
}

template <
  template <typename, typename> class Functor,
  class MapType,
  typename Scalar
  >
std::vector<typename MapType::KeyType>
doKeyFromCoordFunctorTask(const MapType& map, const Eigen::Matrix<Scalar, 3, 1>& range, const Scalar step) {

  typedef typename MapType::KeyType KeyType;
  typedef Eigen::Matrix<Scalar, 3, 1> VectorType;
  typedef Functor<KeyType, Scalar> FunctorType;
  typedef std::vector<KeyType> KeyStdVector;
  typedef typename KeyStdVector::size_type size_type;


  std::cout << boost::format("%-28s| %-9s|")
                % PRETTY_TYPE(FunctorType, "Naksha", 0)
                % PRETTY_TYPE_INFO(Scalar)
            << std::flush;

  size_type reserve_size = ((range.array() / step).template cast<size_type>().array() + 1u).prod();

  KeyStdVector keys;
  keys.reserve(reserve_size);

  {
    TimerType t;
    FunctorType keyFromCord_functor(map.resolution());
    for (Scalar x = 0; x <= range.x(); x += step)
      for (Scalar y = 0; y <= range.y(); y += step)
        for (Scalar z = 0; z <= range.z(); z += step) {
          keys.push_back(keyFromCord_functor(VectorType(x, y, z)));
        }
  }

  if(reserve_size != keys.size())
    std::cout << "Wrong Reserve size!! Reserved "
      << reserve_size << " instead of " << keys.size() << "\n";

  return keys;
}


template<typename KeyType, typename Scalar>
class KeyFromCoordFunctor1 {
  /// Scalar Type of Key (should be Integral type)
  typedef typename KeyType::Scalar KeyScalar;

 public:
  KeyFromCoordFunctor1(const Scalar resolution)
      : inv_resolution_(1. / resolution),
        max_key_radius_(std::numeric_limits<KeyScalar>::max() / 2 + 1) {
  }

  template<typename Derived>
  KeyType operator()(const Eigen::MatrixBase<Derived>& coord) {
    typedef typename Derived::Scalar CoordScalar;
    using std::floor;
    return KeyType(((coord * inv_resolution_).unaryExpr(std::ptr_fun<CoordScalar, CoordScalar>(floor)).template cast<int>().array() + max_key_radius_).template cast<KeyScalar>());
  }

 private:
  const Scalar inv_resolution_;
  const int max_key_radius_;
};

// define a custom template unary functor
template<typename Scalar>
struct KeyFromCoordFunctor2CwiseHelper {
  KeyFromCoordFunctor2CwiseHelper(const Scalar inv_resolution, const int max_key_radius)
      : inv_resolution_(inv_resolution),
        max_key_radius_(max_key_radius) {
  }

  const int operator()(const Scalar& x) const {
    return int(std::floor(x * inv_resolution_)) + max_key_radius_;
  }

  const Scalar inv_resolution_;
  const int max_key_radius_;
};

template<typename KeyType, typename Scalar>
class KeyFromCoordFunctor2 {
  /// Scalar Type of Key (should be Integral type)
  typedef typename KeyType::Scalar KeyScalar;

  typedef KeyFromCoordFunctor2CwiseHelper<Scalar> CwiseFunctor;

 public:
  KeyFromCoordFunctor2(const Scalar resolution)
      : cwise_functor(1. / resolution, std::numeric_limits<KeyScalar>::max() / 2 + 1) {
  }

  template<typename Derived>
  KeyType operator()(const Eigen::MatrixBase<Derived>& coord) {
    return KeyType(coord.unaryExpr(cwise_functor).template cast<KeyScalar>());
  }

 private:
  const CwiseFunctor cwise_functor;
};

// define a custom template unary functor
template<typename Scalar>
struct KeyFromCoordFunctor3CwiseHelper {
  KeyFromCoordFunctor3CwiseHelper(const Scalar inv_resolution)
    : inv_resolution_(inv_resolution) {}

  const int operator()(const Scalar& x) const{
    return std::floor(x * inv_resolution_);
  }

  const Scalar inv_resolution_;
};

template<typename KeyType, typename Scalar>
class KeyFromCoordFunctor3 {
  /// Scalar Type of Key (should be Integral type)
  typedef typename KeyType::Scalar KeyScalar;

  typedef KeyFromCoordFunctor3CwiseHelper<Scalar> CwiseFunctor;

 public:
  KeyFromCoordFunctor3(const Scalar resolution)
      : cwise_functor(1. / resolution),
        max_key_radius_(std::numeric_limits<KeyScalar>::max() / 2 + 1) {
  }

  template<typename Derived>
  KeyType operator()(const Eigen::MatrixBase<Derived>& coord) {
    return KeyType((coord.unaryExpr(cwise_functor).array() + max_key_radius_).template cast<KeyScalar>());
  }

 private:
  const CwiseFunctor cwise_functor;
  const int max_key_radius_;
};

template<typename KeyType, typename Scalar>
class KeyFromCoordFunctor4 {
  /// Scalar Type of Key (should be Integral type)
  typedef typename KeyType::Scalar KeyScalar;

 public:
  KeyFromCoordFunctor4(const Scalar resolution)
      : inv_resolution_(1. / resolution),
        max_key_radius_mod_(std::numeric_limits<KeyScalar>::max() / 2 + 1 - 10000) {
  }

  template<typename Derived>
  KeyType operator()(const Eigen::MatrixBase<Derived>& coord) {
    return KeyType((((coord * inv_resolution_).array() + 10000).template cast<int>().array() + max_key_radius_mod_).template cast<KeyScalar>());
  }

 private:
  const Scalar inv_resolution_;
  const int max_key_radius_mod_;
};

template<typename Scalar>
void runAllBenchMarks() {
  using namespace Naksha;

  typedef DiscreteRandomVariable<3, double, ProbabilityTag> RV;
  typedef VolumetricMap<3, RV, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
  typedef MapType::KeyType KeyType;
  MapType map(0.1);
  Scalar step_size = 0.1;

  {
    typedef Eigen::Matrix<Scalar, 3, 1> Range3D;
    Range3D range(100, 100, 100);
    std::vector<KeyType> keys = doKeyFromCoordTask(map, range, step_size);
//    cout << " computeKeysSum(keys) = " << computeContainerSum(keys) << "\n";
  }

  {
    typedef Eigen::Matrix<Scalar, 3, 1> Range3D;
    Range3D range(100, 100, 100);
    std::vector<KeyType> keys = doKeyFromCoordTaskWithMapTransformsFunctor(map, range, step_size);
//    cout << " computeKeysSum(keys) = " << computeContainerSum(keys) << "\n";
  }

  {
    typedef Eigen::Matrix<Scalar, 3, 1> Range3D;
    Range3D range(100, 100, 100);
    std::vector<KeyType> keys = doKeyFromCoordFunctorTask<KeyCoordTransformer>(map, range, step_size);
//    cout << " computeKeysSum(keys) = " << computeContainerSum(keys) << "\n";
  }

  {
    typedef Eigen::Matrix<Scalar, 3, 1> Range3D;
    Range3D range(100, 100, 100);
    std::vector<KeyType> keys = doKeyFromCoordFunctorTask<KeyFromCoordFunctor1>(map, range, step_size);
//    cout << " computeKeysSum(keys) = " << computeContainerSum(keys) << "\n";
  }

  {
    typedef Eigen::Matrix<Scalar, 3, 1> Range3D;
    Range3D range(100, 100, 100);
    std::vector<KeyType> keys = doKeyFromCoordFunctorTask<KeyFromCoordFunctor2>(map, range, step_size);
//    cout << " computeKeysSum(keys) = " << computeContainerSum(keys) << "\n";
  }

  {
    typedef Eigen::Matrix<Scalar, 3, 1> Range3D;
    Range3D range(100, 100, 100);
    std::vector<KeyType> keys = doKeyFromCoordFunctorTask<KeyFromCoordFunctor3>(map, range, step_size);
//    cout << " computeKeysSum(keys) = " << computeContainerSum(keys) << "\n";
  }

  {
    typedef Eigen::Matrix<Scalar, 3, 1> Range3D;
    Range3D range(100, 100, 100);
    std::vector<KeyType> keys = doKeyFromCoordFunctorTask<KeyFromCoordFunctor4>(map, range, step_size);
//    cout << " computeKeysSum(keys) = " << computeContainerSum(keys) << "\n";
  }

}



int main(int argc, char **argv) {

  using namespace Naksha;
  using std::cout;
  using std::endl;
  using std::flush;

  cout << "\n# Benchmark of KeyFromCoord methods\n\n";

  Build::printBuildConfig();

  cout << "\n";

  std::cout << "Method                      | Scalar   | Time                                  \n";
  std::cout << "--------------------------- | -------- | --------------------------------------\n";

  runAllBenchMarks<double>();
  runAllBenchMarks<float>();

  return EXIT_SUCCESS;
}



