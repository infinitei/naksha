/**
 * @file DiscreteRandomVariableConstructors.cpp
 * @brief This profiles DiscreteRandomVariable Constructors
 *
 * This profiles DiscreteRandomVariable Constructors
 * i.e. Default vs the Unary constructor from RV::DataType
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Payload/DiscreteRandomVariable.h"
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/progress.hpp>

using namespace std;
using namespace Eigen;
using namespace Naksha;

typedef DiscreteRandomVariable<8, float, LogProbabilityTag> RV;
typedef boost::ptr_vector<RV> RVPtrVector;

template<typename NodeContainer>
void checkNodes(const NodeContainer& nodes, const RV::DataType& check) {
  for (typename NodeContainer::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
    if (!it->probability().isApprox(check))
      cout << "Unexpected: " << it->probability().transpose() << endl;
  }
}

int main(int argc, char **argv) {

  RV::DataType check(RV::DataType::Ones() * (0.5f / (RV::Dimension - 1)));
  check[0] = 0.5f;

  RV::DataType init_data(RV::DataType::Ones() * (0.5f / (RV::Dimension - 1)));
  init_data[0] = 0.5f;
  init_data = convertRandomVariable(init_data, RV::ProbTypeTag(), ProbabilityTag());

  const size_t num_of_nodes = 100000000;
  const bool do_checking = false;

  cout << "Size of each RV = " << sizeof(RV) << " bytes." << endl;
  cout << "Size of each RV::Datatype = " << sizeof(check) << " bytes." << endl;
  size_t size = sizeof(RV) * num_of_nodes;
  cout << "Size of whole vector = " << size << " bytes (" << size / (1024. * 1024.) << " MB)."
       << endl;

  cout << "================================================================================" << endl;
  cout << "Using std::vector container" << endl;

  {
    std::vector<RV> nodes;
    nodes.reserve(num_of_nodes);
    cout << "Creating " << num_of_nodes << " Nodes with Default Constructor (std::vector push_back):" << endl;
    {
      boost::progress_timer t;
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(RV());
      }
    }
    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    cout << "Creating " << num_of_nodes << " Nodes constructed with Default Constructor (std::vector constructor) :" << endl;
    {
      boost::progress_timer t;
      std::vector<RV> nodes(num_of_nodes);
    }
    if(do_checking) {
      std::vector<RV> nodes(num_of_nodes);
      checkNodes(nodes, check);
    }
  }

  {
    std::vector<RV> nodes;
    nodes.reserve(num_of_nodes);
    cout << "Creating " << num_of_nodes << " Nodes with Init Constructor (std::vector push_back) :" << endl;
    {
      boost::progress_timer t;
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(RV(init_data));
      }
    }
    if(do_checking) {
      checkNodes(nodes, check);
    }
  }

  {
    std::vector<RV> nodes;
    nodes.reserve(num_of_nodes);
    cout << "Creating " << num_of_nodes << " Nodes from single local instance (std::vector push_back) :" << endl;
    {
      boost::progress_timer t;
      const RV rv(init_data);
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(rv);
      }
    }
    if(do_checking) {
      checkNodes(nodes, check);
    }
  }

  {
    std::vector<RV> nodes;
    nodes.reserve(num_of_nodes);
    cout << "Creating " << num_of_nodes << " Nodes from a local static (std::vector push_back) :" << endl;
    {
      boost::progress_timer t;
      static const RV rv(init_data);
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(rv);
      }
    }
    if(do_checking)
      checkNodes(nodes, check);
  }

  cout << "================================================================================" << endl;
  cout << "Using boost::ptr_vector container" << endl;


  {
    RVPtrVector nodes;
    nodes.reserve(num_of_nodes);
    cout << "Creating " << num_of_nodes << " Nodes with Default Constructor (boost::ptr_vector push_back):" << endl;
    {
      boost::progress_timer t;
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(new RV());
      }
    }
    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    RVPtrVector nodes;
    nodes.reserve(num_of_nodes);
    cout << "Creating " << num_of_nodes << " Nodes with Init Constructor (boost::ptr_vector push_back):" << endl;
    {
      boost::progress_timer t;
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(new RV(init_data));
      }
    }
    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    RVPtrVector nodes;
    nodes.reserve(num_of_nodes);
    cout << "Creating " << num_of_nodes << " Nodes with Copy Constructor (instance) (boost::ptr_vector push_back):" << endl;
    {
      boost::progress_timer t;
      const RV rv(init_data);
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(new RV(rv));
      }
    }
    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    RVPtrVector nodes;
    nodes.reserve(num_of_nodes);
    cout << "Creating " << num_of_nodes << " Nodes with Copy Constructor (static) (boost::ptr_vector push_back):" << endl;
    {
      boost::progress_timer t;
      static const RV rv(init_data);
      for (size_t i = 0; i < num_of_nodes; ++i) {
        nodes.push_back(new RV(rv));
      }
    }
    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    cout << "Creating " << num_of_nodes << " Nodes with boost::ptr_vector Resize :" << endl;
    RVPtrVector nodes;
    {
      boost::progress_timer t;
      nodes.resize(num_of_nodes);
    }
    if(do_checking)
      checkNodes(nodes, check);
  }

  {
    cout << "Creating " << num_of_nodes << " Nodes with boost::ptr_vector constructor :" << endl;
    {
      boost::progress_timer t;
      RVPtrVector nodes(num_of_nodes);
    }
    if(do_checking) {
      RVPtrVector nodes(num_of_nodes);
      checkNodes(nodes, check);
    }
  }

  return EXIT_SUCCESS;
}

