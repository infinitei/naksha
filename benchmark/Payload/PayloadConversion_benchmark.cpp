/**
 * @file PayloadConversion_benchmark.cpp
 * @brief PayloadConversion_benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Payload/ConvertPayload.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Common/BuildConfig.h"

#include <boost/timer/timer.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <map>


template <class ToPayload, class FromPayload>
std::vector<ToPayload>
doConversion(const std::vector<FromPayload>& from_payloads) {
  std::vector<ToPayload> to_payloads;
  to_payloads.reserve(from_payloads.size());
  for(const FromPayload& from_payload : from_payloads) {
    to_payloads.push_back(Naksha::convertPayload<ToPayload>(from_payload));
  }
  return to_payloads;
}

struct PrettyTypeShortener {
  PrettyTypeShortener() {
    string_map["float"] = "f";
    string_map["double"] = "d";
    string_map["int"] = "i";
    string_map["ProbabilityTag"] = "P";
    string_map["LogProbabilityTag"] = "LP";
    string_map["LogOddsTag"] = "LO";
    string_map["CategoricalRandomVariable"] = "CRV";
    string_map["BinaryRandomVariable"] = "BRV";
    string_map["DiscreteRandomVariable"] = "DRV";
    string_map["CombinePayloads2"] = "Combine";
    string_map["ColoredPayload"] = "Color";
  }
  std::string operator()(const std::string& input) const {
    std::string shortened = input;
    typedef std::map<std::string, std::string>::const_iterator it_type;
    for(it_type iterator = string_map.begin(); iterator != string_map.end(); iterator++) {
      boost::replace_all(shortened, iterator->first, iterator->second);
    }
    return shortened;
  }
  std::map<std::string, std::string> string_map;
};

template<typename T>
struct CheckResult {
  CheckResult(T expected)
      : expected_result(expected) {
  }
  void operator()(T result) const {
    if(result != expected_result)
      std::cout << "Error: " << result << " != " << expected_result << "\n";
  }
  T expected_result;
};


int main(int argc, char **argv) {
  using namespace Naksha;

  std::cout << "\n# Benchmark of PayloadConversion\n\n";
  Build::printBuildConfig();


  typedef DiscreteRandomVariable<4, double, ProbabilityTag> RVLP9d;
  typedef ColoredRandomVariable<RVLP9d>::type ColoredRVLP9d;

  typedef ColoredRandomVariable<RVLP9d>::type ColoredRVLP9d;

  std::size_t num_of_payloads = 100000000;
  CheckResult<std::size_t> check_map_size(num_of_payloads);

  const std::vector<ColoredRVLP9d> from_payloads(
      num_of_payloads,
      ColoredRVLP9d(RVLP9d::DataType::Constant(1.0 / 4), ColoredPayload::DataType(0, 255, 0)));

  PrettyTypeShortener shrortner;

  typedef boost::timer::auto_cpu_timer TimerType;


  std::cout << "\nFromPayload: " << shrortner(PRETTY_TYPE(ColoredRVLP9d, "Naksha", 2)) << "\n\n";

  std::cout << "  ToPayload                        |          Time                                         \n";
  std::cout << "---------------------------------- | ------------------------------------------------------\n";

  {
    typedef ColoredPayload ToPayload;
    std::cout << boost::format("%-35s|") % shrortner(PRETTY_TYPE(ToPayload, "Naksha", 2)) << std::flush;
    TimerType t;
    std::vector<ToPayload> result = doConversion<ToPayload>(from_payloads);
  }

  {
    typedef DiscreteRandomVariable<4, float, ProbabilityTag> ToPayload;
    std::cout << boost::format("%-35s|") % shrortner(PRETTY_TYPE(ToPayload, "Naksha", 2)) << std::flush;
    TimerType t;
    std::vector<ToPayload> result = doConversion<ToPayload>(from_payloads);
  }

  {
    typedef DiscreteRandomVariable<4, double, ProbabilityTag> ToPayload;
    std::cout << boost::format("%-35s|") % shrortner(PRETTY_TYPE(ToPayload, "Naksha", 2)) << std::flush;
    TimerType t;
    std::vector<ToPayload> result = doConversion<ToPayload>(from_payloads);
  }

  {
    typedef DiscreteRandomVariable<4, double, LogProbabilityTag> ToPayload;
    std::cout << boost::format("%-35s|") % shrortner(PRETTY_TYPE(ToPayload, "Naksha", 2)) << std::flush;
    TimerType t;
    std::vector<ToPayload> result = doConversion<ToPayload>(from_payloads);
  }

  {
    typedef DiscreteRandomVariable<4, float, LogProbabilityTag> ToPayload;
    std::cout << boost::format("%-35s|") % shrortner(PRETTY_TYPE(ToPayload, "Naksha", 2)) << std::flush;
    TimerType t;
    std::vector<ToPayload> result = doConversion<ToPayload>(from_payloads);
  }

  {
    typedef ColoredRVLP9d ToPayload;
    std::cout << boost::format("%-35s|") % shrortner(PRETTY_TYPE(ToPayload, "Naksha", 2)) << std::flush;
    TimerType t;
    std::vector<ToPayload> result = doConversion<ToPayload>(from_payloads);
  }

  {
    typedef ColoredRandomVariable<DiscreteRandomVariable<4, double, LogProbabilityTag>>::type ToPayload;
    std::cout << boost::format("%-35s|") % shrortner(PRETTY_TYPE(ToPayload, "Naksha", 2)) << std::flush;
    TimerType t;
    std::vector<ToPayload> result = doConversion<ToPayload>(from_payloads);
  }

  {
    typedef ColoredRandomVariable<DiscreteRandomVariable<4, float, ProbabilityTag>>::type ToPayload;
    std::cout << boost::format("%-35s|") % shrortner(PRETTY_TYPE(ToPayload, "Naksha", 2)) << std::flush;
    TimerType t;
    std::vector<ToPayload> result = doConversion<ToPayload>(from_payloads);
  }

  {
    std::vector<ColoredRVLP9d> copy = from_payloads;
    typedef ColoredRVLP9d ToPayload;
    std::cout << boost::format("%-35s|") % shrortner(PRETTY_TYPE(ToPayload, "Naksha", 2)) << std::flush;
    TimerType t;
    copy = doConversion<ToPayload>(copy);
  }

  return EXIT_SUCCESS;
}
