/**
 * @file MapSerialization_benchmark.cpp
 * @brief MapSerialization_benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Maps/MapSerialization.h"
#include "Naksha/Tools/ImportExport.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Keys/DenseKeyIterator.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Common/BuildConfig.h"

#include <boost/timer/timer.hpp>

template<class MapType>
void doDumbMapping(MapType& map) {
  typedef typename MapType::KeyType KeyType;
  typedef Naksha::MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;

  for (const KeyType& key : DenseKeyRange(KeyType(0, 0, 0), KeyType (200, 200, 200))) {
    map.updateVoxel(key, MeasurementModelType::HIT);
  }
  for (const KeyType& key : DenseKeyRange(KeyType(200, 200, 200), KeyType (400, 400, 300))) {
    map.updateVoxel(key, MeasurementModelType::MISS);
  }
}

template<class MapType>
void doMapSerializationBenchmak() {

  std::cout << "\n===================================================================\n";
  std::cout << "MapType: " << PRETTY_TYPE(MapType, "Naksha", 1) << "\n";


  typedef boost::timer::auto_cpu_timer TimerType;

  MapType map(0.1);

  {
    std::cout << "Task: doDumbMapping: " << std::flush;
    TimerType t;
    doDumbMapping(map);
  }

  {
    std::cout << "Task: serializeMap: " << std::endl;
    TimerType t;
    serializeMap(map, "serialized_map.naksha");
  }

  {
    std::cout << "Task: deSerializeMap: " << std::endl;
    TimerType t;
    boost::scoped_ptr<MapType> map_in(Naksha::deSerializeMap<MapType>("serialized_map.naksha"));
  }
  std::remove("serialized_map.naksha");

  {
    std::cout << "Task: saveMap: " << std::endl;
    TimerType t;
    saveMap(map, "map.naksha");
  }

  {
    std::cout << "Task: loadMap: " << std::endl;
    TimerType t;
    boost::scoped_ptr<MapType> map_in(Naksha::loadMap<MapType>("map.naksha"));
  }
  std::remove("map.naksha");
}

using namespace Naksha;

int main(int argc, char **argv) {

  std::cout << "\n# Benchmark of serialization of Naksha Maps\n\n";

  Naksha::Build::printBuildConfig();

  {
    typedef DiscreteRandomVariable<9, float, LogProbabilityTag> RV;
    typedef Naksha::ColoredRandomVariable<RV>::type Payload;
    typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doMapSerializationBenchmak<MapType>();
  }

  {
    typedef DiscreteRandomVariable<9, float, LogProbabilityTag> RV;
    typedef Naksha::ColoredRandomVariable<RV>::type Payload;
    typedef VolumetricMap<3, Payload, TreeDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doMapSerializationBenchmak<MapType>();
  }

  {
    typedef DiscreteRandomVariable<9, float, LogProbabilityTag> RV;
    typedef Naksha::ColoredRandomVariable<RV>::type Payload;
    typedef VolumetricMap<3, Payload, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    doMapSerializationBenchmak<MapType>();
  }

}
