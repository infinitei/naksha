/**
 * @file SerializationSize_benchmark.cpp
 * @brief SerializationSize_benchmark
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Common/SerializationHelper.h"
#include "Naksha/Common/PrettyTypeInfo.h"

#include "Naksha/Nodes/VoxelNode.h"

// Payloads
#include "Naksha/Payload/PayloadMaker.h"
// Interafces
#include "Naksha/Nodes/OccupancyNodeInterface.h"
#include "Naksha/Nodes/RVOccupancyNodeInterface.h"
// ChildPolicy
#include "Naksha/Nodes/NodeTypedefs.h"

#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>

#include <boost/progress.hpp>
#include <boost/filesystem.hpp>

#include <boost/serialization/vector.hpp>

template<class T>
void serializationBenchmarkNaksha(const size_t num) {

  {
    std::cout << "=============================================================\n";
    std::cout << "NodeType: " << PRETTY_TYPE_INFO(T) << std::endl;

    std::cout << "Sizeof(" << PRETTY_TYPE_INFO(T) << ") = " << sizeof(T) << " bytes." << std::endl;
    std::cout << "Sizeof(" << num << " nodes) = " << num * sizeof(T) << " bytes." << std::endl;

    {
      std::cout << "save:" << std::endl;
      boost::progress_timer t;

      std::ofstream ofs("T.naksha", std::ios::out | std::ios::binary);
      if (!ofs.is_open())
        return;
      { // use scope to ensure archive goes out of scope before stream
        boost::archive::binary_oarchive oa(ofs);

        //TODO:: The following commented code does not work with C++03 (something to do with un initialized scoped pointer)
        // Need to fix this
        //std::vector<T> nodes(num);
        //oa << nodes;

        for(size_t i = 0; i < num; ++i) {
          T node;
          oa << node;
        }
      }
      ofs.close();
    }

    namespace bf = boost::filesystem;
    std::cout << "FileSize = " << bf::file_size("T.naksha") << " bytes." << std::endl;
    std::cout << "Overhead = " << bf::file_size("T.naksha") -  num * sizeof(T) << " bytes." << std::endl;
    std::cout << "=============================================================\n\n";
  }
}



using namespace std;
using namespace Naksha;

typedef DiscreteRandomVariable<9, double, LogProbabilityTag> LPRV9d;
typedef VoxelNode<LPRV9d, RVOccupancyNodeInterface, OctreeNode> LPRV9dRVOccOctreeNode;

int main(int argc, char **argv) {

  cout << "===================================================================" << endl;
  cout << "Node Benchmark" << endl;
  cout << "===================================================================" << endl;

  serializationBenchmarkNaksha<LPRV9d>(1);
  serializationBenchmarkNaksha<LPRV9d>(4);


  serializationBenchmarkNaksha<LPRV9dRVOccOctreeNode>(4);
  serializationBenchmarkNaksha<LPRV9dRVOccOctreeNode>(1);

}
