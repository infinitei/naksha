/**
 * @file demoMapIO.cpp
 * @brief demoMapIO
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Tools/ImportExport.h"
#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Algorithms/InsertMeasurements.h"
#include "Naksha/Algorithms/CompareMaps.h"

int main(int argc, char **argv) {
  using namespace Naksha;
  using std::cout;

  // Create point cloud
  typedef std::vector<Eigen::Vector3d> PointCloudType;
  const int width = 10;
  const int xoffset = 20;
  PointCloudType point_cloud;
  for (double x = xoffset - width; x < (xoffset + width); x += 0.1)
    for (double y = -width; y < width; y += 0.1) {
      point_cloud.push_back(Eigen::Vector3d(x, y, -10));
    }


  typedef DiscreteRandomVariable<9, float, LogProbabilityTag> RV;
  typedef ColoredRandomVariable<RV>::type Payload;

  typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, HybridMappingInterface, MapTransform> UMMapType;


  // Initialize a map with resolution 0.1
  UMMapType map(0.1);
  // Insert point cloud
  insertDepthScan(map, Eigen::Vector3d::Zero(), point_cloud);

  cout << "# Map Size = " << map.size() << " voxels\n\n";

  saveMap(map, "map.naksha");

  {
    typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    boost::scoped_ptr<MapType> map_in(Naksha::loadMap<MapType>("map.naksha"));
    compareMaps(map, *map_in);
  }
  {
    typedef VolumetricMap<3, Payload, TreeDataStructure, HybridMappingInterface, MapTransform> MapType;
    boost::scoped_ptr<MapType> map_in(Naksha::loadMap<MapType>("map.naksha"));
    compareMaps(map, *map_in);
  }
  {
    typedef VolumetricMap<3, Payload, HashedVectorDataStructure, OccupancyMappingInteface, MapTransform> MapType;
    boost::scoped_ptr<MapType> map_in(Naksha::loadMap<MapType>("map.naksha"));
    compareMaps(map, *map_in);
  }
}
