/**
 * @file generateRayTraceArt.cpp
 * @brief generateRayTraceArt
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Algorithms/RayTracing.h"
#include "Naksha/Algorithms/RayIntersections.h"
#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Tools/ImportExport.h"

#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/progress.hpp>

using namespace Naksha;
using namespace Eigen;
using namespace std;

int main(int argc, char **argv) {
  typedef boost::progress_timer TimerType;
  typedef VolumetricMap<3, ColoredPayload, UnorderedMapDataStructure, OccupancyMappingInteface, MapTransform> MapType;

  typedef MapType::KeyType KeyType;
  typedef std::vector<KeyType> KeyTypeContainer;

  boost::mt19937 rng;
  boost::normal_distribution<> nd(0.0, 1.0);
  boost::variate_generator<boost::mt19937&,
                             boost::normal_distribution<> > var_nor(rng, nd);

  const size_t num_of_rays = 1000;
  vector<Vector3d, aligned_allocator<Vector3d> > directions;
  directions.reserve(num_of_rays);
  for (size_t i = 0; i < num_of_rays; ++i) {
    directions.push_back(Vector3d(var_nor(), var_nor(), var_nor()).normalized());
  }

  ColoredPayload::DataType green(0,255,0);

  {
    std::cout << "\nRayTraceArtSphere : " << std::flush;
    MapType map(0.1);
    {
      TimerType t;
      for (size_t i = 0; i < directions.size(); ++i) {
        Vector3d start = Vector3d(0, 0, 0);
        Vector3d end = start + directions[i] * 40.0;

        KeyTypeContainer ray_keys;
        if (computeKeysOnRay(map, start, end, ray_keys)) {
          for (const KeyType& key : ray_keys) {
            map.insertNewVoxel(key, green);
          }
        }
      }
    }
    saveMap(map, "RayTraceArtSphere.naksha");
  }

  std::cout << "\n------------------------------------------\n";

  {
    std::cout << "RayTraceArtBox : " << std::flush;
    MapType map(0.1);
    {
      TimerType t;
      Vector3d start = Vector3d(0, 0, 0);
      RayAABBIntersection<double, 3> ray_bbx_intersection(Vector3d::Constant(-20),
                                                          Vector3d::Constant(20), start);
      for (size_t i = 0; i < directions.size(); ++i) {
        Vector3d end = ray_bbx_intersection(directions[i]);
        KeyTypeContainer ray_keys;
        if (computeKeysOnRay(map, start, end, ray_keys)) {
          for (const KeyType& key : ray_keys) {
            map.insertNewVoxel(key, green);
          }
        }
      }
    }
    saveMap(map, "RayTraceArtBox.naksha");
  }

  std::cout << "\n------------------------------------------\n";

  {
    std::cout << "RayTraceArtBox2 : " << std::flush;
    MapType map(0.1);
    {
      TimerType t;
      map.setMaximumBbx(Vector3d::Constant(-20), Vector3d::Constant(20));

      const Vector3d start = Vector3d(0, 0, 0);
      RayMapBoundaryIntersection<MapType> find_key_at_map_boundary(map, start);
      for (size_t i = 0; i < directions.size(); ++i) {

        Vector3d end = start + directions[i] * 40.0;

        KeyType start_key = map.keyFromCoord(start);
        bool inside_map = map.maximumBbx().contains(end);
        KeyType end_key =
            inside_map ? map.keyFromCoord(end) : find_key_at_map_boundary(directions[i]);

        KeyTypeContainer ray_keys;
        computeKeysOnRay(map, start_key, end_key, start, directions[i], ray_keys);
        for (const KeyType& key : ray_keys) {
          map.insertNewVoxel(key, green);
        }
      }
    }
    saveMap(map, "RayTraceArtBox2.naksha");
  }

  return EXIT_SUCCESS;
}

