/**
 * @file VoxelChangeDetector.cpp
 * @brief VoxelChangeDetector
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Common/PrettyTypeInfo.h"
#include "Naksha/Keys/DenseKeyIterator.h"

int main(int argc, char **argv) {
  typedef Naksha::VolumetricMap<3, Naksha::EmptyPayload, Naksha::TreeDataStructure,
      Naksha::HybridMappingInterface, Naksha::MapTransform, Naksha::OccupancyNodeInterface> MapType;

  typedef MapType::KeyType KeyType;

  std::cout << "MapType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType, "Naksha") << "\n";
  std::cout << "NodeType: " << PRETTY_TYPE_INFO_REMOVE_NS(MapType::VoxelNodeType, "Naksha") << "\n";
  std::cout << "Sizeof(NodeType) =  " << sizeof(MapType::VoxelNodeType) << "\n";
  std::cout << "Sizeof(EmptyPayload) =  " << sizeof(Naksha::EmptyPayload) << "\n";

  MapType map(0.1);
  map.enableChangeDetection();

  for (const KeyType& key : DenseKeyRange(KeyType(0, 0, 0), KeyType(300, 300, 300)))
    map.insertNewVoxel(key);

  std::cout << "Inserted # of Voxels =  " << map.numberOfVoxels() << "\n";
  std::cout << "Changed Keys Size =  " << map.changedKeys().size() << "\n";

  std::cout << "Doing Reset of Change Detection\n";
  map.resetChangeDetection();

  for (const KeyType& key : DenseKeyRange(KeyType(0, 0, 0), KeyType(10, 10, 10)))
    map.insertNewVoxel(key);

  std::cout << "Changed Keys Size =  " << map.changedKeys().size() << "\n";

  for (const KeyType& key : DenseKeyRange(KeyType(400, 400, 400), KeyType(410, 410, 410)))
    map.insertNewVoxel(key);

  std::cout << "Changed Keys Size =  " << map.changedKeys().size() << "\n";

  return EXIT_SUCCESS;
}
