/**
 * @file importOctoMap.cpp
 * @brief importOctoMap
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Tools/ReadOctoMap.h"

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Tools/ImportExport.h"

int main(int argc, char **argv) {
  if (argc != 2) {
    std::cout << "ERROR: OctoMap file path not provided" << "\n";
    std::cout << "Usage: " << argv[0] << " /path/to/octomap/file";
    std::cout << "\n";
    return EXIT_FAILURE;
  }

  namespace bfs = boost::filesystem;

  bfs::path file_path(argv[1]);
  if (!bfs::exists(file_path)) {
    std::cout << "File " << argv[1] << " does not exists.\n";
    return EXIT_FAILURE;
  }
  if (!bfs::is_regular_file(file_path)) {
    std::cout << "Input " << argv[1] << " is not a file.\n";
    return EXIT_FAILURE;
  }

  //! [Example usage of readOctomapFile]

  typedef Naksha::DiscreteRandomVariable<9, float> RV;
  typedef Naksha::ColoredRandomVariable<RV>::type Payload;
  typedef Naksha::OctreeMap<Payload>::type MapType;

  boost::scoped_ptr<MapType> map(Naksha::readOctoMapFile<MapType>(file_path.string()));

  //! [Example usage of readOctomapFile]

  saveMap(*map, file_path.stem().string() + ".naksha");


  return 0;
}




