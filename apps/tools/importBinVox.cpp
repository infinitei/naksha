/**
 * @file importBinVox.cpp
 * @brief importBinVox
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Tools/ReadBinVox.h"

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/LabelToMeasurementMapping.h"
#include "Naksha/Tools/ImportExport.h"

int main(int argc, char **argv) {
  if (argc != 2) {
    std::cout << "ERROR: BinVox file path not provided" << "\n";
    std::cout << "Usage: " << argv[0] << " /path/to/binvox/file";
    std::cout << "\n";
    return EXIT_FAILURE;
  }

  namespace bfs = boost::filesystem;

  bfs::path file_path(argv[1]);
  if (!bfs::exists(file_path)) {
    std::cout << "File " << argv[1] << " does not exists.\n";
    return EXIT_FAILURE;
  }
  if (!bfs::is_regular_file(file_path)) {
    std::cout << "Input " << argv[1] << " is not a file.\n";
    return EXIT_FAILURE;
  }

  std::vector<unsigned char> voxels;
  Eigen::Vector3i dimension;
  std::tie(voxels, dimension, std::ignore, std::ignore) = Naksha::readBinVoxFile(file_path.string());

  typedef Naksha::VolumetricMap<3, Naksha::EmptyPayload, Naksha::TreeDataStructure, Naksha::OccupancyMappingInteface, Naksha::MapTransform> MapType;

  MapType map(1.0 /dimension.maxCoeff());

  std::cout << "Inserting " << dimension.prod() << " potential voxels" << std::endl;
  int x(0), y(0), z(0);
  for (std::size_t index = 0; index < voxels.size(); ++index) {
    if(voxels[index]) {
      Eigen::Vector3f coord = Eigen::Vector3f(x + 0.5, y + 0.5, z + 0.5).cwiseQuotient(dimension.cast<float>());
//      coord *= scale;
//      coord += translation;
      MapType::KeyType key = map.keyFromCoord(coord);
      map.addNewVoxel(key);
    }
    ++y;
    if(y >= dimension.y()) {
      y = 0;
      ++z;
      if(z >= dimension.z()) {
        z = 0;
        ++ x;
      }
    }
  }

  std::cout << "Created map with " << map.size() << " voxels" << std::endl;
  Naksha::saveMap(map, file_path.stem().string() + ".naksha");

  return EXIT_SUCCESS;
}


