# setting a common place to put all executable files
SET(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)

ADD_SUBDIRECTORY(usage-examples)
ADD_SUBDIRECTORY(tools)