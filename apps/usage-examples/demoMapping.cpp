/**
 * @file demoMapping.cpp
 * @brief demoMapping
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Payload/MeasurementModel.h"
#include "Naksha/Nodes/NodeUtils.h"
#include "Naksha/Tools/ImportExport.h"
#include "Naksha/Algorithms/CompareMaps.h"

#include <algorithm>
#include <boost/scoped_ptr.hpp>

// Mapping works for Naksha and HybridOctree
template<class MapType>
void mapping(MapType& map) {
  std::cout << "Mapping .. " << std::flush;

  const int occ_span = 70;
  const int free_span = 100;

  typedef Naksha::MeasurementModel<typename MapType::Payload::RVType> MeasurementModelType;

  for (int x = -occ_span; x < occ_span; ++x) {
    for (int y = -occ_span; y < occ_span; ++y) {
      for (int z = -occ_span; z < occ_span; ++z) {
        const Eigen::Vector3f endpoint((float) x * 0.05f, (float) y * 0.05f,
            (float) z * 0.05f);

        map.updateVoxel(endpoint, MeasurementModelType::HIT); // integrate 'occupied' measurement
      }
    }
  }

  // insert some measurements of free cells

  for (int x = -free_span; x < free_span; ++x) {
    for (int y = -free_span; y < free_span; ++y) {
      for (int z = -free_span; z < free_span; ++z) {
        const Eigen::Vector3f endpoint((float) x * 0.02f - 1.0f, (float) y * 0.02f - 1.0f,
            (float) z * 0.02f - 1.0f);

        map.updateVoxel(endpoint, MeasurementModelType::MISS); // integrate 'free' measurement
      }
    }
  }

  std::cout << " Done." << std::endl;
}

int main(int argc, char **argv) {
  using namespace Naksha;

  typedef DiscreteRandomVariable<9, float> RV;
  typedef ColoredRandomVariable<RV>::type Payload;
  typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, HybridMappingInterface, MapTransform> MapType;
  typedef MapType::VoxelNodeType VoxelNodeType;

  // Initialize a map with resolution 0.1
  MapType map(0.1);

  // perform a simple mapping
  mapping(map);

  // Count the number of occupied voxels
  std::size_t solid_voxel_count = std::count_if(map.begin(), map.end(), IsNodeOccupied<VoxelNodeType>());
  std::size_t free_voxel_count = map.size() - solid_voxel_count;

  using std::cout;
  cout << "Map Size = " << map.size() << " voxels\n";
  cout << "# Free Voxels = " << free_voxel_count << "\n";
  cout << "# Solid Voxels = " << solid_voxel_count << "\n";

  // Save Map
  saveMap(map, "demoMap.naksha");

  // load the saved map
  boost::scoped_ptr<MapType> map_in(loadMap<MapType>("demoMap.naksha"));

  // Compare
  compareMaps(map, *map_in);

  // delete the saved map file from directory
  std::remove("demoMap.naksha");

  return EXIT_SUCCESS;
}

