/**
 * @file demoTraits.cpp
 * @brief demoTraits
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Payload/PayloadMaker.h"

#include "Naksha/Payload/PayloadTraits.h"

namespace Naksha {

template <class PayloadA, class PayloadB, class Enable = void>
struct HasCompatibleRandomVariableInfo : public std::false_type {
};

template<class PayloadA, class PayloadB>
struct HasCompatibleRandomVariableInfo<PayloadA, PayloadB,
    typename std::enable_if<
        RandomVariableTraits<typename PayloadA::RVType>::Dimension
            == RandomVariableTraits<typename PayloadB::RVType>::Dimension>::type> :
    public std::true_type {
};

}


int main(int argc, char **argv) {
  using namespace Naksha;
  using std::cout;

  typedef DiscreteRandomVariable<9, float, ProbabilityTag> RVP9f;
  typedef DiscreteRandomVariable<9, double, LogProbabilityTag> RVLP9d;
  typedef DiscreteRandomVariable<6, float, ProbabilityTag> RVP6f;

  typedef ColoredRandomVariable<RVP9f>::type ColoredRVP9f;

  cout << "HasColorInfo<float>::value = " << HasColorInfo<float>::value << "\n";
  cout << "HasColorInfo<RV>::value = " << HasColorInfo<RVP9f>::value << "\n";
  cout << "HasColorInfo<ColoredRV>::value = " << HasColorInfo<ColoredRVP9f>::value << "\n";

  cout << "HasRandomVariableInfo<float>::value = " << HasRandomVariableInfo<float>::value << "\n";
  cout << "HasRandomVariableInfo<RV>::value = " << HasRandomVariableInfo<RVP9f>::value << "\n";
  cout << "HasRandomVariableInfo<ColoredRV>::value = " << HasRandomVariableInfo<ColoredRVP9f>::value << "\n";

  cout << "HasCompatibleRandomVariableInfo<RVP9f, RVP6f>::value = " << HasCompatibleRandomVariableInfo<RVP9f, RVP6f>::value  << "\n";
  cout << "HasCompatibleRandomVariableInfo<RVP9f, RVLP9d>::value = " << HasCompatibleRandomVariableInfo<RVP9f, RVLP9d>::value  << "\n";
  cout << "HasCompatibleRandomVariableInfo<RVP9f, RVP9f>::value = " << HasCompatibleRandomVariableInfo<RVP9f, RVP9f>::value  << "\n";
  cout << "HasCompatibleRandomVariableInfo<RVP9f, ColoredRVP9f>::value = " << HasCompatibleRandomVariableInfo<RVP9f, ColoredRVP9f>::value  << "\n";
  cout << "HasCompatibleRandomVariableInfo<RVP9f, int>::value = " << HasCompatibleRandomVariableInfo<RVP9f, int>::value  << "\n";
  cout << "HasCompatibleRandomVariableInfo<float, int>::value = " << HasCompatibleRandomVariableInfo<float, int>::value  << "\n";

  return EXIT_SUCCESS;
}

