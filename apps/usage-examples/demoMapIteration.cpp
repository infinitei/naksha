/**
 * @file demoMapIteration.cpp
 * @brief Example of iterating over Map voxels
 *
 * @author Abhijit Kundu
 */

#include "Naksha/Maps/MapMaker.h"
#include "Naksha/Payload/PayloadMaker.h"
#include "Naksha/Common/EigenTypedefs.h"
#include "Naksha/Algorithms/InsertMeasurements.h"
#include "Naksha/Nodes/NodeUtils.h"
#include <algorithm>

int main(int argc, char **argv) {
  using namespace Naksha;
  using std::cout;

  typedef DiscreteRandomVariable<2, float> RV;
  typedef ColoredRandomVariable<RV>::type Payload;
  typedef VolumetricMap<3, Payload, UnorderedMapDataStructure, HybridMappingInterface, MapTransform> MapType;
  typedef MapType::VoxelNodeType VoxelNodeType;

  // Initialize a map with resolution 0.1
  MapType map(0.1);

  // Create point cloud
  typedef EigenAligned<Eigen::Vector3d>::std_vector PointCloudType;
  const int width = 10;
  const int xoffset = 20;
  PointCloudType point_cloud;
  for (double x = xoffset - width; x < (xoffset + width); x += 0.1)
    for (double y = -width; y < width; y += 0.1) {
      point_cloud.push_back(Eigen::Vector3d(x, y, -10));
    }

  // Insert point cloud
  insertDepthScan(map, Eigen::Vector3d::Zero(), point_cloud);

  cout << "# Map Size = " << map.size() << " voxels\n\n";

  {
    cout << "## Method 1: Standard Loop \n\n";
    std::size_t num_solid_voxels = 0;
    IsNodeOccupied<VoxelNodeType> occ_checker;
    for(MapType::const_iterator it = map.begin(); it!= map.end(); ++it) {
      if(occ_checker(*it))
        ++num_solid_voxels;
    }
    cout << "- Solid Voxels = " << num_solid_voxels << "\n";
    cout << "- Free Voxels = " << map.size() - num_solid_voxels << "\n\n";
  }

  {
    cout << "## Method 3: C++11 Range Loop \n\n";
    std::size_t num_solid_voxels = 0;
    IsNodeOccupied<VoxelNodeType> occ_checker;

    for(const auto& voxel : map) {
      if(occ_checker(voxel))
      ++num_solid_voxels;
    }
    cout << "- Solid Voxels = " << num_solid_voxels << "\n";
    cout << "- Free Voxels = " << map.size() - num_solid_voxels << "\n\n";
  }

  {
    cout << "## Method 4: STL Algorithm \n\n";
    std::size_t num_solid_voxels = std::count_if(map.begin(), map.end(),
                                                 IsNodeOccupied<VoxelNodeType>());
    cout << "- Solid Voxels = " << num_solid_voxels << "\n";
    cout << "- Free Voxels = " << map.size() - num_solid_voxels << "\n\n";
  }

}


